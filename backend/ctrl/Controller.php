<?php
    
    class Controller {

        protected $_template;
        protected $_content;

        function __construct() {
            User::factory();
            $this->redirects();
            $this->config();
            $this->center();
            $this->template();
            $this->render();
        }


        private function redirects() {
            $uri = explode('/', $_SERVER['REQUEST_URI']);
            if( !User::factory()->_admin AND $uri[1] != 'auth' AND $uri[1] != 'ajax' ) {
                location('backend/auth/login');
            }
            if ($_SERVER['REQUEST_URI'] == '/backend/index' OR 
                $_SERVER['REQUEST_URI'] == '/backend/index/' OR 
                $_SERVER['REQUEST_URI'] == '/backend/' OR 
                $_SERVER['REQUEST_URI'] == '/backend') {
                location('/backend/index/index');
            }
        }


        private function template() {
            $controller = strtolower( str_replace( 'Controller', '', Route::controller() ) );
            $action = strtolower( str_replace( 'Action', '', Route::action() ) );
            switch ( $controller ) {
                case 'auth':
                    if( $action == 'login' ) {
                        $this->_template = 'Auth';
                        break;
                    }
                default:
                    $this->_template = 'Main';
                    break;
            }
        }


        private function config() {
            $result = Builder::factory('config')->select(array('key', 'zna'))->where('status', '=', 1)->find_all();
            foreach($result AS $obj) {
                conf::set( $obj->key, $obj->zna );
            }
        }


        private function center() {
            Route::factory()->parseBackend();
            $controller = strtolower(Route::controller()) . 'Controller';
            $ctrl_file = ADMIN_PATH."/ctrl/".$controller.".php";
            if (is_file($ctrl_file)) {
                require_once $ctrl_file;
                $class = Route::controller().'Controller';
                if (class_exists($controller)) {
                    $class = new $controller;
                    $action = strtolower(Route::action()) . 'Action';
                    if (method_exists($class, $action)) {
                        $this->_content = $class->$action();
                    } else {
                        die('!method_exists: '.$action);
                    }
                } else {
                    die('!class_exists: '.$controller);
                }
            } else {
               die('!controller: '.$ctrl_file);
            }
        }


        private function render() {
            echo support::tpl(array( 'content' => $this->_content ), $this->_template);
        }

    }

    new Controller();