<?php

class ajaxController
{

    public function translitAction()
    {
        die(json_encode(array(
            'success' => true,
            'result' => Text::translit(Arr::get($_POST, 'source')),
        )));
    }


    public function getModelsByBrandIDAction()
    {
        $brand_id = (int)Arr::get($_POST, 'brand_id');
        $models = Builder::factory('models')
            ->where('brand_id', '=', $brand_id)
            ->order_by('name')
            ->find_all();
        $options = array();
        foreach ($models as $model) {
            $options[] = array('name' => $model->name, 'id' => $model->id);
        }
        die(json_encode(array(
            'options' => $options,
        )));
    }

    public function getSpecificationsByCatalogTreeIDAction()
    {
        $catalog_tree_id = (int)Arr::get($_POST, 'catalog_tree_id');
        $result = Builder::factory('brands')
            ->join('catalog_tree_brands')
            ->on('catalog_tree_brands.brand_id', '=', Builder::expr('`brands`.`id`'))
            ->where('catalog_tree_brands.catalog_tree_id', '=', $catalog_tree_id)
            ->order_by('name')
            ->find_all();
        $brands = array();
        foreach ($result as $obj) {
            $brands[] = array('name' => $obj->name, 'id' => $obj->id);
        }
        $result = Builder::factory('sizes')
            ->join('catalog_tree_sizes')
            ->on('catalog_tree_sizes.size_id', '=', Builder::expr('`sizes`.`id`'))
            ->where('catalog_tree_sizes.catalog_tree_id', '=', $catalog_tree_id)
            ->order_by('name')
            ->find_all();
        $sizes = array();
        foreach ($result as $obj) {
            $sizes[] = array('name' => $obj->name, 'id' => $obj->id);
        }
        $specifications = Builder::factory('specifications')
            ->join('catalog_tree_specifications')
            ->on('catalog_tree_specifications.specification_id', '=', Builder::expr('`specifications`.`id`'))
            ->where('catalog_tree_specifications.catalog_tree_id', '=', $catalog_tree_id)
            ->order_by('name')
            ->find_all();
        $arr = array();
        foreach ($specifications AS $s) {
            $arr[] = $s->id;
        }
        $specValues = Builder::factory('specifications_values')
            ->where('specification_id', 'IN', $arr)
            ->order_by('name')
            ->find_all();
        $arr = array();
        foreach ($specValues as $obj) {
            $arr[$obj->specification_id][] = $obj;
        }
        die(json_encode(array(
            'brands' => $brands,
            'sizes' => $sizes,
            'specifications' => $specifications,
            'specValues' => $arr,
        )));
    }

    public function setStatusAction()
    {
        if (!isset($_POST['id'])) {
            die ('Не указаны данные записи');
        }

        $status = (int)Arr::get($_POST, 'current', 0);
        if ($status) {
            $status = 0;
        } else {
            $status = 1;
        }
        $id = Arr::get($_POST, 'id', 0);
        $table = Arr::get($_POST, 'table', 0);

        Builder::factory($table)->data(array('status' => $status))->where('id', '=', $id)->edit();

        die(json_encode(array(
            'status' => $status
        )));
    }

    public function deleteMassAction()
    {
        if (!isset($_POST['ids'])) {
            die ('Не указаны данные записи');
        }
        $ids = Arr::get($_POST, 'ids', 0);
        $table = Arr::get($_POST, 'table', 0);
        if (!empty($ids)) {
            if ($table == 'news' OR $table == 'articles') {
                foreach ($ids AS $id) {
                    $images = Builder::factory($table)->where('id', '=', $id)->find_all();
                    foreach ($images AS $im) {
                        @unlink(HOST . '/images/' . $table . '/small/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/big/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/original/' . $im->image);
                    }
                }
            } else if ($table == 'catalog') {
                foreach ($ids AS $id) {
                    $images = Builder::factory('catalog_images')->select('image')->where('catalog_id', '=', $id)->find_all();
                    foreach ($images AS $im) {
                        @unlink(HOST . '/images/' . $table . '/small/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/medium/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/big/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/original/' . $im->image);
                    }
                }
            } else if ($table == 'slider') {
                foreach ($ids AS $id) {
                    $images = Builder::factory($table)->where('id', '=', $id)->find_all();
                    foreach ($images AS $im) {
                        @unlink(HOST . '/images/' . $table . '/small/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/big/' . $im->image);
                    }
                }
            } else if ($table == 'banners') {
                foreach ($ids AS $id) {
                    $images = Builder::factory($table)->where('id', '=', $id)->find_all();
                    foreach ($images AS $im) {
                        @unlink(HOST . '/images/' . $table . '/' . $im->image);
                    }
                }
            } else if ($table == 'brands') {
                foreach ($ids AS $id) {
                    $images = Builder::factory($table)->where('id', '=', $id)->find_all();
                    foreach ($images AS $im) {
                        @unlink(HOST . '/images/' . $table . '/small/' . $im->image);
                        @unlink(HOST . '/images/' . $table . '/original/' . $im->image);
                    }
                }
            }
            Builder::factory($table)->where('id', 'IN', $ids)->delete();
            Message::GetMessage(1, 'Данные удалены!');
        }
        die(json_encode(array(
            'success' => true
        )));
    }

    public function setStatusMassAction()
    {
        if (!isset($_POST['ids'])) {
            die ('Не указаны данные записи');
        }

        $status = (int)Arr::get($_POST, 'status', 0);
        $ids = Arr::get($_POST, 'ids', 0);
        $table = Arr::get($_POST, 'table', 0);

        if (!empty($ids)) {
            Builder::factory($table)->data(array('status' => $status))->where('id', 'IN', $ids)->edit();
            Message::GetMessage(1, 'Статусы изменены!');
        }

        die(json_encode(array(
            'success' => true
        )));
    }

    public function getURIAction()
    {
        $uri = Arr::get($_POST, "uri");
        $date_s = Arr::get($_POST, "from");
        $date_po = Arr::get($_POST, "to");

        $uri = Backend::generateLink('date_s', $date_s, $uri);
        $uri = Backend::generateLink('date_po', $date_po, $uri);

        die(json_encode(array(
            'success' => true,
            'uri' => href($uri),
        )));
    }

    public function sortableAction()
    {
        $table = Arr::get($_POST, 'table');
        $json = Arr::get($_POST, 'json');
        $arr = json_decode(stripslashes($json), true);

        function saveSort($arr, $table, $parentID, $i = 0)
        {
            $noInner = array('sitemenu', 'slider', 'gallery');
            foreach ($arr AS $a) {
                if (!in_array($table, $noInner)) {
                    $data = array('sort' => $i, 'parent_id' => $parentID);
                } else {
                    $data = array('sort' => $i);
                }
                $id = Arr::get($a, 'id');
                Builder::factory($table)
                    ->data($data)
                    ->where('id', '=', $id)
                    ->edit();
                $i++;
                $children = Arr::get($a, 'children', array());
                if (count($children)) {
                    if (in_array($table, $noInner)) {
                        $i = saveSort($children, $table, $id, $i);
                    } else {
                        saveSort($children, $table, $id);
                    }
                }
            }
            return $i;
        }

        saveSort($arr, $table, 0);

        die(json_encode(array(
            'success' => true,
        )));
    }

    public function loginAction()
    {
        $login = Arr::get($_POST, 'login');
        $password = Arr::get($_POST, 'password');
        $remember = Arr::get($_POST, 'remember');

        $u = User::factory();

        $user = $u->get_user_if_isset($login, $password, 1);
        if (!$user OR $user->role_id != 2) {
            die(json_encode(array(
                'success' => false
            )));
        }

        $u->auth($user, $remember);

        die(json_encode(array(
            'success' => true
        )));
    }

    public function change_fieldAction()
    {
        $id = (int)Arr::get($_POST, 'id');
        $field = Arr::get($_POST, 'field');
        $table = Arr::get($_POST, 'table');
        if (!$id) {
            die ('Не указаны данные записи');
        }

        $old = Builder::factory($table)->select($field)->where('id', '=', $id)->find();
        if (!$old) {
            die('No data to change!');
        }
        $old = $old->$field;
        $new = $old == 1 ? 0 : 1;
        $data = array();
        $data[$field] = $new;
        if ($table == 'catalog' AND $field == 'new') {
            $data['new_from'] = time();
        }
        Builder::factory($table)->data($data)->where('id', '=', $id)->edit();

        die(json_encode(array(
            'current' => $new
        )));
    }

####################### Catalog uploader actions

    public function set_default_imageAction()
    {
        $id = Arr::get($_POST, 'id');
        $catalog_id = Arr::get($_POST, 'catalog_id');
        Builder::factory('gallery_images')->data(array('main' => 0))->where('parent_id', '=', $catalog_id)->edit();
        Builder::factory('gallery_images')->data(array('main' => 1))->where('id', '=', $id)->edit();
        die;
    }

    public function delete_catalog_photoAction()
    {
        $id = (int)Arr::get($_POST, 'id');
        if (!$id) die('Error!');

        $path = HOST . '/images/gallery/';
        chmod($path . 'big/', 0777);
        chmod($path . 'medium/', 0777);
        chmod($path . 'small/', 0777);
        chmod($path . 'original/', 0777);

        $image = Builder::factory('gallery_images')->select('image')->where('id', '=', $id)->find()->image;
        Builder::factory('gallery_images')->select('image')->where('id', '=', $id)->delete();

        @unlink($path . 'small/' . $image);
        @unlink($path . 'medium/' . $image);
        @unlink($path . 'big/' . $image);
        @unlink($path . 'original/' . $image);

        die(json_encode(array(
            'status' => true,
        )));
    }

    public function sort_imagesAction()
    {
        $order = Arr::get($_POST, 'order');
        if (!is_array($order)) die('Error!');
        $updated = 0;
        foreach ($order as $key => $value) {
            $value = (int)$value;
            $order = $key + 1;
            Builder::factory('gallery_images')->data(array('sort' => $order))->where('id', '=', $value)->edit();
            $updated++;
        }
        die(json_encode(array(
            'updated' => $updated,
        )));
    }

    public function get_uploaded_imagesAction()
    {
        $id = (int)Arr::get($_POST, 'id');
        $time = (int)Arr::get($_POST, 'time');
        if (!$id) die('Error!');
        $images = Builder::factory('gallery_images')
            ->where('parent_id', '=', $id)
            ->where('date', '>', $time)
            ->order_by('sort')
            ->find_all();
        if ($images) {
            $show_images = support::tpl(array('images' => $images), 'gallery/uploaded_images');
        } else {
            $show_images = 0;
        }
        die(json_encode(array(
            'images' => $show_images,
        )));
    }

    public function upload_imagesAction()
    {
        if (empty($_FILES['file'])) die('No File!');

        $arr = explode('/', Arr::get($_SERVER, 'HTTP_REFERER'));
        $id_good = (int)end($arr);

        $headers = emu_getallheaders();
        if (array_key_exists('Upload-Filename', $headers)) {
            $data = file_get_contents('php://input');
            $name = $headers['Upload-Filename'];
        } else {
            $name = $_FILES['file']['name'];
        }

        $name = explode('.', $name);
        $ext = strtolower(end($name));

        if (!support::image_extension($ext)) die('Not image!');

        $ext = end(explode('.', $_FILES['file']['name']));
        $filename = md5($_FILES['file']['name'] . microtime()) . '.' . $ext;
        $original = HOST . '/images/gallery/original/' . $filename;
        $big = HOST . '/images/gallery/big/' . $filename;
        $medium = HOST . '/images/gallery/medium/' . $filename;


        $watermark = Image::factory(HOST . '/pic/logo.png');

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->watermark($watermark, NULL, NULL, $opacity = 75);
        $image->save($original);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(GALLERY_WIDTH, GALLERY_HEIGHT, Image::INVERSE);
        $image->crop(GALLERY_WIDTH, GALLERY_HEIGHT);
        $image->save($medium);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(678, 520, Image::INVERSE);
        $image->crop(678, 520, Image::INVERSE);
        // $image->watermark($watermark, NULL, NULL, $opacity = 75);
        //$image->crop(678, 520);
        $image->save($big);

        if (is_file($original)) {
            $has_main = Builder::factory('gallery_images')->where('parent_id', '=', $id_good)->where('main', '=', 1)->count_all();
            $data = array(
                'parent_id' => $id_good,
                'image' => $filename,
                'date' => time()
            );
            if (!$has_main) {
                $data['main'] = 1;
            }
            Builder::factory('gallery_images')->data($data)->add();
            $confirm = true;
        }
        die(json_encode(array(
            'confirm' => $confirm,
        )));
    }

####################### photoreport uploader actions
    public function photo_set_default_imageAction()
    {
        $id = Arr::get($_POST, 'id');
        $catalog_id = Arr::get($_POST, 'catalog_id');
        Builder::factory('photoreport_images')->data(array('main' => 0))->where('photoreport_id', '=', $catalog_id)->edit();
        Builder::factory('photoreport_images')->data(array('main' => 1))->where('id', '=', $id)->edit();
        die;
    }

    public function photo_sort_imagesAction()
    {
        $order = Arr::get($_POST, 'order');
        if (!is_array($order)) die('Error!');
        $updated = 0;
        foreach ($order as $key => $value) {
            $value = (int)$value;
            $order = $key + 1;
            Builder::factory('photoreport_images')->data(array('sort' => $order))->where('id', '=', $value)->edit();
            $updated++;
        }
        die(json_encode(array(
            'updated' => $updated,
        )));
    }

    public function photo_delete_catalog_photoAction()
    {
        $id = (int)Arr::get($_POST, 'id');
        if (!$id) die('Error!');

        $path = HOST . '/images/photoreport/';
        chmod($path . 'big/', 0777);
        chmod($path . 'medium/', 0777);
        chmod($path . 'small/', 0777);
        chmod($path . 'original/', 0777);
        chmod($path . 'bigwatermark/', 0777);
        chmod($path . 'originalwatermark/', 0777);

        $image = Builder::factory('photoreport_images')->select('image')->where('id', '=', $id)->find()->image;
        Builder::factory('photoreport_images')->select('image')->where('id', '=', $id)->delete();

        @unlink($path . 'small/' . $image);
        @unlink($path . 'medium/' . $image);
        @unlink($path . 'big/' . $image);
        @unlink($path . 'original/' . $image);
        @unlink($path . 'bigwatermark/' . $image);
        @unlink($path . 'originalwatermark/' . $image);

        die(json_encode(array(
            'status' => true,
        )));
    }

    public function photo_get_uploaded_imagesAction()
    {
        $id = (int)Arr::get($_POST, 'id');
        $time = (int)Arr::get($_POST, 'time');
        if (!$id) die('Error!');
        $images = Builder::factory('photoreport_images')
            ->where('photoreport_id', '=', $id)
            ->where('date', '>', $time)
            ->order_by('sort')
            ->find_all();
        if ($images) {
            $show_images = support::tpl(array('images' => $images), 'photo/uploaded_images');
        } else {
            $show_images = 0;
        }
        die(json_encode(array(
            'images' => $show_images,
        )));
    }

    public function photo_upload_imagesAction()
    {
        if (empty($_FILES['file'])) die('No File!');

        $arr = explode('/', Arr::get($_SERVER, 'HTTP_REFERER'));
        $id_good = (int)end($arr);

        $headers = emu_getallheaders();
        if (array_key_exists('Upload-Filename', $headers)) {
            $data = file_get_contents('php://input');
            $name = $headers['Upload-Filename'];
        } else {
            $name = $_FILES['file']['name'];
        }

        $name = explode('.', $name);
        $ext = strtolower(end($name));

        if (!support::image_extension($ext)) die('Not image!');

        $ext = end(explode('.', $_FILES['file']['name']));
        $filename = md5($_FILES['file']['name'] . microtime()) . '.' . $ext;
        $original = HOST . '/images/photoreport/original/' . $filename;
        $big = HOST . '/images/photoreport/big/' . $filename;
        $small = HOST .'/images/photoreport/small/' . $filename;
        $bigwater_mark = HOST . '/images/photoreport/bigwatermark/' . $filename;
        $original_watermark = HOST . '/images/photoreport/originalwatermark/' . $filename;
        $watermark = Image::factory(HOST . '/pic/logo.png');


        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->save($original);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(200, 160, Image::INVERSE);
        $image->crop(200, 160);
        $image->save($small);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(678, 520, Image::INVERSE);
        $image->crop(678, 520, Image::INVERSE);
        $image->save($big);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(678, 520, Image::INVERSE);
        $image->crop(678, 520, Image::INVERSE);
        $image->watermark($watermark, 0, NULL, $opacity = 75);
        $image->save($bigwater_mark);

        $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
        $image->watermark($watermark, 0, NULL, $opacity = 75);
        $image->save($original_watermark);

        if (is_file($original)) {
            $has_main = Builder::factory('photoreport_images')->where('photoreport_id', '=', $id_good)->where('main', '=', 1)->count_all();
            $data = array(
                'photoreport_id' => $id_good,
                'image' => $filename,
                'date' => time()
            );
            if (!$has_main) {
                $data['main'] = 1;
            }
            Builder::factory('photoreport_images')->data($data)->add();
            $confirm = true;
        }
        die(json_encode(array(
            'confirm' => $confirm,
        )));
    }

####################### News uploader actions

    public function news_set_default_imageAction() {
        $id = Arr::get( $_POST, 'id' );
        $catalog_id = Arr::get( $_POST, 'news_id' );
        Builder::factory( 'news_images' )->data( array( 'main' => 0 ) )->where( 'news_id', '=', $catalog_id )->edit();
        Builder::factory( 'news_images' )->data( array( 'main' => 1 ) )->where( 'id', '=', $id )->edit();
        die;
    }

    public function news_delete_catalog_photoAction() {
        $id = (int) Arr::get($_POST, 'id');
        if (!$id) die('Error!');

        $path = HOST . '/images/news/';
        chmod($path.'big/', 0777);
        chmod($path.'medium/', 0777);
        chmod($path.'small/', 0777);
        chmod($path.'original/', 0777);
        chmod($path.'bigwatermark/', 0777);
        chmod($path . 'originalwatermark/', 0777);

        $image = Builder::factory( 'news_images' )->select( 'image' )->where( 'id', '=', $id )->find()->image;
        Builder::factory( 'news_images' )->select( 'image' )->where( 'id', '=', $id )->delete();

        @unlink($path . 'small/' . $image);
        @unlink($path . 'medium/' . $image);
        @unlink($path . 'big/' . $image);
        @unlink($path . 'original/' . $image);
        @unlink($path . 'bigwatermark/' . $image);
        @unlink($path . 'originalwatermark/' . $image);

        die(json_encode(array(
            'status' => true,
        )));
    }

    public function news_sort_imagesAction() {
        $order = Arr::get($_POST, 'order');
        if (!is_array($order)) die('Error!');
        $updated = 0;
        foreach($order as $key => $value) {
            $value = (int) $value;
            $order = $key + 1;
            Builder::factory( 'news_images' )->data( array( 'sort' => $order ) )->where( 'id', '=', $value )->edit();
            $updated++;
        }
        die(json_encode(array(
            'updated' => $updated,
        )));
    }

    public function news_get_uploaded_imagesAction() {
        $id = (int) Arr::get($_POST, 'id');
        $time = (int) Arr::get($_POST, 'time');
        if( !$id ) die('Error!');
        $images = Builder::factory( 'news_images' )
            ->where( 'news_id', '=', $id )
            ->where( 'date', '>', $time )
            ->order_by('sort')
            ->find_all();
        if ($images) {
            $show_images = support::tpl(array( 'images' => $images ), 'news/uploaded_images');
        } else {
            $show_images = 0;
        }
        die(json_encode(array(
            'images' => $show_images,
        )));
    }

    public function news_upload_imagesAction() {
        if (empty($_FILES['file'])) die('No File!');

        $arr = explode('/', Arr::get($_SERVER, 'HTTP_REFERER'));
        $id_good = (int) end($arr);

        $headers = emu_getallheaders();
        if(array_key_exists('Upload-Filename', $headers)) {
            $data = file_get_contents('php://input');
            $name = $headers['Upload-Filename'];
        } else {
            $name = $_FILES['file']['name'];
        }

        $name = explode('.', $name);
        $ext = strtolower(end($name));

        if (!support::image_extension($ext)) die('Not image!');

        $ext = end( explode('.', $_FILES['file']['name']) );
        $filename = md5($_FILES['file']['name'].microtime()).'.'.$ext;
        $original = HOST . '/images/news/original/' . $filename;
        $big = HOST . '/images/news/big/' . $filename;
        $medium = HOST . '/images/news/small/' . $filename;
        $bigwater_mark = HOST . '/images/news/bigwatermark/' . $filename;
        $original_watermark = HOST . '/images/news/originalwatermark/' . $filename;

        $watermark = Image::factory(HOST.'/pic/logo.png');

        $image = Image::factory($_FILES['file']['tmp_name']);
//        $image->watermark($watermark, 0, NULL, $opacity = 75);
        $image->save($original);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(200, 160, Image::INVERSE);
        $image->crop(200, 160);
        $image->save($medium);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(678, 520, Image::INVERSE);
        $image->crop(678, 520, Image::INVERSE);
        $image->save($big);

        $image = Image::factory($_FILES['file']['tmp_name']);
        $image->resize(678, 520, Image::INVERSE);
        $image->crop(678, 520, Image::INVERSE);
        $image->watermark($watermark, 0, NULL, $opacity = 75);
        $image->save($bigwater_mark);
        $image = Image::factory(HOST . '/images/news/original/' . $filename);
        $image->watermark($watermark, 0, NULL, $opacity = 75);
        $image->save($original_watermark);

        if ( is_file( $original ) ) {
            $has_main = Builder::factory( 'news_images' )->where( 'news_id', '=', $id_good )->where( 'main', '=', 1 )->count_all();
            $data = array(
                'news_id' => $id_good,
                'image' => $filename,
                'date' => time()
            );
            if( !$has_main ) {
                $data['main'] = 1;
            }
            Builder::factory( 'news_images' )->data( $data )->add();
            $confirm = true;
        }
        die(json_encode(array(
            'confirm' => $confirm,
        )));
    }

########## ORDER ACTIONS

    // Generate associative array from serializeArray data
    public function getDataFromSerialize($data)
    {
        $arr = array();
        foreach ($data AS $el) {
            $arr[$el['name']] = $el['value'];
        }
        return $arr;
    }


    // Change status of the order
    public function orderStatusAction()
    {
        if (!Arr::get($_POST, 'id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post = $this->getDataFromSerialize(Arr::get($_POST, 'data'));
        $statuses = conf::get('order.statuses');
        if (!isset($statuses[Arr::get($post, 'status')]) OR !isset($post['status'])) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post['id'] = Arr::get($_POST, 'id');
        Builder::factory('orders')->data(array('status' => Arr::get($post, 'status')))->where('id', '=', Arr::get($post, 'id'))->edit();
        die(json_encode(array(
            'success' => true,
        )));
    }


    // Change delivery settings
    public function orderDeliveryAction()
    {
        if (!Arr::get($_POST, 'id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post = $this->getDataFromSerialize(Arr::get($_POST, 'data'));
        $delivery = conf::get('order.delivery');
        if (!isset($delivery[Arr::get($post, 'delivery')]) OR !isset($post['delivery'])) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        if (Arr::get($post, 'delivery') == 2 AND !Arr::get($post, 'number')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post['id'] = Arr::get($_POST, 'id');
        $data = array('delivery' => Arr::get($post, 'delivery'));
        if (Arr::get($post, 'delivery') == 2) {
            $data['number'] = Arr::get($post, 'number');
        }
        Builder::factory('orders')->data($data)->where('id', '=', Arr::get($post, 'id'))->edit();
        die(json_encode(array(
            'success' => true,
        )));
    }


    // Change payment settings
    public function orderPaymentAction()
    {
        if (!Arr::get($_POST, 'id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post = $this->getDataFromSerialize(Arr::get($_POST, 'data'));
        $payment = conf::get('order.payment');
        if (!isset($payment[Arr::get($post, 'payment')]) OR !isset($post['payment'])) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post['id'] = Arr::get($_POST, 'id');
        Builder::factory('orders')->data(array('payment' => Arr::get($post, 'payment')))->where('id', '=', Arr::get($post, 'id'))->edit();
        die(json_encode(array(
            'success' => true,
        )));
    }


    // Change user information
    public function orderUserAction()
    {
        if (!Arr::get($_POST, 'id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        $post = $this->getDataFromSerialize(Arr::get($_POST, 'data'));
        $post['id'] = Arr::get($_POST, 'id');
        Builder::factory('orders')->data(array(
            'name' => Arr::get($post, 'name'),
            'phone' => Arr::get($post, 'phone'),
        ))->where('id', '=', Arr::get($post, 'id'))->edit();
        die(json_encode(array(
            'success' => true,
        )));
    }


    // Change items information
    public function orderItemsAction()
    {
        $post = $_POST;
        if (!Arr::get($post, 'id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        if (!Arr::get($post, 'catalog_id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        Builder::factory('orders_items')->data(array(
            'count' => Arr::get($post, 'count'),
        ))
            ->where('order_id', '=', Arr::get($post, 'id'))
            ->where('catalog_id', '=', Arr::get($post, 'catalog_id'))
            ->where('size_id', '=', Arr::get($post, 'size_id'))
            ->edit();
        die(json_encode(array(
            'success' => true,
        )));
    }


    // Delete item position from the order
    public function orderPositionDeleteAction()
    {
        $post = $_POST;
        if (!Arr::get($post, 'id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        if (!Arr::get($post, 'catalog_id')) {
            die(json_encode(array(
                'success' => false,
            )));
        }
        Builder::factory('orders_items')
            ->where('order_id', '=', Arr::get($post, 'id'))
            ->where('catalog_id', '=', Arr::get($post, 'catalog_id'))
            ->where('size_id', '=', Arr::get($post, 'size_id'))
            ->delete();
        die(json_encode(array(
            'success' => true,
        )));
    }


    // Get items by parent_id
    public function getItemsAction()
    {
        $id = Arr::get($_POST, 'parent_id');
        $result = Builder::factory('catalog')
            ->select(array('*', 'catalog_images.image'))
            ->join('catalog_images')
            ->on('catalog_images.catalog_id', '=', Builder::expr('`catalog`.`id`'))
            ->on('catalog_images.main', '=', 1)
            ->where('parent_id', '=', $id)
            ->order_by('created_at', 'DESC')
            ->find_all();
        $data = array();
        foreach ($result AS $obj) {
            $data[] = array(
                // 'url' => '/catalog/'.$obj->alias,
                'image' => support::is_file(HOST . '/images/catalog/medium/' . $obj->image) ? '/images/catalog/medium/' . $obj->image : '',
                'name' => $obj->name,
                'cost' => $obj->cost,
                'id' => $obj->id,
            );
        }
        die(json_encode(array(
            'success' => true,
            'result' => $data,
        )));
    }


    // Get sizes by catalog_id
    public function getItemSizesAction()
    {
        $catalog_id = (int)Arr::get($_POST, 'catalog_id');
        $result = Builder::factory('sizes')
            ->join('catalog_sizes')
            ->on('catalog_sizes.size_id', '=', Builder::expr('`sizes`.`id`'))
            ->on('catalog_sizes.catalog_id', '=', $catalog_id)
            ->order_by('name')
            ->find_all();
        $data = array();
        foreach ($result AS $obj) {
            if ((int)$obj->id) {
                $data[] = array(
                    'id' => $obj->id,
                    'name' => $obj->name,
                );
            }
        }
        die(json_encode(array(
            'success' => true,
            'result' => $data,
        )));
    }


    // Add simple specification
    public function addSimpleSpecificationValueAction()
    {
        $post = $_POST;
        // Check data
        $name = Arr::get($post, 'name');
        $alias = Arr::get($post, 'alias');
        $specification_id = Arr::get($post, 'specification_id');
        if (!$name OR !$alias OR !$specification_id) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Вы ввели не все данные',
            )));
        }
        // Get count of rows with the same alias and specification_id
        $count = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->where('alias', '=', $alias)
            ->count_all();
        // Error if such alias exists
        if ($count) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Измените алиас. Такой уже есть',
            )));
        }
        // Trying to save data
        $result = Builder::factory('specifications_values')->data(array(
            'name' => $name,
            'alias' => $alias,
            'specification_id' => $specification_id,
            'status' => 1,
        ))->add();
        // Error if failed saving
        if (!$result) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Ошибка на сервере. Повторите попытку позднее',
            )));
        }
        // Get full list of values for current specification
        $result = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->order_by('name')
            ->find_all();
        // Answer
        die(json_encode(array(
            'success' => true,
            'result' => $result,
        )));
    }


    // Edit simple specification
    public function editSimpleSpecificationValueAction()
    {
        $post = $_POST;
        // Check data
        $name = Arr::get($post, 'name');
        $alias = Arr::get($post, 'alias');
        $status = Arr::get($post, 'status');
        $id = Arr::get($post, 'id');
        $specification_id = Arr::get($post, 'specification_id');
        if (!$name OR !$alias OR !$id OR !$specification_id) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Вы ввели не все данные',
            )));
        }
        // Get count of rows with the same alias and specification_id
        $count = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->where('alias', '=', $alias)
            ->where('id', '!=', $id)
            ->count_all();
        // Error if such alias exists
        if ($count) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Измените алиас. Такой уже есть',
            )));
        }
        // Trying to save data
        $result = Builder::factory('specifications_values')->data(array(
            'name' => $name,
            'alias' => $alias,
            'status' => $status,
        ))->where('id', '=', $id)->edit();
        // Error if failed saving
        if (!$result) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Ошибка на сервере. Повторите попытку позднее',
            )));
        }
        // Get full list of values for current specification
        $result = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->order_by('name')
            ->find_all();
        // Answer
        die(json_encode(array(
            'success' => true,
            'result' => $result,
        )));
    }


    // Delete value for anyone specification
    public function deleteSpecificationValueAction()
    {
        $post = $_POST;
        // Check data
        $id = Arr::get($post, 'id');
        if (!$id) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Вы ввели не все данные',
            )));
        }
        // Trying to delete value
        $result = Builder::factory('specifications_values')->where('id', '=', $id)->delete();
        // Error if failed saving
        if (!$result) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Ошибка на сервере. Повторите попытку позднее',
            )));
        }
        // Answer
        die(json_encode(array(
            'success' => true,
            'result' => $result,
        )));
    }


    // Add simple specification
    public function addColorSpecificationValueAction()
    {
        $post = $_POST;
        // Check data
        $name = Arr::get($post, 'name');
        $color = Arr::get($post, 'color');
        $alias = Arr::get($post, 'alias');
        $specification_id = Arr::get($post, 'specification_id');
        if (!$name OR !$alias OR !$specification_id OR !preg_match('/^#[0-9abcdef]{6}$/', $color, $matches)) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Вы ввели не все данные',
            )));
        }
        // Get count of rows with the same alias and specification_id
        $count = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->where('alias', '=', $alias)
            ->count_all();
        // Error if such alias exists
        if ($count) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Измените алиас. Такой уже есть',
            )));
        }
        // Trying to save data
        $result = Builder::factory('specifications_values')->data(array(
            'name' => $name,
            'alias' => $alias,
            'specification_id' => $specification_id,
            'status' => 1,
            'color' => $color,
        ))->add();
        // Error if failed saving
        if (!$result) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Ошибка на сервере. Повторите попытку позднее',
            )));
        }
        // Get full list of values for current specification
        $result = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->order_by('name')
            ->find_all();
        // Answer
        die(json_encode(array(
            'success' => true,
            'result' => $result,
        )));
    }


    // Edit simple specification
    public function editColorSpecificationValueAction()
    {
        $post = $_POST;
        // Check data
        $name = Arr::get($post, 'name');
        $color = Arr::get($post, 'color');
        $alias = Arr::get($post, 'alias');
        $status = Arr::get($post, 'status');
        $id = Arr::get($post, 'id');
        $specification_id = Arr::get($post, 'specification_id');
        if (!$name OR !$alias OR !$id OR !$specification_id OR !preg_match('/^#[0-9abcdef]{6}$/', $color, $matches)) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Вы ввели не все данные',
            )));
        }
        // Get count of rows with the same alias and specification_id
        $count = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->where('alias', '=', $alias)
            ->where('id', '!=', $id)
            ->count_all();
        // Error if such alias exists
        if ($count) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Измените алиас. Такой уже есть',
            )));
        }
        // Trying to save data
        $result = Builder::factory('specifications_values')->data(array(
            'name' => $name,
            'alias' => $alias,
            'status' => $status,
            'color' => $color,
        ))->where('id', '=', $id)->edit();
        // Error if failed saving
        if (!$result) {
            die(json_encode(array(
                'success' => false,
                'error' => 'Ошибка на сервере. Повторите попытку позднее',
            )));
        }
        // Get full list of values for current specification
        $result = Builder::factory('specifications_values')
            ->where('specification_id', '=', $specification_id)
            ->order_by('name')
            ->find_all();
        // Answer
        die(json_encode(array(
            'success' => true,
            'result' => $result,
        )));
    }

}