<?php

class authController {
    
    protected $password_length = 5;

    function __construct() {
        conf::set( 'title', 'Админ-панель Khersondaily' );
    }

    function loginAction () {
        if( User::admin() ) {
            location( 'backend/index' );
        }
        return support::tpl( array(), 'auth/login' );
    }

    function editAction () {
        if( !User::admin() ) {
            location( 'backend/'.Route::controller().'/login' );
        }

        $user = User::info();

        if( $_POST ) {
            $post = $_POST;
            if( 
                strlen( Arr::get( $post, 'password' ) ) < $this->password_length OR
                strlen( Arr::get( $post, 'new_password' ) ) < $this->password_length OR
                strlen( Arr::get( $post, 'confirm_password' ) ) < $this->password_length OR
                !User::factory()->check_password( Arr::get( $post, 'password' ), $user->password ) OR
                Arr::get( $post, 'new_password' ) != Arr::get( $post, 'confirm_password' )
            ) {
                Message::GetMessage( 0, 'Вы что-то напутали с паролями!' );
                location( 'backend/'.Route::controller().'/edit' );
            }
            if( !strlen( trim( Arr::get( $post, 'name' ) ) ) ) {
                Message::GetMessage( 0, 'Имя не может быть пустым!' );
                location( 'backend/'.Route::controller().'/edit' );
            }
            if( !strlen( trim( Arr::get( $post, 'login' ) ) ) ) {
                Message::GetMessage( 0, 'Логин не может быть пустым!' );
                location( 'backend/'.Route::controller().'/edit' );
            }
            $count = Builder::factory( 'users' )->where( 'id', '!=', $user->id )->where( 'login', '=', Arr::get( $post, 'login' ) )->count_all();
            if( $count ) {
                Message::GetMessage( 0, 'Пользователь с таким логином уже существует!' );
                location( 'backend/'.Route::controller().'/edit' );
            }

            $data = array(
                'name' => Arr::get( $post, 'name' ),
                'login' => Arr::get( $post, 'login' ),
                'password' => User::factory()->hash_password( Arr::get( $post, 'new_password' ) ),
            );
            Builder::factory( 'users' )->data( $data )->where( 'id', '=', $user->id )->edit();
            Message::GetMessage( 1, 'Вы успешно изменили данные!' );
            location( 'backend/'.Route::controller().'/edit' );
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Мой профиль' );
        conf::set( 'title', 'Редактирование личных данных' );
        conf::bread( '/backend/'.Route::controller().'/edit', conf::get( 'h1' ) );

        return support::tpl( array( 'obj' => $user ), 'auth/edit' );
    }

    function logoutAction() {
        if( !User::factory()->_admin ) {
            location( 'backend/'.Route::controller().'/login' );
        }
        User::factory()->logout();
        location( 'backend/'.Route::controller().'/login' );
    }

}