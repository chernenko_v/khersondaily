<?php

class brandsController {

    public $tpl_folder = 'brands';
    public $tablename  = 'brands';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Производители' );
        conf::set( 'title', 'Производители' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $status = NULL;
        if ( isset($_GET['status']) ) { $status = Arr::get($_GET, 'status', 1); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count = $count->count_all();
        $result = Builder::factory($this->tablename);
        if( $status !== NULL ) { $result->where( 'status', '=', $status ); }
        $result = $result->order_by('name', 'ASC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Наименование страницы не может быть пустым!');
                $error = 1;
            } else if( !trim(Arr::get($post, 'alias')) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
                $error = 1;
            } else {
                $id = Builder::factory($this->tablename)
                        ->select('id')
                        ->where('alias', '=', Arr::get($post, 'alias'))
                        ->where('id', '!=', Arr::get($_POST, 'id'))
                        ->find()
                        ->id;
                if($id) { $post['alias'] = Arr::get($post, 'alias').rand(1000,9999); }
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].time()).'.'.$ext;
                        $small = HOST . '/images/brands/small/' . $filename;
                        $original = HOST . '/images/brands/original/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(NULL, 80, Image::WIDTH);
                        $image->save($small);
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->save($original);
                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', Arr::get($_POST, 'id'))->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }

            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование производителя' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }
    
    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Наименование страницы не может быть пустым!');
                $error = 1;
            } else if( !trim(Arr::get($post, 'alias')) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
                $error = 1;
            } else {
                $id = Builder::factory($this->tablename)->select('id')->where('alias', '=', Arr::get($post, 'alias'))->find()->id;
                if($id) { $post['alias'] = Arr::get($post, 'alias').rand(1000,9999); }
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $id = Builder::factory($this->tablename)->get_last_id();
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_banners'.time()).'.'.$ext;
                        $small = HOST . '/images/brands/small/' . $filename;
                        $original = HOST . '/images/brands/original/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(NULL, 80, Image::WIDTH);
                        $image->save($small);
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->save($original);
                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', $id)->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }

            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление нового производителя' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }

        @unlink( HOST . '/images/brands/small/' . $page->image );
        @unlink( HOST . '/images/brands/original/' . $page->image );

        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }

    function deleteimageAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/brands/small/' . $page->image );
        @unlink( HOST . '/images/brands/original/' . $page->image );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
    }
}