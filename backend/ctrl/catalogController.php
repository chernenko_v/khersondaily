<?php

class catalogController {

    public $tpl_folder = 'catalog';
    public $tablename  = 'catalog';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Товары' );
        conf::set( 'title', 'Товары' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $status = NULL;
        if ( isset($_GET['status']) ) { $status = Arr::get($_GET, 'status', 1); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count = $count->count_all();
        $sql = 'SELECT catalog.*, catalog_images.image, catalog_tree.name AS catalog_tree_name, catalog_tree.id AS catalog_tree_id
                FROM catalog
                LEFT JOIN catalog_images ON catalog_images.catalog_id = catalog.id AND catalog_images.main = "1"
                LEFT JOIN catalog_tree ON catalog_tree.id = catalog.parent_id';
        if( $status !== NULL ) { $sql .= ' WHERE catalog.status = "1" '; }
        $sql .= ' ORDER BY catalog.created_at DESC LIMIT '.(($page - 1) * $this->limit).', '.$this->limit;
        $result = mysql::query($sql);
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        $itemSizes = Arr::get( $_POST, 'SIZES', array() );
        $specArray = Arr::get( $_POST, 'SPEC', array() );
        if ($_POST) {
            $post = $_POST['FORM'];

            // Set default settings for some fields
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            $post['new'] = Arr::get( $_POST, 'new', 0 );
            $post['top'] = Arr::get( $_POST, 'top', 0 );
            $post['sale'] = Arr::get( $_POST, 'sale', 0 );
            $post['available'] = Arr::get( $_POST, 'available', 0 );
            $post['sex'] = Arr::get( $_POST, 'sex', 0 );
            $post['cost'] = (int) Arr::get( $post, 'cost', 0 );
            $post['cost_old'] = (int) Arr::get( $post, 'cost_old', 0 );
            if( Arr::get( $post, 'new' ) AND ! (int) Builder::factory($this->tablename)->select('new')->where('id', '=', (int) Route::param('id'))->find()->new) {
                $post['new_from'] = time();
            }
            // Check form for rude errors
            if( !Arr::get( $post, 'alias' ) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
            } else if( !Arr::get( $post, 'name' ) ) {
                Message::GetMessage(0, 'Название не может быть пустым!');
            } else if( !Arr::get( $post, 'cost' ) ) {
                Message::GetMessage(0, 'Цена не может быть пустой!');
            } else {
                // Check data for same alias, if same - add some numbers to the end of alias
                $res = Builder::factory($this->tablename)
                        ->where( 'alias', '=', Arr::get( $post, 'alias' ) )
                        ->where( 'id', '!=', Arr::get( $_POST, 'id' ) )
                        ->count_all();
                if( $res ) {
                    $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999);
                }
                // Save item
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Builder::factory('catalog_sizes')->where('catalog_id', '=', Arr::get($_POST, 'id'))->delete();
                    foreach ($itemSizes as $size_id) {
                        Builder::factory('catalog_sizes')->data(array(
                            'catalog_id' => Arr::get($_POST, 'id'),
                            'size_id' => $size_id,
                        ))->add();
                    }
                    Builder::factory('catalog_specifications_values')->where('catalog_id', '=', Arr::get($_POST, 'id'))->delete();
                    foreach ($specArray as $key => $value) {
                        if( is_array($value) ) {
                            foreach ($value as $specification_value_id) {
                                Builder::factory('catalog_specifications_values')->data(array(
                                    'catalog_id' => Arr::get($_POST, 'id'),
                                    'specification_value_id' => $specification_value_id,
                                    'specification_id' => $key,
                                ))->add();
                            }
                        } else {
                            Builder::factory('catalog_specifications_values')->data(array(
                                'catalog_id' => Arr::get($_POST, 'id'),
                                'specification_value_id' => $value,
                                'specification_id' => $key,
                            ))->add();
                        }
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
            $res = Builder::factory('catalog_sizes')->where('catalog_id', '=', (int) Route::param('id'))->find_all();
            foreach ($res as $obj) {
                $itemSizes[] = $obj->size_id;
            }
            $res = Builder::factory('catalog_specifications_values')
                    ->select(array('specification_id', 'specification_value_id', 'specifications.type_id'))
                    ->join('specifications')
                    ->on('catalog_specifications_values.specification_id', '=', Builder::expr('specifications.id'))
                    ->where('catalog_id', '=', (int) Route::param('id'))
                    ->find_all();
            foreach ($res as $obj) {
                if( $obj->type_id == 3 ) {
                    $specArray[$obj->specification_id][] = $obj->specification_value_id;
                } else {
                    $specArray[$obj->specification_id] = $obj->specification_value_id;
                }
            }
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование товара' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        $images = Builder::factory( 'catalog_images' )->where( 'catalog_id', '=', $result->id )->order_by('sort')->find_all();
        $show_images = support::tpl(array('images' => $images), $this->tpl_folder . '/uploaded_images');

        $brands = Builder::factory('brands')
                    ->join('catalog_tree_brands')
                    ->on('catalog_tree_brands.brand_id', '=', Builder::expr('`brands`.`id`'))
                    ->where('catalog_tree_brands.catalog_tree_id', '=', $result->parent_id)
                    ->order_by('name')
                    ->find_all();
        $sizes = Builder::factory('sizes')
                    ->join('catalog_tree_sizes')
                    ->on('catalog_tree_sizes.size_id', '=', Builder::expr('`sizes`.`id`'))
                    ->where('catalog_tree_sizes.catalog_tree_id', '=', $result->parent_id)
                    ->order_by('name')
                    ->find_all();


        $specifications = Builder::factory('specifications')
                    ->join('catalog_tree_specifications')
                    ->on('catalog_tree_specifications.specification_id', '=', Builder::expr('`specifications`.`id`'))
                    ->where('catalog_tree_specifications.catalog_tree_id', '=', $result->parent_id)
                    ->order_by('name')
                    ->find_all();
        $arr = array();
        foreach($specifications AS $s) {
            $arr[] = $s->id;
        }
        $specValues = Builder::factory('specifications_values')
                    ->where('specification_id', 'IN', $arr)
                    ->order_by('name')
                    ->find_all();
        $arr = array();
        foreach ($specValues as $obj) {
            $arr[$obj->specification_id][] = $obj;
        }

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('catalog/select', 'catalog_tree', $result->parent_id),
                'brands' => $brands,
                'models' => Builder::factory('models')->where('brand_id', '=', $result->brand_id)->order_by('name')->find_all(),
                'show_images' => $show_images,
                'countSimpleOrders' => Builder::factory('orders_simple')->where('catalog_id', '=', $result->id)->count_all(),
                'countOrders' => Builder::factory('orders')->join('orders_items', 'LEFT')->on('orders_items.order_id', '=', Builder::expr('orders.id'))->where('orders_items.catalog_id', '=', $result->id)->count_all(),
                'happyCount' => Builder::factory('orders_items')
                                    ->join('orders', 'LEFT')
                                    ->on('orders_items.order_id', '=', Builder::expr('orders.id'))
                                    ->on('orders.status', '=', '1')
                                    ->where('catalog_id', '=', $result->id)
                                    ->count_all(),
                'happyMoney' => mysql::query_one('SELECT SUM(orders_items.count * orders_items.cost) AS amount
                                                  FROM orders_items
                                                  LEFT JOIN orders ON orders.id = orders_items.order_id AND orders.status = "1"
                                                  WHERE orders_items.catalog_id = "'.$result->id.'" GROUP BY orders_items.catalog_id')->amount,
                'itemSizes' => $itemSizes,
                'sizes' => $sizes,
                'specifications' => $specifications,
                'specValues' => $arr,
                'specArray' => $specArray,
            ), $this->tpl_folder.'/form');
    }
    
    function addAction () {
        $itemSizes = Arr::get( $_POST, 'SIZES', array() );
        $specArray = Arr::get( $_POST, 'SPEC', array() );
        if ($_POST) {
            $post = $_POST['FORM'];

            // Set default settings for some fields
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            $post['new'] = Arr::get( $_POST, 'new', 0 );
            $post['top'] = Arr::get( $_POST, 'top', 0 );
            $post['sale'] = Arr::get( $_POST, 'sale', 0 );
            $post['available'] = Arr::get( $_POST, 'available', 0 );
            $post['sex'] = Arr::get( $_POST, 'sex', 0 );
            $post['cost'] = (int) Arr::get( $post, 'cost', 0 );
            $post['cost_old'] = (int) Arr::get( $post, 'cost_old', 0 );
            if( Arr::get( $post, 'new' ) ) {
                $post['new_from'] = time();
            }
            // Check form for rude errors
            if( !Arr::get( $post, 'alias' ) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
            } else if( !Arr::get( $post, 'name' ) ) {
                Message::GetMessage(0, 'Название не может быть пустым!');
            } else if( !Arr::get( $post, 'cost' ) ) {
                Message::GetMessage(0, 'Цена не может быть пустой!');
            } else {
                // Check data for same alias, if same - add some numbers to the end of alias
                $res = Builder::factory($this->tablename)->where( 'alias', '=', Arr::get( $post, 'alias' ) )->count_all();
                if( $res ) {
                    $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999);
                }
                // Set item as last in list
                $post['sort'] = Builder::factory('catalog')->count_all();
                // Save item
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    $id = Builder::factory($this->tablename)->get_last_id();
                    foreach ($itemSizes as $size_id) {
                        Builder::factory('catalog_sizes')->data(array(
                            'catalog_id' => $id,
                            'size_id' => $size_id,
                        ))->add();
                    }
                    foreach ($specArray as $key => $value) {
                        if( is_array($value) ) {
                            foreach ($value as $specification_value_id) {
                                Builder::factory('catalog_specifications_values')->data(array(
                                    'catalog_id' => $id,
                                    'specification_value_id' => $specification_value_id,
                                    'specification_id' => $key,
                                ))->add();
                            }
                        } else {
                            Builder::factory('catalog_specifications_values')->data(array(
                                'catalog_id' => $id,
                                'specification_value_id' => $value,
                                'specification_id' => $key,
                            ))->add();
                        }
                    }
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    $id = Builder::factory($this->tablename)->get_last_id();
                    location('/backend/'.Route::controller().'/new/id/'.$id);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result = Arr::to_object($post);
            $parent_id = $result->parent_id;
            $models = Builder::factory('models')->where('brand_id', '=', $result->brand_id)->find_all();
        } else {
            $result = array();
            $models = array();
            $parent_id = 0;
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление нового товара' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        $brands = Builder::factory('brands')
                    ->join('catalog_tree_brands')
                    ->on('catalog_tree_brands.brand_id', '=', Builder::expr('`brands`.`id`'))
                    ->where('catalog_tree_brands.catalog_tree_id', '=', $parent_id)
                    ->order_by('name')
                    ->find_all();
        $sizes = Builder::factory('sizes')
                    ->join('catalog_tree_sizes')
                    ->on('catalog_tree_sizes.size_id', '=', Builder::expr('`sizes`.`id`'))
                    ->where('catalog_tree_sizes.catalog_tree_id', '=', $parent_id)
                    ->order_by('name')
                    ->find_all();

        $specifications = Builder::factory('specifications')
                    ->join('catalog_tree_specifications')
                    ->on('catalog_tree_specifications.specification_id', '=', Builder::expr('`specifications`.`id`'))
                    ->where('catalog_tree_specifications.catalog_tree_id', '=', $result->parent_id)
                    ->order_by('name')
                    ->find_all();
        $arr = array();
        foreach($specifications AS $s) {
            $arr[] = $s->id;
        }
        $specValues = Builder::factory('specifications_values')
                    ->where('specification_id', 'IN', $arr)
                    ->order_by('name')
                    ->find_all();
        $arr = array();
        foreach ($specValues as $obj) {
            $arr[$obj->specification_id][] = $obj;
        }

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('catalog/select', 'catalog_tree', $result->parent_id),
                'brands' => $brands,
                'sizes' => $sizes,
                'models' => $models,
                'itemSizes' => $itemSizes,
                'specifications' => $specifications,
                'specValues' => $arr,
                'specArray' => $specArray,
            ), $this->tpl_folder.'/form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $images = Builder::factory( 'catalog_images' )->where( 'catalog_id', '=', $id )->find_all();
        foreach ( $images AS $im ) {
            @unlink( HOST . '/images/catalog/small/' . $im->image );
            @unlink( HOST . '/images/catalog/medium/' . $im->image );
            @unlink( HOST . '/images/catalog/big/' . $im->image );
            @unlink( HOST . '/images/catalog/original/' . $im->image );
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }

}