<?php

class catalog_treeController {

    public $tpl_folder = 'catalog_tree';
    public $tablename  = 'catalog_tree';

    function __construct() {
        conf::set( 'h1', 'Группы товаров' );
        conf::set( 'title', 'Группы товаров' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
    }

    function indexAction () {
        $result = Builder::factory($this->tablename)->order_by('sort', 'ASC')->find_all();
        $arr    = array();
        foreach($result AS $obj) {
            $arr[$obj->parent_id][] = $obj;
        }

        conf::set( 'filter', Widgets::get( 'Filter/Pages', array( 'open' => 1 ) ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $arr,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        $groupBrands = Arr::get( $_POST, 'BRANDS', array() );
        $groupSizes = Arr::get( $_POST, 'SIZES', array() ); 
        $groupSpec = Arr::get( $_POST, 'SPEC', array() ); 
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Наименование страницы не может быть пустым!');
            } else if( !trim(Arr::get($post, 'alias')) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
            } else {
                $id = Builder::factory($this->tablename)
                        ->select('id')
                        ->where('alias', '=', Arr::get($post, 'alias'))
                        ->where('id', '!=', Arr::get($_POST, 'id'))
                        ->find()
                        ->id;
                if($id) { $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999); }
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].time()).'.'.$ext;
                        $big = HOST . '/images/catalog_tree/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(240, 240, Image::INVERSE);
                        $image->crop(240, 240);
                        $image->save($big);
                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', Arr::get($_POST, 'id'))->edit();
                    }
                    Builder::factory('catalog_tree_brands')->where('catalog_tree_id', '=', Arr::get($_POST, 'id'))->delete();
                    foreach ($groupBrands as $brand_id) {
                        Builder::factory('catalog_tree_brands')->data(array(
                            'catalog_tree_id' => Arr::get($_POST, 'id'),
                            'brand_id' => $brand_id,
                        ))->add();
                    }
                    Builder::factory('catalog_tree_sizes')->where('catalog_tree_id', '=', Arr::get($_POST, 'id'))->delete();
                    foreach ($groupSizes as $size_id) {
                        Builder::factory('catalog_tree_sizes')->data(array(
                            'catalog_tree_id' => Arr::get($_POST, 'id'),
                            'size_id' => $size_id,
                        ))->add();
                    }
                    Builder::factory('catalog_tree_specifications')->where('catalog_tree_id', '=', Arr::get($_POST, 'id'))->delete();
                    foreach ($groupSpec as $specification_id) {
                        Builder::factory('catalog_tree_specifications')->data(array(
                            'catalog_tree_id' => Arr::get($_POST, 'id'),
                            'specification_id' => $specification_id,
                        ))->add();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }

            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
            $res = Builder::factory('catalog_tree_brands')->where('catalog_tree_id', '=', (int) Route::param('id'))->find_all();
            foreach ($res as $obj) {
                $groupBrands[] = $obj->brand_id;
            }
            $res = Builder::factory('catalog_tree_sizes')->where('catalog_tree_id', '=', (int) Route::param('id'))->find_all();
            foreach ($res as $obj) {
                $groupSizes[] = $obj->size_id;
            }
            $res = Builder::factory('catalog_tree_specifications')->where('catalog_tree_id', '=', (int) Route::param('id'))->find_all();
            foreach ($res as $obj) {
                $groupSpec[] = $obj->specification_id;
            }
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование группы товаров' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('catalog_tree/select', 'catalog_tree', $result->parent_id, $result->id),
                'brands' => Builder::factory('brands')->order_by('name')->find_all(),
                'sizes' => Builder::factory('sizes')->order_by('name')->find_all(),
                'specifications' => Builder::factory('specifications')->order_by('name')->find_all(),
                'groupBrands' => $groupBrands,
                'groupSizes' => $groupSizes,
                'groupSpec' => $groupSpec,
            ), $this->tpl_folder.'/form');
    }
    
    function addAction () {
        $groupBrands = Arr::get( $_POST, 'BRANDS', array() );
        $groupSizes = Arr::get( $_POST, 'SIZES', array() );
        $groupSpec = Arr::get( $_POST, 'SPEC', array() );
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Наименование страницы не может быть пустым!');
                $error = 1;
            } else if( !trim(Arr::get($post, 'alias')) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
                $error = 1;
            } else {
                $id = Builder::factory($this->tablename)->select('id')->where('alias', '=', Arr::get($post, 'alias'))->find()->id;
                if($id) { $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999); }

                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    $id = Builder::factory($this->tablename)->get_last_id();
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_banners'.time()).'.'.$ext;
                        $big = HOST . '/images/catalog_tree/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(240, 240, Image::INVERSE);
                        $image->crop(240, 240);
                        $image->save($big);
                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', $id)->edit();
                    }
                    foreach ($groupBrands as $brand_id) {
                        Builder::factory('catalog_tree_brands')->data(array(
                            'catalog_tree_id' => $id,
                            'brand_id' => $brand_id,
                        ))->add();
                    }
                    foreach ($groupSizes as $size_id) {
                        Builder::factory('catalog_tree_sizes')->data(array(
                            'catalog_tree_id' => $id,
                            'size_id' => $size_id,
                        ))->add();
                    }
                    foreach ($groupSpec as $specification_id) {
                        Builder::factory('catalog_tree_specifications')->data(array(
                            'catalog_tree_id' => $id,
                            'specification_id' => $specification_id,
                        ))->add();
                    }
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление новой группы товаров' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('catalog_tree/select', 'catalog_tree', $result->parent_id),
                'brands' => Builder::factory('brands')->order_by('name')->find_all(),
                'sizes' => Builder::factory('sizes')->order_by('name')->find_all(),
                'specifications' => Builder::factory('specifications')->order_by('name')->find_all(),
                'groupBrands' => $groupBrands,
                'groupSizes' => $groupSizes,
                'groupSpec' => $groupSpec,
            ), $this->tpl_folder.'/form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $countChildGroups = Builder::factory('catalog_tree')->where('parent_id', '=', $id)->count_all();
        if ( $countChildGroups ) {
            Message::GetMessage(0, 'Нельзя удалить эту группу, так как у нее есть подгруппы!');
            location('backend/'.Route::controller().'/index');
        }
        $countChildItems = Builder::factory('catalog')->where('parent_id', '=', $id)->count_all();
        if ( $countChildItems ) {
            Message::GetMessage(0, 'Нельзя удалить эту группу, так как в ней содержатся товары!');
            location('backend/'.Route::controller().'/index');
        }
        @unlink( HOST . '/images/catalog_tree/' . $page->image );
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }

    function deleteimageAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/catalog_tree/' . $page->image );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
    }
}