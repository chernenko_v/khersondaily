<?php

class commentsController {

    public $tpl_folder = 'comments';
    public $tablename  = 'catalog_comments';
    public $limit;
    
    function __construct() {
        conf::set( 'h1', 'Отзывы к товарам' );
        conf::set( 'title', 'Отзывы к товарам' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $date_s = NULL; $date_po = NULL; $status = NULL;
        if ( Arr::get($_GET, 'date_s') ) { $date_s = strtotime( Arr::get($_GET, 'date_s') ); }
        if ( Arr::get($_GET, 'date_po') ) { $date_po = strtotime( Arr::get($_GET, 'date_po') ); }
        if ( isset($_GET['status']) ) { $status = Arr::get($_GET, 'status', 1); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $date_s ) { $count->where( 'date', '>=', $date_s ); }
        if( $date_po ) { $count->where( 'date', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count = $count->count_all();

        $sql = 'SELECT catalog_comments.*, catalog.name AS item_name, catalog.alias AS item_alias
                FROM catalog_comments
                LEFT JOIN catalog ON catalog.id = catalog_comments.catalog_id
                WHERE catalog_comments.id > 0 ';
        if( $date_s ) { $sql .= ' AND catalog_comments.date >= "'.$date_s.'" '; }
        if( $date_po ) { $sql .= ' AND catalog_comments.date <= "'.($date_po + 24 * 60 * 60 - 1).'" '; }
        if( $status !== NULL ) { $sql .= ' AND catalog_comments.status = "'.$status.'" '; }
        $sql .= 'ORDER BY catalog_comments.date DESC
                LIMIT ' . ($page - 1) * $this->limit . ', ' . $this->limit;
        $result = mysql::query($sql);
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );

            $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
            if($res) {
                Message::GetMessage(1, 'Вы успешно изменили данные!');
                location($_SERVER['HTTP_REFERER']);
            } else {
                Message::GetMessage(0, 'Не удалось изменить данные!');
            }

            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование отзыва к товару' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'item' => Builder::factory('catalog')->where('id', '=', $result->catalog_id)->find(),
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }
}