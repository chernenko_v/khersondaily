<?php

class configController {

	public $tpl_folder = 'config';
	public $tablename  = 'config';	

    function __construct() {
        conf::set( 'h1', 'Настройки сайта' );
        conf::set( 'title', 'Настройки сайта' );
        conf::bread( '/backend/'.Route::controller().'/edit', conf::get( 'h1' ) );
    }

	function editAction () {
		if ($_POST) {
            foreach($_POST['FORM'] as $key => $value) {
                Builder::factory($this->tablename)->data(Array('zna' => htmlspecialchars($value)))->where('id', '=', $key)->edit();
            }
            Message::GetMessage(1, 'Вы успешно изменили данные!');
            location( 'backend/'.Route::controller().'/edit' );
		} 
		$result = Builder::factory($this->tablename)->where('status', '=', 1)->order_by('sort')->find_all();

        conf::set( 'toolbar', Widgets::get( 'Toolbar/EditSaveOnly' ) );
		return support::tpl(
			array(
				'result' => $result,
				'tpl_folder' => $this->tpl_folder
			), $this->tpl_folder.'/edit');        
	}
}