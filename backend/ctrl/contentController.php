<?php

class contentController {

	public $tpl_folder = 'content';
	public $tablename  = 'content';

    function __construct() {
        conf::set( 'noDelete', array( 1, 5, 6, 7, 8, 14 ) );
        conf::set( 'h1', 'Страницы' );
        conf::set( 'title', 'Страницы' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
    }

	function indexAction () {
        $result = Builder::factory($this->tablename)->order_by('sort', 'ASC')->find_all();
        $arr    = array();
        foreach($result AS $obj) {
            $arr[$obj->parent_id][] = $obj;
        }

        conf::set( 'filter', Widgets::get( 'Filter/Pages', array( 'open' => 1 ) ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1 ) ) );

		return support::tpl(
			array(
				'result'        => $arr,
				'tpl_folder'    => $this->tpl_folder,
                'tablename'     => $this->tablename,
			), $this->tpl_folder.'/index');
	}

	function newAction () {
        if ($_POST) {
            $error = 0;
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Наименование страницы не может быть пустым!');
                $error = 1;
            }
            if(isset($_POST['FORM']['alias'])) {
                if( !trim(Arr::get($post, 'alias')) ) {
                    Message::GetMessage(0, 'Алиас не может быть пустым!');
                    $error = 1;
                }
                $id = Builder::factory($this->tablename)->select('id')->where('alias', '=', Arr::get($post, 'alias'))->find()->id;
                if($id AND $id != $_POST['id']) {
                    $post['alias'] = Arr::get($post, 'alias').rand(1000,9999);
                }
            }
            if( !$error ) {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                   // print_r($_FILES);
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_content'.time()).'.'.$ext;
                        $original = HOST . '/images/content/original/' . $filename;
                        $big = HOST . '/images/content/big/' . $filename;
                        $small = HOST . '/images/content/small/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->save($original);

                        $image = Image::factory($original);
                        $image->resize(INFO_WIDTH, INFO_HEIGHT, Image::INVERSE);
                        $image->crop(INFO_WIDTH, INFO_HEIGHT);
                        $image->save($small);

                        $image = Image::factory($original);
                        $image->resize(656, NULL, Image::WIDTH);
                        $image->save($big);

                        Builder::factory($this->tablename)
                            ->data(array('image' => $filename))
                            ->where('id', '=', Arr::get($_POST, 'id'))
                            ->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
		} else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
		}

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'colls', 'column-2' );
        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование страницы' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('content/select', 'content', $result->parent_id),
            ), $this->tpl_folder.'/form');
	}
    
    function addAction () {
		if ($_POST) {
            $error = 0;
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Наименование страницы не может быть пустым!');
                $error = 1;
            }
            if( !trim(Arr::get($post, 'alias')) ) {
                Message::GetMessage(0, 'Алиас не может быть пустым!');
                $error = 1;
            }
            if( !$error ) {
                if(Builder::factory($this->tablename)->select('id')->where('alias', '=', Arr::get($post, 'alias'))->find()->id) {
                    $post['alias'] = Arr::get($post, 'alias').rand(1000,9999);
                }
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->add();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $id = Builder::factory($this->tablename)->get_last_id();

                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_content'.time()).'.'.$ext;
                        $original = HOST . '/images/content/original/' . $filename;
                        $big = HOST . '/images/content/big/' . $filename;
                        $small = HOST . '/images/content/small/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->save($original);

                        $image = Image::factory($original);
                        $image->resize(INFO_WIDTH, INFO_HEIGHT, Image::INVERSE);
                        $image->crop(INFO_WIDTH, INFO_HEIGHT);
                        $image->save($small);

                        $image = Image::factory($original);
                        $image->resize(656, NULL, Image::WIDTH);
                        $image->save($big);

                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', $id)->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result     = Arr::to_object($post);
		} else {
            $result     = array();
		}

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'colls', 'column-2' );
        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление новой страницы' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

		return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('content/select', 'content', $result->parent_id),
            ), $this->tpl_folder.'/form');
    }

	function deleteAction() {
        $id = (int) Route::param('id');
        if( in_array( $id, conf::get( 'noDelete' ) ) ) {
            Message::GetMessage(0, 'Эту страницу удалить нельзя!');
            location('backend/'.Route::controller().'/index');
        }
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->data(array( 'parent_id' => $page->parent_id ))->where('parent_id', '=', $id)->edit();
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
	}

    function deleteimageAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/content/small/' . $page->image );
        @unlink( HOST . '/images/content/big/' . $page->image );
        @unlink( HOST . '/images/content/original/' . $page->image );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
    }
}