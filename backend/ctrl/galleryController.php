<?php

class galleryController {
	public $tablename = 'gallery_albums';
	public $tpl_folder = 'gallery';
	public $tablename_images = 'gallery_images';
	public $limit;

	function __construct() {
		conf::set( 'h1', 'Галерея' );
        conf::set( 'title', 'Галерея' );
        conf::set( 'keywords', 'Галерея' );
        conf::set( 'description', 'Галерея' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
	}

	function indexAction(){
        $page = (int) Route::param('page');
		conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );
		$result = Builder::factory($this->tablename)
                        ->order_by('date','DESC')
                        ->limit($this->limit, ($page - 1) * $this->limit)
						->find_all();
        $count = Builder::factory($this->tablename)->count_all();
        $pager = Backend::pager( $count, $this->limit, $page );
		return support::tpl(array(
				'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager
            ), $this->tpl_folder.'/index');
	}

	function newAction(){
		if ($_POST) {

            /**
             *  Загрузка zip архивов
             */
            if ($_FILES["zip_file"]["name"]) {
                $filename = $_FILES["zip_file"]["name"];
                $source = $_FILES["zip_file"]["tmp_name"];
                $type = $_FILES["zip_file"]["type"];
                $name = explode(".", $filename);
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                foreach ($accepted_types as $mime_type) {
                    if ($mime_type == $type) {
                        $okay = true;
                        break;
                    }
                }
                $continue = strtolower($name[1]) == 'zip' ? true : false;
                if (!$continue) {
                    $message = "The file you are trying to upload is not a .zip file. Please try again.";
                }
                $target_path = HOST . '/images/gallery/'.$name;
                if (move_uploaded_file($source, $target_path)) {
                    $zip = new ZipArchive();
                    $x = $zip->open($target_path);
                    if ($x === true) {
                        for ($i = 0; $i < $zip->numFiles; $i++) {
                            $single_name = $zip->getNameIndex($i);
                            $ext = end(explode('.', $single_name));
                            $single_name = md5($single_name . '_gallery' . time()) . '.' . $ext;
                            $zip->renameIndex($i, $single_name);
                            $filenames[] = $zip->getNameIndex($i);
                        }
                        $zip->close();
                        $zip1 = new ZipArchive();
                        $res = $zip1->open($target_path);
                        if ($res) {
                            $zip1->extractTo(HOST . '/images/gallery/original'); // change this to the correct site path
                            foreach ($filenames as $file_name) {
                                $filename = $file_name;

                                $big = HOST . '/images/gallery/big/' . $filename;
                                $small = HOST . '/images/gallery/small/' . $filename;
                                $original = HOST . '/images/gallery/original/' . $filename;
                                $medium = HOST. '/images/gallery/medium/'. $filename;
                                $watermark = Image::factory(HOST . '/pic/logo.png');

                                $image = Image::factory(HOST . '/images/gallery/original/' . $filename);

                                $image->save($original);

                                $image = Image::factory(HOST . '/images/gallery/original/' . $filename);
                                $image->resize(200, 160, Image::INVERSE);
                                $image->crop(200, 160);
                                $image->save($small);

                                $image = Image::factory(HOST . '/images/gallery/original/' . $filename);
                                $image->resize(678, 520, Image::INVERSE);
                                $image->crop(678, 520, Image::INVERSE);
                              //  $image->watermark($watermark, 0, NULL, $opacity = 75);
                                $image->save($big);

                                $image = Image::factory(HOST . '/images/gallery/original/' . $filename);
                                $image->resize(GALLERY_WIDTH, GALLERY_HEIGHT, Image::INVERSE);
                                $image->crop(GALLERY_WIDTH, GALLERY_HEIGHT);
                                $image->save($medium);

                                DB::insert($this->tablename_images, array('image', 'parent_id', 'date','sort'))->values(array($file_name, $_POST['id'], time(), ''))->execute();
                            }
                            $zip1->close();
                            unlink($target_path);
                            $message = "Your .zip file was uploaded and unpacked.";
                        }
                    }
                    $message = "Problem with renema files";
                } else {
                    $message = "There was a problem with the upload. Please try again.";
                }
            }

            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            $post['date'] = time();
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'alias'))) {
                Message::GetMessage(0, 'Поле "Алиас" не может быть пустым!');
            } else if(!Arr::get($post, 'date')) {
                Message::GetMessage(0, 'Поле "Дата" не может быть пустым!');
            } else {
                $count = Builder::factory($this->tablename)->where('alias', '=', Arr::get($post, 'alias'))->where('id', '!=', Arr::get($_POST, 'id'))->count_all();
                if($count) { $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999); }
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)
                                ->where('id', '=', (int) Route::param('id'))
                                ->find();

            $page = (int) Route::param('page');

        	$images = Builder::factory( 'gallery_images' )
	        	->where( 'parent_id', '=', $result->id )
	        	->order_by('sort')
                ->limit($this->limit, ($page - 1) * $this->limit)
	        	->find_all();
            $count = Builder::factory( 'gallery_images' )
                ->where( 'parent_id', '=', $result->id )
                ->count_all();
            $pager = Backend::pager( $count, $this->limit, $page );

        	$show_images = support::tpl(array('images' => $images), $this->tpl_folder . '/uploaded_images');
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
        conf::bread( '/backend/'.Route::controller().'/new/'.Route::param('id'), 'Редактировать' );

		return support::tpl(array(
				'obj' => $result,
				'show_images' => $show_images,
                'pager' => $pager,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename
            ), $this->tpl_folder.'/form');
	}

	function addAction() {
        conf::bread( '/backend/'.Route::controller().'/add', 'Добавить' );
		if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            $post['date'] = time();
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'alias'))) {
                Message::GetMessage(0, 'Поле "Алиас" не может быть пустым!');
            } else if(!Arr::get($post, 'date')) {
                Message::GetMessage(0, 'Поле "Дата" не может быть пустым!');
            } else {
                $count = Builder::factory($this->tablename)->where('alias', '=', Arr::get($post, 'alias'))->count_all();
                if($count) { $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999); }
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

		return support::tpl(array(
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename
            ), $this->tpl_folder.'/form');
	}

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();

        $images = Builder::factory($this->tablename_images)
        ->where('parent_id', '=', $id)
        ->find_all();
        foreach ($images as $obj) {
            @unlink( HOST . '/images/gallery/small/' . $obj->image );
            @unlink( HOST . '/images/gallery/big/' . $obj->image );
            @unlink( HOST . '/images/gallery/medium/' . $obj->image );
            @unlink( HOST . '/images/gallery/original/' . $obj->image );
        }

        Builder::factory($this->tablename_images)->where('parent_id', '=', $id)->delete();
        
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }
}