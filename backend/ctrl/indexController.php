<?php

class indexController {

	function indexAction () {
		conf::set( 'h1', 'Панель управления' );
        conf::set( 'title', 'Панель управления' );

		//$count_catalog = Builder::factory('catalog')->count_all();
		//$count_orders = Builder::factory('orders')->count_all();
		//$count_comments = Builder::factory('catalog_comments')->count_all();
		//$count_subscribers = Builder::factory('subscribers')->count_all();
		$count_users = Builder::factory('users')->where('role_id', '!=', 2)->count_all();
		//$count_banners = Builder::factory('banners')->count_all();
		//$count_articles = Builder::factory('articles')->count_all();
		$count_news = Builder::factory('news')->count_all();

		return support::tpl( array(
			//'count_catalog' => $count_catalog,
			//'count_orders' => $count_orders,
			//'count_comments' => $count_comments,
			//'count_subscribers' => $count_subscribers,
			'count_users' => $count_users,
			//'count_banners' => $count_banners,
			//'count_articles' => $count_articles,
			'count_news' => $count_news
		), 'index/main');
	}

}