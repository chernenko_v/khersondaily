<?php

class logController {

    public $tpl_folder = 'log';
    public $tablename  = 'log';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Лента событий' );
        conf::set( 'title', 'Лента событий' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $date_s = NULL; $date_po = NULL;
        if ( Arr::get($_GET, 'date_s') ) { $date_s = strtotime( Arr::get($_GET, 'date_s') ); }
        if ( Arr::get($_GET, 'date_po') ) { $date_po = strtotime( Arr::get($_GET, 'date_po') ); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $date_s ) { $count->where( 'created_at', '>=', $date_s ); }
        if( $date_po ) { $count->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        $count = $count->count_all();
        $result = Builder::factory($this->tablename);
        if( $date_s ) { $result->where( 'created_at', '>=', $date_s ); }
        if( $date_po ) { $result->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        $result = $result->order_by('created_at', 'DESC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

}