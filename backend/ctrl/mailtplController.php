<?php

class mailtplController {

	public $tpl_folder = 'mailtpl';
	public $tablename  = 'mail_templates';

    function __construct() {
        conf::set( 'h1', 'Шаблоны писем' );
        conf::set( 'title', 'Шаблоны писем' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
    }

	function indexAction () {
		$result = Builder::factory($this->tablename)->order_by('id')->find_all();

        conf::set( 'filter', support::widget( array(), 'Filter/Pages' ) );
        conf::set( 'toolbar', support::widget( array(), 'Toolbar/List' ) );

		return support::tpl(
			array(
				'result' => $result,
				'msg' => $this->msg,
				'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
			),$this->tpl_folder.'/index');
	}

	function newAction () {
        conf::set( 'toolbar', support::widget( array( 'list' => 1 ), 'Toolbar/Edit' ) );
        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование шаблона письма' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.Route::param('id'), conf::get( 'h1' ) );

		if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !Arr::get( $post, 'subject' ) OR !Arr::get( $post, 'name' ) ) {
                Message::GetMessage(0, 'Не все даныне заполнены верно!');
            } else {
                $res  = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
		} else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
		}

		return support::tpl(
			array(
				'obj' => $result, 
				'tpl_folder' => $this->tpl_folder,
			), $this->tpl_folder . '/form');
	}	

}