<?php

class menuController {

    public $tpl_folder = 'menu';
    public $tablename  = 'sitemenu';

    function __construct() {
        conf::set( 'h1', 'Меню сайта' );
        conf::set( 'title', 'Меню сайта' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
    }

    function indexAction () {
        $result = Builder::factory($this->tablename)->order_by('sort', 'ASC')->find_all();

        conf::set( 'filter', Widgets::get( 'Filter/Pages' ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'url'))) {
                Message::GetMessage(0, 'Поле "Ссылка" не может быть пустым!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
            $tree = Builder::factory('news_tree')->select(array('name','id'))->where('status', '=', 1)->find_all();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование пункта меню' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return system::show_tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => $tree
            ), $this->tpl_folder.'/new.php');
    }
    
    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'url'))) {
                Message::GetMessage(0, 'Поле "Ссылка" не может быть пустым!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result     = Arr::to_object($post);
        } else {
            $result     = array();
            $tree = Builder::factory('news_tree')->select(array('name','id'))->where('status', '=', 1)->find_all();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление нового пункта меню' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => $tree
            ), $this->tpl_folder.'/add');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }
}