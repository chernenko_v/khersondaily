<?php
class ordersController {

	public $tpl_folder = 'orders';
	public $tablename  = 'orders';
    public $statuses;
    public $delivery;
    public $payment;
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Заказы' );
        conf::set( 'title', 'Заказы' );
        conf::bread( '/backend/orders/index', conf::get( 'h1' ) );

        $this->limit = conf::get( 'limit_backend' );
        $this->delivery = conf::get('order.delivery');
        $this->payment = conf::get('order.payment');
        $this->statuses = conf::get('order.statuses');
    }

	function indexAction () {
        $date_s = NULL; $date_po = NULL; $status = NULL;

        if ( Arr::get($_GET, 'date_s') ) { $date_s = strtotime( Arr::get($_GET, 'date_s') ); }
        if ( Arr::get($_GET, 'date_po') ) { $date_po = strtotime( Arr::get($_GET, 'date_po') ); }
        if ( isset( $this->statuses[ Arr::get($_GET, 'status') ] ) ) { $status = Arr::get($_GET, 'status', 1); }
		
        $page    = (int) Arr::get($_GET, 'page', 1);
        $count   = Builder::factory($this->tablename);
        if( $date_s !== NULL ) { $count->where( 'created_at', '>=', $date_s ); }
        if( $date_po !== NULL ) { $count->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count   = $count->count_all();
        
        $sql = 'SELECT  orders.*,
                        SUM(orders_items.count) AS count,
                        SUM(orders_items.cost * orders_items.count) AS amount
                FROM orders
                LEFT JOIN orders_items ON orders_items.order_id = orders.id
                WHERE orders.id > 0 ';
        if( $date_s !== NULL ) { $sql .= ' AND orders.created_at >= "'.$date_s.'" '; }
        if( $date_po !== NULL ) { $sql .= ' AND orders.created_at <= "'.($date_po + 24 * 60 * 60 - 1).'" '; }
        if( $status !== NULL ) { $sql .= ' AND orders.status = "'.$status.'" '; }
        $sql .= ' GROUP BY orders.id ORDER BY orders.created_at DESC LIMIT ' . (( $page - 1 ) * $this->limit). ', ' . $this->limit;
        $result = mysql::query($sql);
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/ListOrders', array( 'add' => 1 ) ) );

		return support::tpl(
			array(
				'result' => $result,
				'pager' => $pager, 
				'status' => $status,
                'date_s' => $date_s,
                'date_po' => $date_po,
                'statuses' => $this->statuses,
                'count' => Builder::factory( 'orders' )->count_all(),
			),'orders/index');
	}

    function newAction() {
        $result = mysql::query_one('SELECT orders.*, SUM(orders_items.count) AS count, SUM(orders_items.cost * orders_items.count) AS amount
                                    FROM orders
                                    LEFT JOIN orders_items ON orders_items.order_id = orders.id
                                    WHERE orders.id = "'.Route::param('id').'"');
        $cart = mysql::query('SELECT catalog.*, catalog_images.image, orders_items.count, sizes.name AS size_name, orders_items.cost AS price, orders_items.size_id
                              FROM orders_items
                              LEFT JOIN catalog ON orders_items.catalog_id = catalog.id
                              LEFT JOIN catalog_images ON catalog_images.main = "1" AND catalog_images.catalog_id = catalog.id
                              LEFT JOIN sizes ON orders_items.size_id = sizes.id
                              WHERE orders_items.order_id = "'.Route::param('id').'"');
        $user = mysql::query_one('SELECT users.*,
                                         COUNT(DISTINCT orders.id) AS orders,
                                         SUM(orders_items.cost * orders_items.count) AS amount,
                                         SUM(orders_items.count) AS count_items
                                  FROM users
                                  LEFT JOIN orders ON orders.user_id = users.id AND orders.status = "1"
                                  LEFT JOIN orders_items ON orders_items.order_id = orders.id
                                  WHERE users.id = "'.$result->user_id.'"');

        conf::set( 'h1', 'Заказ №' . Route::param('id') );
        conf::set( 'title', 'Заказ №' . Route::param('id') );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'user' => $user,
                'obj' => $result,
                'cart' => $cart,
                'statuses' => $this->statuses,
                'payment' => $this->payment,
                'delivery' => $this->delivery,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/inner');
    }

    function addAction(){
        $result = array();
        if( $_POST ) {
            $post = $_POST;
            if( !Arr::get($post, 'name') ) {
                Message::GetMessage( 0, 'Имя не может быть пустым!' );
            } else if( !Arr::get($post, 'phone') OR !preg_match('/^\+38 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', Arr::get($post, 'phone'), $matches) ) {
                Message::GetMessage( 0, 'Укажите верный номер телефона! Формат: +38 (ХХХ) ХХХ-ХХ-ХХ' );
            } else if( Arr::get($post, 'delivery') == 2 AND !Arr::get($post, 'number') ) {
                Message::GetMessage( 0, 'Номер отделения Новой почты не может быть пустым!' );
            } else {
                $data = array();
                $data['payment'] = Arr::get($post, 'payment');
                $data['delivery'] = Arr::get($post, 'delivery');
                $data['number'] = Arr::get($post, 'number');
                $data['name'] = Arr::get($post, 'name');
                $data['phone'] = Arr::get($post, 'phone');
                Builder::factory('orders')->data($data)->add();
                $id = Builder::factory('orders')->get_last_id();
                location('/backend/orders/new/id/'.$id);
            }
            $result = Arr::to_object($post);
        }

        conf::set( 'h1', 'Добавление заказа' );
        conf::set( 'title', 'Добавление заказа' );
        conf::bread( '/backend/'.Route::controller().'/add_position/id/'.(int) Route::param('id'), conf::get( 'h1' ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
        return support::tpl(
            array(
                'obj' => $result,
                'statuses' => $this->statuses,
                'payment' => $this->payment,
                'delivery' => $this->delivery,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/add');
    }

    function add_positionAction(){
        $result = array();
        if( $_POST ) {
            $post = $_POST;
            if( !Route::param('id') ) {
                Message::GetMessage( 0, 'Нельзя добавить товар несуществующему заказу!' );
            } else if( !Arr::get($post, 'catalog_id') ) {
                Message::GetMessage( 0, 'Нужно выбрать товар для добавления!' );
            } else if( !Arr::get($post, 'count') ) {
                Message::GetMessage( 0, 'Укажите количество товара больше 0!' );
            } else {
                $item = Builder::factory('catalog')->select('cost')->where('id', '=', Arr::get($post, 'catalog_id'))->find();
                if(!$item) {
                    Message::GetMessage( 0, 'ужно выбрать существующий товар для добавления!' );
                } else {
                    $data = array();
                    $data['order_id'] = (int) Route::param('id');
                    $data['catalog_id'] = (int) Arr::get($post, 'catalog_id');
                    $data['size_id'] = (int) Arr::get($post, 'size_id');
                    $data['count'] = (int) Arr::get($post, 'count');
                    $data['cost'] = (int) $item->cost;
                    Builder::factory('orders_items')->data($data)->add();
                    location('/backend/orders/add_position/id/'.Route::param('id'));
                }
            }
            $result = Arr::to_object($post);
        }

        conf::set( 'h1', 'Добавление позиции в заказ №' . Route::param('id') );
        conf::set( 'title', 'Добавление позиции в заказ №' . Route::param('id') );
        $back_link = '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id');
        conf::bread( $back_link, 'Заказ №' . (int) Route::param('id') );
        conf::bread( '/backend/'.Route::controller().'/add_position/id/'.(int) Route::param('id'), conf::get( 'h1' ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit', array('list_link' => $back_link) ) );

        $sizes = Builder::factory('sizes')
                    ->join('catalog_tree_sizes')
                    ->on('catalog_tree_sizes.size_id', '=', Builder::expr('`sizes`.`id`'))
                    ->where('catalog_tree_sizes.catalog_tree_id', '=', $result->parent_id)
                    ->order_by('name')
                    ->find_all();
        return support::tpl(
            array(
                'obj' => $result,
                'statuses' => $this->statuses,
                'payment' => $this->payment,
                'delivery' => $this->delivery,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'tree' => Backend::getSelectOptions('catalog/select', 'catalog_tree', $result->parent_id),
                'sizes' => $sizes,
            ), $this->tpl_folder.'/add_position');
    }

	function deleteAction () {
		$id   = (int) Route::param('id');
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные успешно удалены!');
        location('backend/'.Route::controller().'/index');		
	}


    function printAction() {
        $order = mysql::query_one('SELECT orders.*, SUM(orders_items.count) AS count, SUM(orders_items.cost * orders_items.count) AS amount
                                   FROM orders
                                   LEFT JOIN orders_items ON orders_items.order_id = orders.id
                                   WHERE orders.id = "'.Route::param('id').'"');
        $list = mysql::query('SELECT catalog.*, catalog_images.image, orders_items.count, sizes.name AS size_name, orders_items.cost AS price, orders_items.size_id
                                FROM orders_items
                                LEFT JOIN catalog ON orders_items.catalog_id = catalog.id
                                LEFT JOIN catalog_images ON catalog_images.main = "1" AND catalog_images.catalog_id = catalog.id
                                LEFT JOIN sizes ON orders_items.size_id = sizes.id
                                WHERE orders_items.order_id = "'.Route::param('id').'"');
        echo support::tpl( array(
            'order' => $order,
            'list' => $list,
            'payment' => conf::get('order.payment'),
            'delivery' => conf::get('order.delivery'),
            'statuses' => conf::get('order.statuses'),
        ), $this->tpl_folder.'/print' );
        die;
    }
    
}