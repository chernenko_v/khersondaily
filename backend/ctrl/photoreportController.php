<?php

class photoreportController
{

    public $tpl_folder = 'photoreport';
    public $tablename = 'photoreport';
    public $tree_tablename = 'news_tree';
    public $comm_table = 'photoreport_comments';
    public $image_table = 'photoreport_images';
    public $limit;

    function __construct()
    {
        conf::set('h1', 'Фоторепортаж');
        conf::set('title', 'Фоторепортаж');
        conf::bread('/backend/' . Route::controller() . '/index', conf::get('h1'));
        $this->limit = conf::get('limit_backend');
    }

    function indexAction()
    {
        $date_s = NULL;
        $date_po = NULL;
        $status = NULL;
        if (Arr::get($_GET, 'date_s')) {
            $date_s = strtotime(Arr::get($_GET, 'date_s'));
        }
        if (Arr::get($_GET, 'date_po')) {
            $date_po = strtotime(Arr::get($_GET, 'date_po'));
        }
        if (isset($_GET['status'])) {
            $status = Arr::get($_GET, 'status', 1);
        }

        $page = (int)Route::param('page');
        $count = Builder::factory($this->tablename);
        if ($date_s) {
            $count->where('date', '>=', $date_s);
        }
        if ($date_po) {
            $count->where('date', '<=', $date_po + 24 * 60 * 60 - 1);
        }
        if ($status !== NULL) {
            $count->where('status', '=', $status);
        }
        $count = $count->count_all();
        $sql = 'SELECT     photoreport.text,
                                    photoreport.views,
                                    photoreport.alias,
                                    photoreport.id,
                                    photoreport.images,
                                    photoreport.date,
                                    photoreport.title,
                                    photoreport.name,
                                    photoreport.h1,
                                    photoreport.keywords,
                                    photoreport.description,
                                    photoreport.status,
                                    news_tree.name as rubric
                        FROM photoreport
                        LEFT JOIN news_tree
                        ON news_tree.id = photoreport.parent_id
                        ORDER BY date DESC
                        LIMIT ' . $this->limit . '
                        OFFSET ' . ($page - 1) * $this->limit;

        $result = mysql::query($sql);

        $pager = Backend::pager($count, $this->limit, $page);

        conf::set('toolbar', Widgets::get('Toolbar/List', array('add' => 1, 'delete' => 1)));

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder . '/index');
    }

    function newAction()
    {
        if ($_POST) {
            /**
             *  Загрузка zip архивов
             */
            if ($_FILES["zip_file"]["name"]) {
                $filename = $_FILES["zip_file"]["name"];
                $source = $_FILES["zip_file"]["tmp_name"];
                $type = $_FILES["zip_file"]["type"];
                $name = explode(".", $filename);
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                foreach ($accepted_types as $mime_type) {
                    if ($mime_type == $type) {
                        $okay = true;
                        break;
                    }
                }
                $continue = strtolower($name[1]) == 'zip' ? true : false;
                if (!$continue) {
                    $message = "The file you are trying to upload is not a .zip file. Please try again.";
                }

                $target_path = HOST . '/images/photoreport/'.$name;
                if (move_uploaded_file($source, $target_path)) {
                    $zip = new ZipArchive();
                    $x = $zip->open($target_path);
                    if ($x === true) {
                        for ($i = 0; $i < $zip->numFiles; $i++) {
                            $single_name = $zip->getNameIndex($i);
                            $ext = end(explode('.', $single_name));
                            $single_name = md5($single_name . '_photoreport' . time()) . '.' . $ext;
                            $zip->renameIndex($i, $single_name);
                            $filenames[] = $zip->getNameIndex($i);
                        }
                        $zip->close();
                        $zip1 = new ZipArchive();
                        $res = $zip1->open($target_path);
                        if ($res) {
                            $zip1->extractTo(HOST . '/images/photoreport/original'); // change this to the correct site path
                            foreach ($filenames as $file_name) {
                                $filename = $file_name;

                                $big = HOST . '/images/photoreport/big/' . $filename;
                                $small = HOST . '/images/photoreport/small/' . $filename;
                                $original = HOST . '/images/photoreport/original/' . $filename;
                                $bigwater_mark = HOST . '/images/photoreport/bigwatermark/' . $filename;
                                $original_watermark = HOST . '/images/photoreport/originalwatermark/' . $filename;
                                $watermark = Image::factory(HOST . '/pic/logo.png');

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);

                                $image->save($original);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->resize(200, 160, Image::INVERSE);
                                $image->crop(200, 160);
                                $image->save($small);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->resize(678, 520, Image::INVERSE);
                                $image->crop(678, 520, Image::INVERSE);
                                $image->save($big);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->resize(678, 520, Image::INVERSE);
                                $image->crop(678, 520, Image::INVERSE);
                                $image->watermark($watermark, 0, NULL, $opacity = 75);
                                $image->save($bigwater_mark);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->watermark($watermark, 0, NULL, $opacity = 75);
                                $image->save($original_watermark);

                                DB::insert($this->image_table, array('image', 'photoreport_id', 'date', 'created_at', 'updated_at', 'sort'))->values(array($file_name, $_POST['id'], time(), time(), time(), ''))->execute();
                            }
                            $zip1->close();
                            unlink($target_path);
                            $message = "Your .zip file was uploaded and unpacked.";
                        }
                    }
                    $message = "Problem with rename files";
                } else {
                    $message = "There was a problem with the upload. Please try again.";
                }
            }
            // Конец загрузки архива
            $_POST['FORM']['h1'] = ($_POST['FORM']['h1'] != null) ? $_POST['FORM']['h1'] : $_POST['FORM']['name'];
            $_POST['FORM']['name_rss'] = ($_POST['FORM']['name_rss'] != null) ? $_POST['FORM']['name_rss'] : $_POST['FORM']['name'];
            $_POST['FORM']['title'] = ($_POST['FORM']['title'] != null) ? $_POST['FORM']['title'] : $_POST['FORM']['name'];
            $post = $_POST['FORM'];

            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['push_sent'] = Arr::get($_POST, 'push_sent', 0);
            $post['read'] = Arr::get($_POST, 'read', 0);
            $post['highlights'] = Arr::get($_POST, 'highlights', 0);
            $post['rss'] = Arr::get($_POST, 'rss', 0);
            //$post['show_image'] = Arr::get( $_POST, 'show_image', 0 );
            $post['date'] = strtotime(Arr::get($post, 'date'));
            $post['parent_id'] = Arr::get($_POST, 'parent_id', 0);
            $post['watermark_image'] = Arr::get($_POST, 'watermark_image', 0);
            $post['show_photoreport'] = Arr::get($_POST, 'show_photoreport', 0);
            if (!trim(Arr::get($post, 'name'))) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if (!trim(Arr::get($post, 'alias'))) {
                Message::GetMessage(0, 'Поле "Алиас" не может быть пустым!');
            } else if (!Arr::get($post, 'date')) {
                Message::GetMessage(0, 'Поле "Дата" не может быть пустым!');
            } else {
                $count = Builder::factory($this->tablename)->where('alias', '=', Arr::get($post, 'alias'))->where('id', '!=', Arr::get($_POST, 'id'))->count_all();
                if ($count) {
                    $post['alias'] = Arr::get($post, 'alias') . rand(1000, 9999);
                }
                $res = Builder::factory($this->tablename)
                    ->data($post)
                    ->where('id', '=', Arr::get($_POST, 'id'))
                    ->edit();
                if ($res) {
                    if ($_POST['push_sent'] == 1) {
                        $keys = DB::select('id', 'reg_id')->from('push_users')->where('subscribe', '=', 1)->as_object()->execute();
                        $news_info = DB::select('image')->from('photoreport_images')->where('photoreport_id', '=', Route::param('id'))->where('main', '=', 1)->as_object()->execute();


                        $push = $_POST['PUSH'];
                        $push['title'] = ($push['title'] != null) ? $push['title'] : $_POST['FORM']['name'];
                        $push['body'] = ($push['body'] != null) ? $push['body'] : $_POST['FORM']['text_short'];
                        //$push['tag'] = ($push['tag'] != null) ? $push['tag'] : $_POST['FORM']['name'];
                        $push['url'] = 'https://khersondaily.com/photoreport/' . $_POST['FORM']['alias'];
                        $push['lifetime'] = ($push['lifetime'] != null) ? strtotime($push['lifetime']) : null;
                        $push['news_id'] = Route::param('id');

                        if ($news_info[0]->image) {
                            $push['icon'] = 'https://khersondaily.com/images/photoreport/small/' . $news_info[0]->image;
                        } else {
                            $push['icon'] = 'https://khersondaily.com/pic/no-image.png';
                        }

                        $insert = Push::insertPushDB($push);

                        foreach ($keys as $item) {
                            DB::insert('push_cron', array('user_id', 'mas_id', 'reg_id'))->values(array($item->id, $insert, $item->reg_id))->execute();
                        }
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);
        } else {
            $sql = 'SELECT  `photoreport`.*, 
                            `push_message`.`title`, 
                            `push_message`.`body`, 
                            `push_message`.`url`, 
                            `push_message`.`tag`, 
                            `push_message`.`lifetime` 
                    FROM `photoreport` 
                    LEFT JOIN `push_message` ON `push_message`.`news_id` = photoreport.id
                    WHERE photoreport.id=' . (int)Route::param('id');
            $result = mysql::query_one($sql);


            $page = (int)Route::param('page');
            $count = Builder::factory('photoreport_images')
                ->where('photoreport_id', '=', $result->id)
                ->count_all();

            $images = Builder::factory('photoreport_images')
                ->where('photoreport_id', '=', $result->id)
                ->order_by('sort')
                ->limit($this->limit, ($page - 1) * $this->limit)
                ->find_all();

            $pager = Backend::pager($count, $this->limit, $page);

            $show_images = support::tpl(array('images' => $images), $this->tpl_folder . '/uploaded_images');
        }
        conf::set('toolbar', Widgets::get('Toolbar/Edit'));
        conf::set('h1', 'Редактирование');
        conf::set('title', 'Редактирование фоторепортажа');
        conf::bread('/backend/' . Route::controller() . '/new/id/' . (int)Route::param('id'), conf::get('h1'));
        $tree = Builder::factory($this->tree_tablename)->find_all();
        return support::tpl(
            array(
                'obj' => $result,
                'tree' => $tree,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'show_images' => $show_images,
                'pager' => $pager
            ), $this->tpl_folder . '/form');
    }

    function addAction()
    {
        if ($_POST) {

            /**
             *  Загрузка zip архивов
             */
            if ($_FILES["zip_file"]["name"]) {
                $filename = $_FILES["zip_file"]["name"];
                $source = $_FILES["zip_file"]["tmp_name"];
                $type = $_FILES["zip_file"]["type"];
                $name = explode(".", $filename);
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                foreach ($accepted_types as $mime_type) {
                    if ($mime_type == $type) {
                        $okay = true;
                        break;
                    }
                }
                $continue = strtolower($name[1]) == 'zip' ? true : false;
                if (!$continue) {
                    $message = "The file you are trying to upload is not a .zip file. Please try again.";
                }
                $target_path = HOST . '\archive';
                if (move_uploaded_file($source, $target_path)) {
                    $zip = new ZipArchive();
                    $x = $zip->open($target_path);
                    if ($x === true) {
                        for ($i = 0; $i < $zip->numFiles; $i++) {
                            $single_name = $zip->getNameIndex($i);
                            $ext = end(explode('.', $single_name));
                            $single_name = md5($single_name . '_photoreport' . time()) . '.' . $ext;
                            $zip->renameIndex($i, $single_name);
                            $filenames[] = $zip->getNameIndex($i);
                        }
                        $zip->close();
                        $zip1 = new ZipArchive();
                        $res = $zip1->open($target_path);
                        if ($res) {
                            $zip1->extractTo(HOST . '/images/photoreport/original'); // change this to the correct site path
                            foreach ($filenames as $file_name) {
                                $filename = $file_name;

                                $big = HOST . '/images/photoreport/big/' . $filename;
                                $small = HOST . '/images/photoreport/small/' . $filename;
                                $original = HOST . '/images/photoreport/original/' . $filename;
                                $bigwater_mark = HOST . '/images/photoreport/bigwatermark/' . $filename;
                                $original_watermark = HOST . '/images/photoreport/originalwatermark/' . $filename;
                                $watermark = Image::factory(HOST . '/pic/logo.png');

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);

                                $image->save($original);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->resize(200, 160, Image::INVERSE);
                                $image->crop(200, 160);
                                $image->save($small);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->resize(678, 520, Image::INVERSE);
                                $image->crop(678, 520, Image::INVERSE);
                                $image->save($big);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->resize(678, 520, Image::INVERSE);
                                $image->crop(678, 520, Image::INVERSE);
                                $image->watermark($watermark, 0, NULL, $opacity = 75);
                                $image->save($bigwater_mark);

                                $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                                $image->watermark($watermark, 0, NULL, $opacity = 75);
                                $image->save($original_watermark);

                                DB::insert($this->image_table, array('image', 'photoreport_id', 'date', 'created_at', 'updated_at', 'sort'))->values(array($file_name, $_POST['id'], date(),  time(),  time(), ''))->execute();
                            }
                            $zip1->close();
                            unlink($target_path);
                            $message = "Your .zip file was uploaded and unpacked.";
                        }
                    }
                    $message = "Problem with renema files";
                } else {
                    $message = "There was a problem with the upload. Please try again.";
                }
            }

            /**
             * $POST rss,title,h1 like name
             */
            $_POST['FORM']['h1'] = ($_POST['FORM']['h1'] != null) ? $_POST['FORM']['h1'] : $_POST['FORM']['name'];
            $_POST['FORM']['name_rss'] = ($_POST['FORM']['name_rss'] != null) ? $_POST['FORM']['name_rss'] : $_POST['FORM']['name'];
            $_POST['FORM']['title'] = ($_POST['FORM']['title'] != null) ? $_POST['FORM']['title'] : $_POST['FORM']['name'];
            $post = $_POST['FORM'];
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['push_sent'] = Arr::get($_POST, 'push_sent', 0);
            $post['read'] = Arr::get($_POST, 'read', 0);
            $post['highlights'] = Arr::get($_POST, 'highlights', 0);
            $post['rss'] = Arr::get($_POST, 'rss', 0);
            $post['show_image'] = Arr::get($_POST, 'show_image', 0);
            $post['date'] = strtotime(Arr::get($post, 'date'));
            $post['parent_id'] = Arr::get($_POST, 'parent_id');
            $post['watermark_image'] = Arr::get($_POST, 'watermark_image', 0);
            $post['show_photoreport'] = Arr::get($_POST, 'show_photoreport', 0);
            if (!trim(Arr::get($post, 'name'))) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if (!trim(Arr::get($post, 'alias'))) {
                Message::GetMessage(0, 'Поле "Алиас" не может быть пустым!');
            } else if (!Arr::get($post, 'date')) {
                Message::GetMessage(0, 'Поле "Дата" не может быть пустым!');
            } else if (!Arr::get($post, 'parent_id')) {
                Message::GetMessage(0, 'Поле "Рубрика" не может быть пустым!');
            } else {
                $count = Builder::factory($this->tablename)->where('alias', '=', Arr::get($post, 'alias'))->count_all();
                if ($count) {
                    $post['alias'] = Arr::get($post, 'alias') . rand(1000, 9999);
                }
                $res = Builder::factory($this->tablename)->data($post)->add();
                if ($res) {
                    if (Arr::get($_FILES['file'], 'name')) {
                        $id = Builder::factory($this->tablename)->get_last_id();

                        $ext = end(explode('.', $_FILES['file']['name']));
                        $filename = md5($_FILES['file']['name'] . '_news' . time()) . '.' . $ext;
                        $big = HOST . '/images/photoreport/big/' . $filename;
                        $small = HOST . '/images/photoreport/small/' . $filename;
                        $original = HOST . '/images/photoreport/original/' . $filename;
                        $bigwater_mark = HOST . '/images/photoreport/bigwatermark/' . $filename;
                        $original_watermark = HOST . '/images/photoreport/originalwatermark/' . $filename;

                        $watermark = Image::factory(HOST . '/pic/logo.png');

                        $image = Image::factory($_FILES['file']['tmp_name']);

                        $image->save($original);

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(200, 160, Image::INVERSE);
                        $image->crop(200, 160);
                        $image->save($small);

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(678, 520, Image::INVERSE);
                        $image->crop(678, 520, Image::INVERSE);
                        $image->save($big);

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(678, 520, Image::INVERSE);
                        $image->crop(678, 520, Image::INVERSE);
                        $image->watermark($watermark, 0, NULL, $opacity = 75);
                        $image->save($bigwater_mark);

                        $image = Image::factory(HOST . '/images/photoreport/original/' . $filename);
                        $image->watermark($watermark, 0, NULL, $opacity = 75);
                        $image->save($original_watermark);

                        Builder::factory('photoreport_images')->data(array(
                            'image' => $filename,
                            'main' => 1,
                            'photoreport_id' => $id,
                            'date' => time(),
                            'created_at' => time()
                        ))->add();
                    }
                    if ($_POST['push_sent'] == 1) {
                        $keys = DB::select('id', 'reg_id')->from('push_users')->where('subscribe', '=', 1)->as_object()->execute();
                        $news_info = DB::select('image')->from('photoreport_images')->where('photoreport_id', '=', $id)->where('main', '=', 1)->as_object()->execute();

                        $push = $_POST['PUSH'];
                        $push['title'] = ($push['title'] != null) ? $push['title'] : $_POST['FORM']['name'];
                        $push['body'] = ($push['body'] != null) ? $push['body'] : $_POST['FORM']['text_short'];
                        $push['tag'] = ($push['tag'] != null) ? $push['tag'] : $_POST['FORM']['name'];
                        $push['url'] = 'https://khersondaily.com/photoreport/' . $_POST['FORM']['alias'];
                        $push['lifetime'] = ($push['lifetime'] != null) ? strtotime($push['lifetime']) : null;
                        $push['news_id'] = $id;

                        if ($news_info[0]->image) {
                            $push['icon'] = 'https://khersondaily.com/images/photoreport/small/' . $news_info[0]->image;
                        } else {
                            $push['icon'] = 'https://khersondaily.com/pic/no-image.png';
                        }
                        $insert = Push::insertPushDB($push);

                        foreach ($keys as $item) {
                            DB::insert('push_cron', array('user_id', 'mas_id', 'reg_id'))->values(array($item->id, $insert, $item->reg_id))->execute();
                        }
                    }
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result = Arr::to_object($post);
        } else {
            $result = array();
        }

        conf::set('toolbar', Widgets::get('Toolbar/Edit'));

        conf::set('h1', 'Добавление');
        conf::set('title', 'Добавление фоторепортажа');
        conf::bread('/backend/' . Route::controller() . '/add', conf::get('h1'));
        $tree = Builder::factory($this->tree_tablename)->find_all();

        return support::tpl(
            array(
                'obj' => $result,
                'tree' => $tree,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder . '/form');
    }

    function deleteAction()
    {
        $id = (int)Route::param('id');
        if (!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/' . Route::controller() . '/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if (!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/' . Route::controller() . '/index');
        }

        @unlink(HOST . '/images/photoreport/small/' . $page->images);
        @unlink(HOST . '/images/photoreport/big/' . $page->images);
        @unlink(HOST . '/images/photoreport/original/' . $page->images);
        @unlink(HOST . '/images/photoreport/bigwatermark/' . $page->images);
        @unlink(HOST . '/images/photoreport/originalwatermark/' . $page->images);

        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/' . Route::controller() . '/index');
    }

    function deleteimageAction()
    {
        $id = (int)Route::param('id');
        if (!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/' . Route::controller() . '/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if (!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/' . Route::controller() . '/index');
        }

        @unlink(HOST . '/images/photoreport/small/' . $page->images);
        @unlink(HOST . '/images/photoreport/big/' . $page->images);
        @unlink(HOST . '/images/photoreport/original/' . $page->images);
        @unlink(HOST . '/images/photoreport/bigwatermark/' . $page->images);
        @unlink(HOST . '/images/photoreport/originalwatermark/' . $page->images);
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/' . Route::controller() . '/new/id/' . $id);
    }

    function treeAction()
    {
        conf::bread('/backend/' . Route::controller() . '/tree', 'Рубрики');
        $result = Builder::factory('news_tree')
            ->order_by('sort')
            ->find_all();

        return support::tpl(
            array(
                'result' => $result,
                'count' => 7,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder . '/tree');
    }

    function treenewAction()
    {
        if ($_POST) {
            $post = $_POST['FORM'];

            if (!trim(Arr::get($post, 'name'))) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else {
                $res = Builder::factory('news_tree')
                    ->data($post)
                    ->where('id', '=', Arr::get($_POST, 'id'))
                    ->edit();
                if ($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);
        } else {
            $result = Builder::factory('news_tree')
                ->where('id', '=', (int)Route::param('id'))
                ->find();


        }

        conf::set('toolbar', Widgets::get('Toolbar/Edit'));

        conf::set('h1', 'Редактирование ');
        conf::set('title', 'Редактирование рубрик');
        conf::bread('/backend/' . Route::controller() . '/treenew/id/' . (int)Route::param('id'), conf::get('h1'));


        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder . '/treeform');
    }

    function commentsAction()
    {
        conf::set('h1', 'Комментарии');
        conf::set('title', 'Комментарии');
        conf::bread('/backend/' . Route::controller() . '/comments', 'Комментарии');
        $page = (int)Route::param('page');
        $count = Builder::factory($this->comm_table)->count_all();

        $result = 'SELECT   photoreport_comments.text,
                            photoreport_comments.answer,
                            photoreport_comments.status,
                            photoreport_comments.date,
                            photoreport_comments.user_name,
                            photoreport_comments.id,
                            users.name,
                            photoreport.name as photoreport
                    FROM    photoreport_comments
                    LEFT JOIN users ON users.id = photoreport_comments.user_id
                    LEFT JOIN photoreport ON photoreport.id = photoreport_comments.photoreport_id
                    ORDER BY date DESC
                    LIMIT ' . $this->limit . ' OFFSET ' . (($page - 1) * $this->limit);

        $mysql = mysql::query($result);
        $pager = Backend::pager($count, $this->limit, $page);

        conf::set('toolbar', Widgets::get('Toolbar/List', array('add' => 1, 'delete' => 1)));
        return support::tpl(
            array(
                'result' => $mysql,
                'count' => $count,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->comm_table,
                'pager' => $pager
            ), $this->tpl_folder . '/comments');
    }

    function commEditAction()
    {
        conf::set('h1', 'Комментарии');
        conf::set('title', 'Комментарии');
        conf::bread('/backend/' . Route::controller() . '/comments', 'Комментарии');
        conf::bread('/backend/' . Route::controller() . '/commEdit', 'Редактировать комментарий');
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get($_POST, 'status', 0);
            $post['answer_date'] = time();
            if (!trim(Arr::get($post, 'text'))) {
                Message::GetMessage(0, 'Поле "Комментарий" не может быть пустым!');
            } else {
                $res = Builder::factory($this->comm_table)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if ($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->comm_table)
                ->where('id', '=', (int)Route::param('id'))
                ->find();
        }

        conf::set('toolbar', Widgets::get('Toolbar/Edit'));
        return support::tpl(
            array(
                'obj' => $result,
                'count' => Builder::factory('photoreport_comments')->count_all(),
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->comm_table,
            ), $this->tpl_folder . '/form_comm');
    }

    function commDelAction()
    {
        $id = (int)Route::param('id');
        if (!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/' . Route::controller() . '/index');
        }
        $page = Builder::factory($this->comm_table)->where('id', '=', $id)->find();
        if (!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/' . Route::controller() . '/index');
        }

        Builder::factory($this->comm_table)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/' . Route::controller() . '/comments');
    }
}