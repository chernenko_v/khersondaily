<?php

class pusherController {

    public $tpl_folder = 'pushes';
    public $tablename  = 'push_message';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Web PUSH' );
        conf::set( 'title', 'Web PUSH' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function sendAction() {
        $keys = DB::select('id','reg_id')->from('push_users')->where('subscribe', '=', 1)->as_object()->execute();

        if ($_POST) {
            $data = $_POST['FORM'];
            $data['lifetime'] = strtotime($_POST['lifetime']);
            if( !trim(Arr::get($data, 'title')) ) {
                Message::GetMessage(0, 'Заголовок push-уведомления не может быть пустой!');
            } else if(!trim(Arr::get($data, 'body'))) {
                Message::GetMessage(0, 'Текст push-уведомления не может быть пустым!');
            } else if(!Arr::get($data, 'url')) {
                Message::GetMessage(0, 'url перехода не может быть пустым!');
            }else {
                /* insert into DB */
                $insert = Push::insertPushDB($data);

                if( Arr::get( $_FILES['icon'], 'name' ) ) {
                    $ext = end( explode('.', $_FILES['icon']['name']) );
                    $filename = md5($_FILES['icon']['name'].'_icon'.time()).'.'.$ext;
                    $push = HOST . '/images/push/push/' . $filename;
                    $original = HOST . '/images/push/original/' . $filename;

                    $image = Image::factory($_FILES['icon']['tmp_name']);
                    $image->save($original);

                    $image = Image::factory($_FILES['icon']['tmp_name']);
                    $image->resize(430, 430, Image::INVERSE);
                    $image->crop(430, 430, Image::INVERSE);
                    $image->save($push);

                    Builder::factory($this->tablename)->data(array('icon' => 'https://khersondaily.com/images/push/push/'.$filename))->where('id', '=', $insert)->edit();
                }
                /* send push */
                foreach ($keys as $item) {
                    DB::insert('push_cron', array('user_id', 'mas_id','reg_id'))->values(array($item->id, $insert,$item->reg_id))->execute();
                }
                if ($insert) {
                    Message::GetMessage(1, 'Вы успешно отправили PUSH сообщение!');
                    location('backend/' . Route::controller() . '/index');
                } else {
                    Message::GetMessage(0, 'Не удалось отправить PUSH сообщение!');
                }
            }
        }
        conf::set( 'toolbar', Widgets::get( 'Toolbar/Subscribe' ) );
        return support::tpl(array(), $this->tpl_folder.'/form');
    }
    
    function indexAction () {
        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename)->count_all();
        $sql = 'SELECT *
                        FROM push_message
                        ORDER BY id DESC
                        LIMIT '.$this->limit.'
                        OFFSET '.($page - 1) * $this->limit;
        $result = mysql::query($sql);

        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array('delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => $count,
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['lifetime'] = strtotime($_POST['lifetime']);
            if( !trim(Arr::get($post, 'title')) ) {
                Message::GetMessage(0, 'Заголовок push-уведомления не может быть пустой!');
            } else if(!trim(Arr::get($post, 'body'))) {
                Message::GetMessage(0, 'Текст push-уведомления не может быть пустым!');
            } else if(!Arr::get($post, 'url')) {
                Message::GetMessage(0, 'url перехода не может быть пустым!');
            }else {
                $res = Builder::factory($this->tablename)
                            ->data($post)
                            ->where('id', '=', Arr::get($_POST, 'id'))
                            ->edit();
                if($res) {
                    //Model::uploadImage($insert, 'icon', 'icon');
                    if( Arr::get( $_FILES['icon'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['icon']['name']) );
                        $filename = md5($_FILES['icon']['name'].'_icon'.time()).'.'.$ext;
                        $push = HOST . '/images/push/push/' . $filename;
                        $original = HOST . '/images/push/original/' . $filename;

                        $image = Image::factory($_FILES['icon']['tmp_name']);
                        $image->save($original);

                        $image = Image::factory($_FILES['icon']['tmp_name']);
                        $image->resize(430, 430, Image::INVERSE);
                        $image->crop(430, 430, Image::INVERSE);
                        $image->save($push);

                        Builder::factory($this->tablename)->data(array('icon' => 'https://khersondaily.com/images/push/push/'.$filename))->where('id', '=', Arr::get($_POST, 'id'))->edit();
                    }

                    $keys = DB::select('id','reg_id')->from('push_users')->where('subscribe', '=', 1)->as_object()->execute();

                    foreach ($keys as $item) {
                        DB::insert('push_cron', array('user_id', 'mas_id','reg_id'))->values(array($item->id, (int) Route::param('id'),$item->reg_id))->execute();
                    }


                    Message::GetMessage(1, 'Вы успешно изменили PUSH сообщение!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменили PUSH сообщение!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)
                                ->where('id', '=', (int) Route::param('id'))
                                ->find();

            
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование' );
        conf::bread( '/backend/'.Route::controller().'/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
            ), $this->tpl_folder.'/form');
    }
    
    
    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/push/push/' . $page->images );
        @unlink( HOST . '/images/push/original/' . $page->images );
        
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }

    function deleteimageAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }

        @unlink( HOST . '/images/push/push/' . $page->icon );
        @unlink( HOST . '/images/push/original/' . $page->icon );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
    }

    function usersAction () {
        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename)->count_all();
        $sql = 'SELECT   push_users.*, 
                            users.email, 
                            push_config.name as group_name
                    FROM    push_users
                    LEFT JOIN users ON users.id = push_users.user_id
                    LEFT JOIN push_config ON push_config.id = push_users.group_user
                    ORDER BY id DESC
                    LIMIT '.$this->limit.' OFFSET '.(($page - 1) * $this->limit);
        $result = mysql::query($sql);
        $pager = Backend::pager( $count, $this->limit, $page );
        

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => $count,
                'pager' => $pager,
            ), $this->tpl_folder.'/users');
    }
    
}