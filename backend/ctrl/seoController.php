<?php

class seoController {
	
    public $tpl_folder = 'seo';
	public $tablename = 'seo';	

	function newAction() {
        if ($_POST) {
            $post = Arr::get( $_POST, 'FORM' );
            $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
            if($res) {
                Message::GetMessage(1, 'Вы успешно изменили данные!');
                location($_SERVER['HTTP_REFERER']);
            } else {
                Message::GetMessage(0, 'Не удалось изменить данные!');
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }
        if(!$result) {
            return conf::error();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
        conf::set( 'h1', $result->name );
        conf::set( 'title', 'Редактирование шаблона' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(array(
            'obj' => $result,
            'tpl_folder' => $this->tpl_folder
        ),$this->tpl_folder.'/form');
    }
	
} 