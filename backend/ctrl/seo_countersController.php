<?php

class seo_countersController {

    public $tpl_folder = 'seo_counters';
    public $tablename = 'seo_counters';

    function __construct() {
        conf::set( 'h1', 'Счетчики' );
        conf::set( 'title', 'Счетчики' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
    }

    function indexAction() {
        $result = Builder::factory($this->tablename)->order_by('id', 'DESC')->find_all();
        conf::set( 'filter', Widgets::get( 'Filter/Pages' ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );
        return support::tpl(array(
            'result' => $result,
            'tpl_folder' => $this->tpl_folder,
            'tablename' => $this->tablename
        ),$this->tpl_folder.'/index');
    }
    
    function newAction() {
        
        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование счетчика' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        if ($_POST) {
            $post = Arr::get( $_POST, 'FORM' );
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !Arr::get($post, 'name') ) {
                Message::GetMessage(0, 'Не все поля заполнены верно!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }
        return support::tpl(array(
            'obj' => $result,
            'tpl_folder' => $this->tpl_folder
        ),$this->tpl_folder.'/form');
    }
    
    function addAction() {

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление счетчика' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        if ($_POST) {
            $post = Arr::get( $_POST, 'FORM' );
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !Arr::get($post, 'name') ) {
                Message::GetMessage(0, 'Не все поля заполнены верно!');
            } else {
                $res = Builder::factory($this->tablename)->data($_POST['FORM'])->add();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);            
        } else {
            $result = array();
        }           
    
        return support::tpl(
            array(              
                'tpl_folder' => $this->tpl_folder,
                'result'     => $result,
            ),$this->tpl_folder.'/form');
    }
    
    function deleteAction() {
        $id = (int) Route::param('id');
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные успешно удалены!');
        location('backend/'.Route::controller().'/index');
    }
    
} 