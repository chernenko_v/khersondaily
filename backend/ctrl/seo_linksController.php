<?php

class seo_linksController extends Controller {

    public $tpl_folder = 'seo_links';
    public $tablename  = 'seo_links';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Теги для ссылок' );
        conf::set( 'title', 'Теги для ссылок' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }
    
    function indexAction() {
        $_GET['page'] = Arr::get($_GET, 'page', 1);
        $page   = (int) Arr::get($_GET, 'page', 1);
        $count  = Builder::factory($this->tablename)->count_all();
        $pager = Backend::pager( $count, $this->limit, $page );
        $result  = Builder::factory($this->tablename)->order_by('id', 'DESC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        conf::set( 'filter', Widgets::get( 'Filter/Pages' ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        return support::tpl(array(
            'result' => $result,
            'count' => $count,
            'tpl_folder' => $this->tpl_folder,
            'tablename' => $this->tablename,
            'pager' => $pager,
        ), $this->tpl_folder . '/index');
    }

    function addAction() {
        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление тегов' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        if ($_POST) {
            $post = Arr::get( $_POST, 'FORM' );
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !Arr::get( $post, 'name' ) ) {
                Message::GetMessage(0, 'Не все поля заполнены верно!');
            } else {
                $post['link'] = href(Arr::get($post, 'link'));
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location('backend/'.Route::controller().'/new/id/'.Builder::factory($this->tablename)->get_last_id());
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
        
        return support::tpl(
            array(
                'tpl_folder' => $this->tpl_folder,
                'obj' => $result,
            ),$this->tpl_folder.'/form');
    }

    function newAction() {  
        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование тегов' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.Route::param('id'), conf::get( 'h1' ) );

        if ($_POST) {
            $post = Arr::get( $_POST, 'FORM' );
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !Arr::get( $post, 'name' ) ) {
                Message::GetMessage(0, 'Не все поля заполнены верно!');
            } else {
                $post['link'] = href(Arr::get($post, 'link'));
                $res  = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно отредактировали данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось отредактировать данные!');
                }
            }
            $result = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', Route::param('id'))->find();
        }
        
        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        return support::tpl(array(
            'tpl_folder' => $this->tpl_folder,
            'obj' => $result,
        ), $this->tpl_folder . '/form');
        conf::set( 'content', $content );
    }

    function deleteAction() {
        $id   = (int) Route::param('id');
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные успешно удалены!');
        location('backend/'.Route::controller().'/index');
    }

}