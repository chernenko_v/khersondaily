<?php

class sliderController {

    public $tpl_folder = 'slider';
    public $tablename  = 'slider';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Слайдшоу' );
        conf::set( 'title', 'Слайдшоу' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename)->count_all();
        $result = Builder::factory($this->tablename)
                    ->order_by('sort')
                    ->limit($this->limit, ($page - 1) * $this->limit)
                    ->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'filter', Widgets::get( 'Filter/Pages' ) );
        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );

            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_news'.time()).'.'.$ext;
                        $big = HOST . '/images/slider/big/' . $filename;
                        $small = HOST . '/images/slider/small/' . $filename;

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(SLIDER_MIN_WIDTH, SLIDER_MIN_HEIGHT, Image::INVERSE);
                        $image->crop(SLIDER_MIN_WIDTH, SLIDER_MIN_HEIGHT);
                        $image->save($small);

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(SLIDER_WIDTH, SLIDER_HEIGHT, Image::INVERSE);
                        $image->crop(SLIDER_WIDTH, SLIDER_HEIGHT);
                        $image->save($big);

                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', Arr::get($_POST, 'id'))->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование слайда' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }
    
    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $id = Builder::factory($this->tablename)->get_last_id();

                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_news'.time()).'.'.$ext;
                        $big = HOST . '/images/slider/big/' . $filename;
                        $small = HOST . '/images/slider/small/' . $filename;

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(SLIDER_MIN_WIDTH, SLIDER_MIN_HEIGHT, Image::INVERSE);
                        $image->crop(SLIDER_MIN_WIDTH, SLIDER_MIN_HEIGHT);
                        $image->save($small);

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(SLIDER_WIDTH, SLIDER_HEIGHT, Image::INVERSE);
                        $image->crop(SLIDER_WIDTH, SLIDER_HEIGHT);
                        $image->save($big);

                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', $id)->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }

            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление нового слайда' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/slider/small/' . $page->image );
        @unlink( HOST . '/images/slider/big/' . $page->image );

        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }

    function deleteimageAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/slider/small/' . $page->image );
        @unlink( HOST . '/images/slider/big/' . $page->image );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
    }
}