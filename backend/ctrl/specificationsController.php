<?php

class specificationsController {

    public $tpl_folder = 'specifications';
    public $tablename  = 'specifications';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Характеристики' );
        conf::set( 'title', 'Характеристики' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $date_s = NULL;
        if ( isset($_GET['status']) ) { $status = Arr::get($_GET, 'status', 1); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count = $count->count_all();
        $result = Builder::factory($this->tablename);
        if( $status !== NULL ) { $result->where( 'status', '=', $status ); }
        $result = $result->order_by('name', 'ASC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        $t = Builder::factory('specifications_types')->order_by('name')->find_all();
        $types = array();
        foreach( $t AS $_t ) {
            $types[$_t->id] = $_t->name;
        }

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
                'types' => $types,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'alias'))) {
                Message::GetMessage(0, 'Поле "Алиас" не может быть пустым!');
            } else {
                $count = Builder::factory($this->tablename)->where('alias', '=', Arr::get($post, 'alias'))->where('id', '!=', Arr::get($_POST, 'id'))->count_all();
                if($count) { $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999); }
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование характеристики' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'list' => Builder::factory('specifications_values')->where('specification_id', '=', Route::param('id'))->order_by('name')->find_all(),
            ), $this->tpl_folder.'/edit');
    }
    
    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Наименование" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'alias'))) {
                Message::GetMessage(0, 'Поле "Алиас" не может быть пустым!');
            } else {
                $count = Builder::factory($this->tablename)->where('alias', '=', Arr::get($post, 'alias'))->count_all();
                if($count) { $post['alias'] = Arr::get($post, 'alias').rand(1000, 9999); }
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    $id = Builder::factory($this->tablename)->get_last_id();
                    Message::GetMessage(1, 'Вы успешно добавили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавить данные!');
                }
            }
            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление характеристики' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'types' => Builder::factory('specifications_types')->order_by('name')->find_all(),
            ), $this->tpl_folder.'/add');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }

}