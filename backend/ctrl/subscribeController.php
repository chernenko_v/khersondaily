<?php

class subscribeController {

    public $tpl_folder = 'subscribe';
    public $tablename  = 'subscribe_mails';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Рассылка писем' );
        conf::set( 'title', 'Рассылка писем' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $date_s = NULL; $date_po = NULL;
        if ( Arr::get($_GET, 'date_s') ) { $date_s = strtotime( Arr::get($_GET, 'date_s') ); }
        if ( Arr::get($_GET, 'date_po') ) { $date_po = strtotime( Arr::get($_GET, 'date_po') ); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $date_s ) { $count->where( 'created_at', '>=', $date_s ); }
        if( $date_po ) { $count->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        $count = $count->count_all();
        $result = Builder::factory($this->tablename);
        if( $date_s ) { $result->where( 'created_at', '>=', $date_s ); }
        if( $date_po ) { $result->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        $result = $result->order_by('id', 'DESC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

    function sendAction () {
        $emails = array();
        $list = array();
        if ( $_POST ) {
            $post = $_POST['FORM'];
            $subscribers = Builder::factory('subscribers')->select(array( 'email', 'hash' ) )->where('status', '=', 1)->find_all();
            foreach( $subscribers AS $obj ) {
                if( filter_var( $obj->email, FILTER_VALIDATE_EMAIL ) AND !in_array( $obj->email, $emails ) ) {
                    $emails[] = $obj;
                    $list[] = $obj->email;
                }
            }
            if( !trim(Arr::get($post, 'subject')) ) {
                Message::GetMessage(0, 'Поле "Тема" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'text'))) {
                Message::GetMessage(0, 'Поле "Содержание" не может быть пустым!');
            } else if( empty( $emails ) ) {
                Message::GetMessage(0, 'Список выбраных E-Mail для рассылки пуст!');
            } else {
                $data = $post;
                $data['count_emails'] = count( $list );
                $data['emails'] = implode( ';', $list );
                Builder::factory( $this->tablename )->data( $data )->add();

                foreach( $emails AS $obj ) {
                    $link = 'http://' . Arr::get( $_SERVER, 'HTTP_HOST' ) . '/unsubscribe/hash/' . $obj->hash;
                    $from = array( '{{unsubscribe}}', '{{site}}', '{{date}}' );
                    $to = array( $link, Arr::get( $_SERVER, 'HTTP_HOST' ), date('d.m.Y') );
                    $message = str_replace( $from, $to, Arr::get( $post, 'text' ) );
                    $subject = str_replace( $from, $to, Arr::get( $post, 'subject' ) );
                    if( !conf::get('main.cron') ) {die;
                        Email::send( $subject, $message, $obj->email );
                    } else {
                        $_data = array(
                            'subject' => $subject,
                            'text' => $message,
                            'email' => $obj->email,
                        );
                        Builder::factory( conf::get('main.tableCron') )->data( $_data )->add();
                    }
                }

                Message::GetMessage(1, 'Письмо успешно разослано '.$data['count_emails'].' подписчикам!');
                location($_SERVER['HTTP_REFERER']);
            }
            $result = Arr::to_object( $post );
        } else {
            $result = Arr::to_object( array( 'subscribers' => 1 ) );
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Subscribe' ) );

        conf::set( 'h1', 'Рассылка' );
        conf::set( 'title', 'Разослать письмо' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/send');
    }
    
}