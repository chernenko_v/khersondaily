<?php

class subscribersController {

    public $tpl_folder = 'subscribers';
    public $tablename  = 'subscribers';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Подписчики' );
        conf::set( 'title', 'Подписчики' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }

    function indexAction () {
        $status = NULL;
        if ( isset($_GET['status']) ) { $status = Arr::get($_GET, 'status', 1); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename);
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count = $count->count_all();
        $result = Builder::factory($this->tablename);
        if( $status !== NULL ) { $result->where( 'status', '=', $status ); }
        $result = $result->order_by('id', 'DESC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
    }

    function newAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !filter_var( Arr::get($post, 'email'), FILTER_VALIDATE_EMAIL ) ) {
                Message::GetMessage(0, 'Поле "E-Mail" введено некорректно!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование подписчика' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }

    function addAction () {
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !filter_var( Arr::get($post, 'email'), FILTER_VALIDATE_EMAIL ) ) {
                Message::GetMessage(0, 'Поле "E-Mail" введено некорректно!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление подписчика' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
            ), $this->tpl_folder.'/form');
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }
    
}