<?php

class usersController {

	public $tpl_folder = 'users';
	public $tablename  = 'users';
    public $limit;

    function __construct() {
        conf::set( 'h1', 'Пользователи' );
        conf::set( 'title', 'Пользователи' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
    }


	function indexAction () {
	    $date_s = NULL; $date_po = NULL; $status = NULL;
        if ( Arr::get($_GET, 'date_s') ) { $date_s = strtotime( Arr::get($_GET, 'date_s') ); }
        if ( Arr::get($_GET, 'date_po') ) { $date_po = strtotime( Arr::get($_GET, 'date_po') ); }
        if ( isset($_GET['status']) ) { $status = Arr::get($_GET, 'status', 1); }

        $page = (int) Route::param('page');
        $count = Builder::factory($this->tablename)->where('role_id', '=', 1);
        if( $date_s ) { $count->where( 'created_at', '>=', $date_s ); }
        if( $date_po ) { $count->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        if( $status !== NULL ) { $count->where( 'status', '=', $status ); }
        $count = $count->count_all();
        $result = Builder::factory($this->tablename)->where('role_id', '=', 1);
        if( $date_s ) { $result->where( 'created_at', '>=', $date_s ); }
        if( $date_po ) { $result->where( 'created_at', '<=', $date_po + 24 * 60 * 60 - 1 ); }
        if( $status !== NULL ) { $result->where( 'status', '=', $status ); }
        $result = $result->order_by('created_at', 'DESC')->limit($this->limit, ($page - 1) * $this->limit)->find_all();
        $pager = Backend::pager( $count, $this->limit, $page );

        conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'delete' => 1 ) ) );

        return support::tpl(
            array(
                'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => Builder::factory($this->tablename)->where('role_id', '=', 1)->count_all(),
                'pager' => $pager,
            ), $this->tpl_folder.'/index');
		
	}


	function newAction () {
        $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->where('role_id', '=', 1)->find();
        if(!$result) {
            return conf::error();
        }
        if ($_POST) {
            $post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            $password = trim( Arr::get($_POST, 'password') );
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Имя" не может быть пустым!');
            } else if( !trim(Arr::get($post, 'email')) OR !filter_var(Arr::get($post, 'email'), FILTER_VALIDATE_EMAIL) ) {
                Message::GetMessage(0, 'Поле "E-Mail" введено некорректно!');
            } else if(Builder::factory('users')->where('email', '=', Arr::get($post, 'email'))->where('id', '!=', Arr::get($_POST, 'id'))->count_all()) {
                Message::GetMessage(0, 'Указанный E-Mail уже занят!');
            } else if($password AND mb_strlen($password, 'UTF-8') < conf::get('main.password_min_length')) {
                Message::GetMessage(0, 'Пароль должен быть не короче '.conf::get('main.password_min_length').' символов!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    if( $password ) {
                        User::factory()->update_password(Arr::get($_POST, 'id'), $password);
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result = Arr::to_object($post);
        }

        conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование пользователя' );
        conf::bread( '/backend/'.Route::controller().'/new/id/'.(int) Route::param('id'), conf::get( 'h1' ) );

        return support::tpl(
            array(
                'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'happyCount' => Builder::factory('orders_items')
                                    ->join('orders', 'LEFT')
                                    ->on('orders_items.order_id', '=', Builder::expr('orders.id'))
                                    ->on('orders.status', '=', '1')
                                    ->where('orders.user_id', '=', $result->id)
                                    ->count_all(),
                'countOrders' => Builder::factory('orders')->where('user_id', '=', $result->id)->count_all(),
                'happyMoney' => mysql::query_one('SELECT SUM(orders_items.count * orders_items.cost) AS amount
                                                  FROM orders_items
                                                  LEFT JOIN orders ON orders.id = orders_items.order_id AND orders.status = "1"
                                                  WHERE orders.user_id = "'.$result->id.'" GROUP BY orders_items.catalog_id')->amount,
            ), $this->tpl_folder.'/form');
	
	}


	function deleteAction() {
        $id   = (int) Route::param('id');
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные успешно удалены!');
        location('backend/'.Route::controller().'/index');
	}

}