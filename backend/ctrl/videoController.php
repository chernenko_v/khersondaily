<?php

class videoController {
	public $tablename = 'videos';
	public $tpl_folder = 'videos';
	public $limit;

	function __construct() {
		conf::set( 'h1', 'Видео' );
        conf::set( 'title', 'Видео' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
	}

	function indexAction(){
        $page = (int) Route::param('page');

		conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );
		$result = Builder::factory($this->tablename)
                    ->order_by('date','DESC')
                    ->limit($this->limit, ($page - 1) * $this->limit)
                    ->find_all();
        $count = Builder::factory($this->tablename)->count_all();
        $pager = Backend::pager( $count, $this->limit, $page );

		return support::tpl(array(
				'result' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'count' => $count,
                'pager' => $pager
            ), $this->tpl_folder.'/index');
	}

	function newAction(){
		if($_POST) {
			$post = $_POST['FORM'];
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            $post['date'] = time();
            if( !trim(Arr::get($post, 'title')) ) {
                Message::GetMessage(0, 'Поле "Название" не может быть пустым!');
            } /*else if(!Arr::get($post, 'link')) {
                Message::GetMessage(0, 'Поле "URL" не может быть пустым!');
            }*/ else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_news'.time()).'.'.$ext;
                        $big = HOST . '/images/videos/big/' . $filename;
                        $small = HOST . '/images/videos/small/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(600, NULL, Image::WIDTH);
                        $image->save($big);

                        $image = Image::factory($big);
                        $image->resize(100, 100, Image::INVERSE);
                        $image->crop(100, 100);
                        $image->save($small);


                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', Arr::get($_POST, 'id'))->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
		}
		else {
            $id_video = (int) Route::param('id');
            conf::bread( '/backend/'.Route::controller().'/new/'.$id_video, 'Редактирование видео' );
			$result = Builder::factory($this->tablename)->where('id', '=', $id_video)->find();
		}
		conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
		return support::tpl(array(
				'obj'=> $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename
            ), $this->tpl_folder.'/form');
	}

	function addAction(){
            conf::bread( '/backend/'.Route::controller().'/add', 'Добавление' );
			if ($_POST) {
            $post = $_POST['FORM'];
            $post['date'] = time();
            $post['status'] = Arr::get( $_POST, 'status', 0 );
            if( !trim(Arr::get($post, 'title')) ) {
                Message::GetMessage(0, 'Поле "Имя" не может быть пустым!');
            } /*else if(!Arr::get($post, 'link')) {
                Message::GetMessage(0, 'Поле "Текст" не может быть пустым!');
            }*/ else {
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                    	$id = Builder::factory($this->tablename)->get_last_id();
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_news'.time()).'.'.$ext;
                        $big = HOST . '/images/videos/big/' . $filename;
                        $small = HOST . '/images/videos/small/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(600, NULL, Image::WIDTH);
                        $image->save($big);

                        $image = Image::factory($big);
                        $image->resize(100, 100, Image::INVERSE);
                        $image->crop(100, 100);
                        $image->save($small);


                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', $id)->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result     = array();
        }
		conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );
		return support::tpl(array(
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename
            ), $this->tpl_folder.'/form');
	}

	function deleteAction() {
	    $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/videos/small/' . $page->image );
        @unlink( HOST . '/images/videos/big/' . $page->image );
        //@unlink( HOST . '/images/videos/original/' . $page->image );
        
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
	}

	function deleteimageAction() {
		$id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
        @unlink( HOST . '/images/videos/small/' . $page->image );
        @unlink( HOST . '/images/videos/big/' . $page->image );
        //@unlink( HOST . '/images/videos/original/' . $page->image );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
	}
}