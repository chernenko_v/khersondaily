<?php

class wisdowController {
	public $tablename = 'wisdow';
	public $tpl_folder = 'wisdow';
    public $limit;

	function __construct() {
		conf::set( 'h1', 'Цитаты' );
        conf::set( 'title', 'Цитаты' );
        conf::bread( '/backend/'.Route::controller().'/index', conf::get( 'h1' ) );
        $this->limit = conf::get('limit_backend');
	}


	function indexAction(){
        $page = (int) Route::param('page');
		conf::set( 'toolbar', Widgets::get( 'Toolbar/List', array( 'add' => 1, 'delete' => 1 ) ) );
		$result = Builder::factory('wisdow')
                    ->order_by('show_date','DESC')
                    ->limit($this->limit, ($page - 1) * $this->limit)
                    ->find_all();

        $count = Builder::factory('wisdow')
                    ->count_all();
        $pager = Backend::pager( $count, $this->limit, $page );

		return support::tpl(array(
				'result' => $result,
				'count' => Builder::factory($this->tablename)->count_all(),
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename,
                'pager' => $pager
            ), $this->tpl_folder.'/index');
	}

	function newAction(){
		if ($_POST) {
            $post = $_POST['FORM'];
            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Имя" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'job'))) {
                Message::GetMessage(0, 'Поле "Статус" не может быть пустым!');
            } else if(!Arr::get($post, 'text')) {
                Message::GetMessage(0, 'Поле "Цитата" не может быть пустым!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->where('id', '=', Arr::get($_POST, 'id'))->edit();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_news'.time()).'.'.$ext;
                        //$original = HOST . '/images/wisdow/original/' . $filename;
                        $small = HOST . '/images/wisdow/' . $filename;
                        $image = Image::factory($_FILES['file']['tmp_name']);
                        //$image->save($original);

                        //$image = Image::factory($original);
                        $image->resize(100, 100, Image::INVERSE);
                        $image->crop(100, 100);
                        $image->save($small);

                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', Arr::get($_POST, 'id'))->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно изменили данные!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось изменить данные!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);
        } else {
            $result = Builder::factory($this->tablename)->where('id', '=', (int) Route::param('id'))->find();
        }

		conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Редактирование' );
        conf::set( 'title', 'Редактирование цитаты' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );
		return support::tpl(array(
				'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename
            ), $this->tpl_folder.'/form');
	}

	function addAction(){
		if ($_POST) {
            $post = $_POST['FORM'];
            $last_date = mysql::query_one('SELECT show_date FROM wisdow ORDER BY id DESC LIMIT 1');

            //добавляя цитату накидываем 24 часа к дате последней цитаты в таблице
            if($last_date->show_date) {
                $post['show_date'] = date('Y-m-d',strtotime($last_date->show_date)+(1*24*60*60));
                if($post['show_date'] == $last_date->show_date) {
                    $post['show_date'] = date('Y-m-d',strtotime($last_date->show_date)+(1*36*60*60));
                }
            }
            else {
                $post['show_date'] = date('Y-m-d',time()); 
            }
            print_r($post['show_date']);

            if( !trim(Arr::get($post, 'name')) ) {
                Message::GetMessage(0, 'Поле "Имя" не может быть пустым!');
            } else if(!trim(Arr::get($post, 'job'))) {
                Message::GetMessage(0, 'Поле "Статус" не может быть пустым!');
            } else if(!Arr::get($post, 'text')) {
                Message::GetMessage(0, 'Поле "Текст" не может быть пустым!');
            } else {
                $res = Builder::factory($this->tablename)->data($post)->add();
                if($res) {
                    if( Arr::get( $_FILES['file'], 'name' ) ) {
                    	$id = Builder::factory($this->tablename)->get_last_id();
                        $ext = end( explode('.', $_FILES['file']['name']) ); 
                        $filename = md5($_FILES['file']['name'].'_news'.time()).'.'.$ext;
                        //$original = HOST . '/images/wisdow/original/' . $filename;
                        $small = HOST . '/images/wisdow/' . $filename;
                        //$image->save($original);

                        $image = Image::factory($_FILES['file']['tmp_name']);
                        $image->resize(100, 100, Image::INVERSE);
                        $image->crop(100, 100);
                        $image->save($small);

                        Builder::factory($this->tablename)->data(array('image' => $filename))->where('id', '=', $id)->edit();
                    }
                    Message::GetMessage(1, 'Вы успешно добавили!');
                    location($_SERVER['HTTP_REFERER']);
                } else {
                    Message::GetMessage(0, 'Не удалось добавили!');
                }
            }
            $post['id'] = Arr::get($_POST, 'id');
            $result     = Arr::to_object($post);

        } else {
            $result     = array();
        }

  		conf::set( 'toolbar', Widgets::get( 'Toolbar/Edit' ) );

        conf::set( 'h1', 'Добавление' );
        conf::set( 'title', 'Добавление цитаты' );
        conf::bread( '/backend/'.Route::controller().'/add', conf::get( 'h1' ) );

		return support::tpl(array(
				'obj' => $result,
                'tpl_folder' => $this->tpl_folder,
                'tablename' => $this->tablename
            ), $this->tpl_folder.'/form');
	}

    function deleteimageAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        
         @unlink( HOST . '/images/wisdow/' . $page->image );
        @unlink( HOST . '/images/wisdow/original/' . $page->image );

        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/new/id/'.$id);
    }

    function deleteAction() {
        $id = (int) Route::param('id');
        if(!$id) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }
        $page = Builder::factory($this->tablename)->where('id', '=', $id)->find();
        if(!$page) {
            Message::GetMessage(0, 'Данные не существуют!');
            location('backend/'.Route::controller().'/index');
        }

        @unlink( HOST . '/images/wisdow/' . $page->image );
        @unlink( HOST . '/images/wisdow/original/' . $page->image );
        
        Builder::factory($this->tablename)->where('id', '=', $id)->delete();
        Message::GetMessage(1, 'Данные удалены!');
        location('backend/'.Route::controller().'/index');
    }
}