<?php
    // Hosting version
    define('PRODUCTION', TRUE);
    define('APPLICATION', '/backend');
    define('DEFAULT_ACTION', 'index');
    define('PROFILER', FALSE);
    define('START_TIME', microtime(TRUE));
    define('START_MEMORY', memory_get_usage());
    //миниатюра для контента
    define('INFO_WIDTH', 262);
    define('INFO_HEIGHT', 249);
    //миниатюра для цитат
    define('CITATION_WIDTH', 72);
    define('CITATION_HEIGHT', 72);
    //medium для галерей
    define('GALLERY_WIDTH', 701); //403
    define('GALLERY_HEIGHT', 466); //268
    //миниатюра для посещаемых.комментируемых
    define('TOP_WIDTH', 62);
    define('TOP_HEIGHT', 62);
    //slider size
    define('SLIDER_WIDTH', 656);
    define('SLIDER_HEIGHT', 320);
    define('SLIDER_MIN_WIDTH', 200);
    define('SLIDER_MIN_HEIGHT', 97);
    require "../main.php";
	
	/*if (get_magic_quotes_gpc()) {
		$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
		while (list($key, $val) = each($process)) {
			foreach ($val as $k => $v) {
				unset($process[$key][$k]);
				if (is_array($v)) {
					$process[$key][stripslashes($k)] = $v;
					$process[] = &$process[$key][stripslashes($k)];
				} else {
					$process[$key][stripslashes($k)] = stripslashes($v);
				}
			}
		}
		unset($process);
	}
	
	if ($_POST['FORM']) {
			$names=array('name','h1','title','pole','zag');
			foreach ($_POST['FORM'] as $key=>$val) {
				if (in_array($key,$names)) {$_POST['FORM'][$key]=str_replace('"','&quot;',$_POST['FORM'][$key]); }
			}
	}*/
	
    $_SERVER['REQUEST_URI'] = str_replace('/backend', '', $_SERVER['REQUEST_URI']);
    require_once HOST . "/backend/ctrl/Controller.php";
    echo general::global_massage();
    Profiler::view();