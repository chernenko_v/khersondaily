	<?php define ('APPLICATION','/backend'); ?>
	
	<!-- мультизагрузка файлов -->
    <div class="items_wrap">
		  <div class="item_1">
			<div id="content">
				<form id="form1" action="index.php" method="post" enctype="multipart/form-data">
					<div class="fieldset flash" id="fsUploadProgress" align="center">
						<h3 style="font: 16px/1.4em tahoma !important;">Очередь загрузки</h3>
					</div>
					
					<div style="clear:both;"></div>
					
					<div id="divStatus" style="font: 12px/1.2em tahoma !important;"></div>
					
					<div style="clear:both;"></div>
					
					<div  align="center">
						<span id="spanButtonPlaceHolder"></span>
						<input id="btnCancel" type="button" value="Отмена" onclick="swfu.cancelQueue();" disabled="disabled" />
					</div>
				</form>
			</div>
		   </div>		
    </div>	
	
	
	<link href="<?php echo APPLICATION; ?>/plagin/swfupload/css/default.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/plagin/swfupload/js/swfupload/swfupload.js"></script>
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/plagin/swfupload/js/swfupload.queue.js"></script>
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/plagin/swfupload/js/fileprogress.js"></script>
	<script type="text/javascript" src="<?php echo APPLICATION; ?>/plagin/swfupload/js/handlers.js"></script>
	<script type="text/javascript">
			var swfu;

		window.onload = function() {
			var settings = {
				flash_url : "<?php echo APPLICATION; ?>/plagin/swfupload/js/swfupload/swfupload.swf",
				upload_url: "<?php echo APPLICATION; ?>/plagin/swfupload/upload.php",
				post_params: {"id_content" : "<?php echo $_GET['id']; ?>"},
				file_size_limit : "10 MB",
				file_types : "*.jpg;*.jpeg;*.png;*.doc;*.xls;*.pdf",
				file_types_description : "Image Files",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings
				button_image_url: "<?php echo APPLICATION; ?>/plagin/swfupload/images/XPButtonNoText_100x22.png",
				button_width: "100",
				button_height: "22",
				button_placeholder_id: "spanButtonPlaceHolder",
				button_text: '<span class="theFont">Выбрать</span>',
				button_text_style: ".theFont { font-size: 16; }",
				button_text_left_padding: 15,
				button_text_top_padding: -1,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
	</script>
	<!-- мультизагрузка файлов -->