<!DOCTYPE html>
<html>
<head>
    <?php echo Widgets::get('head'); ?>
</head>
<body>
    <?php echo Widgets::get('header'); ?>
    <div class="container fixedHeader">
        <?php echo Widgets::get('sidebar'); ?>
        <div class="contentWrap">
            <div class="contentWrapMar">
                <?php echo Widgets::get('crumbs'); ?>
                <div class="page-header clearFix">
                    <?php echo Widgets::get('hello'); ?>
                    <?php echo conf::get( 'toolbar' ); ?>
                </div>
                <div class="rowSection <?php echo conf::get( 'collsClass' ); ?>">
                    <div class="col-md-12">
                        <div class="widget checkbox-wrap">
                            <?php echo conf::get( 'filter' ); ?>
                            <div class="widgetContent">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="text-align: right;"><?php echo conf::get( 'toolbar' ); ?></div>
                <?php echo Widgets::get('footer'); ?>
            </div>
        </div>
    </div>
</body>
</html>