<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta http-equiv="imagetoolbar" content="no">
<title><?php echo conf::get( 'title' ); ?></title>
<link rel="stylesheet" href="<?php echo css('style'); ?>">
<link rel="stylesheet" href="<?php echo css('ui-lightness/jquery-ui-1.10.1.min'); ?>">
<link rel="stylesheet" href="<?php echo css('liValidForm'); ?>">
<link rel="stylesheet" href="<?php echo css('magnific-popup'); ?>">
<link rel="stylesheet" href="<?php echo css('font-awesome'); ?>">
<link rel="stylesheet" href="<?php echo css('jquery.mCustomScrollbar'); ?>">
<link rel="stylesheet" href="<?php echo css('daterangepicker-bs3'); ?>">
<link rel="stylesheet" href="<?php echo css('liTabs'); ?>">
<link rel="stylesheet" href="<?php echo css('liQuickEdit'); ?>">
<link rel="stylesheet" href="<?php echo css('liHarmonica'); ?>">
<link rel="stylesheet" href="<?php echo css('jquery.minicolors'); ?>">

<link rel="stylesheet" href="<?php echo css('nestable'); ?>">

<link rel="stylesheet" href="<?php echo css('typeahead'); ?>">
<link rel="stylesheet" href="<?php echo css('tagsinput'); ?>">
<link rel="stylesheet" href="<?php echo css('iphone-style-checkboxes'); ?>">
<link rel="stylesheet" href="<?php echo css('jquery.dataTables'); ?>">
<link rel="stylesheet" href="<?php echo css('my'); ?>">

<link rel="stylesheet" href="<?php echo css('sresponsiveness'); ?>">


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="target-densitydpi=device-dpi">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="apple-touch-icon" href="<?php echo pic('apple-touch-fa-57x57.png'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo pic('apple-touch-fa-72x72.png'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo pic('apple-touch-fa-114x114.png'); ?>">

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--<script src="<?php echo js('wysihtml5.min'); ?>"></script> -->


<script src="<?php echo js('modernizr'); ?>"></script>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="<?php echo js("jquery-1.9.0.min"); ?>">\x3C/script>')</script>
<script src="<?php echo js('jquery-ui-1.10.1.min'); ?>"></script>
<script src="<?php echo js('i18n/jquery-ui-i18n'); ?>"></script>
<!--<script src="<?php echo js('bootstrap.min'); ?>"></script> -->
<script src="<?php echo js('jquery.ui.touch-punch.min'); ?>"></script>
<script src="<?php echo js('jquery.liActualSize'); ?>"></script>
<script src="<?php echo js('liValidForm'); ?>"></script>
<script src="<?php echo js('jquery.cookie.min'); ?>"></script>
<script src="<?php echo js('jquery.mousewheel'); ?>"></script>
<script src="<?php echo js('jquery.mCustomScrollbar'); ?>"></script>
<script src="<?php echo js('moment'); ?>"></script>
<script src="<?php echo js('daterangepicker'); ?>"></script>
<script src="<?php echo js('jquery.liTip'); ?>"></script>
<script src="<?php echo js('jquery.liTabs'); ?>"></script>
<script src="<?php echo js('jquery.liHarmonica'); ?>"></script>
<script src="<?php echo js('jquery.nestable.min'); ?>"></script>
<script src="<?php echo js('typeahead.min'); ?>"></script>
<script src="<?php echo js('jquery.tagsinput.min'); ?>"></script>
<script src="<?php echo js('iphone-style-checkboxes'); ?>"></script>
<script src="<?php echo js('jquery.dataTables.min'); ?>"></script>
<script src="<?php echo js('dataTables.bootstrap'); ?>"></script>
<script src="<?php echo js('jquery.liTranslit'); ?>"></script>
<script src="<?php echo js('highcharts'); ?>"></script>
<script src="<?php echo js('data'); ?>"></script>
<script src="<?php echo js('jquery.liQuickEdit'); ?>"></script>
<script src="<?php echo js('jquery.magnific-popup.min'); ?>"></script>
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
<script src="<?php echo js('jquery.minicolors'); ?>"></script>

<script src="<?php echo js('tinymce/tinymce.min'); ?>"></script>

<script src="<?php echo js('main'); ?>"></script>
<script src="<?php echo js('plugins'); ?>"></script>

<script src="<?php echo js('ui'); ?>"></script>

<script src="<?php echo js('my'); ?>"></script>

<link rel="shortcut icon" href="favicon.ico">
<link rel="image_src" href="<?php echo pic('expres_icon.jpg'); ?>">
<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->


<link rel="stylesheet" href="/backend/js/lightbox/css/lightbox.css">
<script src="/backend/js/lightbox/lightbox.min.js"></script>