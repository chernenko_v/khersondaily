<div class="crumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa-home"></i>
            <a href="/backend/index">Панель управления</a>
        </li>
        <?php echo conf::get( 'bread' ); ?>
    </ul>
    <ul class="crumb-buttons">
        <li class="dropdown dropdownMenuHidden">
            <a href="/backend/news/comments">
                <i class="fa-fixed-width">&#xf0e6;</i>
                <span>Комментарии <strong>(<?php echo $cc > 0 ? '+' . $cc : $cc; ?>)</strong></span>
            </a>
        </li>
    </ul>
</div>