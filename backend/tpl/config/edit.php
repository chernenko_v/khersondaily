<form id="myForm" class="rowSection validat" method="post" action="<?php echo general::link($tpl_folder.'/edit') ; ?>">
    <input type="hidden" name="id" value="<?php echo $obj->id; ?>" />
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="form-horizontal row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <?php foreach ($result as $obj): ?>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $obj->name; ?></label>
                            <div class="col-md-10">
                                <input class="form-control valid" type="text" name="FORM[<?php echo $obj->id; ?>]" value="<?php echo $obj->zna; ?>"/>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</form>