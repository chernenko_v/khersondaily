<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Показывать?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Название</label>
                        <div class="">
                            <input class="form-control translitSource valid" name="FORM[name]" type="text" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Алиас
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>"></i>
                        </label>
                        <div class="">
                            <div class="input-group">
                                <input class="form-control translitConteiner valid" name="FORM[alias]" type="text" value="<?php echo $obj->alias; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn translitAction" type="button">Заполнить автоматически</button>
                                </span>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label">Описание</label>
                        <div class="">
                            <textarea class="tinymceEditor form-control" rows="20" name="FORM[text]"><?php echo $obj->text; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="rowSection">
            <div class="col-md-5">
                <div class="widget box">

                    <div class="widgetHeader myWidgetHeader">
                        <div class="widgetTitle">
                            <i class="fa-reorder"></i>
                            Мета-данные
                        </div>
                    </div>
            
                    <div class="widgetContent">
                        
                        <div class="form-vertical row-border">
                            <div class="form-group">
                                <label class="control-label">
                                    Заголовок страницы (h1)
                                    <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title" style="white-space: nowrap;"></i>
                                </label>
                                <div class="">
                                    <input class="form-control" name="FORM[h1]" type="text" value="<?php echo $obj->h1; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    title
                                    <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>" style="white-space: nowrap;"></i>
                                </label>
                                <div class="">
                                    <input class="form-control" type="text" name="FORM[title]" value="<?php echo $obj->title; ?>" />
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label class="control-label">Ключевые слова (keywords)</label>
                                <div class="">
                                    <textarea class="form-control" name="FORM[keywords]" rows="5"><?php echo $obj->keywords; ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label">Описание (description)</label>
                                <div class="">
                                    <textarea class="form-control" name="FORM[description]" rows="5"><?php echo $obj->description; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php if ( $obj->id ): ?>
        <div class="rowSection">
            <div class="col-md-12">
                <div class="widget box">
                    <div class="widgetHeader myWidgetHeader">
                        <div class="widgetTitle">
                            <i class="fa-reorder"></i>
                            Загрузка фото
                        </div>
                    </div>

                    <div class="widgetContent">
                        <div class="form-vertical row-border">
                            <div class="form-group" style="display: inline-block; margin-right: 15px; border-right: 1px solid #ececec; vertical-align: top; padding-right: 15px;">
                                <div id="leftpanel" style="padding-left:25px;">
                                    <div id="actions">
                                        <span id="info-count-image">Изображений не выбрано</span><br/>
                                        Общий размер: <span id="info-size-image">0</span> Кб<br/><br/>
                                        <button id="upload-all-image">Загрузить все</button>
                                        <button id="cancel-all-image">Отменить все</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="display: inline-block;border-top: 0; vertical-align: top; padding-top: 5px;">
                                Чтобы добавить картинки, выберите их в поле<br/><br/>
                                <input type="file" name="price" id="file-field-image" /><br/><br/>
                                <span id="dropBox-label">... или просто перетащите их в область ниже &dArr;</span>
                            </div>
                            <div class="form-group">
                                <div id="image-container">
                                    <ul id="image-list"></ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="block title_1">Загруженные фото:</label>
                                <div id="uploaded-images" class="uploaded_images">
                                    <?php echo $show_images; ?>
                                </div>
                                <?php echo $pager; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($obj->id): ?>
            <div class="form-group">
                <label class="control-label">Загрузите файл zip с изображениями</label>
                <div class="">
                    <input type="file" name="zip_file" />
                </div>
            </div>
        <?php endif; ?>
        <input type="hidden" id="id_good" name="id" value="<?php echo $obj->id; ?>" />
        <input type="hidden" id="time_good" name="time" value="<?php echo time(); ?>" />
    <?php endif; ?>
</form>

<?php if ( $obj->id ): ?>
    <script type="text/javascript" src="/backend/plagin/jsuploader/jquery.damnUploader.js"></script>
    <script type="text/javascript" src="/backend/plagin/jsuploader/interfaceImageApi.js"></script>

    <script>
        $(function(){
            $('#uploaded-images').on('click', 'input[name="default_image"]', function(){
                var id = $(this).val();
                var catalog_id = $('#id_good').val();
                $.ajax({
                    url: '/backend/ajax/set_default_image',
                    type: 'POST',
                    data: {
                        id: id,
                        catalog_id: catalog_id
                    }
                });
            });

            $('#uploaded-images').on('click', '.delete-catalog-image', function(){
                if( !confirm( 'Это действие удалит фото. Продолжить?' ) ) { return false; }

                var it = $(this);
                var id = it.data('id');
                $.ajax({
                    url: '/backend/ajax/delete_catalog_photo',
                    type: 'POST',
                    data: {
                        id: id
                    },
                    complete: function(){
                        it.closest('div.catalog-image').remove();
                        if( $('#uploaded-images > div').length == 0 ) {
                            $('#uploaded-images').html('<div style="font-size: 16px; color: red;">Нет загруженных фото!</div>');
                        }
                    }
                });
            });

            $("#uploaded-images").sortable({                
                stop: function () {
                    var order = [];
                    $("#uploaded-images > .catalog-image").each(function() {
                        order.push($(this).attr('data-image'));
                    });         
                    $.ajax({
                        type: 'POST',
                        url: '/backend/ajax/sort_images',
                        dataType: 'JSON',
                        data: {
                            order : order                   
                        },
                        success: {}
                    });
                }       
            });
        });
    </script>
<?php endif; ?>