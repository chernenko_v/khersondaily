<div class="rowSection clearFix row-bg">
    <div class="col-sm-6 col-md-3">
        <div class="statbox widget box box-shadow">
            <div class="widgetContent">
                <div class="visual black">
                    <i class="fa-users"></i>
                </div>
                <div class="title">
                    Пользователи
                </div>
                <div class="value">
                    <?php echo $count_users; ?>
                </div>
                <a href="/backend/users/index" class="more">Подробнее <i class="pull-right fa-angle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="statbox widget box box-shadow">
            <div class="widgetContent">
                <div class="visual red">
                    <i class="fa-bullhorn"></i>
                </div>
                <div class="title">
                    Новости
                </div>
                <div class="value">
                    <?php echo $count_news; ?>
                </div>
                <a href="/backend/news/index" class="more">Подробнее <i class="pull-right fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>