<form id="myForm" class="rowSection validat" method="post" action="">
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="form-horizontal row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Опубликовано</label>
                        <div class="col-md-10">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Название</label>
                        <div class="col-md-10">
                            <input class="form-control valid" type="text" name="FORM[name]" value="<?php echo $obj->name; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Ссылка</label>
                        <div class="col-md-10">
                            <input class="form-control valid" type="text" name="FORM[url]" value="<?php echo $obj->url; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Привязать рубрику</label>
                        <div class="col-md-10">
                            <select name="FORM[parent_id]">
                                <option value="0">---</option>
                                <?php foreach ($tree as $rubric):?>
                                    <option value="<?php echo $rubric->id; ?>" <?php if($obj->parent_id == $rubric->id) echo  'selected'; ?>><?php echo $rubric->name;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $obj->id; ?>">
</form>