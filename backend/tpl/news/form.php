<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                    <?php if (isset($obj->views)): ?>
                        <span>&nbsp;&nbsp;&nbsp;(просмотров - <?php echo $obj->views; ?>)</span>
                    <?php endif ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Опубликовано?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Публиковать в фоторепортаж?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="show_photoreport" value="0" type="radio" <?php echo (!$obj->show_photoreport AND !$obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="show_photoreport" value="1" type="radio" <?php echo ($obj->show_photoreport) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Иконка видео в ленте новостей?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="icon_video" value="0" type="radio" <?php echo (!$obj->icon_video or !$obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="icon_video" value="1" type="radio" <?php echo ($obj->icon_video) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Иконка изображения  в ленте новостей?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="icon_image" value="0" type="radio" <?php echo (!$obj->icon_image or !$obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="icon_image" value="1" type="radio" <?php echo ($obj->icon_image) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Показывать в разделе "Читают"?</label>
                        <div class=""> 
<!--                            по умолчанию нет 9-02-2016 -->
                            <label class="checkerWrap-inline">
                                <input name="read" value="0" type="radio" <?php echo (!$obj->read OR !$obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="read" value="1" type="radio" <?php echo ($obj->read) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Выделять жирным шрифтом на главной?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="highlights" value="0" type="radio" <?php echo (!$obj->highlights or !$obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="highlights" value="1" type="radio" <?php echo ($obj->highlights) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Цвет заголовка новости в ленте новостей</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="color_h1" value="0" type="radio" <?php echo (!$obj->color_h1 or !$obj) ? 'checked' : ''; ?>>
                                Черный цвет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="color_h1" value="1" type="radio" <?php echo ($obj->color_h1) ? 'checked' : ''; ?>>
                                Красный цвет
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Не отправлять в файл экспорта?</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="rss" value="0" type="radio" <?php echo (!$obj->rss or !$obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="rss" value="1" type="radio" <?php echo ($obj->rss) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Логотип на изображении</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="watermark_image" value="0" type="radio" <?php echo (!$obj->watermark_image or !$obj) ? 'checked' : ''; ?>>
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="watermark_image" value="1" type="radio" <?php echo ($obj->watermark_image) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Название</label>
                        <div class="">
                            <input class="form-control translitSource valid" name="FORM[name]" type="text" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Название для RSS</label>
                        <div class="">
                            <input class="form-control" name="FORM[name_rss]" type="text" value="<?php echo $obj->name_rss; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Алиас
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Алиас (англ. alias - псевдоним)</b><br>Алиасы используются для короткого именования страниц. <br>Предположим, имеется страница с псевдонимом «<b>about</b>». Тогда для вывода этой страницы можно использовать или полную форму: <br><b>http://domain/?go=frontend&page=about</b><br>или сокращенную: <br><b>http://domain/about.html</b>"></i>
                        </label>
                        <div class="">
                            <div class="input-group">
                                <input class="form-control translitConteiner valid" name="FORM[alias]" type="text" value="<?php echo $obj->alias; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn translitAction" type="button">Заполнить автоматически</button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Дата</label>
                        <div class="">
                            <input type="text" id="news_date" name="FORM[date]" value="<?php echo $obj->date ? date('d.m.Y G:i', $obj->date) : date('d.m.Y G:i'); ?>" class="valid form-control input-width-medium" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Рубрика</label>
                        <div class="">
                            <select name="parent_id">
                                <option></option>
                                <?php 
                                    foreach ($tree as $val) {
                                        ?>
                                            <option value="<?php echo $val->id; ?>" <?php echo $val->id === $obj->parent_id ? 'selected' : ''; ?>><?php echo $val->name;?></option>
                                        <?php
                                    } 
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Краткое описание</label>
                        <div class="">
                            <textarea class="tinymceEditor form-control" rows="20" name="FORM[text_short]"><?php echo $obj->text_short; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Содержание</label>
                        <div class="">
                            <textarea class="tinymceEditor form-control" rows="20" name="FORM[text]"><?php echo $obj->text; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
        <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Пуш сообщения
                </div>
            </div>
			<div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="control-label">
                                Отправить пуш?
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>В случае выбора рассылаем PUSH сообщения подписчикам</b>"></i>
                            </label>
                            <div class="">
                                <label class="checkerWrap-inline">
                                    <input name="push_sent" value="0" type="radio" checked>
                                    Нет
                                </label>
                                <label class="checkerWrap-inline">
                                    <input name="push_sent" value="1" type="radio">
                                    Да
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Title
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Заголовок push-уведомления</b>"></i>
                            </label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="PUSH[title]" value="<?php echo $obj->title; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Body
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>текст push-уведомления</b>"></i>
                            </label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="PUSH[body]" value="<?php echo strip_tags($obj->body); ?>"/>
                            </div>
                        </div>
<!--                        <div class="form-group">
                            <label class="col-md-2 control-label">Icon
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>иконка уведомления</b>"></i>
                            </label>
                            <div class="col-md-10">
                                <?php // $img = explode('/push/push/', $obj->icon); ?>
                                <?php // if (is_file(HOST . Core\HTML::media('images/push/push/' . $img[1]))): ?>
                                    <a href="<?php // echo Core\HTML::media('images/push/original/' . $img[1]); ?>" rel="lightbox">
                                        <img src="<?php // echo Core\HTML::media('images/push/push/' . $img[1]); ?>" style="max-width: 160px;">
                                    </a>
                                    <br>
                                    <a href="/wezom/<?php // echo Core\Route::controller(); ?>/delete_pic/<?php // echo $obj->id; ?>">Удалить изображение</a>
                                <?php // else: ?>
                                    <input type="file" name="icon">
                                <?php // endif ?>
                            </div>
                        </div>-->
                        <!-- <div class="form-group">
                            <label class="col-md-2 control-label">Tag
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>тег уведомления</b>"></i>
                            </label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="PUSH[tag]" value="<?php echo $obj->tag; ?>"/>
                            </div>
                        </div> -->
<!--                        <div class="form-group">
                            <label class="col-md-2 control-label">URL
                                <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>url перехода при клике на уведомление</b>"></i>
                            </label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="PUSH[url]" value="<?php echo $obj->push_url; ?>"/>
                            </div>
                        </div>-->
                        <!--DATAPICKER-->
                    <div class="form-group">
                        <label class="col-md-2 control-label">Время действия push уведомления
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Дата до которого действителен пуш</b>"></i>
                        </label>
                        <div class="col-md-10">
                            <input type="text" id="push_date" name="PUSH[lifetime]" value="<?php echo $obj->lifetime ? date('d.m.Y G:i', $obj->lifetime) : date('d.m.Y G:i'); ?>" class="myPicker form-control" />
                        </div>
                    </div>
                    </div>
                </div>
            </div>
			<?php if(!$obj){?>
             <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Изображение
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Изображение</label>
                        <div class="">
                            <?php if (support::is_file( HOST . '/images/news/big/' . $obj->images )): ?>
                                <a href="/images/news/big/<?php echo $obj->images; ?>" rel="lightbox">
                                    <img src="/images/news/small/<?php echo $obj->images; ?>" />
                                </a>
                                <br />
                                <a href="/backend/<?php echo Route::controller(); ?>/deleteimage/id/<?php echo $obj->id; ?>">Удалить изображение</a>
                            <?php else: ?>
                               <input type="file" name="file" /> 
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div> 
			<?php }?>
            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Мета-данные
                </div>
            </div>
        
            <div class="widgetContent">
                
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">
                            Заголовок страницы (h1)
                            <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title" style="white-space: nowrap;"></i>
                        </label>
                        <div class="">
                            <input class="form-control" name="FORM[h1]" type="text" value="<?php echo $obj->h1; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            title
                            <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>" style="white-space: nowrap;"></i>
                        </label>
                        <div class="">

                            <input class="form-control" type="text" name="FORM[title]" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label">Ключевые слова (keywords)</label>
                        <div class="">
                            <textarea class="form-control" name="FORM[keywords]" rows="5"><?php echo $obj->keywords; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">Описание (description)</label>
                        <div class="">
                            <textarea class="form-control" name="FORM[description]" rows="5"><?php echo $obj->description; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ( $obj->id ): ?>
        <div class="rowSection">
            <div class="col-md-12">
                <div class="widget box">
                    <div class="widgetHeader myWidgetHeader">
                        <div class="widgetTitle">
                            <i class="fa-reorder"></i>
                            Загрузка фото
                        </div>
                    </div>

                    <div class="widgetContent">
                        <div class="form-vertical row-border">
                            <div class="form-group" style="display: inline-block; margin-right: 15px; border-right: 1px solid #ececec; vertical-align: top; padding-right: 15px;">
                                <div id="leftpanel" style="padding-left:25px;">
                                    <div id="actions">
                                        <span id="info-count-image">Изображений не выбрано</span><br/>
                                        Общий размер: <span id="info-size-image">0</span> Кб<br/><br/>
                                        <button id="upload-all-image">Загрузить все</button>
                                        <button id="cancel-all-image">Отменить все</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="display: inline-block;border-top: 0; vertical-align: top; padding-top: 5px;">
                                Чтобы добавить картинки, выберите их в поле<br/><br/>
                                <input type="file" name="price" id="file-field-image" /><br/><br/>
                                <span id="dropBox-label">... или просто перетащите их в область ниже &dArr;</span>
                            </div>
                            <div class="form-group">
                                <div id="image-container">
                                    <ul id="image-list"></ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="block title_1">Загруженные фото:</label>
                                <div id="uploaded-images" class="uploaded_images">
                                    <?= $show_images; ?>
                                </div>
                                <?php echo $pager; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="id_good" name="id" value="<?php echo $obj->id; ?>" />
    <?php endif; ?>
    <?php if ($obj->id): ?>
        <div class="form-group">
            <label class="control-label">Загрузите файл zip с изображениями</label>
            <div class="">
                <input type="file" name="zip_file" />
            </div>
        </div>
    <?php endif; ?>
    <?php if ( $obj->id ): ?>
        <input type="hidden" name="id" value="<?php echo $obj->id; ?>" />
    <?php endif; ?>
</form>
<script src="/backend/js/datetimepicker/jquery.datetimepicker.js"></script>
<link rel="stylesheet" href="/backend/js/datetimepicker/jquery.datetimepicker.css">
<script>
    $('#news_date').datetimepicker({
        format:'d.m.Y H:i',
        lang:'ru'
    });
    $('#push_date').datetimepicker({
        format:'d.m.Y H:i',
        lang:'ru'
    });
</script>

<?php if ( $obj->id ): ?>
    <script type="text/javascript" src="/backend/plagin/jsuploader/jquery.damnUploader.js"></script>
    <script type="text/javascript" src="/backend/plagin/jsuploader/interfaceImageApiNews.js"></script>

    <script>
        $(function(){
            $('#uploaded-images').on('click', 'input[name="default_image"]', function(){
                var id = $(this).val();
                var news_id = $('#id_good').val();
                $.ajax({
                    url: '/backend/ajax/news_set_default_image',
                    type: 'POST',
                    data: {
                        id: id,
                        news_id: news_id
                    }
                });
            });

            $('#uploaded-images').on('click', '.delete-catalog-image', function(){
                if( !confirm( 'Это действие удалит фото. Продолжить?' ) ) { return false; }

                var it = $(this);
                var id = it.data('id');
                $.ajax({
                    url: '/backend/ajax/news_delete_catalog_photo',
                    type: 'POST',
                    data: {
                        id: id
                    },
                    complete: function(){
                        it.closest('div.catalog-image').remove();
                        if( $('#uploaded-images > div').length == 0 ) {
                            $('#uploaded-images').html('<div style="font-size: 16px; color: red;">Нет загруженных фото!</div>');
                        }
                    }
                });
            });

            $("#uploaded-images").sortable({                
                stop: function () {
                    var order = [];
                    $("#uploaded-images > .catalog-image").each(function() {
                        order.push($(this).attr('data-image'));
                    });         
                    $.ajax({
                        type: 'POST',
                        url: '/backend/ajax/news_sort_images',
                        dataType: 'JSON',
                        data: {
                            order : order                   
                        },
                        success: {}
                    });
                }       
            });
        });
    </script>
<?php endif; ?>