<?php if (!empty($images)): ?>
    <?php foreach($images as $im): ?>
        <?php if (support::is_file(HOST.'/images/news/big/'.$im->image)): ?>
            <div class="catalog-image" data-image="<?=$im->id; ?>">
                <div class="preview-image">
                    <a href="<?=href('/images/news/big/'.$im->image) ?>" data-lightbox="images">
                        <img style="width: 200px;" src="<?='/images/news/small/'.$im->image; ?>" />
                    </a>               
                    <div class="delete-catalog-image" data-id="<?php echo $im->id; ?>"></div>                    
                </div>
                <div class="default-image">
                    <label for="def-img-<?=$im->id; ?>">Обложка</label>
                    <input style="opacity: 1; position: static;" id="def-img-<?=$im->id; ?>" type="radio" <?= $im->main == 1 ? 'checked="checked"' : ''; ?> name="default_image" value="<?=$im->id; ?>" />
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php else: ?>
    <div style="font-size: 16px; color: red;">Нет загруженных фото!</div>
<?php endif; ?>
