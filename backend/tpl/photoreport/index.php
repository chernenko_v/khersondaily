<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Новости
                    <span class="label label-primary"><?php echo $count; ?></span>
                </div>
            </div>

            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap ">
                        <thead>
                        <tr>
                            <th class="checkbox-head">
                                <label><input type="checkbox"></label>
                            </th>
                            <th>Название</th>
                            <th>Рубрика</th>
                            <th>Алиас</th>
                            <th>Просмотров</th>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th class="nav-column textcenter">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $result as $obj ): ?>
                            <tr data-id="<?php echo $obj->id; ?>">
                                <td class="checkbox-column">
                                    <label><input type="checkbox"></label>
                                </td>
                                <td><a href="/backend/<?php echo Route::controller(); ?>/new/id/<?php echo $obj->id; ?>"><?php echo Text::limit_words($obj->name,10); ?></a></td>
                                <td><?php echo $obj->rubric; ?></td>
                                <td><a href="/news/<?php echo $obj->alias; ?>" title="<?php echo $obj->alias; ?>" target="_blank"><?php echo strlen($obj->alias) > 20 ? substr($obj->alias,0,20).'...' : $obj->alias; ?></a></td>
                                <td><?php echo $obj->views; ?></td>
                                <td><?php echo $obj->date ? date( 'd.m.Y', $obj->date ) : '----'; ?></td>
                                <td width="45" valign="top" class="icon-column status-column">
                                    <?php echo Support::widget(array( 'status' => $obj->status, 'id' => $obj->id ), 'StatusList'); ?>
                                </td>
                                <td class="nav-column">
                                    <ul class="table-controls">
                                        <li>
                                            <a class="bs-tooltip dropdownToggle" href="javascript:void(0);" title="Управление"><i class="fa-cog size14"></i></a>
                                            <ul class="dropdownMenu pull-right">
                                                <li>
                                                    <a href="/backend/<?php echo Route::controller(); ?>/new/id/<?php echo $obj->id; ?>" title="Редактировать"><i class="fa-pencil"></i> Редактировать</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a onclick="return confirm('Это действие необратимо. Продолжить?');" href="/backend/<?php echo Route::controller(); ?>/delete/id/<?php echo $obj->id; ?>" title="Удалить"><i class="fa-trash-o text-danger"></i> Удалить</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <?php echo $pager; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="parameters" data-table="<?php echo $tablename; ?>"></span>