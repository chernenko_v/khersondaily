<?php foreach ($result as $obj): ?>
    <li class="dd-item dd3-item" data-id="<?php echo $obj->id; ?>">
        <div style="border:#ccc 1px solid; margin: 5px; padding: 5px;">
            <table>
                <tr>
                    <td valign="top" class="pagename-column">
                        <div class="clearFix">
                            <div class="pull-left">
                                <div class="pull-left">
                                    <div><?php echo $obj->name; ?></div>
                                    <div class="size11 nowrap">(<span class="gray">Алиас:</span> <?php echo $obj->alias; ?>)</div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="hidden-xs" valign="middle">
                        <div>
                            <?php echo $obj->title; ?>
                        </div>
                    </td>
                    <td class="nav-column icon-column" valign="top">
                        <ul class="table-controls">
                            <li>
                                <a title="Управление" href="javascript:void(0);" class="bs-tooltip dropdownToggle"><i class="fa-cog"></i> </a>
                                <ul class="dropdownMenu pull-right">
                                    <li>
                                        <a title="Редактировать" href="<?php echo href('backend/'.Route::controller().'/treenew/id/'.$obj->id); ?>"><i class="fa-pencil"></i> Редактировать</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </li>
<?php endforeach; ?>