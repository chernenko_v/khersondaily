<?php if ( count($result) ): ?>
    <div class="rowSection">
        <div class="col-md-12">
            <div class="widget">
                <div class="dd pageList" id="myNest">
                    <ol class="dd-list">
                        <?php echo support::tpl(array('result' => $result, 'tpl_folder' => $tpl_folder, 'cur' => 0), $tpl_folder.'/menu'); ?>
                    </ol>
                </div>
                <span id="parameters" data-table="<?php echo $tablename; ?>"></span>
                <input type="hidden" id="myNestJson">
            </div>
        </div>
    </div>
<?php endif ?>