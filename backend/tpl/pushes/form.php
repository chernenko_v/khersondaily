<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Title
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>Заголовок push-уведомления</b>"></i>
                        </label>
                        <div class="col-md-10">
                            <input class="form-control valid" type="text" name="FORM[title]" value="<?php echo $obj->title; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Body
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>текст push-уведомления</b>"></i>
                        </label>
                        <div class="col-md-10">
                            <input class="form-control valid" type="text" name="FORM[body]" value="<?php echo $obj->body; ?>"/>
                        </div>
                    </div>
                    <div class="widgetContent">
                        <div class="form-vertical row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Icon
                                    <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>иконка уведомления</b>"></i>
                                </label>
                                <div class="">
                                    <?php /*$img = explode('/push/push/', $obj->icon); */?> 
                                    <?php $img = str_replace('https://khersondaily.com', '', $obj->icon); ?>
                                    <?php if (support::is_file( HOST .$img)): ?>
                                        <a href="<?php echo $obj->icon; ?>" rel="lightbox">
                                            <img src="<?php echo $obj->icon; ?>" />
                                        </a>
                                        <br />
                                        <a href="/backend/<?php echo Route::controller(); ?>/deleteimage/id/<?php echo $obj->id; ?>">Удалить изображение</a>
                                    <?php else: ?>
                                        <input type="file" name="icon" />
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-md-2 control-label">Tag
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>тег уведомления</b>"></i>
                        </label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="FORM[tag]" value="<?php echo $obj->tag; ?>"/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-md-2 control-label">URL
                            <i class="fa-info-circle text-info bs-tooltip nav-hint" title="<b>url перехода при клике на уведомление</b>"></i>
                        </label>
                        <div class="col-md-10">
                            <input class="form-control valid" type="text" name="FORM[url]" value="<?php echo $obj->url; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Время действия push уведомления</label>
                        <div class="">
                            <input type="text" id="news_date" name="lifetime" value="<?php echo $obj->lifetime ? date('d.m.Y G:i', $obj->lifetime) : date('d.m.Y G:i'); ?>" class="valid form-control input-width-medium" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Описание
                </div>
            </div>
            <div class="pageInfo alert alert-info">
                <p>title: "Заголовок push-уведомления"</p>
                <p>body: "Текст push-уведомления"</p>
                <p>icon: "pic/icon.png" // (Иконка уведомления)</p>
                <!-- <p>tag: "Тег уведомления"</p> -->
                <p>url: "url перехода при клике на уведомление"</p>
            </div>
        </div>
    </div>
    <?php if ( $obj->id ): ?>
        <input type="hidden" name="id" value="<?php echo $obj->id; ?>" />
    <?php endif; ?>
</form>
<script src="/backend/js/datetimepicker/jquery.datetimepicker.js"></script>
<link rel="stylesheet" href="/backend/js/datetimepicker/jquery.datetimepicker.css">
<script>
    $('#news_date').datetimepicker({
        format:'d.m.Y H:i',
        lang:'ru'
    });
</script>
