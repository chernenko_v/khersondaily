<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap ">
                        <thead>
                            <tr>
                                <th class="checkbox-head">
                                    <label><input type="checkbox"></label>
                                </th>
                                <th>IP</th>
                                <th style="width: 700px;">Браузер</th>
                                <th>RegID</th>
                                <th>Группа</th>
                                <th>Подписан</th>
                                <th class="nav-column textcenter">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($result as $obj): ?>
                                <tr data-id="<?php echo $obj->id; ?>">
                                    <td class="checkbox-column">
                                        <label><input type="checkbox"></label>
                                    </td>
                                    <td><?php echo $obj->ip; ?></td>
                                    <td><?php echo str_replace('Mozilla/5.0 ', '', $obj->browser); ?></td>
                                    <td title="<?php echo $obj->reg_id; ?>"><?php echo mb_strimwidth($obj->reg_id, 0, 30, "..."); ?></td>
                                    <td><?php echo $obj->group_name; ?></td>
                                    <td><?php echo $obj->time ? date('d.m.Y H:i', $obj->time) : '----'; ?></td>
                                    <td class="nav-column"></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?php echo $pager; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="parameters" data-table="<?php echo $tablename; ?>"></span>