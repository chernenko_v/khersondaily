<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="widget">

            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
        
            <div class="widgetContent">
                
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Опубликовано</label>
                        <div class="">
                            <label class="checkerWrap-inline">
                                <input name="status" value="0" type="radio" <?php echo (!$obj->status AND $obj) ? 'checked' : ''; ?>>                            
                                Нет
                            </label>
                            <label class="checkerWrap-inline">
                                <input name="status" value="1" type="radio" <?php echo ($obj->status OR !$obj) ? 'checked' : ''; ?>>
                                Да
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Название
                        </label>
                        <div class="">
                            <input class="form-control" name="FORM[title]" type="text" value="<?php echo $obj->title; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            URL
                            <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="Ссылка на видео на Youtube" style="white-space: nowrap;"></i>
                        </label>
                        <div class="">
                            <input class="form-control" type="text" name="FORM[link]" value="<?php echo $obj->link; ?>" />
                        </div>
                    </div>   
                    <div class="form-group">
                        <label class="control-label">
                            CODE
                            <i class="fa-info-circle text-info bs-tooltip nav-hint liTipLink" title="Ссылка на видео на facebook" style="white-space: nowrap;"></i>
                        </label>
                        <div class="">
							<input class="form-control" type="text" name="FORM[facebook_code]" value="<?php echo $obj->facebook_code; ?>" />
                        </div>
                    </div>  					
                </div>
            </div>

            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Обложка
                </div>
            </div>

            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Обложка</label>
                        <div class="">
                            <?php if (support::is_file( HOST . '/images/videos/big/' . $obj->image )): ?>
                                <a href="/images/videos/big/<?php echo $obj->image; ?>" rel="lightbox">
                                    <img src="/images/videos/small/<?php echo $obj->image; ?>" />
                                </a>
                                <br />
                                <a href="/backend/<?php echo Route::controller(); ?>/deleteimage/id/<?php echo $obj->id; ?>">Удалить изображение</a>
                            <?php else: ?>
                                <input type="file" name="file" />
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ( $obj->id ): ?>
        <input type="hidden" name="id" value="<?php echo $obj->id; ?>" />
    <?php endif; ?>
</form>

<script>
    $(function(){
        var pickerInit = function( selector ) {
            var date = $(selector).val();
            $(selector).datepicker({
                showOtherMonths: true,
                selectOtherMonths: false
            });
            $(selector).datepicker('option', $.datepicker.regional['ru']);
            var dateFormat = $(selector).datepicker( "option", "dateFormat" );
            $(selector).datepicker( "option", "dateFormat", 'dd.mm.yy' );
            $(selector).val(date);
        }
        pickerInit('.myPicker');
    });
</script>