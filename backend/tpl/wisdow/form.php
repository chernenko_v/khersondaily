<form id="myForm" class="rowSection validat" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="widget">

            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-actions" style="display: none;">
                        <input class="submit btn btn-primary pull-right" type="submit" value="Отправить">
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Имя
                        </label>
                        <div class="">
                            <input class="form-control" name="FORM[name]" type="text" value="<?php echo $obj->name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Статус
                        </label>
                        <div class="">
                            <input class="form-control" type="text" name="FORM[job]" value="<?php echo $obj->job; ?>" />
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label">Цитата</label>
                        <div class="">
                            <textarea class="form-control" name="FORM[text]" rows="5"><?php echo $obj->text; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="widgetHeader myWidgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Изображение
                </div>
            </div>
        
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Изображение</label>
                        <div class="">
                            <?php if (support::is_file( HOST . '/images/wisdow/' . $obj->image )): ?>
                                <a href="/images/wisdow/<?php echo $obj->image; ?>" rel="lightbox">
                                    <img src="/images/wisdow/<?php echo $obj->image; ?>" />
                                </a>
                                <br />
                                <a href="/backend/<?php echo Route::controller(); ?>/deleteimage/id/<?php echo $obj->id; ?>">Удалить изображение</a>
                            <?php else: ?>
                                <input type="file" name="file" />
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ( $obj->id ): ?>
        <input type="hidden" name="id" value="<?php echo $obj->id; ?>" />
    <?php endif; ?>
</form>