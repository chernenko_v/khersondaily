<?php
    /*
     *      Class with ONLY backend functionality
     */
    class Backend {

        // Options/optgroups tree for tag select
        public static function getSelectOptions( $filename, $table, $parentID = NULL, $currentElement = 0, $sort = 'sort', $parentAlias = 'parent_id' ) {
            if ($filename != 'catalog/select') {
                $current = Route::param('id');
            } else {
                $current = 0;
            }
            $tree = array();
            $result = Builder::factory($table)->order_by($sort)->find_all();
            foreach( $result AS $obj ) {
                if ( !$current ) {
                    $tree[$obj->$parentAlias][] = $obj;
                } else if ( $obj->parent_id != $current AND $obj->id != $current ) {
                    $tree[$obj->$parentAlias][] = $obj;
                }
            }
            return support::tpl( array( 
                    'result' => $tree,
                    'currentParent' => 0,
                    'space' => '',
                    'filename' => $filename,
                    'parentID' => $parentID,
                    'parentAlias' => $parentAlias,
                    'currentID' => $currentElement,
            ), $filename );
        }

        // backend pagination
        public static function pager( $count, $limit, $current ) {
            $pages  = ceil($count / $limit);
            if($pages < 2) { return ""; }

            $link = '/backend'.Arr::get($_SERVER, 'REQUEST_URI');
            $uri = explode('?', $link);
            $link = $uri['0'];
            $get = Arr::get($uri, '1', '');
            if( $current > 1 ) {
                $link = str_replace( '/page/'.$current, '', $link );
            }
            $border = 10;
            $pager = '<ul class="pagination">';
            if($pages <= $border) {
                for($i = 1; $i <= $pages; $i++) {
                    $add = '';
                    if( $i > 1 ) {
                        $add = '/page/'.$i;
                    }
                    if($i == $current) {
                        $pager .= '<li class="active">';
                        $pager .= '<a href="'.$link.$add.($get ? '?'.$get : '').'">'.$i.'</a>';
                    } else {
                        $pager .= '<li>';
                        $pager .= '<a href="'.$link.$add.($get ? '?'.$get : '').'">'.$i.'</a>';
                    }
                    $pager .= '</li>';
                }
            } else if($pages > $border) {
                switch($current) {
                    case ($current < 5):
                        for($i = 1; $i <= 5; $i++) {
                            $add = '';
                            if( $i > 1 ) {
                                $add = '/page/'.$i;
                            }
                            if($i == $current) {
                                $pager .= '<li class="active">';
                                $pager .= '<a href="'.$link.$add.($get ? '?'.$get : '').'" class="cur">'.$i.'</a>';
                            } else {
                                $pager .= '<li>';
                                $pager .= '<a href="'.$link.$add.($get ? '?'.$get : '').'">'.$i.'</a>';
                            }
                            $pager .= '</li>';
                        }
                        $pager .= '<li>...</li>';
                        $pager .= '<li><a href="'.$link.'/page/'.$pages.($get ? '?'.$get : '').'">'.$pages.'</a></li>';
                        break;
                    case ($current > $pages - 5):
                        $pager .= '<li><a href="'.$link.($get ? '?'.$get : '').'">1</a></li>';
                        $pager .= '<li>...</li>';
                        for($i = $pages - 5; $i <= $pages; $i++) {
                            if($i == $current) {
                                $pager .= '<li class="active">';
                                $pager .= '<a href="'.$link.'/page/'.$i.($get ? '?'.$get : '').'" class="cur">'.$i.'</a>';
                            } else {
                                $pager .= '<li>';
                                $pager .= '<a href="'.$link.'/page/'.$i.($get ? '?'.$get : '').'">'.$i.'</a>';
                            }
                        }
                        break;
                    default:
                        $pager .= '<li><a href="'.$link.($get ? '?'.$get : '').'">1</a></li>';
                        $pager .= '<li>...</li>';
                        for($i = $current - 2; $i <= $current + 2; $i++) {
                            if($i == $current) {
                                $pager .= '<li class="active">';
                                $pager .= '<a href="'.$link.'/page/'.$i.($get ? '?'.$get : '').'" class="cur">'.$i.'</a>';
                            } else {
                                $pager .= '<li>';
                                $pager .= '<a href="'.$link.'/page/'.$i.($get ? '?'.$get : '').'">'.$i.'</a>';
                            }
                            $pager .= '</li>';
                        }
                        $pager .= '<li>...</li>';
                        $pager .= '<li><a href="'.$link.'/page/'.$pages.($get ? '?'.$get : '').'">'.$pages.'</a></li>';
                        break;
                }
            }
            $pager .= '</ul>';
            $pager .= '<style>ul.pagination li{display:inline-block;}ul.pagination{display:block;text-align:center;}</style>';
            return $pager;
        }

        // Get human readable date range for admin filter widget
        public function getWidgetDatesRange() {
            $dates = array();
            if( Arr::get($_GET, 'date_s') ) {
                $dates[] = Backend::getWidgetDate( Arr::get($_GET, 'date_s') );
            }
            if( Arr::get($_GET, 'date_po') AND Arr::get($_GET, 'date_s') != Arr::get($_GET, 'date_po') ) {
                $dates[] = Backend::getWidgetDate( Arr::get($_GET, 'date_po') );
            }
            return implode(' - ', $dates);
        }

        // Get human readable date range
        public function getWidgetDate( $date ) {
            $time = strtotime($date);
            return date('j', $time) . ' ' . support::month(date('m', $time)) . ' ' . date('Y', $time);
        }

        // Generate link for filters in backend
        public static function generateLink( $key, $value = NULL, $fakeLink = NULL ) {
            $link = $fakeLink ? $fakeLink : '/backend'.Arr::get($_SERVER, 'REQUEST_URI');
            $uri = explode('?', $link);

            $__get = array();
            if(count($uri) > 1) {
                $arr  = explode('&', $uri[1]);
                foreach($arr AS $_a) {
                    $g   = urldecode($_a);
                    $g   = strip_tags($g);
                    $g   = stripslashes($g);
                    $g   = trim($g);
                    $___get = explode('=', $g);
                    $__get[$___get[0]] = $___get[1];
                }
            }

            if( $value === NULL ) {
                if( !isset($__get[$key]) ) {
                    return $link;
                }
                $arr = explode('&', $uri[1]);
                $get = array();
                foreach( $arr AS $el ) {
                    $h = explode( '=', $el );
                    if( $key != $h[0] ) {
                        $get[] = $h[0].'='.$h[1];
                    }
                }
                $uri[1] = implode('&', $get);
                if( $uri[1] ) {
                    return $uri[0].'?'.$uri[1];
                }
                return $uri[0];
            }
            
            if( !isset( $__get[$key] ) ) {
                if( isset($uri[1]) ) {
                    return Arr::get($uri, 0).'?'.Arr::get($uri, 1).'&'.$key.'='.$value;
                }
                return Arr::get($uri, 0).'?'.$key.'='.$value;
            }
            if( Arr::get($__get, $key) == $value ) {
                return $link;
            }
            $arr = explode('&', $uri[1]);
            $get = array();
            foreach( $arr AS $el ) {
                $h = explode( '=', $el );
                if( $key == $h[0] ) {
                    $get[] = $key.'='.$value;
                } else {
                    $get[] = $h[0].'='.$h[1];
                }
            }

            $uri[1] = implode('&', $get);
            return $uri[0].'?'.$uri[1];
        }

    }