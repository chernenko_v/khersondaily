<?php
	class Builder {

        protected $_tbl;
		
		private   $_select, $_where, $_join, $_on, $_order, $_limit; //$_group, $_as, $_selcount
		public    $_data, $_query, $_count, $_object;
        
        function __construct() {}
        function __destruct() {}
		
        public static function factory($table = NULL) {
            if($table == NULL) { return $this; }
            $obj = new self();
            return $obj->set_table($table);
        }
		
		/**
		 *		Устанавливаем таблицу
		 *		@result Вернет текущий объект
		 */
		public function set_table($table = NULL) {
			if($table == NULL) { return $this; }
			$this->_tbl = $table;
			return $this;
		}
		
		/**
         *      @param mixed $fields_list - поля, которые мы достаем. Пример:
		 *					$fields_list = '*';
		 *					$fields_list = 'id';
		 *					$fields_list = array('id', '`catalog`.`name`', '`status`', '`catalog_tree`.`h1`', '' ... );
         */
		public function select($fields_list = NULL) {
			if(!is_array($fields_list)) { $fields_list = $fields_list === NULL ? Array('*') : Array($fields_list); }
			$fields = Array();
			foreach($fields_list AS $f) {
				$f = str_replace('`', '', $f);
				$f = explode('.', $f);
				if(sizeof($f) == 1) {
					if($f[0] != '*') { $fields[] = '`'.$this->_tbl.'`.`'.$f[0].'`'; } else { $fields[] = '`'.$this->_tbl.'`.*'; }
				} else if(sizeof($f) == 2) {
					if($this->_tbl != $f[0]) { $tbl = $f[0]; } else { $tbl = $this->_tbl; }
					if($f[1] != '*') { $fields[] = '`'.$tbl.'`.`'.$f[1].'`'; } else { $fields[] = '`'.$tbl.'`.*'; }
				}
			}
			$this->_select = implode(', ', $fields);
			return $this;
		}
		
		/**
         *      Формируем условие
         */
		public function where($field = NULL, $action = NULL, $value = NULL, $sep = NULL) {
			$expr_f = $this->is_expr($field);
			if($expr_f) {
				$expr_v = $this->is_expr($value);
				if($expr_v) {
					$w = $expr_f.' '.trim($action).' '.$expr_v;
				} else {
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) {
							$w = $expr_f.' IN ("'.implode('", "', $value).'")';
						} else {
							$w = $expr_f.' IN ('.$value.')';
						}
					} else {
						$w = $expr_f.' '.trim($action).' "'.$value.'"';
					}
				}
			} else {
				$f = str_replace('`', '', $field);
				$f = explode('.', $f);
				if(sizeof($f) == 1) {
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) { $w = '`'.$this->_tbl.'`.`'.$f[0].'`'; } else { $w = '`'.$this->_tbl.'`.`'.$f[0].'`'; }
					} else {
						$w = '`'.$this->_tbl.'`.`'.$f[0].'`';
					}
				} else if(sizeof($f) == 2) {
					if($this->_tbl != $f[0]) { $tbl = $f[0]; } else { $tbl = $this->_tbl; }
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) { $w = '`'.$tbl.'`.`'.$f[1].'`'; } else { $w = '`'.$tbl.'`.`'.$f[1].'`'; }
					} else {
						$w = '`'.$tbl.'`.`'.$f[1].'`';
					}
				}
				
				$expr_v = $this->is_expr($value);
				if($expr_v) {
					$w .= ' '.trim($action).' '.$expr_v;
				} else {
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) {
							$w .= ' IN ("'.implode('", "', $value).'")';
						} else {
							$w .= ' IN ('.$value.')';
						}
					} else {
						$w .= ' '.trim($action).' "'.$value.'"';
					}
				}
			}
			if($this->_where !== NULL) {
				if(strtoupper(trim($sep)) === 'OR') {
					$where = $this->_where.' OR '.$w;
				} else {
					$where = $this->_where.' AND '.$w;
				}
			} else {
                $where = $w;
			}
			if($where !== NULL) {
				if($this->_where !== NULL) {
					$this->_where = $where;
				} else {
					$this->_where = ' WHERE '.$where.' ';
				}
			}
			return $this;
		}


        /**
         * @param null $where
         * @return $this
         */
        public function where_add($where = null) {
            if($where){
                if($this->_where !== NULL) {
                    $this->_where.= ' AND '.$where;
                } else {
                    $this->_where = ' WHERE '.$where.' ';
                }
            }

            return $this;
        }


		/**
         *      Джоиним
         */
		public function join($table = NULL, $type = NULL) {
			if($table == NULL) { return false; }
			$this->_join = ' '.$type.' JOIN `'.str_replace('`', '', $table).'`';
			return $this;
		}

		public function leftJoin($table = NULL, $type = NULL) {
			if($table == NULL) { return false; }
			$this->_join = ' '.$type.' LEFT JOIN `'.str_replace('`', '', $table).'`';
			return $this;
		}

		public function rightJoin($table = NULL, $type = NULL) {
			if($table == NULL) { return false; }
			$this->_join = ' '.$type.' RIGHT JOIN `'.str_replace('`', '', $table).'`';
			return $this;
		}
		
		/**
         *      Формируем условие для джоина
         */
		public function on($field = NULL, $action = NULL, $value = NULL, $sep = NULL) {
			$expr_f = $this->is_expr($field);
			if($expr_f) {
				$expr_v = $this->is_expr($value);
				if($expr_v) {
					$w = $expr_f.' '.trim($action).' '.$expr_v;
				} else {
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) {
							$w = $expr_f.' IN ('.implode(',', $value).')';
						} else {
							$w = $expr_f.' IN ('.$value.')';
						}
					} else {
						$w = $expr_f.' '.trim($action).' "'.$value.'"';
					}
				}
			} else {
				$f = str_replace('`', '', $field);
				$f = explode('.', $f);
				if(sizeof($f) == 1) {
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) { $w = '`'.$this->_tbl.'`.`'.$f[0].'`'; } else { $w = '`'.$this->_tbl.'`.`'.$f[0].'`'; }
					} else {
						$w = '`'.$this->_tbl.'`.`'.$f[0].'`';
					}
				} else if(sizeof($f) == 2) {
					if($this->_tbl != $f[0]) { $tbl = $f[0]; } else { $tbl = $this->_tbl; }
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) { $w = '`'.$tbl.'`.`'.$f[1].'`'; } else { $w = '`'.$tbl.'`.`'.$f[1].'`'; }
					} else {
						$w = '`'.$tbl.'`.`'.$f[1].'`';
					}
				}
				
				$expr_v = $this->is_expr($value);
				if($expr_v) {
					$w .= ' '.trim($action).' '.$expr_v;
				} else {
					if(strtoupper($action) == 'IN') {
						if(is_array($value)) {
							$w .= ' IN ('.implode(',', $value).')';
						} else {
							$w .= ' IN ('.$value.')';
						}
					} else {
						$w .= ' '.trim($action).' "'.$value.'"';
					}
				}
			}
			if($this->_on !== NULL) {
				if(strtoupper(trim($sep)) === 'OR') {
					$on = $this->_on.' OR '.$w;
				} else {
					$on = $this->_on.' AND '.$w;
				}
			} else {
                $on = $w;
			}
			if($on !== NULL) {
				if($this->_on !== NULL) {
					$this->_on = $on;
				} else {
					$this->_on = ' ON '.$on.' ';
				}
			}
			return $this;
		}
		
		/**
         *      Сортировка
         */
		public function order_by($sort = NULL, $type = NULL) {
			if($sort == NULL) { return false; }
            if($sort == 'rand') {
                $this->_order = ' ORDER BY RAND()';
                return $this;
            }
            $expr = $this->is_expr($sort);
            if($expr) {
                $this->_order = ' ORDER BY ' . $expr;
            } else {
                $s = str_replace('`', '', $sort);
                $s = explode('.', $s);
                if(sizeof($s) == 1) {
                    $sort = ' ORDER BY `'.$this->_tbl.'`.`'.str_replace('`','',$sort).'`';
                } else {
                    if($this->_tbl != $s[0]) { $tbl = $s[0]; } else { $tbl = $this->_tbl; }
                    $sort = ' ORDER BY `'.$tbl.'`.`'.str_replace('`','',$s[1]).'`';
                }
                if(!in_array(strtoupper(trim($type)), array('ASC', 'DESC'))) {
                    $type = 'ASC';
                }
                $this->_order = $sort.' '.$type;
            }
            return $this;
		}
		
		/**
         *      Лимит
         */
		public function limit($limit = NULL, $offset = NULL) {
			if($limit == NULL) { return false; }
			if($offset == NULL) {
				$limit = ' LIMIT '. (int) $limit;
			} else {
				$limit = ' LIMIT '. (int) $offset .', '. (int) $limit;
			}
			$this->_limit = $limit;
			return $this;
		}
		
		/**
		 *		Данные загружаем
         */
		public function data($data = NULL) {
			if(!is_array($data) OR empty($data)) { 
				$this->_data = Array();
			} else {
				$this->_data = $data;
			}
			return $this;
		}
		
        /**
         *      Досатем поле из таблицы
         */
        public function find() {
            if($this->_tbl == NULL) { return false; }
			if(!$this->_select) { $this->select(); }
            $this->limit(1);
			$this->_query = 'SELECT '.$this->_select.' FROM `'.$this->_tbl.'`'.$this->_join.$this->_on.$this->_where.$this->_limit;
            $this->_object = mysql::query_one($this->_query);
			$this->_count = count($this->_object);
			return $this->_object;
		}

        /**
         *      Досатем поле из таблицы
         */
		public function find_all() {
            if($this->_tbl == NULL) { return false; }
			if(!$this->_select) { $this->select(); }
			$this->_query = 'SELECT '.$this->_select.' FROM `'.$this->_tbl.'`'.$this->_join.$this->_on.$this->_where.$this->_order.$this->_limit;
            $this->_object = mysql::query($this->_query);
			$this->_count = count($this->_object);
			return $this->_object;
		}
		
		/**
         *      Считаем строки
         */
		public function count_all() {
            if($this->_tbl == NULL) { return false; }
            $this->limit(1);
			$this->_query = 'SELECT COUNT(`'.$this->_tbl.'`.`id`) AS `count` FROM `'.$this->_tbl.'` '.$this->_join.$this->_on.$this->_where.$this->_limit;
			$this->_object = mysql::query_one($this->_query);
            return $this->_object->count;
		}
        
        /**
         *      Считаем строки в существующем объекте
         */
		public function count() {
            if(! is_object($this->_object)) { return 'You have no object to count rows!'; }
            return $this->_count;
		}
		
        /**
         *      Удаляем из таблицы
         */
		public function delete() {
            if($this->_tbl == NULL) { return $this; }
			$this->_query  = 'DELETE FROM `'.$this->_tbl.'` '.$this->_where;
			$this->_object = (boolean) mysql::just_query($this->_query);
			return $this->_object;
		}
        
        /**
         *      Добавляем строку в таблицу
         */
		public function add() {
			if($this->_tbl == NULL) { return false; }
            if(!is_array($this->_data) OR empty($this->_data)) { return $this; }
            if(!$this->_data['created_at']) {
                if(mysql::check_field($this->_tbl, 'created_at')) { $this->_data['created_at'] = time(); }
            }
			$k = $v = array();
			foreach($this->_data as $key => $value) {
				$k[]    = '`'.$key.'`';
				$v[]    = '"'.htmlspecialchars($value).'"';
			}
			$k = implode(', ', $k);
			$v = implode(', ', $v);
			$this->_data = ' ('.$k.') VALUES ('.$v.') ';
			$this->_query = 'INSERT INTO `'.$this->_tbl.'`'.$this->_data;
			$this->_object = (boolean) mysql::just_query($this->_query);
			return $this->_object;
		}
		
        /**
         *      Редактируем данные в таблице
         */
		public function edit() {
			if($this->_tbl == NULL) { return false; }
			if(!is_array($this->_data) OR empty($this->_data)) { return $this; }
            if(!$this->_data['updated_at']) {
                if(mysql::check_field($this->_tbl, 'updated_at')) { $this->_data['updated_at'] = time(); }
            }
			$arr = array();
			foreach($this->_data as $key => $val) {
				$arr[]  = '`'.$key.'` = "'.htmlspecialchars($val).'"';
			}
			$this->_data = implode(', ', $arr);
			$this->_query = 'UPDATE `'.$this->_tbl.'` SET '.$this->_data.$this->_where;
			$this->_object = (boolean) mysql::just_query($this->_query);
			return $this->_object;
		}
        
        /**
         *      Достаем ID последнего добавленого элемента текущей таблице
         */
        public function get_last_id() {
			$this->_query  = 'SELECT MAX(`'.$this->_tbl.'`.`id`) AS `max` FROM `'.$this->_tbl.'` LIMIT 1';
			$this->_object = mysql::query_one($this->_query);
            return $this->_object->max;
        }
        
        /**
         *      DEBUG
         */
        public function debug() {
            echo '<pre>';
            print_r($this);
            echo '</pre>';
            die;
        }
        
        /**
         *      CLEAR OBJECT
         */
        public function clear() {
            $this->_tbl         = NULL;
            $this->_select      = NULL;
            $this->_where       = NULL;
            $this->_join        = NULL;
			$this->_on          = NULL;
            $this->_order       = NULL;
            $this->_limit       = NULL;
            $this->_data        = NULL;
            $this->_query       = NULL;
            $this->_count       = NULL;
            $this->_object      = NULL;
            return $this;
        }
		
		public function is_expr($string, $sep = ', ') {
			if(is_array($string)) { $string = implode($sep, $string); }
			$arr = explode('expression|||', $string);
			if(sizeof($arr) == 1) { return false; }
			return $arr[1];
		}
		
		public static function expr($string) {
			return 'expression|||'.$string;
		}
		
	}