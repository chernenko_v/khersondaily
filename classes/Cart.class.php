<?php
    
    class Cart {
        
        static    $_instance;

        public    $_cart_id = 0; // Cart ID in our system
        public    $_cart = array(); // Items in our cart
        public    $_count_goods = 0; // Count of items in our cart

        function __construct() {
            $this->set_cart_id();
            $this->check_cart();
            $this->recount();
        }

        static function factory() {
            if(self::$_instance == NULL) { self::$_instance = new self(); }
            return self::$_instance;
        }


        // Get goods list for basket from database
        public function get_list_for_basket() {
            $ids = Array();
            $sids = Array();
            foreach($this->_cart AS $key => $item) { 
                $ids[] = $item['id'];
                $sids[] = $item['size'];
            }
            if(!count($ids)) { $ids = array(0); }
            if(!count($sids)) { $sids = array(-1); }
            $result = mysql::query('SELECT catalog.*, catalog_images.image, sizes.id AS size_id, sizes.name AS size_name
                                    FROM catalog
                                    LEFT JOIN catalog_images ON catalog_images.catalog_id = catalog.id AND catalog_images.main = "1"
                                    LEFT JOIN carts_items ON carts_items.catalog_id = catalog.id
                                    LEFT JOIN sizes ON sizes.id = carts_items.size_id AND sizes.status = "1"
                                    WHERE carts_items.cart_id = "'.$this->_cart_id.'" AND catalog.status = "1" AND catalog.id IN ('.implode(',', $ids).') AND carts_items.size_id IN ('.implode(',', $sids).')');
            $basket = $this->_cart;
            foreach($result AS $obj) {
                $basket[$obj->id . '-' . (int) $obj->size_id]['obj'] = $obj;
            }
            return $basket;
        }


        // Setting cart ID
        public function set_cart_id(){
            // Check cookie for existance of the cart
            $hash = Arr::get($_COOKIE, "cart", 0);
            if( !$hash ) { return $this->create_cart(); }
            // Check if our cookie not bad
            $cart = Builder::factory('carts')->where('hash', '=', $hash)->find();
            if( !$cart ) { return $this->create_cart(); }
            // Set cart_id
            $this->_cart_id = $cart->id;
            return true;
        }


        // Creation of the new cart
        public function create_cart() {
            // Generate hash of new cart for cookie
            $hash = sha1(microtime().Text::random());
            // Save cart into database
            Builder::factory("carts")->data(Array(
                'hash' => $hash,
            ))->add();
            // Save cart to cookie
            setcookie('cart', $hash, time() + 60*60*24*28, '/');
            // Set cart id
            $this->_cart_id = Builder::factory("carts")->get_last_id();
            return true;
        }

        
        // Check existance of cart
        public function check_cart() {
            if(!$this->_cart_id) { return false; }
            $result = mysql::query('SELECT carts_items.catalog_id, carts_items.size_id, carts_items.count
                                    FROM carts_items
                                    LEFT JOIN catalog ON catalog.id = carts_items.catalog_id
                                    LEFT JOIN sizes ON sizes.id = carts_items.size_id AND sizes.status = "1"
                                    WHERE catalog.status = "1" AND carts_items.cart_id = "'.$this->_cart_id.'"
                                    ORDER BY carts_items.id DESC');
            foreach($result AS $obj) {
                $this->_cart[$obj->catalog_id . '-' . $obj->size_id] = Array(
                    "id" => $obj->catalog_id,
                    "size" => $obj->size_id,
                    "count" => $obj->count,
                );
            }
            return true;
        }


        // Count goods in cart
        public function recount() {
            $count = 0;
            foreach($this->_cart AS $b) {
                $count += (int) $b['count'];
            }
            $this->_count_goods = $count;
        }

        
        // Get full cost of all cart
        public function get_summa() {
            $summa = 0;
            if(empty($this->_cart)) { return 0; }
            $ids = array();
            foreach($this->_cart AS $b) {
                if(!in_array($b['id'], $ids)) {
                    $ids[] = $b['id'];
                }
            }
            $result = Builder::factory("catalog")
                ->select(Array('cost', 'id'))
                ->where("status", "=", 1)
                ->where("id", "IN", $ids)
                ->find_all();
            $items = array();
            foreach($result AS $obj) {
                $items[$obj->id] = $obj;
            }
            foreach($this->_cart AS $b) {
                $summa += $items[$b['id']]->cost * $b['count'];
            }
            return $summa;
        }

        
        /**
         *      Add goods to cart
         *      @param int $catalog_id - goods ID
         *      @param int $count - count goods in the cart
         */
        public function add($catalog_id, $size_id, $count = 1) {
            if(!Arr::get($this->_cart, $catalog_id . '-' . $size_id, false)) {
                $this->_cart[$catalog_id . '-' . $size_id] = array(
                    'id' => $catalog_id,
                    'size' => $size_id,
                    'count' => $count,
                );
                Builder::factory("carts_items")->data(Array(
                    'catalog_id' => $catalog_id,
                    'size_id' => $size_id,
                    'cart_id' => $this->_cart_id,
                    'count' => $count,
                ))->add();
                $this->recount();
                return true;
            }
            foreach($this->_cart AS $key => $item) {
                if($item['id'] == $catalog_id AND $item['size'] == $size_id) {
                    $this->_cart[$key]['count'] = $this->_cart[$key]['count'] + $count;
                    Builder::factory("carts_items")
                        ->data(Array(
                            'count' => $this->_cart[$key]['count'],
                        ))
                        ->where('cart_id', '=', $this->_cart_id)
                        ->where('catalog_id', '=', $catalog_id)
                        ->where('size_id', '=', $size_id)
                        ->edit();
                    $this->recount();
                    return true;
                }
            }
            return false;
        }

        
        /**
         *      Change count in the cart
         *      @param int $catalog_id - goods ID
         *      @param int $count - new count in the cart
         */
        public function edit($catalog_id, $size_id, $count) {
            if(Arr::get($this->_cart, $catalog_id . '-' . $size_id, false)) {
                $this->_cart[$catalog_id . '-' . $size_id]['count'] = $count;
                Builder::factory("carts_items")
                    ->data(Array(
                        'count' => $count,
                    ))
                    ->where('cart_id', '=', $this->_cart_id)
                    ->where('catalog_id', '=', $catalog_id)
                    ->where('size_id', '=', $size_id)
                    ->edit();
                $this->recount();
                return true;
            }
            return false;
        }

        
        /**
         *      Delete goods from the cart
         *      @param $catalog_id - goods ID
         */
        public function delete($catalog_id, $size_id) {
            if(Arr::get($this->_cart, $catalog_id . '-' . $size_id, false)) {
                unset($this->_cart[$catalog_id . '-' . $size_id]);
                Builder::factory("carts_items")
                    ->where("catalog_id", "=", $catalog_id)
                    ->where("size_id", "=", $size_id)
                    ->where("cart_id", "=", $this->_cart_id)
                    ->delete();
                $this->recount();
            }
        }
        

        // Total cleaning of the cart
        public function clear() {
            Builder::factory("carts")->where("id", "=", $this->_cart_id)->delete();
            $this->_cart_id = 0;
            $this->_cart = Array();
            setcookie('cart', NULL, time() - 1, '/');
            $this->recount();
        }

    }