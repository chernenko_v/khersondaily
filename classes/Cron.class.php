<?php
    
    class Cron {

        public function check() {
            if( !conf::get( 'main.cron' ) OR !conf::get( 'main.selfCron' ) ) {
                return false;
            }
            $ids = array();
            $result = Builder::factory( conf::get( 'main.tableCron' ) )->limit( conf::get( 'main.selfCron' ) )->find_all();
            foreach ( $result as $obj ) {
                $ids[] = $obj->id;
                Email::send( $obj->subject, $obj->text, $obj->email );
            }
            if( count($ids) ) {
                Builder::factory( conf::get( 'main.tableCron' ) )->where( 'id', 'IN', $ids )->delete();
            }
        }

    }