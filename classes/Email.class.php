<?php

    class Email {
        
        /**
         *      Отправляем письмо
         *      @param $email - E-Mail пользователя
         *      @param $subject - Заголовок письма
         *      @param $text - Содержание письма
         *      @param $_to - если 0, то отправляем пользователю от админа, а если 1 - админу от пользователя
         */
        public static function send($subject, $text, $email = NULL) {
            $from = conf::get( 'admin_email' );
            if( !$email ) {
                $to = $from;
            } else {
                $to = $email;
            }
            self::sendBody($from, $to, $subject, $text);
        }


        public static function sendBody($from, $to, $subject, $message) {
            $message='
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <!-- (c) Wezom web-студия | http://www.wezom.com.ua/ -->
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Web-студия «Wezom»</title>
                </head>
                <body>
                <style type="text/css">
                    body,html { padding:0; margin:0}
                    body { font:13px/1.2em Arial, Helvetica, sans-serif;}
                    h1 { font:26px/1.2em Arial, Helvetica, sans-serif; padding:0; margin:0}
                    h2 { font:22px/1.2em Arial, Helvetica, sans-serif; padding:0; margin:0}
                    h3 { font:20px/1.2em Arial, Helvetica, sans-serif; padding:0; margin:0}
                    table {border-collapse: collapse; }
                    table { margin:0}
                    table th {color: #999;font-weight: bold; padding: 6px 6px 5px;text-align: left;  border-bottom: 1px solid #ccc; font:italic 14px/1.2em Arial, Helvetica, sans-serif;}
                    table td {padding: 6px 6px 5px; border:0;vertical-align: top;line-height: 1.2em; border-bottom: 1px solid #ccc;}
                    table tr:nth-child(even) {background-color: #f7f7f7;}
                    #h {padding:10px 0; border-bottom:1px solid #ccc; margin:0 10px 0}
                    #c {padding:10px 10px 10px;}
                    #f {padding:10px 0; border-top:1px solid #ccc; margin:0 10px 0}
                </style>
                '.$message.'
                </body>
                </html> 
            ';

            $to = Email::mime_header_encode($to, 'utf-8', 'utf-8'). ' <' . $to . '>';
            $subject = Email::mime_header_encode($subject, 'utf-8', 'utf-8');
            $from =  Email::mime_header_encode('Khersondaily', 'utf-8', 'utf-8') . ' <' . $from . '>';
            $headers = "Mime-Version: 1.0\r\n";
            $headers .= "From: $from\r\n";
            $headers .= "Reply-To: $from\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= 'X-Mailer: PHP/' . phpversion();
            return mail($to, $subject, $message, $headers);   
        }

        public static function mime_header_encode($str, $data_charset, $send_charset) {
            if($data_charset != $send_charset) {
                $str = iconv($data_charset, $send_charset.'//IGNORE', $str);
            }
            return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
        }
        
    }