<?php
    /*
     *      Class with ONLY frontend functionality
     */
    class Frontend {

        /** 
         * Frontend pagination
         * @param  [int] $count   [count of elements]
         * @param  [int] $limit   [limit of elements per page]
         * @param  [int] $current [current number of page]
         * @return [string]       [pagination HTML]
         */
        public static function pager( $count, $limit, $current ) {
            // Count pages
            $pages  = ceil($count / $limit);
            // Check if count pages less then 2
            if($pages < 2) { return ""; }
            // Generate link
            $link = Arr::get($_SERVER, 'REQUEST_URI');
            $uri = explode('?', $link);
            $link = $uri['0'];
            $get = Arr::get($uri, '1', '');
            if( $current > 1 ) {
                $link = str_replace( '/page/'.$current, '', $link );
            }
            // Set border
            $border = 10;
            $pager = '<div class="tac paginWrapp">';
            // If count of pages less then $border
            if($pages <= $border) {
                for($i = 1; $i <= $pages; $i++) {
                    $add = '';
                    if( $i > 1 ) { $add = '/page/'.$i; }
                    $curURI = $link.$add.($get ? '?'.$get : '');
                    if($i == $current) {
                        $pager .= '<a href="'.$curURI.'" class="curr"><span>'.$i.'</span></a>';
                    } else {
                        $pager .= '<a href="'.$curURI.'">'.$i.'</a>';
                    }
                }
            // If count of pages more then $border
            } else if($pages > $border) {
                switch($current) {
                    // If current number of page less then 5
                    case ($current < 5):
                        for($i = 1; $i <= 5; $i++) {
                            $add = '';
                            if( $i > 1 ) { $add = '/page/'.$i; }
                            $curURI = $link.$add.($get ? '?'.$get : '');
                            if($i == $current) {
                                $pager .= '<a href="'.$curURI.'" class="curr"><span>'.$i.'</span></a>';
                            } else {
                                $pager .= '<a href="'.$curURI.'"><span>'.$i.'</span></a>';
                            }
                        }
                        $pager .= '<p>...</p>';
                        $pager .= '<a href="'.$link.'/page/'.$pages.($get ? '?'.$get : '').'"><span>'.$pages.'</span></a>';
                        $pager .= '<a href="'.$link.'/page/'.($current+1).($get ? '?'.$get : '').'"><span>></span></a>';
                        break;
                    // If current number of page bigger then count of pages minus 5
                    case ($current > $pages - 5):
                        $pager .= '<a href="'.$link.'/page/'.($current-1).($get ? '?'.$get : '').'"><span><</span></a>';
                        $pager .= '<a href="'.$link.($get ? '?'.$get : '').'"><span>1</span></a>';
                        $pager .= '<p>...</p>';
                        for($i = $pages - 5; $i <= $pages; $i++) {
                            $add = '';
                            if( $i > 1 ) { $add = '/page/'.$i; }
                            $curURI = $link.$add.($get ? '?'.$get : '');
                            if($i == $current) {
                                $pager .= '<a href="'.$curURI.'" class="curr"><span>'.$i.'</span></a>';
                            } else {
                                $pager .= '<a href="'.$curURI.'"><span>'.$i.'</span></a>';
                            }
                        }
                        break;
                    // If current page number bigger then 4 and less then count of pages minus 5
                    default:
                        $pager .= '<a href="'.$link.'/page/'.($current-1).($get ? '?'.$get : '').'"><span><</span></a>';
                        $pager .= '<a href="'.$link.($get ? '?'.$get : '').'"><span>1</span></a>';
                        $pager .= '<p>...</p>';
                        for($i = $current - 2; $i <= $current + 2; $i++) {
                            $add = '';
                            if( $i > 1 ) { $add = '/page/'.$i; }
                            $curURI = $link.$add.($get ? '?'.$get : '');
                            if($i == $current) {
                                $pager .= '<a href="'.$curURI.'" class="curr"><span>'.$i.'</span></a>';
                            } else {
                                $pager .= '<a href="'.$curURI.'"><span>'.$i.'</span></a>';
                            }
                        }
                        $pager .= '<p>...</p>';
                        $pager .= '<a href="'.$link.'/page/'.$pages.($get ? '?'.$get : '').'"><span>'.$pages.'</span></a>';
                        $pager .= '<a href="'.$link.'/page/'.($current+1).($get ? '?'.$get : '').'"><span>></span></a>';
                        break;
                }
            }
            $pager .= '</div>';
            return $pager;            
        }

    }