<?php

class News
{


    public function getNews($id)
    {
        $news = Builder::factory('news')
            ->select(array(
                '`news_images`.`image`',
                '`news`.*'
            ))
            ->join('news_images')
            ->on('news_images.main', '=', 1)
            ->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->where('parent_id', '=', $id)
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->limit(4)
            ->find_all();
        return $news;
    }

    public function getVideoAndPhoto()
    {
        $video = Builder::factory('videos')
            ->select(array(

                '`videos`.*'
            ))
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->limit(3)
            ->find_all();
        $photo = Builder::factory('gallery_albums')
            ->select(array(
                '`gallery_images`.`image`',
                '`gallery_albums`.*'
            ))
            ->join('gallery_images')
            ->on('gallery_images.parent_id', '=', Builder::expr('`gallery_albums`.`id`'))
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->limit(1)
            ->find();
        $arr = array('video' => $video, 'photo' => $photo);
        return $arr;
    }

    public function getVideo()
    {
        $video = Builder::factory('videos')
            ->select(array(

                '`videos`.*'
            ))
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->limit(4)
            ->find_all();

        $arr = array('video' => $video);
        return $arr;
    }

}