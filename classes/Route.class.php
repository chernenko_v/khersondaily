<?php
    class Route {
        
        static $_instance;

        static function factory() {
            if(self::$_instance == NULL) { self::$_instance = new self(); }
            return self::$_instance;
        }

        private $_reserve = array(
            'page', 'per_page', 'filter', 'sort', 'hash'
        );

        private $_params;
        private $_controller;
        private $_action;


        public static function params() {
            return Route::factory()->getParams();
        }

        public static function param( $key ) {
            return Route::factory()->getParam( $key );
        }

        public static function controller() {
            return Route::factory()->getController();
        }

        public static function action() {
            return Route::factory()->getAction();
        }

        public function getParams() {
            return $this->_params;
        }

        public function setParam( $key, $value ) {
            $this->_params[$key] = $value;
        }

        public function getParam( $key ) {
            return Arr::get($this->_params, $key);
        }

        public function getController() {
            return $this->_controller;
        }

        public function getAction() {
            return $this->_action;
        }

        public function parse() {
            $_arr     = explode('?', $_SERVER['REQUEST_URI']);

            // Parse part of URL after "?"
            if(count($_arr) > 1) {
                $uri  = str_replace('?'.$_arr[0], '', $_SERVER['REQUEST_URI']);
                $arr  = explode('&', $_arr[1]);
                foreach($arr AS $_a) {
                    $g   = urldecode($_a);
                    $g   = strip_tags($g);
                    $g   = stripslashes($g);
                    $g   = trim($g);
                    $get = explode('=', $g);
                    $_GET[$get[0]] = trim( addslashes( strip_tags( $get[1] ) ) );
                }
            }

            $_arr     = explode("/", $_arr[0]);
            $_perem   = preg_replace('/.*\//', '', $_arr[1]);
            $_string  = preg_replace('/\..*/', '', $_perem);
            
            // Removing "/" in the end of URL
            if(end($_arr) == '' and $_string != '' and $_string != 'index') {
                $t = array_pop($_arr);
                header('Location: '.implode('/', $_arr), true, 301);
                exit;
            }
            
            // Parse main part of URL
            $ctrl = ucfirst( strtolower( $_string ) ) . 'Controller';
            if ( $_string == '' OR $_string == 'index' ) {
                $this->_controller = 'content';
                $this->_params['alias'] = 'index';
                require_once HOST . '/ctrl/ContentController.php';
                $controller = new ContentController();
                return $this->setAction( $controller, 'indexAction' );
            }
            
            if( !file_exists( HOST . '/ctrl/' . $ctrl . '.php' ) ) {
                // If reserved catalog actions
                if( in_array( $_string, array( 'new', 'popular', 'sale', 'viewed' ) ) ) {
                    require_once HOST . '/ctrl/CatalogController.php';
                    if( !class_exists( 'CatalogController' ) ) {
                        if ( PRODUCTION ) { return conf::error(); }
                        die( 'Class ' . $ctrl . ' does not exist!' );
                    }
                    if( isset( $_arr[2] ) ) {
                        $this->setParams( $_arr, 2 );
                    }
                    if( !(int) Route::param('page') ) {
                        $this->_params['page'] = 1;
                    }
                    $this->_controller = 'catalog';
                    $controller = new CatalogController();
                    $method = $_string . 'Action';
                    if( !method_exists( $controller, $method ) ) {
                        return conf::error();
                    }
                    return $this->setAction( $controller, $method );
                }
                // Check for content page
                if ( Builder::factory('content')->where('status', '=', 1)->where('alias', '=', $_string )->count_all() ) {
                    $this->_controller = 'content';
                    $this->_params['alias'] = $_string;
                    require_once HOST . '/ctrl/ContentController.php';
                    $controller = new ContentController();
                    $method = $_string . 'Action';
                    if( method_exists( $controller, $method ) ) {
                        return $this->setAction( $controller, $method );
                    }
                    return $this->setAction( $controller, 'indexAction' );
                } else {
                    return conf::error();
                }
            }

            require_once HOST . '/ctrl/' . $ctrl . '.php';
            if( !class_exists( $ctrl ) ) {
                if ( PRODUCTION ) { return conf::error(); }
                die( 'Class ' . $ctrl . ' does not exist!' );
            }

            $this->_controller = $_string;
            $controller = new $ctrl();


            $count = count($_arr) - 1;


            if($count == 1) {
                return $this->setAction( $controller, 'indexAction' );
            }

            if( in_array( $_arr[2], $this->_reserve ) ) {
                $this->setParams( $_arr, 2 );
                return $this->setAction( $controller, 'indexAction' );
            }

            if( method_exists( $controller, $_arr[2] . 'Action' ) ) {
                if( isset( $_arr[3] ) ) {
                    $this->setParams( $_arr, 3 );
                }
                return $this->setAction( $controller, $_arr[2] . 'Action' );
            }

            if( method_exists( $controller, 'innerAction' ) ) {
                if( isset( $_arr[3] ) ) {
                    $this->setParams( $_arr, 3 );
                }
                $this->_params['alias'] = $_arr[2];
                return $this->setAction( $controller, 'innerAction' );
            }

            // NEWS
            if (Route::controller() == 'news') {
                if( isset( $_arr[3] ) ) {
                    $this->setParams( $_arr, 3 );
                }
                if( !(int) Route::param('page') ) {
                    $this->_params['page'] = 1;
                }
                $this->_params['alias'] = $_arr[2];
                //itemAction
                $item = Builder::factory('news')
                    ->select(array('parent_id','id'))
                    ->where('status','=',1)
                    ->where('alias','=',Route::param('alias'))
                    ->find();
                if($item) {
                    // Count of news
                    $this->_params['group'] = $item->parent_id;
                    $this->_params['id'] = $item->id;
                    return $this->setAction( $controller, 'itemAction' );
                }
                //groupAction
                $group = Builder::factory('news_tree')
                    ->select(array(
                        'id',
                        'title',
                        'keywords',
                        'description'
                    ))
                    ->where('alias','=',Route::param('alias'))
                    ->find();
                if($group) {
                    conf::set( 'title', $group->title );
                    conf::set( 'keywords', $group->keywords );
                    conf::set( 'description', $group->description );
                    $this->_params['group'] = $group->id;
                    return $this->setAction($controller,'groupAction');
                }

            }

            // PHOTOREPORT
            if (Route::controller() == 'photoreport') {

                if( isset( $_arr[2] ) ) {
                    $this->setParams( $_arr, 3 );
                    $this->_params['alias'] = $_arr[2];
                }
                if( !(int) Route::param('page') ) {
                    $this->_params['page'] = 1;
                }

                $this->_params['alias'] = $_arr[2];
                //itemAction
                $item = Builder::factory('news')
                    ->select(array('parent_id','id'))
                    ->where('status','=',1)
                    ->where('show_photoreport','=',1)
                    ->where('alias','=',Route::param('alias'))
                    ->find();
                if($item) {
                    // Count of news
                    $this->_params['group'] = $item->parent_id;
                    $this->_params['id'] = $item->id;
                    return $this->setAction( $controller, 'itemAction' );
                }
                //groupAction
                $group = Builder::factory('news_tree')
                    ->select(array(
                        'id',
                        'title',
                        'keywords',
                        'description'
                    ))
                    ->where('alias','=',Route::param('alias'))
                    ->find();
                if($group) {
                    conf::set( 'title', $group->title );
                    conf::set( 'keywords', $group->keywords );
                    conf::set( 'description', $group->description );
                    $this->_params['group'] = $group->id;
                    return $this->setAction($controller,'groupAction');
                }

            }

            /*else if(Route::controller() == 'gallery') {
                if( isset( $_arr[3] ) ) {
                    $this->setParams( $_arr, 3 );
                }
                if( !(int) Route::param('page') ) {
                    $this->_params['page'] = 1;
                }
                $this->_params['alias'] = $_arr[2];
                //albums
                //images
            }*/
            if (Route::controller() == 'rss') {
                if( isset( $_arr[3] ) ) {
                    $this->setParams( $_arr, 3 );
                }
                if( !(int) Route::param('page') ) {
                    $this->_params['page'] = 1;
                }
                $this->_params['alias'] = $_arr[2];
                //itemAction
                $alias = explode('.', Route::param('alias'));

                $item = Builder::factory('news')
                    ->select(array('parent_id','id'))
                    ->where('status','=',1)
                    ->where('alias','=', $alias[0])
                    ->find();
                if($item) {
                    // Count of news
                    $this->_params['group'] = $item->parent_id;
                    $this->_params['id'] = $item->id;
                    return $this->setAction( $controller, 'itemAction' );
                }
                //groupAction
                $group = Builder::factory('news_tree')
                    ->select(array(
                        'id',
                        'title',
                        'keywords',
                        'description'
                    ))
                    ->where('alias','=',$alias[0])
                    ->find();
                if($group) {
                    conf::set( 'title', $group->title );
                    conf::set( 'keywords', $group->keywords );
                    conf::set( 'description', $group->description );
                    $this->_params['group'] = $group->id;
                    return $this->setAction($controller,'groupAction');
                }

            }
            return conf::error();
        }

        private function setParams( $_arr, $startNumber ) {
            for ($i = $startNumber; $i < count($_arr); $i++) {
                $y = $i+1;
                $this->_params[$_arr[$i]] = $_arr[$y];
                $i++;
            }
        }

        private function setAction( $controller, $action ) {
            if( !method_exists( $controller, $action ) ) {
                if ( PRODUCTION ) { return conf::error(); }
                die( 'Method ' . $action . ' in class ' . $controller . ' does not exist!' );
            }
            $this->_action = str_replace( 'Action', '', $action );
            return $controller->$action();
        }

        public function parseBackend() {
            $_arr = explode('?', $_SERVER['REQUEST_URI']);
            if(count($_arr) > 1) {
                $arr  = explode('&', $_arr[1]);
                foreach($arr AS $_a) {
                    $g   = urldecode($_a);
                    $g   = strip_tags($g);
                    $g   = stripslashes($g);
                    $g   = trim($g);
                    $get = explode('=', $g);
                    $_GET[$get[0]] = $get[1];
                }
            }

            $request_uri = $_arr[0];
            $request_uri = preg_replace('/^\/|ctrl\/|action\/|\/$/', '', $request_uri);
            if ($request_uri !== '') {
                foreach(system::_cfg('route_alias', array()) as $k => $v) {
                    if (is_string($v)) {
                        if ($request_uri == $k) {
                            $request_uri = $v;
                            break;
                        }
                    } else {
                        foreach ($v as $rule=>$vars) {
                            foreach($vars as $key=>$value) {
                                $k = str_replace('$'.($key+1), '('.$value.')', $k);
                            }
                            if (preg_match('/^'.$k.'$/', $request_uri)) {
                                $request_uri = preg_replace('/'.$k.'/', $rule, $request_uri);
                                break;
                            }
                        }                               
                    }
                }
            }

            $_arr = explode('/', $request_uri);
            $this->_controller = Arr::get($_arr, 0, 'index');
            $this->_action = Arr::get($_arr, 1, 'index');
            if( isset( $_arr[2] ) ) {
                $this->setParams( $_arr, 2 );
            }

            $this->_params[ 'page' ] = Arr::get( $this->_params, 'page', 1 );
        }

    }