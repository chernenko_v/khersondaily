<?php
    class rss{
        public static function get_content(){
            $result = Builder::factory('news')
                ->select(array(
                    '`news_images`.`image`',
                    '`news`.*'
                ))
                ->join('news_images')
                ->on('news_images.main', '=', 1)
                ->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
                ->where('status', '=', 1)
                ->where('rss', '=', 0)
                ->where('date', '<=', time())
                /*->where('parent_id', '=', Route::param('group'))*/
                ->order_by('date','DESC')
                ->limit(10)
                ->find_all();

            // выполняем запрос + при необходимости выводим сам запрос
            /*$result=mysql::query($_sql,0);*/
            // выполняем tpl
            return system::show_tpl(array('news'=>$result),'Rss/rss.php');
        }
    }
?>