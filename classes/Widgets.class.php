<?php
/**
 *  Class that helps with widgets on the site
 */
class Widgets {

    static $_instance; // Constant that consists self class

    public $_data = array(); // Array of called widgets
    public $_tree = array(); // Only for catalog menus on footer and header. Minus one query

    // Instance method
    static function factory() {
        if(self::$_instance == NULL) { self::$_instance = new self(); }
        return self::$_instance;
    }

    /**
     *  Get widget
     *  @param  [string] $name  [Name of template file]
     *  @param  [array]  $array [Array with data -> go to template]
     *  @return [string]        [Widget HTML]
     */
    public static function get( $name, $array = array() ) {
        $token = Profiler::start('Profiler', 'Widget '.$name);
        $w = Widgets::factory();
        if( isset( $w->_data[ $name ] ) ) {
            return $w->_data[ $name ];
        }
        if( method_exists( $w, $name ) ) {
            return $w->$name( $array );
        }
        Profiler::stop($token);
        return $w->common( $name, $array );
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     *  @param  [string] $name  [Name of template file]
     *  @param  [array]  $array [Array with data -> go to template]
     *  @return [string]        [Widget HTML or NULL if template doesn't exist]
     */
    public function common( $name, $array ) {
        $filename = ucfirst($name);
        if( file_exists(HOST.APPLICATION.'/tpl/Parts/Widgets/'.$filename.'.php') ) {
            return $this->_data[$name] = support::widget($array, $filename);
        }
        if( file_exists(HOST.APPLICATION.'/tpl/Parts/'.$filename.'.php') ) {
            return $this->_data[$name] = support::part($array, $filename);
        }
        return NULL;
    }


    /*public function hiddenData() {
        $cart = Cart::factory()->get_list_for_basket();
        return $this->_data['hiddenData'] = support::widget(array( 'cart' => $cart ), 'HiddenData');
    }*/


    public function slider() {
        $result = Builder::factory('slider')
            ->select(array('name','url','description','image'))
            ->where('status', '=', 1)
            ->order_by('sort')
            ->limit(conf::get('limit_sliders'))
            ->find_all();
        if( !count( $result ) ) {
            return $this->_data['slider'] = '';
        }
        return $this->_data['slider'] = support::widget(array( 'result' => $result ), 'Slider');
    }

    public function rightnews() {
        $result_news = Builder::factory('news')
            ->select()
            ->where('status', '=', 1)
            ->where('parent_id', '!=', 7)
            ->where('news.date', '<=', time())
            ->order_by('news.date','DESC')
            //->limit(conf::get('limit_articles'))
            ->limit(75)
            ->find_all();
        $result_photoreport = Builder::factory('photoreport')
            ->select()
            ->where('status', '=', 1)
            ->where('parent_id', '!=', 7)
            ->where('photoreport.show_photoreport' , '=' , 1)
            ->where('photoreport.date', '<=', time())
            ->order_by('photoreport.date','DESC')
            //->limit(conf::get('limit_articles'))
            ->limit(75)
            ->find_all();

        $result = array_merge($result_news,$result_photoreport);
        function dateSort( $a, $b ) {
            return $a->date == $b->date ? 0 : ( $a->date > $b->date ) ? -1 : 1;
        }
        usort( $result, 'dateSort' );

        return $this->_data['rightnews'] = support::widget(array( 'result' => $result), 'Rightnews');
    }

    public function news_tree() {
        $result = Builder::factory('news_tree')
            ->select(array('`news_tree`.*'))
            ->where('status', '=', 1)
            ->order_by('sort')
            ->find_all();
        if( !count( $result ) ) {
            return $this->_data['news_tree'] = '';
        }
        return $this->_data['news_tree'] = support::widget(array( 'result' => $result ), 'News_tree');
    }

    public function news() {

        $obj = Builder::factory('news')
            ->select(array(
                '`news_images`.`image`',
                '`news`.*'
            ))
            ->leftJoin('news_images')
            ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->on('news_images.main', '=', 1, 'AND')
            // ->on('news_images.main', '=', 1)
            // ->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->where('status', '=', 1)
            ->where('parent_id', '!=', 7)
            ->where('news.date', '<=', time())
            ->order_by('news.date','DESC')
            ->limit(conf::get('limit_articles'))
            ->find_all();

        if( !count($obj) ) {
            return $this->_data['news'] = '';
        }
        //Определям сколько галерей выйдет, если по одной нужно на каждые три новости.
        $gallery_num = floor(count($obj)/conf::get('show_gallery'));
        if($gallery_num > 0) {
            $obj_gal = Builder::factory('gallery_albums')
                ->select(array('`gallery_albums`.`name`',
                    '`gallery_albums`.`alias`',
                    '`gallery_albums`.`id`',
                    '`gallery_images`.`image`'))
                ->join('gallery_images')
                ->on('gallery_images.parent_id','=',Builder::expr('`gallery_albums`.`id`'))
                ->on('gallery_images.main', '=', 1)
                ->where('status', '=', 1)
                ->order_by('date','DESC')
                ->limit($gallery_num)
                ->find_all();
        }

        return $this->_data['news'] = support::widget(array('obj' => $obj,
            'gallery' => $obj_gal),
            'News');
    }


    public function articles() {
        $result = Builder::factory('articles')->where('status', '=', 1)->order_by('id', 'DESC')->limit( conf::get('limit_articles_main_page') )->find_all();
        if( !count( $result ) ) {
            return $this->_data['articles'] = '';
        }
        return $this->_data['articles'] = support::widget(array( 'result' => $result ), 'Articles');
    }


    public function info() {
        $result = Builder::factory('content')
            ->where('status', '=', 1)
            ->where('id', 'IN', array( 5, 6, 7, 8 ))
            ->order_by('sort')
            ->find_all();
        if( !count( $result ) ) {
            return $this->_data['info'] = '';
        }
        return $this->_data['info'] = support::widget(array( 'result' => $result ), 'Info');
    }


    /*public function headerCart() {
        $contentMenu = Builder::factory('sitemenu')->where('status', '=', 1)->order_by('sort')->find_all();
        return $this->_data['headerCart'] = support::widget(array( 'contentMenu' => $contentMenu ), 'HeaderCart');
    }*/


    public function footer() {
        if( APPLICATION ) {
            return $this->footerBackend();
        }

        /*$contentMenu = Builder::factory('sitemenu')->where('status', '=', 1)->order_by('sort')->find_all();*/
        return $this->_data['footer'] = support::widget(array(), 'Footer');
    }


    public function header() {
        if( APPLICATION ) {
            return $this->headerBackend();
        }

        $contentMenu = Builder::factory('sitemenu')
            ->select(array('id','parent_id','url','name'))
            ->where( 'status', '=', 1 )
            ->order_by('sort')
            ->find_all();

        $final = array();

        //Выберем для каждой категории 6 последних новостей
        if($contentMenu) {

            /*  $_sql = ''; Запрос, который ложил сервер
              foreach ($contentMenu as $id) {
                  $_sql .= '
                              (SELECT news.*,
                                      news_images.image
                              FROM news
                              LEFT JOIN news_images
                              ON news_images.news_id = news.id
                                  AND news_images.main = 1
                              WHERE news.parent_id = '.$id->parent_id.'
                              AND news.status = 1
                              AND news.date <= '.time().'
                              ORDER BY news.date DESC LIMIT 6)
                              UNION';
              }
              //Отсечь последний UNION
              $_sql = preg_replace("/UNION$/u", "", $_sql);
              $result = mysql::query($_sql);

              $i = 0;
              foreach ($contentMenu as $key => $obj) {
                  foreach ($result as $ind => $news) {
                      if($obj->parent_id == $news->parent_id) {
                          $final[$obj->parent_id][$i++] = $result[$ind];
                      }
                  }
                  $i = 0;
              }*/
            foreach($contentMenu as $id){
				if($id->parent_id == 0) {
					continue;
				}
				
                $result = Builder::factory('news')
                    ->select(array(
                        '`news_images`.`image`',
                        '`news`.`images`','`news`.`name`','`news`.`alias`','`news`.`parent_id`'
                    ))
                    ->leftJoin('news_images')
                    ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
                    ->on('news_images.main', '=', 1, 'AND')
                    ->where('status', '=', 1)
                    ->where('news.parent_id', '=', $id->parent_id)
                    ->where('news.date', '<=', time())
                    ->order_by('news.date','DESC')
                    ->limit(6)
                    ->find_all();
                foreach ($result as $ind => $news) {

                    if($id->parent_id == $news->parent_id) {
                        $final[$id->parent_id][] = $news;
                    }
                }

            }

        }

        return $this->_data['header'] = support::widget(array(
            'contentMenu' => $contentMenu,
            'menuNews' => $final
        ),
            'Header');
    }


    public function sidebar() {
        if( APPLICATION ) {
            return $this->sidebarBackend();
        }
    }

    public function crumbs() {
        if( APPLICATION ) {
            return $this->crumbsBackend();
        }
    }

    /*----NEW-------------*/

    public function People() {
        $obj = Builder::factory('news')
            ->select(array(
                '`news_images`.`image`',
                '`news`.*'
            ))
            ->leftJoin('news_images')
            ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->on('news_images.main', '=', 1, 'AND')
            //->on('news_images.main', '=', 1)
            //->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->where('status', '=', 1)
            ->where('parent_id', '=', 7)
            ->where('news.date', '<=', time())
            ->order_by('news.date','DESC')
            ->limit(conf::get('limit_people'))
            ->find_all();
        if( !count( $obj ) || !$obj ) {
            return $this->_data['people'] = '';
        }
        return $this->_data['people'] = support::widget(array( 'obj' => $obj ), 'People');
    }

    public function topnews() {

        $obj_top = Builder::factory('news')
            ->select(array(
                '`news_images`.`image`',
                '`news`.*'
            ))
            ->leftJoin('news_images')
            ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->on('news_images.main', '=', 1, 'AND')
            //->on('news_images.main', '=', 1)
            // ->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->where('status', '=', 1)
            //->where('news.date', '<=', time())
            ->where('read', '=', 1)
            ->order_by('news.date','DESC')
            ->limit(conf::get('limit_top'))
            ->find_all();

        //Выбираем новости по большему кол-ву комментариев за 7 дней
        $mysql_news_id = 'SELECT    news_comments.news_id, 
                                        count(news_comments.news_id) as comm,
                                        max(news_comments.date) as nd,
                                        news.alias,
                                        news.name,
										news.images,
                                        news_images.image as image,
                                        news.text
                            FROM news_comments
                            LEFT JOIN users
                            ON users.id = news_comments.user_id
                            AND users.status = 1
                            LEFT JOIN news
                            ON  news.id = news_comments.news_id
                            AND news.status = 1
                            LEFT JOIN news_images
                            ON news_images.news_id = news.id 
                            AND news_images.main = 1
                            WHERE news_comments.date <= "'.time().'"
                            AND news.date <= "'.time().'"
                            AND news_comments.status = 1
                            GROUP BY news_comments.news_id
                            ORDER BY nd DESC
                            LIMIT '.conf::get('limit_top');
// AND news_comments.date > "'.(time()-45*24*60*60).'"
// ORDER BY comm


        $obj_news = mysql::query($mysql_news_id);

        if( !count($obj_top) AND !count($obj_news) ) {
            return $this->_data['topnews'] = '';
        }

        return $this->_data['topnews'] = support::widget(
            array('top' => $obj_top,
                'comm' => $obj_news ),
            'TopNews');
    }

    public function citation() {
        $obj = Builder::factory('citations')
            ->select(array('image','name','job','text'))
            ->where('status', '=', 1)
            ->where('show_date', '=', date('Y-m-d'))
            ->limit(1)
            ->find();
        $wisdow = Builder::factory('wisdow')
            ->select(array('image','name','job','text'))
            ->where('status', '=', 1)
            ->where('show_date', '<=', date('Y-m-d'))
            ->limit(1)
            ->find();

        if( !count( $obj ) || !$obj) {
            return $this->_data['citation'] = '';
        }
        return $this->_date['citation'] = support::widget(
            array('obj' => $obj, 'wisdow'=>$wisdow),
            'Citation');
    }

    public function video() {
        $obj = Builder::factory('videos')
            ->select(array('image','title','link', 'facebook_code'))
            ->where('status', '=', 1)
            ->order_by('date', 'DESC')
            ->limit(conf::get('limit_videos'))
            ->find_all();
        if( !count( $obj ) || !$obj ) {
            return $this->_data['video'] = '';
        }
        return $this->_date['video'] = support::widget(
            array('obj' => $obj),
            'video');
    }

    public function mustRead($array = []) {
        $id = Arr::get($array, 'id', null);
        $parent_id = Arr::get($array, 'parent_id', null);
        $keywords = trim(Arr::get($array, 'keywords', null));

        if(!$id || !$parent_id){
            return $this->_data['mustRead'] = '';
        }
        $keywords_query = null;
        if($keywords){
            $keywords = str_replace(',', ' ', $keywords);
            $keywords = explode(' ', $keywords);
            if(count($keywords)){
                $query = [];
                foreach($keywords as $keyword){
                    if($keyword){
                        $query[] = '`news`.`keywords` LIKE "%'.$keyword.'%"';
                    }
                }
                if(count($query)){
                    $keywords_query = '('.implode(' OR ', $query).')';
                }
            }
        }

        $obj = Builder::factory('news')
            ->select(['`news_images`.`image`', '`news`.*'])
            ->join('news_images')
            ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->on('news_images.main', '=', 1, 'AND')
            ->where('parent_id','=', $parent_id)
            ->where('id','<>',$id)
            ->where('status', '=', 1)
            ->where('news.date', '<=', time())
            ->where_add($keywords_query)
            ->order_by('id','DESC')
            ->limit('4')
            ->find_all();

        if(!count($obj)){
            return $this->_data['mustRead'] = '';
        }
        return $this->_data['mustRead'] = support::widget(['obj' => $obj], 'mustRead');
    }

    public function infographics() {
        $image = Builder::factory('content')
            ->select(array('image'))
            ->where('status','=',1)
            ->where('alias','=','infografika')
            ->limit(1)
            ->find();
        if( !count( $image ) || !$image ) {
            return $this->_data['infographics'] = '';
        }
        return $this->_date['infographics'] = support::widget(
            array('image' => $image->image),
            'Infographics');
    }


    public function photoreport() {
        $obj = Builder::factory('photoreport')
            ->select(array(
                '`photoreport_images`.`image`',
                '`photoreport`.*'
            ))
            ->leftJoin('photoreport_images')
            ->on('photoreport_images.photoreport_id', '=', Builder::expr('`photoreport`.`id`'))
            ->on('photoreport_images.main', '=', 1, 'AND')
            // ->on('news_images.main', '=', 1)
            // ->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->where('status', '=', 1)
            ->where('parent_id', '!=', 7)
            ->where('photoreport.date', '<=', time())
            ->order_by('photoreport.date','DESC')
            ->find_all();

        if( !count($obj) ) {
            return $this->_data['photoreport'] = '';
        }


        return $this->_data['photoreport'] = support::widget(array('obj' => $obj),
            'Photoreport');

    }


    ##################### BACKEND ONLY ##########################
    public function sidebarBackend() {
        $result = Builder::factory('menu')->where( 'status', '=', 1 )->order_by( 'sort' )->find_all();
        $arr = array();
        foreach( $result AS $obj ) {
            $arr[ $obj->id_parent ][] = $obj;
        }
        return $this->_data['sidebar'] = support::widget(array( 'result' => $arr ), 'Sidebar');
    }


    public function crumbsBackend() {
        //$count_orders = Builder::factory( 'orders' )->where( 'status', '=', 0 )->count_all();
        $count_comments = Builder::factory( 'news_comments' )->where( 'status', '=', 0 )->count_all();
        return $this->_data['crumbs'] = support::widget(array( 'cc' => $count_comments ), 'Crumbs');
    }


    public function footerBackend() {
        return $this->_data['footer'] = support::widget(array(), 'Footer');
    }


    public function headerBackend() {
        return $this->_data['header'] = support::widget(array(), 'Header');
    }


    public function headerContacts() {
        $contacts = Builder::factory('contacts')->where('status', '=', 0)->limit(5)->find_all();
        $cContacts = Builder::factory('contacts')->where('status', '=', 0)->count_all();
        return $this->_data['headerContacts'] = support::widget(array(
            'contacts' => $contacts,
            'cContacts' => $cContacts,
        ), 'HeaderContacts');
    }


    public function headerNew() {
        $count = Builder::factory('log')->where('deleted', '=', 0)->count_all();
        $result = Builder::factory('log')->where('deleted', '=', 0)->order_by('created_at', 'DESC')->limit(7)->find_all();
        return $this->_data['headerNew'] = support::widget(array(
            'count' => $count,
            'result' => $result,
        ), 'HeaderNew');
    }

    public function albums() {

        $list = Builder::factory('gallery_albums')
            ->select(array('name','alias'))
            ->where('status','=',1)
            ->where('alias','!=',Route::param('alias'))
            ->order_by('id','desc')
            ->limit(5)
            ->find_all();

        return $this->_data['albums'] = support::widget(array(
            'list' => $list,
        ), 'Albums');
    }

    public function seeAlso() {

        $list = Builder::factory('news')
            ->select(array(
                '`news_images`.`image`',
                '`news`.*'
            ))
            ->join('news_images')
            ->on('news_images.main', '=', 1)
            ->where('news_images.news_id', '=', Builder::expr('`news`.`id`'))
            ->where('status', '=', 1)
            ->where('news.date', '<=', time())
            ->order_by('news.date','DESC')
            ->limit('8')
            ->find_all();

        return $this->_data['seeAlso'] = support::widget(array(
            'list' => $list,
        ), 'SeeAlso');
    }

}