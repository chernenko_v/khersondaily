<?php

/**
 * Класс с различными дополнительными методами
 */
class bLibrary {
	
	/**
	 * Возвращает URL изображения из кеша с нужными размерами.
	 * Если в кеше такого изображения нет, то оно создается и ложиться туда.
	 * Если вообще на сервере HOST нет такого изображения, то производится попытка загрзить изображение с удаленного сервера SITE_PATH.
	 * Если и это не вышло, возвращается URL исходного изображения
	 *
	 * @param   string   $file		адрес исходного изображения относительно корня сайта
	 * @param   integer  $width		ширина изображения в кеше
	 * @param   integer  $height	высота изображения в кеше
	 * @return  string
	 * @uses    bLibrary::getImageFromCache('/images/news/original/1234567890.jpg', 200, 100);
	 */
	public static function getImageFromCache($file, $width, $height) {
		$old_file = HOST . $file;
		
		//Проверка на удаленном сервере. На рабочем сайте можно закомментировать
		/*if (!file_exists($old_file)) {
			$file_content = file_get_contents(SITE_PATH . $file);
			if ($file_content) {
				$path = '';
				$directories = explode('/', dirname($file));
				foreach ($directories as $directory) {
					if ($directory != '') {
						$path = $path . '/' . $directory;
						if (!is_dir(HOST . $path)) {
							@mkdir(HOST . $path, 0777);
						}
					}
				}
				file_put_contents($old_file, $file_content);
			}
		}*/
		
		if (file_exists($old_file)) {
			$extension = pathinfo($file, PATHINFO_EXTENSION);
			$new_file = 'cache' . mb_substr($file, 0, mb_strpos($file, '.', 0, 'utf-8'), 'utf-8') . '-' . $width . 'x' . $height . '.' . $extension;
			if (!file_exists(HOST . '/' . $new_file)) {
				$path = '';
				$directories = explode('/', dirname($new_file));
				foreach ($directories as $directory) {
					if ($directory != '') {
						$path = $path . '/' . $directory;
						if (!is_dir(HOST . $path)) {
							@mkdir(HOST . $path, 0777);
						}
					}
				}

				$image = Image::factory($old_file);
				$image->resize($width, $height, Image::INVERSE);
				$image->crop($width, $height);
				$image->save(HOST . '/' . $new_file);
			}

			$file = '/' . $new_file;
		}
		
		return MAIN_PATH . $file;
	}
	
}