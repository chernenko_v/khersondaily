<?php
    
    class conf {

        static function set( $parameter, $value = NULL ) {
            configuration::instance()->set( $parameter, $value );
        }

        static function get( $parameter, $pos = NULL ) {
            return configuration::instance()->get( $parameter, $pos );
        }

        static function parse() {
            return configuration::instance();
        }

        static function error() {
            return configuration::instance()->error();
        }

        static function bread( $link, $name ) {
            $bread = conf::get( 'bread' );
            conf::set( 'bread', $bread . '<li class="current"><a title="" href="'.$link.'"> '.$name.'</a></li>' );
        }

        static function breadcrumbs( $name, $link = NULL ) {
            $bread = conf::get( 'breadcrumbs' );
            if( !$bread ) {
                $bread = '<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="/">Главная</a></span>';
            }
            if( !$link ) {
                conf::set( 'breadcrumbs', $bread . '<span typeof="v:Breadcrumb">'.$name.'</span>' );
            } else {
                conf::set( 'breadcrumbs', $bread . '<span typeof="v:Breadcrumb"><a href="'.$link.'" rel="v:url" property="v:title">'.$name.'</a></span>' );
            }
        }

        static function generateParentBreadcrumbs( $id, $table, $parentAlias, $pre = '/' ) {
            $bread = conf::generateParentBreadcrumbsElement( $id, $table, $parentAlias, array() );
            if ( $bread ) {
                $bread = array_reverse( $bread );
            }
            foreach ( $bread as $obj ) {
                conf::breadcrumbs( $obj->h1, $pre.$obj->alias );
            }
        }

        static function generateParentBreadcrumbsElement( $id, $table, $parentAlias, $bread ) {
            $page = Builder::factory( $table )->select( array( 'id', $parentAlias, 'alias', 'status', 'h1' ) )->where( 'id', '=', $id )->find();
            if( $page->status ) {
                $bread[] = $page;
            }
            if( (int) $page->$parentAlias > 0 ) {
                return conf::generateParentBreadcrumbsElement( $page->$parentAlias, $table, $parentAlias, $bread );
            }
            return $bread;
        }
    }