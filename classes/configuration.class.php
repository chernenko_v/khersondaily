<?php
    
    class configuration {

        protected $parameters = array();
        protected $metrika = array();
        protected $counters = array();

        protected $error = false;
        protected $e404;

        protected $h1;
        protected $title;
        protected $keywords;
        protected $description;

        static $_instance;

        private function __construct() {
            $this->e404 = array(
                'content' => support::part( array(), '404' ),
                'h1' => 'Ошибка 404! Страница не найдена',
                'title' => 'Ошибка 404! Страница не найдена',
                'keywords' => 'Ошибка 404! Страница не найдена',
                'description' => 'Ошибка 404! Страница не найдена',
            );
        }

        static function instance() {
            if(self::$_instance == NULL) { self::$_instance = new self(); }
            return self::$_instance;
        }

        public function set( $parameter, $value = NULL ) {
            if ( isset( $this->$parameter ) ) {
                $this->$parameter = $value;
            } else {
                $this->parameters[ $parameter ] = $value;
            }
        }

        public function get( $parameter, $pos = NULL ) {
            if ( $this->error AND in_array( $parameter, array( 'h1', 'title', 'keywords', 'description', 'content' ) ) ) {
                return Arr::get( $this->e404, $parameter );
            }
            if ( $parameter == 'metrika' ) {
                if ( $pos != 'head' AND $pos != 'body' ) {
                    return $this->metrika;
                }
                return $this->metrika[ $pos ];
            }
            if ( isset( $this->$parameter ) ) {
                return $this->$parameter;
            }
            $tmp = explode('.', $parameter);
            if ( count( $tmp ) > 1 ) {
                $config = $tmp[ count($tmp) - 2 ];
                $key = end($tmp);
                if( isset($this->parameters[$config]) ) {
                    return Arr::get( $this->parameters[$config], $key );
                }

                $file = HOST . '/config/' . implode( '/', $tmp ) . '.php';
                $file = str_replace( '/' . $key, '', $file );
                if( support::is_file( $file ) ) {
                    $array = require_once $file;
                    conf::set( $config, $array );
                    return Arr::get( $array, $key, NULL );
                }
            }
            return Arr::get( $this->parameters, $parameter, NULL );
        }

        public function error() {
            header('HTTP/1.0 404 Not Found');
            $this->set( 'error', true );
            return Arr::get( $this->e404, 'content' );
        }

    }