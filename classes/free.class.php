<?php
    function translit($word) {
        $ru   = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','і','є','ї','ґ',' ','"',"'","`",':','«','»','.',',','’','„','”','(',')','[',']','*','@','#','“','№','%');
        $en   = array('a','b','v','g','d','e','e','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','ts','ch','sh','sch','','y','','e','ju','ja','i','je','ji','g','_','','','','','','','','','','','','','','','','','','','','N','');
        $str  = '';
        $word = mb_strtolower($word, "UTF-8");
        $i = 0;
        foreach($ru as $_ru) {
            $word = str_replace($_ru,$en[$i],$word);
            $i++;
        }
        return $word;
    }

    /**
     *      Генерирование правильной ссылки
     */
    function href($url = '') {
        
        if($url == 'index') {
            return 'https://'.$_SERVER['HTTP_HOST'];
        }
        
        $arr = explode('https://', $url);
        
        if(count($arr) > 1) {
            return $url;
        }
        
        $arr = explode($_SERVER['HTTP_HOST'].'/', $url);
        
        if(count($arr) > 1 AND $arr[0] == '') {
            return 'https://'.$url;
        }
        
        if($url[0] == '/')
            $url = substr($url, 1, strlen($url));
        
        return 'https://'.$_SERVER['HTTP_HOST'].'/'.$url;

    }
    
    /**
     *      Редирект
     */
    function location($url = '') {
        $url = href($url);
        header('Location:'.$url);
        exit(0);
    }

    function css( $filename ) {
        $path = '/css/';
        if ( APPLICATION ) {
            return href( APPLICATION . $path . $filename . '.css' );
        }
        return href( $path . $filename . '.css' );
    }

    function js( $filename ) {
        $path = '/js/';
        if ( APPLICATION ) {
            return href( APPLICATION . $path . $filename . '.js' );
        }
        return href( $path . $filename . '.js' );
    }

    function pic( $file ) {
        $path = '/pic/';
        if ( APPLICATION ) {
            return href( APPLICATION . $path . $file );
        }
        return href( $path . $file );
    }
    
    /**
     *      Die переделанный
     */
    function newdie($array) {         
        echo '<pre>';
        print_r($array);
        echo '</pre>';
        die;
    }
    
    /**
     *      Обратная функция для nl2br
     */
    function br2nl($string) {
        return str_replace('<br />','',$string);
    	return preg_replace('#<br(\s+)?\/?>#i', "\n", $string);
    }
    
    /**
     *      Фильтруем строку
     */
    function clear_str($str) {
        $str = strip_tags($str);
		$str = stripslashes($str);
        $str = trim($str);
        return $str;
    }
    
    /**
     *      Выводим на экран глобальные массивы
     */
    function globalOnScreen($flag = 1) {
        
        if($flag) {
            
            echo '<pre>';
            echo '/*********************** POST ***********************/<br />';
            print_r($_POST);
            echo '/*********************** GET ***********************/<br />';
            print_r($_GET);
            echo '/*********************** SESSION ***********************/<br />';
            print_r($_SESSION);
            echo '/*********************** COOKIE ***********************/<br />';
            print_r($_COOKIE);
            echo '/*********************** SERVER ***********************/<br />';
            print_r($_SERVER);
            echo '</pre>';
            
        }
        
    }
	
	function emu_getallheaders() {
		foreach($_SERVER as $h=>$v)
		if(ereg('HTTP_(.+)',$h,$hp))
			$headers[$hp[1]]=$v;
		return $headers;
	}
    
    function isset_error() {
        if(isset($_SESSION['error'])) {
            return true;
        }
        return false;
    }

    function cut_text($text, $limit = 100) {
        $text = strip_tags($text);
        $text = trim($text);
        $text = mb_substr($text, 0, $limit, 'UTF-8');
        $text = trim($text);
        return $text;
    }
