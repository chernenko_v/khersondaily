<?php

class general {
		
	/**
     * Return old image
	 * Зачем оно нужно.. Это на случай если для других сущностей, бренды, группы товаров, слайдер и т.д. 
	 * захотят возможность грузить фотки любых разрешений. Выводит первое найденное изображение
	 * с заданным расширением, ищет в том порядке как задано в массиве
     */                
   static function old_image($image)
   {
		global $_img_ext;
       
       $count = sizeof($_img_ext);
       
       for ($i = 0; $i < $count; $i++)
       {
           if (file_exists(HOST.$image . '.' . $_img_ext[$i]) and !is_dir(HOST.$image . '.' . $_img_ext[$i]))
                return $image . '.' . $_img_ext[$i];
       }                    
       return;
   }
	
	/**
	 * Generate random suffix for urls
	 */	
	static function rand_suffix($str, $len)
    {        
        return '_'.substr(md5('_'.rand(9,999).$str), 0, $len);
    }
   
   /**
    *   Check for image
    */
    static function image_extension($ext)
    {
       global $_img_ext;
       return in_array($ext, $_img_ext);
    }

    /**
     * Generate new file name
     */
    static function generate_file_name($name)
    {
        return md5(time().$name.rand(1,9999));
    }

	/**
     * Return image if it exists
     * @param type $image - image name with path
     * @return type string
     */
    static function image($image)
    {        
        return (file_exists(HOST.$image) and !is_dir(HOST.$image)) ? $image : false;
    }

    /**
     * Check existence of template
     * @param type $dir - directory
     * @param type $name - template name
     * return true/false
     */
    static function isset_tpl($dir, $name)
    {
        return file_exists(HOST.$dir . '/' . $name) and !is_dir(HOST.$dir . '/' . $name);
    }
	
	/**
     * Get template
    */           
    static function get_template($tpl_dir = '/tpl/', $dir = '')
    {
        $tpl = '';

        switch ($_GET['action']) {
			default:
                $tpl = 'index_full.php';
                break;
        }
        
        if (self::isset_tpl($tpl_dir.'/'.$dir, $tpl))
            return $dir . '/' . $tpl;
        
        die('Template "' . $tpl . '" in ' . $dir . ' not exsist!');
    }
	
	/**
	 * вывод сообщения
	 *
	 * @param 0/1 $type
	 * @param текст сообщения $message
	 */
	static function messages ($type, $message){
		return system::show_tpl(array('type'=>$type,'msg'=>$message),'general/message.php');
	}

	/**
	 * show full link
	 * @param text $link
	 * @param text $text
	 * @return unknown
	 */
	static function link_to ($link, $text, $options=''){
		return '<a href="'.APPLICATION.'/'.$link.'" '.$options.'>'.$text.'</a>';
	}

	/**
	 * show link
	 * @param text $link
	 */
	static function link ($link){
		return APPLICATION.'/'.$link;
	}	

	/**
	 * проверяем на суперадмина
	 */
	static function sadmin () {	
		return $_SESSION['user_backend'] == 'superadmin' ? true : false;
	}
	
	/**
	 * префикс для tpl
	 */
	static function sadmin_tpl () {	
		return $_SESSION['user_backend'] == 'superadmin' ? '_sadmin' : '';
	}
	
	/**
	 * авторизация - суперадмин
	 */
	static function access() {
		if (isset($_GET['user_backend'])) {
			$_SESSION['user_backend'] = $_GET['user_backend'];
		}
	}
	
	/**
	* переворачиваем дату для записи в таблицу
	 * @param date $_date
	*/
	static function date_to_database($_date) {
		return strtotime($_date);
	}	
	
	/**
	* переворачиваем дату для вывода из таблицы на страницу
	* @param date $_date* 
	*/
	static function date_from_database($_date) {
		return date('d.m.y', $_date);
	}
	
	
	/**
	 * проверяем на статус 0/1/2
	 * админка, Action=index 
	 */
	static function get_status_for_filter ($table_name) {	
		
		if (isset($_GET['menu'])) {
			unset($_SESSION['status']);
			unset($_SESSION['name']);
			unset($_SESSION['artikul']);
			unset($_SESSION['category']);
			unset($_SESSION['block_new']);
			unset($_SESSION['block_lider']);
			unset($_SESSION['id_parent']);
			unset($_SESSION['brand']);
			unset($_SESSION['photo']);
			unset($_SESSION['sklad']);
			unset($_SESSION['city']);				
		}
		$_res='';
		
		// для каталога
		if (isset($_SESSION['status']) and $_SESSION['status']<2) {
			$_res .= ' and '.$table_name.'.status='.$_SESSION['status'];
		}			
		
		// для позиции
		if (isset($_SESSION['category']) and $_SESSION['category']<100) {
			$_res .= ' and '.$table_name.'.category='.$_SESSION['category'];
		}			
		
		if (isset($_SESSION['name']) and $_SESSION['name']!='') {
			$_res .= ' and ('.$table_name.'.name LIKE "%'.$_SESSION['name'].'%" or '.$table_name.'.text_short LIKE "%'.$_SESSION['name'].'%")';
		}

		if (isset($_SESSION['artikul']) and $_SESSION['artikul']!='') {			
			$_res .= ' and '.$table_name.'.artikul='.$_SESSION['artikul'];
		}
		if (isset($_SESSION['block_new'])) {			
			$_res .= ' and '.$table_name.'.block_new='.$_SESSION['block_new'];
		}
		if (isset($_SESSION['block_lider'])) {			
			$_res .= ' and '.$table_name.'.block_lider='.$_SESSION['block_lider'];
		}
		if (isset($_SESSION['id_parent']) and $_SESSION['id_parent']!=0) {			
			$_res .= ' and '.$table_name.'.id_parent='.$_SESSION['id_parent'];
		}
		if (isset($_SESSION['brand']) and $_SESSION['brand']!=0) {			
			$_res .= ' and '.$table_name.'.brand='.$_SESSION['brand'];
		}
		if (isset($_SESSION['photo']) and $_SESSION['photo']<2) {			
			$_res .= ' and '.$table_name.'.is_photo='.$_SESSION['photo'];
		}
		if (isset($_SESSION['sklad']) and $_SESSION['sklad']<3) {			
			$_res .= ' and '.$table_name.'.sklad='.$_SESSION['sklad'];
		} 
		if (isset($_SESSION['city']) and $_SESSION['city']!=0) {			
			$_res .= ' and '.$table_name.'.id_city='.$_SESSION['city'];
		} 			
		
		// для групп каталога
		if (isset($_POST['name_group']) and $_POST['name_group']!='') {
			$_res .= ' and ('.$table_name.'.pole LIKE "%'.$_POST['name_group'].'%" or '.$table_name.'.text LIKE "%'.$_POST['name_group'].'%")';
		}		

		// для заказов
		if (isset($_POST['status_orders']) and $_POST['status_orders']<3) {
			$_res .= ' and '.$table_name.'.status='.$_POST['status_orders'];
		}
		if (isset($_POST['date_s']) and $_POST['date_s']!='') {
			$_res .= ' and '.$table_name.'.created_at>="'.$_POST['date_s'].'"';
		}
		if (isset($_POST['date_po']) and $_POST['date_po']!='') {
			$_res .= ' and '.$table_name.'.created_at<="'.$_POST['date_po'].'"';
		}			
		
		return $_res;
		
	}

	/**
	 * проверяем на фильтры
	 */
	static function get_for_filter ($table_name, $_data_post) {	
		
		if (isset($_POST[$_data_post])) {
			if ($_POST[$_data_post]==0) {
				return '';
			} else {
				return ' and '.$table_name.'.'.$_data_post.'='.$_POST[$_data_post];
			}
		} else {
			return '';
		}

	}		
	
	
	/**
	 * если в сессии есть сообщения - выводим его
	 */
	static function global_massage () {	
		
		$_global_message='';
		if (isset($_SESSION['GLOBAL_MESSAGE'])) {
			$_global_message=$_SESSION['GLOBAL_MESSAGE'];
			unset($_SESSION['GLOBAL_MESSAGE']);
		}			
		return $_global_message;
		
	}
	
	
	
	
	/**
	 * если в сессии есть сообщения - выводим его
	 */
	static function center () {	
	
		$_str=array();

		if (isset($_GET['action']) and file_exists(HOST."/ctrl/".$_GET['action']."/".$_GET['action'].".php")) {
			 require HOST."/ctrl/".$_GET['action']."/".$_GET['action'].".php";
		} else if(@if_content($_GET['action'])) {
			if (_PRINT==1) {
				$_str.=get_tpl("/tpl/mainpage/print.tpl.html");				
			}
			if (!get_access()) {
				$_str.='<span class="red">Доступ запрещен!</span>';
			} else {
				require HOST."/ctrl/content/content.php";
			}	
		} else {
			system::error(404);
		}
		return $_str;		

	}
	
	
	
	/**
	 * В шаблонах может использоваться механизм выделения
	 * активного раздела меню, в виде стиля
	 * этот класс специально создан для того, чтобы парсить метки со стилями
	 * не засоряя код файла index.php
	 *
	 * Amber (20-05-2011)
	 * Добавлена подсветка для типов новостей
	 * Пример:
	 * echo general::active_menu('news', 0);
	 * echo general::active_menu('news', 1);
	 *
	 * AntiX (09-08-2011)
	 * Добавлена подсветка меню для нескольких действий
	 * Пример:
	 * echo general::active_menu('arhiv,gallery');
	 */
	function active_menu ($act_str, $_type=0) {
		if (!isset($_GET['action'])) {
			return '';
		}
		$arr=explode(',',$act_str);
		for ($i=0;$i<count($arr);$i++)
		{
		 $_punkt_menu=$arr[$i];
		 $action = $_GET['action'];
		 $_sql = "SELECT `id_parent` FROM `content` WHERE `action`='$action'";
		 $rs1  = mysql::query_one($_sql,0);

		 if ($rs1) {
			$_sql = "SELECT `action` FROM `content` WHERE `id`=".$rs1->id_parent;
			$rs1  = mysql::query_one($_sql,0);
			if ($rs1) {
				$action = $rs1->action;
			}
		 }
		 if ($action==$_punkt_menu)
		 {
		  if (isset($_GET['type']))
		  {
		   if ($_GET['type']==$_type)
		   {
			return ' class="cur"';
		   }
		  } else return ' class="cur"';
		 }
		}
	}

	static function get_icon_for_file($file_name) {
			
			$ext=end(explode(".", $file_name));
			switch($ext) {
			   case 'doc': 
				  $_img_name='word.png';
			   break;
			   
			   case 'xls': 
				  $_img_name='excel.png';
			   break;

			   case 'jpg': 
				  $_img_name='jpg.png';
			   break;
			   
			   case 'gif': 
				  $_img_name='gif.png';
			   break;

			   case 'png': 
				  $_img_name='png.png';
			   break;			   

			   case 'pdf': 
				  $_img_name='pdf.png';
			   break;			   
			   
			   default:		 
				  $_img_name='no.png';
			}
			return '<img src="/images/_icon/'.$_img_name.'" class="middle">';
	
	}

	static function get_file_ext($file_name) {
		  return end(explode(".", $file_name));
	}	

	function get_all_punkt_menu($id_main_menu) {

		$_sql = "SELECT * FROM `content` WHERE `id_parent`=$id_main_menu AND `status`=1 ORDER BY `sort`";
		$result=mysql::query($_sql,0);
		if($result) {
			return system::show_tpl(array('result'=>$result),'frontend/mainpage/get_all_punkt_menu.php');
		} else {
			return false;
		}
	}
	
	
	   /**
		*   Транслитерируем строку
		*/
	   static function translit($word) {
			$ru     = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','і','є','ї','ґ',' ','"',"'");
			$en     = array('a','b','v','g','d','e','e','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','ts','ch','sh','sch','','y','','e','ju','ja','i','je','ji','g','_','','');
			$str    = '';
			$word   = mb_strtolower($word, "UTF-8");
			$a      = preg_split('//u', $word);
			foreach($a as $_a) {
				$k = 0;
				for($o = 0; $o < 38; $o++)
					if($_a == $ru[$o]) {
						$k = 1;
						$str .= $en[$o];
					}
				if($k == 0)
					$str .= $_a;
			}
			return $str;
	   } 
	   
	   /**
		*       $object - Объект, получаемый из запроса к БД
		*       $pole   - Поле которое из него необходимо достать
		*/
	static function getObjectElementsInArray($object, $pole) {
			$arr        = array();
			foreach($object AS $obj) {
				$arr[]  = $obj->$pole;
			}
			return $arr;
		}
		
		
		

}

?>