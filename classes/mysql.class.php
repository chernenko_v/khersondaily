<?php

    class mysql {

		/**
		* 	   Коннектимся к БД
		*	   @param  none
		*/	
		static  function connect() {
            $token = Profiler::start('DATABASE', 'Connect');
            mysql_connect(conf::get('db.hostname'), conf::get('db.username'), conf::get('db.password')) or die('mysql_connect_faild');
            $result = mysql_db_query(conf::get('db.dbname'),"SET CHARACTER SET utf8");
            Profiler::stop($token);
		}
		
		/**
		* 	    Выполняем SQL запрос и достаем результат в виде объекта
		*	    @param 	varchar $sql  - query
		* 		int	$default   	      - show query (0 - no, 1 - yes)
		*/	
		static  function query($sql, $default = 0) {
            $token = Profiler::start('DATABASE', $sql);
			if ($default == 1)
			     echo '<div style="border: solid 1px red">'.$sql.'</div>'; 
			$result  = mysql_query($sql);
			$a       = array();
			while ($obj = mysql_fetch_object($result)) {
				foreach($obj AS $k => $v) {
					$val     = htmlspecialchars_decode($v);
					$val     = str_replace('&lt;','<',$val);
					$val     = str_replace('&gt;','>',$val);
					$obj->$k = $val;
				}
				$a[] = $obj;
			}
            Profiler::stop($token);
			return $a;

		}

		/**
		 *    Выполняем SQL запрос и достаем первую строку в виде объекта
		 *    @param varchar $sql	-	query
		 */
		static  function query_one($sql, $default = 0) {
            $token = Profiler::start('DATABASE', $sql);
			$a   = self::query($sql, $default);

			if(!count($a)) {
				return false;
			}
			
			$obj = $a[0];
			foreach($obj AS $k => $v) {
				$val     = htmlspecialchars($v);
				$val     = str_replace('&lt;','<',$val);
				$val     = str_replace('&gt;','>',$val);
				$obj->$k = $val;
			}
            Profiler::stop($token);
			return $obj;
			
		}		

		/**
		* 	    Достаем одно поле после выполнения запроса
		*	    @param 	varchar 	 		$sql 			- query
		* 			varchar				$pole_for_return  	- field's name for return
		* 			int					$default   			- show query (0 - no, 1 - yes)
		*/
		static  function query_findpole($sql, $pole_for_return, $default = 0){
            $token = Profiler::start('DATABASE', $sql);
			$a = self::query_one($sql, $default);
            $result = count($a)? $a->$pole_for_return : false;
            Profiler::stop($token);
            return $result;		
		}

		/**
		* 	    просто выполняем запрос и возвращаем true или false
		*	    @param 	varchar  		$sql 		- query
		* 			int				$default   	- show query (0 - no, 1 - yes)
		*/	
		static  function just_query($sql,$default = 0) {
            $token = Profiler::start('DATABASE', $sql);
			if ($default == 1)
			     echo '<div style="border: solid 1px red">'.$sql.'</div>';
			$result = mysql_query($sql);
            Profiler::stop($token);
			return $result;
		}

		/**
		* 	Возвращаем результат запроса как ассоциативный массив у которого ключ $key, а значение - $value
        *   $key и $value должны доставаться запросом с таблицы
        * 
		*	@param 	varchar  		$sql 		- query
		* 			int				$default   	- show query (0 - no, 1 - yes)
		*/			
		static  function query_array($sql, $key, $value, $default = 0) {
            $token = Profiler::start('DATABASE', $sql);
			if ($default == 1) echo '<div style="border: solid 1px red">'.$sql.'</div>';
			$result = mysql_query($sql);
			$a      = array();
		    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$val     = htmlspecialchars_decode($row[$value]);
				$val     = str_replace('&lt;','<',$val);
				$val     = str_replace('&gt;','>',$val);
				$a[$row[$key]] = $val;
			}
            Profiler::stop($token);
			return $a;			

		}
        
        /**
         *      Выполняем запрос и возвращаем результат в виде двумерного ассоциативного массива
         */	
        static function query_as_array($sql, $default = 0) {
            $token = Profiler::start('DATABASE', $sql);
            $result            = mysql::query($sql, $default);
            $arr               = array();
            foreach($result AS $obj) {
                $_arr           = array();
                foreach($obj AS $key => $value) {
					$val     	= htmlspecialchars_decode($value);
					$val     	= str_replace('&lt;','<',$val);
					$val     	= str_replace('&gt;','>',$val);
                    $_arr[$key] = $val;
                }
                $arr[]          = $_arr; 
            }
            Profiler::stop($token);
            return $arr;
        }
         
         /**
          *    Выполняем запрос и возвращаем одну строку как ассоциативный массив
          */
         static function query_one_as_array($sql, $default = 0) {
            $token = Profiler::start('DATABASE', $sql);
             $result        = mysql::query_one($sql, $default);
             $arr           = array();
             foreach($result AS $key => $value) {
                $val     	= htmlspecialchars_decode($value);
				$val     	= str_replace('&lt;','<',$val);
				$val     	= str_replace('&gt;','>',$val);
				$arr[$key]  = $val;
             }
            Profiler::stop($token);
             return $arr;
         }
         
        /**
		* 	Достает Поле по ID из таблицы $table
        *   Надо избавиться от этой таблицы в итоге
		*	@param 	int				id_parent
		*/	
		static  function get_parent_name($table, $id, $pole, $default = 0){
			$sql = 'SELECT '.$pole.' FROM '.$table.' WHERE id = '.$id;
            $token = Profiler::start('DATABASE', $sql);
			$result = self::query_findpole($sql, $pole, $default);
            Profiler::stop($token);
            return $result;
		}
		
		/**
         *      Проверяем поле на существование
         */
        public function check_field($table, $field, $default = 0) {
            $sql = 'SELECT `'.$field.'` FROM `'.$table.'` WHERE 1 LIMIT 1';
            $token = Profiler::start('DATABASE', $sql);
            if ($default == 1) echo '<div style="border: solid 1px red">'.$sql.'</div>';
            $result = (boolean) mysql_query($sql);
            Profiler::stop($token);
            return $result;
        }
    }

?>