<?php
    
    class support {

        static function get_alphabet($result) {
            $en = array('1-9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','XYZ');
            $ru = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','ФХЦ','ЧШЩ','ЭЮЯ');

            $_en = array();
            $_ru = array();

            foreach($result AS $obj) {
                $letter = support::get_first_upper_letter($obj->name);

                if( in_array( (int) $letter, array(1,2,3,4,5,6,7,8,9) ) ) {
                    $_en['1-9'][] = $obj;
                } else if( in_array( $letter, array('X','Y','Z') ) ) {
                    $_en['XYZ'][] = $obj;
                } else if( in_array( $letter, array('Ф','Х','Ц') ) ) {
                    $_ru['ФХЦ'][] = $obj;
                } else if( in_array( $letter, array('Ч','Ш','Щ') ) ) {
                    $_ru['ЧШЩ'][] = $obj;
                } else if( in_array( $letter, array('Э','Ю','Я') ) ) {
                    $_ru['ЭЮЯ'][] = $obj;
                } else if( in_array( $letter, $en ) ) {
                    $_en[$letter][] = $obj;
                } else if( in_array( $letter, $ru ) ) {
                    $_ru[$letter][] = $obj;
                } 
            }

            return array(
                'en' => $en,
                'ru' => $ru,
                'res_en' => $_en,
                'res_ru' => $_ru,
            );

        }

        static function get_first_upper_letter($word) {
            $length = mb_strlen( $obj->name, 'UTF-8' );
            $letter = mb_substr( $word, 0, - $length + 1, 'UTF-8' );
            $letter = strtoupper( $letter );
            return $letter;
        }

        public static function addItemTag($obj) {
            if( $obj->sale ) {
                return '<div class="splash2"><span>акция</span></div>';
            }
            if( $obj->top ) {
                return '<div class="splash3"><span>популярное</span></div>';
            }
            if( $obj->new AND time() - (int) $obj->new_from < conf::get('new_days') * 24 * 60 * 60 ) {
                return '<div class="splash"><span>новинка</span></div>';
            }
        }

        public static function image_extension($ext) {
            $_img_ext = array('jpg','jpeg','png');
            return in_array($ext, $_img_ext);
        }

        public static function getRootParent( $result, $id ) {
            if( !$id ) {
                return 0;
            }
            foreach( $result AS $obj ) {
                if( $obj->id == $id ) {
                    if( $obj->parent_id == 0 ) {
                        return $obj->id;
                    } else {
                        return support::getRootParent( $result, $obj->parent_id);
                    }
                }
            }
        }

        public static function getRealIP() {
            foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED',
               'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR')
                  as $key) {
             
                if (array_key_exists($key, $_SERVER) === true) {
                    foreach (explode(',', $_SERVER[$key]) as $ip) {
                        if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                            return $ip;
                        }
                    }
                }
            }
        }

        public static function tpl($array, $tpl) {
            return system::show_tpl($array, $tpl.'.php');
        }

        public static function part($array, $tpl) {
            return system::show_tpl($array, 'Parts/'.$tpl.'.php');
        }

        public static function widget($array, $tpl) {
            return system::show_tpl($array, 'Parts/Widgets/'.$tpl.'.php');
        }

        public static function access($action) {
            if(file_exists(HOST.'/ctrl/'.$action.'/'.$action.'.php') OR $action == 'index') {
                return false;
            }
            return true;
        }
        
        public static function is_file($src) {
            if(!file_exists($src)) { return false; }
            if(is_dir($src)) { return false; }
            return true;
        }
        
        public static function is_ctrl($ctrl) {
            return self::is_file(HOST.'/ctrl/'.$ctrl.'/'.$ctrl.'.php');
        }
        
        public function month( $num ) {
            $months = Array( '', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря' );
            return $months[ (int) $num ];
        }

        public function shortMonth( $num ) {
            $month = support::month( $num );
            return mb_substr( $month, 0, 3, 'UTF-8' );
        }

        public static function getTime( $seconds, $type ) {
            $min = ceil( $seconds / 60 );
            $hrs = floor( $min / 60 );
            if( $type == 'hrs' ) {
                return $hrs;
            }
            return $min - $hrs * 60;
        }

        public static function getHumanTime( $seconds ) {
            $hrs = support::getTime( $seconds, 'hrs' );
            $hrs = support::addZero( $hrs );
            $min = support::getTime( $seconds, 'min' );
            $min = support::addZero( $min );
            return $hrs . ':' . $min;
        }

        public static function addZero( $number ) {
            $string = (string) $number;
            if( strlen( $string ) == 2 ) {
                return $string;
            }
            return '0' . $string;
        }

        public static function weekday( $number ) {
            $weekday = Array( 'ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ' );
            return $weekday[(int) $number];
        }

        public static function getHumanDateRange( $from, $to ) {
            if( $from == $to ) {
                return date( 'd', $from ) . '&nbsp;' . support::month( date( 'm', $from ) );
            }
            if( date( 'm', $from ) == date( 'm', $to ) ) {
                return date( 'd', $from ) . ' — ' . date( 'd', $to ) . '&nbsp;' . support::month( date( 'm', $from ) );
            }
            return date( 'd', $from ) . '&nbsp;' . support::month( date( 'm', $from ) ) . ' — ' . date( 'd', $to ) . '&nbsp;' . support::month( date( 'm', $to ) );
        }

        public static function firstWordInSpan( $string ) {
            $words = explode( ' ', $string );
            $words[ 0 ] = '<span>' . $words[ 0 ] . '</span>';
            $string = implode( ' ', $words );
            return $string;
        }

        public static function firstWordWithBr( $string ) {
            $words = explode( ' ', $string );
            $words[ 0 ] = $words[ 0 ] . '<br />';
            $string = implode( ' ', $words );
            return $string;
        }

        public static function firstWordInSpanWithBr( $string ) {
            $words = explode( ' ', $string );
            $words[ 0 ] = '<span>' . $words[ 0 ] . '</span><br />';
            $string = implode( ' ', $words );
            return $string;
        }

        public static function youtube_key($link) {
            if(preg_match("/(?:youtube)/",$link)) {
                preg_match("/^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/", $link, $video);
            }
            else if(preg_match("/(?:youtu)/",$link)) {

                preg_match("/^(?:https?:\/\/)?(?:www\.)?youtu\.be\/((\w|-){11})(?:\S+)?$/", $link, $video);
            }
            
            return ($video[1]) ? $video[1] : false;
        }

            public static function get_page_navi($direction) {
            $controller=Route::controller();
            $cur_page=Route::param('page');
            $count_pages=$_GET['count_pages'];
            $action=Route::param('alias');
            
            $_filter='';
            $_filter .= (Arr::get($_GET,'per_page')) ? 'per_page='.Arr::get($_GET,'per_page') : '';
            $_filter .= Arr::get($_GET,'sort') ? 'sort='.Arr::get($_GET,'sort') : '';
            $_filter .= Arr::get($_GET,'type') ? 'type='.Arr::get($_GET,'type') : '';
            if ($_filter!='') {
                $filter = '?'.$_filter;
            }
            

            if ($direction == 'next') {

                if ($cur_page == $count_pages) {

                    //$_page = '';
                    return '';

                } else {

                    $_page = '/page/'.($cur_page+1);

                }

            }

            if ($direction == 'prev') {

                if ($cur_page == 1) {

                    $_page = '/page/'.$count_pages;

                } elseif ($cur_page == 2) {

                    $_page = '';

                } else {

                    $_page = '/page/'.($cur_page-1);

                }

            }

            return MAIN_PATH.'/'.$controller.'/'.$action.$_page.$filter;

        }
   
    }