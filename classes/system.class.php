<?php

class system {
	
		/**
		* 	check ctrl
		*	@param  int		$ctr	name of ctrl
		*/	
		static  function isset_ctrl ($ctrl) {

			if (isset($_GET[$ctrl])) {
				return true;
			} else {
				system::error(404,'Action <b>'.$_GET['action'].'</b> not found');
			}
				
		}		
		
		/**
		* 	check is_numeric
		*	@param  int		$id		числовое значение
		*/	
		static  function isset_numeric ($id) {

			if (is_numeric($id)) {
				return true;
			} else {
				system::error(404,'Parametrs <b>'.$id.'</b> is not a numeric');
			}
				
		}		

		/**
		* 	check parametrs > 2
		* @param  int		$_count		числовое значение* - кол-во 
		*/	
		static  function count_parametr ($_count) {
			
			$_arr=explode("/",$_SERVER [ 'REQUEST_URI']);
	
			if (count($_arr)>$_count) {
				system::error(404,'There are to many parametrs at url. Page - <b>'.$_GET['action'].'</b>');
			} else {
				return true;
			}

		}

		/**
		* 	check action
		*	@param string		$action		name of action with extention
		*/	
		static  function isset_extension ($action) {
			
			$_arr=explode(".",$action);
			if (count($_arr)==1) {
				return true;
			} else {
				system::error(404);
			}
				
		}			
	
		/**
		* 	check action
		*	@param string		$action		name of action with extention
		*/	
		static  function isset_php ($action) {
			
			$_arr    = explode(".",$action);
			if (!$action or $_arr[1] == 'php') {
				return true;
			} else {
				system::error(404);
			}
				
		}		

		/**
		*	Redirect to error page.
		*	@param  int		$code	error code [404 | 500]
		*	@param  string	$msg		message
		*/
		/*static function error($code=404, $msg='') {

			ob_clean();
			switch($code) {
				case 404:
					if (DEBUG_SITE==1) { echo $msg; }
					$header = '404 Not Found';
				    break;
				case 500:
					if (DEBUG_SITE==1) { echo $msg; }					
					$header = '500 Internal Server Error';
				    break;
			}
			header($_SERVER['SERVER_PROTOCOL'].' '.$header);
			$_statik=array(
				'TITLE'             =>  'Ошибка '.$code.'! Страница не найдена!',  // PERMANENT
				'DESCRIPTION'       =>  'Ошибка '.$code.'! Страница не найдена!', // PERMANENT
				'KEYWORDS'          =>  'Ошибка '.$code.'! Страница не найдена!', // PERMANENT
				'H1'                =>  'Ошибка '.$code.'! Страница не найдена!', // PERMANENT
				'CONFIG'            =>  mysql::query_array('SELECT config.`key`,config.zna from config where status=1',0), // PERMANENT
			);
			die (system::show_tpl(
			array(
				'str'=>get_tpl('/tpl/frontend/mainpage/'.$code.'.tpl.php'),
				'statik'=>$_statik
			),'404.php'));

		}*/

		/**
		* clear GET data 
		*/
		static function clear_get() {

			if (isset($_GET)) {
    	  		foreach($_GET as $key => $value)  {
    				$value = str_replace(array('%27', 'select', 'union', 'insert', '%22', '&',':','.', '=', 'mysql'), '', $value);
    				$_GET[$key] = $value;
    	  		}
    	  	}
	
		}

		/**
		 * show template
		 *	@param 	array			$result	 		- array of objects
		 * 			varchar			$name_file 		- name of tamplate (plus path '/tpl/....') 
		 */
		static function show_tpl($tpl_data,$name_file) {
			ob_start();
			extract($tpl_data, EXTR_SKIP);
			if ( APPLICATION ) {
				include(HOST.APPLICATION.'/tpl/'.$name_file);
			} else {
				include(HOST.'/tpl/'.$name_file);
			}
			return ob_get_clean();			

		}
        
        static function show_tpl_backend($tpl_data,$name_file) {

			ob_start();
			extract($tpl_data, EXTR_SKIP);
			include(HOST.'/backend/tpl/'.$name_file);
			return ob_get_clean();			

		}
		
		/**
		 * work with date
		 * @param 			$data	- date
		 */
		static function show_data($date_input,$time=false) {

			if ($time) {
				$result_date=date('d.m.y H:i:s', $date_input);
			} else {
				$result_date=date('d.m.y', $date_input);
			}			
			return $result_date;			

		}

		/**
		*	Auto load classes.
		*	FOR SYSTEM USE ONLY
		*/
		static function classAutoLoad($class)
		{

			if (stripos($class,'Controller')>0) {
				if (file_exists(HOST."/ctrl/".$class.".php")) {
					//echo '<br>'.HOST."/ctrl/".$class.".php<br>";
					include_once(HOST."/ctrl/".$class.".php");
				} else {
					//echo '<br>'.HOST.APPLICATION."/ctrl/".$class.".php<br>";
					include_once(HOST.APPLICATION."/ctrl/".$class.".php");
				}
				
			} else {
				if (file_exists(HOST."/classes/".$class.".class.php")) {
					//echo '<br>'.HOST."/classes/".$class.".class.php<br>";
					include_once(HOST."/classes/".$class.".class.php");
				} else {
					if (file_exists(HOST.APPLICATION."/classes/".$class.".class.php")) {
						//echo '<br>'.HOST.APPLICATION."/classes/".$class.".class.php<br>";
						include_once(HOST.APPLICATION."/classes/".$class.".class.php"); 
					} else {
						include_once(HOST."/backend/plagin/".str_replace('_','/',$class).".php");
						//echo '<br>'.HOST.APPLICATION."/classes/".str_replace('_','/',$class).".php<br>";
					}
				}
				
			}
			
		}	

		/**
		 * if isset GET
		 *
		 */
		static function IsGet(){
			return $_SERVER['REQUEST_METHOD'] == 'GET';
		}
		
		/**
		 * if isset POST
		 *
		 */
		static function IsPost(){
			return $_SERVER['REQUEST_METHOD'] == 'POST';
		}
		
		static function _get($key,$default='') {
			return isset($_GET[$key]) ? $_GET[$key] : $default;
		}

		static function _cfg($key,$default='') {
			return isset($GLOBALS[$key]) ? $GLOBALS[$key] : $default;
		}

		static function _route() {

			$_arr = explode('?', $_SERVER['REQUEST_URI']);

			$request_uri = $_arr[0];
			//$request_uri = preg_replace('/\/.*\?/', '/', $request_uri);
			//$request_uri = str_replace(array('&', '=', ), '/', $request_uri);
			$request_uri = preg_replace('/^\/|ctrl\/|action\/|\/$/', '', $request_uri);
			if ($request_uri !== '') {
				foreach(system::_cfg('route_alias', array()) as $k => $v) {
					if (is_string($v)) {
						if ($request_uri == $k) {
							$request_uri = $v;
							break;
						}
					} else {
						//$k = str_replace('/', '\/', $k);
						//$k = str_replace('.', '\.', $k);
						foreach ($v as $rule=>$vars) {
							foreach($vars as $key=>$value) {
								$k = str_replace('$'.($key+1), '('.$value.')', $k);
							}
							if (preg_match('/^'.$k.'$/', $request_uri)) {
								$request_uri = preg_replace('/'.$k.'/', $rule, $request_uri);
								break;
							}
						}								
					}
				}
			}

			$_GET=array();
			$_arr=explode('/',$request_uri);	
			$_GET['ctrl']=v::toWord(v::getFrom($_arr,0,DEFAULT_CTRL));
			$_GET['action']=v::toWord(v::getFrom($_arr,1,DEFAULT_ACTION));
			array_shift($_arr);
			array_shift($_arr);
			for ($i=0; $i<count($_arr); $i+=2) {
				//$_GET[$_arr[$i]]=v::toWord(v::getFrom($_arr,$i+1,''));
				$_GET[$_arr[$i]] = $_arr[$i+1];
			}

			$_arr = explode('?', $_SERVER['REQUEST_URI']);
	        if(count($_arr) > 1) {
	            $arr  = explode('&', $_arr[1]);
	            foreach($arr AS $_a) {
	                $g   = urldecode($_a);
	                $g   = strip_tags($g);
	                $g   = stripslashes($g);
	                $g   = trim($g);
	                $get = explode('=', $g);
	                $_GET[$get[0]] = $get[1];
	            }
	        }
	    }
       
        public static function error() {
            conf::error();          
        }
}