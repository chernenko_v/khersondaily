<?php
    
    class AjaxController {

        // Add item to cart
        public function addToCartAction() {
            // Get and check incoming data
            $catalog_id = Arr::get($_POST, 'id', 0);
            if( !$catalog_id ) {
                $this->error('No such item!');
            }
            $size_id = (int) Arr::get($_POST, 'size', 0);
            // Add one item to cart
            Cart::factory()->add($catalog_id, $size_id);
            $result = Cart::factory()->get_list_for_basket();
            $cart = array();
            foreach( $result AS $item ) {
                $obj = Arr::get($item, 'obj');
                if( $obj ) {
                    $cart[] = array(
                        'id' => $obj->id,
                        'size_id' => (int) $obj->size_id,
                        'name' => $obj->name . ($obj->size_name ? ', ' . $obj->size_name : ''),
                        'cost' => $obj->cost,
                        'image' => support::is_file(HOST.'/images/catalog/medium/'.$obj->image) ? '/images/catalog/medium/'.$obj->image : '',
                        'alias' => $obj->alias,
                        'count' => Arr::get($item, 'count', 1),
                    );
                }
            }
            $this->success(array('cart' => $cart));
        }


        // Edit count items in the cart
        public function editCartCountItemsAction() {
            // Get and check incoming data
            $catalog_id = Arr::get($_POST, 'id', 0);
            if( !$catalog_id ) {
                $this->error('No such item!');
            }
            $count = Arr::get($_POST, 'count', 0);
            if( !$count ) {
                $this->error('Can\'t change to zero!');
            }
            $size_id = (int) Arr::get($_POST, 'size', 0);
            // Edit count items in cirrent position
            Cart::factory()->edit($catalog_id, $size_id, $count);
            $this->success(array('count' => (int) Cart::factory()->_count_goods));
        }


        // Delete item from the cart
        public function deleteItemFromCartAction() {
            // Get and check incoming data
            $catalog_id = Arr::get($_POST, 'id', 0);
            if( !$catalog_id ) {
                $this->error('No such item!');
            }
            $size_id = (int) Arr::get($_POST, 'size', 0);
            // Add one item to cart
            Cart::factory()->delete($catalog_id, $size_id);
            $this->success(array('count' => (int) Cart::factory()->_count_goods));
        }


        // Generate Ajax answer
        public function answer( $data ) {
            echo json_encode( $data );
            die;
        }


        // Generate Ajax success answer
        public function success( $data ) {
            if( !is_array( $data ) ) {
                $data = array(
                    'response' => $data,
                );
            }
            $data['success'] = true;
            $this->answer( $data );
        }


        // Generate Ajax answer with error
        public function error( $data ) {
            if( !is_array( $data ) ) {
                $data = array(
                    'response' => $data,
                );
            }
            $data['success'] = false;
            $this->answer( $data );
        }
}