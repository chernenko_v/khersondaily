 <?php

    class ContentController {
        public function indexAction() {
            // Check for existance
            $page  = Builder::factory('content')->where('alias', '=', Route::param('alias'))->where('status', '=', 1)->find();
            if( !$page ) { return system::error(); }
            // Seo
            conf::set( 'h1', $page->h1 );
            conf::set( 'title', $page->title );
            conf::set( 'keywords', $page->keywords );
            conf::set( 'description', $page->description );
			if($page->parent_id > 0) {
				conf::generateParentBreadcrumbs( $page->parent_id, 'content', 'parent_id' );
			}
            conf::breadcrumbs( $page->name );
            // Add plus one to views
           /*Builder::factory('content')
                        ->data(array(
                            'views' => (int) $obj->views + 1,
                        ))->where('id', '=', $obj->id)->edit();*/
            // Get content page children
            $kids = Builder::factory('content')
                        ->where('status', '=', 1)
                        ->where('parent_id', '=', $page->id)
                        ->order_by('sort', 'ASC')
                        ->find_all();
            // Render template
            return support::tpl( array( 'text' => htmlspecialchars_decode($page->text), 'image' => $page->image, 'kids' => $kids ), 'Content/Page' );
        }


        public function contactsAction() {
            // Check for existance
            $page  = Builder::factory('content')->where('alias', '=', Route::param('alias'))->where('status', '=', 1)->find();
            if( !$page ) { return system::error(); }
            // Seo
            conf::set( 'h1', $page->h1 );
            conf::set( 'title', $page->title );
            conf::set( 'keywords', $page->keywords );
            conf::set( 'description', $page->description );
            conf::breadcrumbs( $page->name );
            // Add plus one to views
           /* Builder::factory('content')->data(array(
                'views' => (int) $obj->views + 1,
            ))->where('id', '=', $obj->id)->edit();*/
            // Get content page children
            $kids = Builder::factory('content')->where('status', '=', 1)->where('parent_id', '=', $page->id)->order_by('sort', 'ASC')->find_all();
            // Render template
            return support::tpl( array( 'text' => $page->text, 'kids' => $kids ), 'Content/Contacts2' );
        }

    }
    