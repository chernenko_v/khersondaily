<?php

    class Controller {

        protected $_template;
        protected $_content;

        public function before() {
            $this->config();
            $this->center();
            $this->template();
            $this->render();
        }

        private function template() {
            if( conf::get( 'error' ) ) {
                $this->_template = '404';
                return;
            }

            switch ( Route::controller() ) {
                case 'user':
                    if (Route::action() == 'print') {
                        $this->_template = 'Print';
                    } else {
                        $this->_template = 'Cabinet';
                    }
                    break;
                case 'search':
                    $this->_template = 'CatalogItemsWithoutFilter';
                    break;

                case 'rss':
                    if( Route::action() == 'index' ) {
                       /*$this->_template = 'Text';*/
						$this->_template = 'Rss';
                    }
                    else {
                        $this->_template = 'Rss';
                    }
                    break;
                case 'rss2':
                    if( Route::action() == 'index' ) {
                        /*$this->_template = 'Text';*/
                        $this->_template = 'Rss';
                    }
                    else {
                        $this->_template = 'Rss';
                    }
                    break;
                case 'news':
                    $this->_template = 'Text';
                    if( Route::action() == 'index' ) {
                        conf::set( 'content_class', 'news_block' );
                    } else {
                        conf::set( 'content_class', 'new_block' );
                    }
                    break;
//                case 'photoreport':
//                    if( Route::action()=='inner') {
//                        $this->_template = 'Photoreport';
//                        break;
//                    } else {
//                        $this->_template = 'Text';
//                        break;
//                    }
                    break;
                case 'content':
                    if( Route::param('alias') == 'index' ) {
                        $this->_template = 'Main';
                        break;
                    }
				case 'gallery':
                    if( Route::action()=='inner') {
                        $this->_template = 'Gallery';
                        break;
                    } else {
						  $this->_template = 'Text';
						  break;
					}
                default:
                    $this->_template = 'Text';
                    conf::set( 'content_class', 'wTxt' );
                    break;
            }
        }


        private function config() {
            $result = Builder::factory('config')
                    ->select(array('key', 'zna'))
                    ->where('status', '=', 1)
                    ->find_all();
            foreach($result AS $obj) {
                conf::set( $obj->key, $obj->zna );
            }
            $result = Builder::factory('seo_metrika')
                    ->select(array('script', 'place'))
                    ->where('status', '=', 1)
                    ->find_all();
            $metrika = array();
            foreach ( $result AS $obj ) {
                $metrika[ $obj->place ][] = $obj->script;
            }
            conf::set( 'metrika', $metrika );
            $result = Builder::factory('seo_counters')
                    ->select('script')
                    ->where('status', '=', 1)
                    ->find_all();
            $counters = array();
            foreach($result AS $obj) {
                $counters[] = $obj->script;
            }
            conf::set( 'counters', $counters );
        }


        private function center() {
            $token = Profiler::start('Profiler', 'Center');
            $this->_content = Route::factory()->parse();
            Profiler::stop($token);
            if ( !conf::get( 'error' ) ) {
                $seo = Builder::factory( 'seo_links' )
                        ->where( 'status', '=', 1 )
                        ->where( 'link', '=', href( Arr::get( $_SERVER, 'REQUEST_URI' ) ) )
                        ->order_by( 'id', 'DESC' )
                        ->find();
                if ($seo) {
                    conf::set( 'h1', $seo->h1 );
                    conf::set( 'title', $seo->title );
                    conf::set( 'keywords', $seo->keywords );
                    conf::set( 'description', $seo->description );
                    conf::set( 'seo_text', $seo->text );
                }
            }
        }


        private function render() {
            $html = $this->compress(support::tpl(array( 'content' => $this->_content ), $this->_template));
            echo str_replace(
                array(
                    'href="http://'.$_SERVER['HTTP_HOST'],
                    'src="http://'.$_SERVER['HTTP_HOST'],
                    "'http://".$_SERVER['HTTP_HOST'],
                    'src="http://f.i.ua/'),
                array(
                    'href="//'.$_SERVER['HTTP_HOST'],
                    'src="//'.$_SERVER['HTTP_HOST'],
                    "'//".$_SERVER['HTTP_HOST'],
                    'src="https://f.i.ua/'),$html);
        }

        public function compress($html)
        {
            $html = preg_replace('/[\r\n\t]+/', ' ', $html);
            $html = preg_replace('/[\s]+/', ' ', $html);
            $html = preg_replace("/\> \</", "><", $html);
            $html = preg_replace("/\<!--[^\[*?\]].*?--\>/", "", $html);

            return $html;
        }

    }

    $controller = new Controller();
    $controller->before();
