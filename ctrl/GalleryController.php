<?php

class galleryController {

	public $limit;

	function __construct() {
			conf::set( 'title', 'Галерея' );
            conf::set( 'keywords', 'Галерея' );
            conf::set( 'description', 'Галерея' );
            $this->limit = (int)conf::get('gallery_count');
	}

	public function indexAction() {
		$page = !(int) Route::param('page') ? 1 : (int) Route::param('page');

		$result_gallery = Builder::factory('gallery_albums')
                            ->select(array('`gallery_albums`.`name`',
                                            '`gallery_albums`.`alias`',
                                            '`gallery_albums`.`id`',
                                            '`gallery_images`.`image`'))
                            ->join('gallery_images')
                            ->on('gallery_images.parent_id','=',Builder::expr('`gallery_albums`.`id`'))
                            ->on('gallery_images.main', '=', 1)
                            ->where('status', '=', 1)
                            ->order_by('date','DESC')
                            ->limit($this->limit, ($page - 1) * $this->limit)
                            ->find_all();

		$result_videos = Builder::factory('videos')
	                        ->select(array('image','title','link', 'facebook_code', 'id'))
	                        ->where('status', '=', 1)
	                        ->order_by('date', 'DESC')
	                        ->limit($this->limit, ($page - 1) * $this->limit)
	                        ->find_all();

	    if(!count($result_gallery) && !count($result_videos)) {
	    	return system::error();
	    }
	  	$count_g = Builder::factory('gallery_albums')
	  				->join('gallery_images')
                   	->on('gallery_images.parent_id','=',Builder::expr('`gallery_albums`.`id`'))
                  	->on('gallery_images.main', '=', 1)
        			->where('status', '=', 1)
					->count_all();
		$count_v = Builder::factory('videos')
        			->where('status', '=', 1)
					->count_all();
		//чего больше, с того страницы будем считать
		$count = $count_g >= $count_v ? $count_g : $count_v;

	    $pager = Frontend::pager($count, $this->limit, $page);

		return support::tpl(
							array(
								'result_gallery' => $result_gallery,
								'result_videos' => $result_videos,
								'pager' => $pager
							), 
							'gallery/list' );
	}

	/*public function innerAction() {
		$page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
		$gallery_id = Builder::factory('gallery_albums')
						->select(array(
							'id',
							'title',
							'keywords',
							'description'))
						->where('alias','=', Route::param('alias'))
						->where('status','=',1)
						->limit(1)
						->find();
		if(!$gallery_id) {
			return system::error();
		}
		$count = Builder::factory('gallery_images')
					->where('parent_id','=', $gallery_id->id)
					->count_all();
		$result = Builder::factory('gallery_images')
					->select(array('image'))
					->where('parent_id','=', $gallery_id->id)
					->limit((int)conf::get('images_count'), ($page - 1) * (int)conf::get('images_count'))
					->find_all();

		conf::set( 'title', $gallery_id->title );
       	conf::set( 'keywords', $gallery_id->keywords );
      	conf::set( 'description', $gallery_id->description );

		if(!count($result)) {
	    	 return system::error();
	    }
	    $pager = Frontend::pager($count, (int)conf::get('images_count'), $page);
		return support::tpl(array(
							'result' => $result,
							'pager' => $pager
							), 
							'gallery/inner' );
	}*/
	
	
	
	public function innerAction() {
		$page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
		$gallery_id = Builder::factory('gallery_albums')
						->select(array(
							'id',
							'title',
							'keywords',
							'description',
							'text','name','h1'))
						->where('alias','=', Route::param('alias'))
						->where('status','=',1)
						->limit(1)
						->find();
		if(!$gallery_id) {
			return system::error();
		}
		$count = Builder::factory('gallery_images')
					->where('parent_id','=', $gallery_id->id)
					->count_all();
		$result = Builder::factory('gallery_images')
					->select(array('image'))
					->where('parent_id','=', $gallery_id->id)
					->limit((int)conf::get('images_count'), ($page - 1) * (int)conf::get('images_count'))
					->find_all();

		conf::set( 'title', $gallery_id->title );
       	conf::set( 'keywords', $gallery_id->keywords );

		if(strlen(trim(strip_tags($gallery_id->description))) <> 0) {
			conf::set( 'description', $gallery_id->description );
		} else {
			conf::set( 'description', mb_substr(trim(strip_tags($gallery_id->text)),0,160,'UTF-8') );
		}

		if(!count($result)) {
	    	 return system::error();
	    }
	    $pager = Frontend::pager($count, (int)conf::get('images_count'), $page);
		return support::tpl(array(
							'result' => $result,
							'pager' => $pager,
							'gallery'=>$gallery_id
							), 
							'gallery/inner2' );
	}
}