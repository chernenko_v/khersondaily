<?php

class NewsController
{
    public $limit;
    public $alias;

    function __construct()
    {
        conf::set('title', 'Новости');
        conf::set('keywords', 'Новости');
        conf::set('description', 'Новости');
        $this->limit = (int)conf::get('limit');
    }

    public function indexAction()
    {
        $page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        /*$result = Builder::factory('news')
                    ->select(array(
                                    '`news_images`.`image`',
                                    '`news`.*'
                                ))
                    ->leftJoin('news_images')
                    ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
                    ->on('news_images.main', '=', 1, 'AND')
                    //->where('news_images.main', '=', 1)
                    ->where('status', '=', 1)
                    ->where('date', '<=', time())
                    ->order_by('news.date','DESC')
                    ->limit($this->limit, ($page - 1) * $this->limit)
                    ->find_all();*/
        $result = Builder::factory('news')
            ->where('status', '=', 1)
            ->where('date', '<=', time())
            ->order_by('news.date', 'DESC')
            ->limit($this->limit, ($page - 1) * $this->limit)
            ->find_all();

        $images = $this->getNewsImages($result);
        $views = $this->getNewsViews($result);
        // Count of news
        $count = Builder::factory('news')
            ->where('status', '=', 1)
            ->where('date', '<=', time())
            ->count_all();
        // Generate pagination
        $pager = Frontend::pager($count, $this->limit, $page);
        // Render template
        return support::tpl(array('result' => $result, 'images' => $images, 'views' => $views, 'pager' => $pager), 'News/List');
    }


    public function groupAction()
    {
        $page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        /*$result = Builder::factory('news')
                    ->select(array(
                        '`news_images`.`image`',
                        Builder::expr('(SELECT COUNT(`news_comments`.`id`) FROM `news_comments` WHERE `news_comments`.`news_id` = `news`.`id` AND `news`.`status` = 1) as comments_qty'),
                        '`news`.*'
                    ))
                    ->leftJoin('news_images')
                    ->on('news_images.news_id', '=', Builder::expr('`news`.`id`'))
                    ->on('news_images.main', '=', 1, 'AND')
                    //->where('news_images.main', '=', 1)
                    ->where('status', '=', 1)
                    ->where('date', '<=', time())
                    ->where('parent_id', '=', Route::param('group'))
                    ->order_by('news.date','DESC')
                    ->limit($this->limit, ($page - 1) * $this->limit)
                    ->find_all();*/
        /*$query = "SELECT `news_images`.image, `news`.*, (SELECT COUNT(`news_comments`.id) FROM `news_comments` WHERE news_id = `news`.id AND status = 1) as comments_qty
                    FROM `news`
                    LEFT JOIN `news_images`
                    ON `news_images`.news_id = `news`.id AND `news_images`.`main` = 1
                    WHERE (`status` = 1
                    AND `news`.`date` <= " . time() . "
                    AND `parent_id` = " . Route::param('group') . ")
                    ORDER BY `id` DESC
                    LIMIT " . ($this->limit) . " OFFSET " . (($page - 1) * $this->limit);
        $result = mysql::query($query);*/

        $query = "SELECT `news`.*, (SELECT COUNT(`news_comments`.id) FROM `news_comments` WHERE news_id = `news`.id AND status = 1) as comments_qty
                    FROM `news`
                    WHERE (`status` = 1
                    AND `news`.`date` <= " . time() . "
                    AND `parent_id` = " . Route::param('group') . ")
                    ORDER BY `id` DESC
                    LIMIT " . ($this->limit) . " OFFSET " . (($page - 1) * $this->limit);
        $result = mysql::query($query);

        $images = $this->getNewsImages($result);
        $views = $this->getNewsViews($result);

        $count = Builder::factory('news')
            ->where('status', '=', 1)
            ->where('date', '<=', time())
            ->where('parent_id', '=', Route::param('group'))
            ->count_all();
        $parent_ = Builder::factory('news_tree')->select()->where('id', '=', Route::param('group'))->find();
        conf::set('h1', $parent_->h1);
        // Generate pagination
        $pager = Frontend::pager($count, $this->limit, $page);
        // Render template
        return support::tpl(array('result' => $result, 'images' => $images, 'views' => $views, 'pager' => $pager), 'News/List');
    }

    public function itemAction()
    {
        $page = !(int)Route::param('page') ? 1 : (int)Route::param('page');
        $limit_comments = (int)conf::get('limit_comments');
        // Check for existance
        $this->alias = Route::param('alias');
        /*$mysql = 'SELECT     news.text,
                                news.views,
                                news.alias,
                                news.id,
                                news_images.image as image,
                                news.date,
                                news.title,
                                news.name,
                                news.h1,
                                news.keywords,
                                news.description,
                                news.images,
                                news.parent_id,
                                news.watermark_image,
                                news_tree.name as rubric
                    FROM news
                    LEFT JOIN news_tree
                    ON news_tree.id = news.parent_id
                    LEFT JOIN news_images
                    ON news_images.news_id = news.id
                    AND news_images.main = 1
                    WHERE news.alias = "' . $this->alias . '"
                    AND news.date <= "' . time() . '"
                    AND news.status = 1
                    LIMIT 1';
        $obj = mysql::query_one($mysql);*/
        $mysql = 'SELECT     news.text,
                                news.views,
                                news.alias,
                                news.id,
                                news.date,
                                news.title,
                                news.name,
                                news.h1,
                                news.keywords,
                                news.description,
                                news.images,
                                news.parent_id,
                                news.watermark_image
                    FROM news
                    WHERE news.alias = "' . $this->alias . '"
                    AND news.date <= "' . time() . '"
                    AND news.status = 1
                    LIMIT 1';

        $obj = mysql::query_one($mysql);
        $this->setSeoForGroup($obj);

        if (!$obj) {
            return conf::error();
        }
        $obj->text = htmlspecialchars_decode($obj->text);

        $comments_offset = ($page - 1) * $limit_comments;

        $comments = Builder::factory('news_comments')
            ->select(array(
                '`news_comments`.`text`',
                '`news_comments`.`answer`',
                '`news_comments`.`answer_date`',
                '`news_comments`.`user_name`',
                '`news_comments`.`date`',
                '`users`.`name`'
            ))
            ->leftJoin('users')
            ->on('users.id', '=', Builder::expr('`news_comments`.`user_id`'))
            ->where('news_id', '=', $obj->id)
            ->where('status', '=', 1)
            ->order_by('news_comments.date', 'DESC')
            ->limit($limit_comments, $comments_offset)
            ->find_all();

        $count = Builder::factory('news_comments')
            ->where('news_id', '=', $obj->id)
            ->where('status', '=', 1)
            ->count_all();

        $images = Builder::factory('news_images')
            ->select(array('main','image'))
            ->where('news_id', '=', $obj->id)
            ->find_all();
        
        if(count($images)){
            foreach ($images as $image){
                if($image->main == 1){
                    $obj->image = $image->image;
                }
            } 
        }

        $rubric = Builder::factory('news_tree')
            ->select('name')
            ->where('id', '=', $obj->parent_id)
            ->find();
        
        if($rubric){
            $obj->rubric = $rubric->name;
        }
        

        $pager = Frontend::pager($count, $limit_comments, $page);
        // Seo
        //conf::set( 'h1', $obj->h1 );
        /*conf::set( 'title', $obj->title ? $obj->title : $obj->name );
        conf::set( 'keywords', $obj->keywords );
        conf::set( 'description', $obj->description );*/

        if (support::is_file(HOST . '/images/news/big/' . $obj->image)) {
            conf::set('image_src', 'http://' . $_SERVER['HTTP_HOST'] . '/images/news/big/' . $obj->image);
        }

        $og_description = Text::limit_words(strip_tags($obj->text), 20);
        conf::set('og_title', $obj->title ? $obj->title : $obj->name);
        conf::set('og_description', $obj->description ? $obj->description : $og_description);
        if (support::is_file(HOST . '/images/news/small/' . $obj->image)) {
            conf::set('og_image', 'http://' . $_SERVER['HTTP_HOST'] . '/images/news/small/' . $obj->image);
        }

        $view = Builder::factory('news_views')
            ->select('views')
            ->where('news_id', '=', $obj->id)
            ->find();

        if(!$view){
            DB::insert('news_views', array('news_id', 'views'))->values(array($obj->id, 1))->execute();
        }

        $obj->views = $view->views;

        // Add plus one to views
        Builder::factory('news_views')->data(array(
            'views' => (int)$obj->views + 1,
        ))->where('news_id', '=', $obj->id)->edit();

        // Add plus one to views
        /*Builder::factory('news')->data(array(
            'views' => (int)$obj->views + 1,
        ))->where('id', '=', $obj->id)->edit();*/
        // Render template
        return support::tpl(array(
            'obj' => $obj,
            'images' => $images,
            'comments' => $comments,
            'commetns_count' => $count,
            'pager' => $pager
        ),
            'News/Inner');
    }

    public function comment_addAction()
    {
        //die('start');
        if ($_POST) {
            $post = $_POST;
            $pass = true;

            if ($_SESSION['user']) {
                $info = User::info();
                $post['user_id'] = $info->id;
                $post['user_name'] = '';
            } else {
                $post['user_id'] = 0;
                $post['user_name'] = trim($post['commentator_name']) ? trim($post['commentator_name']) : 'Anonymous';
                unset($post['commentator_name']);
            }

            $post['date'] = time();
            $post['news_id'] = $_GET['id'];
            $post['status'] = 0;
            if (strlen(trim($post['user_name'])) < 3 && !$_SESSION['user']) {
                //Message::GetMessage(2, 'Имя должно быть как минимум из 3 символов!');
                $this->error('Имя должно быть как минимум из 3 символов!');
                $pass = false;
            }
            if (strlen($post['text']) < 4) {
                //Message::GetMessage(2, 'Слишком короткий комментарий!');
                $this->error('Слишком короткий комментарий!');
                $pass = false;
            }
            if (strlen($post['text']) > 2000) {
                //Message::GetMessage(2, 'Слишком длинный комментарий!');
                $this->error('Слишком длинный комментарий!');
                $pass = false;
            }
            if ($pass) {
                if (Builder::factory('news_comments')->data($post)->add()) {
                    Message::GetMessage(1, 'Комментарий добавлен! Ждите одобрение администратора.');
                    //echo json_encode( false );
                    //die;
                    $this->success(array(
                        'redirect' => '/news/' . $_GET['alias'],
                        'noclear' => 1
                    ));
                }
            } else {
                $this->error('Comment error!');
                //echo json_encode( true );
                //die;
            }
            //location('/news/'.$_GET['alias']);
        }
        die('end');
    }

    /**
     * @param $result
     * @return array|bool
     */
    public function getNewsImages($result)
    {
        if (!count($result)) {
            return false;
        }

        $ids = array();
        foreach ($result as $obj) {
            $ids[] = $obj->id;
        }
        if (!count($ids)) {
            return false;
        }
        $_images = Builder::factory('news_images')
            ->where('news_images.main', '=', 1)
            ->where('news_id', 'IN', $ids)
            ->find_all();

        $images = array();
        foreach ($_images as $image) {
            $images[$image->news_id] = $image->image;
        }
        return $images;
    }

    /**
     * @param $result
     * @return array|bool
     */
    public function getNewsViews($result)
    {
        if (!count($result)) {
            return false;
        }

        $ids = array();
        foreach ($result as $obj) {
            $ids[] = $obj->id;
        }
        if (!count($ids)) {
            return false;
        }
        $_views = Builder::factory('news_views')
            ->where('news_id', 'IN', $ids)
            ->find_all();

        $views = array();
        foreach ($_views as $image) {
            $views[$image->news_id] = $image->views;
        }
        return $views;
    }

    // Set seo tags from template for items groups
    public function setSeoForGroup($page)
    {
        $tpl = Builder::factory('seo')->where('id', '=', 1)->find();
        $from = array('{{name}}', '{{content}}');
        $text = trim(strip_tags($page->text));
        $to = array($page->name, $text);
        $res = preg_match_all('/{{content:[0-9]*}}/', $tpl->description, $matches);
        if ($res) {
            $matches = array_unique($matches);
            foreach ($matches[0] AS $pattern) {
                preg_match('/[0-9]+/', $pattern, $m);
                $from[] = $pattern;
                $to[] = Text::limit_words($text, $m[0]);
            }
        }


        //$title = str_replace($from, $to, $tpl->title);
        conf::set('h1', str_replace($from, $to, $tpl->h1));
        conf::set('title', $page->name);
        conf::set('keywords', str_replace($from, $to, $tpl->keywords));
        conf::set('description', str_replace($from, $to, strip_tags($tpl->description)));
    }

    // Generate Ajax answer
    public function answer($data)
    {
        echo json_encode($data);
        die;
    }


    // Generate Ajax success answer
    public function success($data)
    {
        if (!is_array($data)) {
            $data = array(
                'response' => $data,
            );
        }
        $data['success'] = true;
        $this->answer($data);
    }


    // Generate Ajax answer with error
    public function error($data)
    {
        if (!is_array($data)) {
            $data = array(
                'response' => $data,
            );
        }
        $data['success'] = false;
        $this->answer($data);
    }

}