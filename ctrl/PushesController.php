<?php


class PushesController  {

    public function sendIdAction() {
        $userID = (int) Arr::get($_POST, 'subscriptionId', 0);

        if (count($userID)) {
            //Cookie::set('subscription', $userID);
            setcookie('subscription', $userID, time() + 60*60*24*28, '/');
        }

        $this->success(array(
            'success' => true,
            'data' => $userID
        ));
    }

    public function subscribeAction() {

        $ip = $_SERVER['HTTP_X_REAL_IP'];
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $date_subscribe = time();
        // Нет регистрации пользователей
        $user_id = NULL;

        $userID = (int) Arr::get($_POST, 'subscriptionId', 0);

        //$key_auth = Config::get('push.authorization_key');
        $key_auth = 'AAAA0py9lGs:APA91bGgLcw7lY4Ac939ydRfG2pvKfR6fxMMaUSxeLkSUkfcr-AY6pTSfZvVDEiCP4kKcnPk7lUU5qWlu-IcbdmsydyHBkmqHiA8sDHg7yjXKyp6NjNjH-oXFlLBuJUsR3tPZHhLSbos';

        if ($_POST) {

            $reg_id = $_POST['regid'];
            //$group = Push::getLastGroup();
            //$count = Push::countSubscribers($group->id);

            Push::insertUserInfo($reg_id, $browser, $ip, $user_id, $date_subscribe);
            setcookie('subscription', $userID, time() + 60*60*24*28, '/');

            /*if ($count <= 70) {
                $data_group = Push::dataGroup($group->id);
                Push::insertUserInfo($reg_id, $browser, $ip, $user_id, $date_subscribe);
                Push::addToGroup($reg_id, $data_group);
                Push::updateUserInfo($reg_id, $data_group->id);

               // Cookie::set('subscription', $userID);
                setcookie('subscription', $userID, time() + 60*60*24*28, '/');
            } else {
                $group_id = (int) $group->id + 1;
                $group_name = 'Kherson_push_grouptest11111-'.$group_id;
                $notification_key = Push::createGroup($group_name, $reg_id);

                $data = array();
                $data['notification_key'] = $notification_key->notification_key;
                $data['name'] = $group_name;
                $data['authorization_key'] = $key_auth;
                $data['project_id'] = '904572802155';

                Push::insertGroup($data);
                $group_new = Push::getLastGroup();

                Push::insertUserInfo($reg_id, $browser, $ip, $user_id, $date_subscribe);
                Push::addToGroup($reg_id, $group_new);
                Push::updateUserInfo($reg_id, (int) $group_new->id);

                //Cookie::set('subscription', $userID);
                setcookie('subscription', $userID, time() + 60*60*24*28, '/');
            }*/
        }
        die();
    }

    public function unsubscribeAction() {

        if ($_POST) {
            $reg_id = $_POST['regid'];
            // Удаляем
            $del = DB::update('push_message')->set(array('status' => 0))->where('reg_id', '=', $reg_id)->execute();
            if ($del) {
                return true;
            }
        }
        die();
    }

    public function pushesAction() {
        $push = DB::select()->from('push_message')->order_by('id', 'DESC')->execute();

        print_r(json_encode($push));
        die();
    }

    public function testAction()
    {
        $result = DB::select()->from('push_users')->where('group_user', '=', 9)->order_by('id', 'DESC')->limit(70)->as_object()->execute();

        foreach ($result as $value) {
            //var_dump($value->reg_id);

            $group_id = (int) $group->id + 1;
            $group_name = 'Kherson_push_' . $group_id;

            $group = Push::getLastGroup();
            $count = Push::countSubscribers($group->id);

            if ($count <= 70) {
                $data_group = Push::dataGroup($group->id);
                Push::addToGroup($value->reg_id, $data_group);
                Push::updateUserInfo($value->reg_id, $data_group->id);
            } else {
                $group_id = (int) $group->id + 1;
                $group_name = 'Kherson_push_grouptest11111-'.$group_id;
                $notification_key = Push::createGroup($group_name, $value->reg_id);

                $data = array();
                $data['notification_key'] = $notification_key->notification_key;
                $data['name'] = $group_name;
                $data['authorization_key'] = $key_auth;
                $data['project_id'] = '904572802155';

                Push::insertGroup($data);
                $group_new = Push::getLastGroup();
                Push::addToGroup($value->reg_id, $group_new);
                Push::updateUserInfo($value->reg_id, (int) $group_new->id);
            }

        }
        die;

    }

    public function cronPushAction()
    {
        $push = DB::select()->from('push_cron')->limit(10000)->as_object()->execute();
        $ids = [];
        $mas_ids = [];
        if (count($push)) {
            foreach ($push AS $group) {
                //Push::sendIntoGroup($group->group);
                $ids[$group->mas_id][] = $group->reg_id;
                $mas_ids[] = $group->mas_id;
                DB::delete('push_cron')->where('id', '=', $group->id)->execute();
            }

            $messages = DB::select()->from('push_message')->where('id', 'IN', $mas_ids)->where('lifetime', '>=', time())->order_by('id', 'DESC')->as_object()->execute();
            
            foreach ($messages as $obj) {
                $data = [];
                $data['registration_ids'] = $ids[$obj->id];
                $data['notification'] = [
                    'title' =>strip_tags($obj->title),
                    'body' =>strip_tags($obj->body),
                    'icon' =>$obj->icon,
                    'click_action' =>$obj->url,
                ];

               Push::sendGroup(json_encode($data));   
            }

        } else {
            $this->error('Нечего рассылать!');
        }
        $this->success('Сообщения разосланы!');
    }

    // Generate Ajax answer
    public function answer( $data ) {
        echo json_encode( $data );
        die;
    }


    // Generate Ajax success answer
    public function success( $data ) {
        if( !is_array( $data ) ) {
            $data = array(
                'response' => $data,
            );
        }
        $data['success'] = true;
        $this->answer( $data );
    }


    // Generate Ajax answer with error
    public function error( $data ) {
        if( !is_array( $data ) ) {
            $data = array(
                'response' => $data,
            );
        }
        $data['success'] = false;
        $this->answer( $data );
    }

}
