<?php

class Rss2Controller {
    public $limit;
    public $alias;

    function __construct() {
        conf::set( 'title', 'Новости' );
        conf::set( 'keywords', 'Новости' );
        conf::set( 'description', 'Новости' );
        $this->limit = (int) conf::get('limit');
    }

    public function indexAction() {

        $this->template = 'Rss';
        //conf::breadcrumbs( 'Новости' );
        header("Content-Type: text/xml");
        echo str_replace(array('&rsquo;', '&ndash;', '&nbsp;', '$', '&'), array('&#x2019;', '', '', '', '&amp;'), Rss2::get_content());

    }

    public function groupAction() {
        $this->template = 'Rss';
        //conf::breadcrumbs( 'Новости' );
        header("Content-Type: text/xml");
        echo str_replace(array('&rsquo', 'ndash', '&nbsp;', '$', '&'),array('&#x2019;', '', '', '',''),Rss2::get_content());
    }

    public function itemAction() {

        $page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $limit_comments = (int) conf::get('limit_comments');
        // Check for existance
        $this->alias = Route::param('alias');
        $mysql =    'SELECT     news.text,
                                    news.views,
                                    news.alias,
                                    news.id,
                                    news_images.image as image,
                                    news.date,
                                    news.title,
                                    news.name,
                                    news.h1,
                                    news.keywords,
                                    news.description,
                                    news_tree.name as rubric
                        FROM news
                        LEFT JOIN news_tree
                        ON news_tree.id = news.parent_id
                        LEFT JOIN news_images
                        ON news_images.news_id = news.id 
                        AND news_images.main = 1
                        WHERE news.alias = "'.$this->alias.'"
                        AND news.date <= "'.time().'"
                        AND news.status = 1
                        LIMIT 1';
        $obj = mysql::query_one($mysql);
        if( !$obj ) { return conf::error(); }
        $comments = Builder::factory('news_comments')
            ->select(array(
                '`news_comments`.`text`',
                '`news_comments`.`answer`',
                '`news_comments`.`answer_date`',
                '`news_comments`.`date`',
                '`users`.`name`'
            ))
            ->join('users')
            ->on('users.id','=',Builder::expr('`news_comments`.`user_id`'))
            ->where('news_id','=',$obj->id)
            ->where('status','=',1)
            ->order_by('date','DESC')
            ->limit($limit_comments, ($page - 1) * $limit_comments)
            ->find_all();

        $count = Builder::factory('news_comments')
            ->join('users')
            ->on('users.id','=',Builder::expr('`news_comments`.`user_id`'))
            ->where('news_id','=',$obj->id)
            ->where('status', '=', 1)
            ->count_all();

        $images = Builder::factory('news_images')
            ->select('image')
            ->where('news_id', '=', $obj->id)
            ->find_all();

        $pager = Frontend::pager($count, $limit_comments, $page);
        // Seo
        //conf::set( 'h1', $obj->h1 );
        conf::set( 'title', $obj->title );
        conf::set( 'keywords', $obj->keywords );
        conf::set( 'description', $obj->description );
        // Add plus one to views
        Builder::factory('news')->data(array(
            'views' => (int) $obj->views + 1,
        ))->where('id', '=', $obj->id)->edit();
        // Render template
        return support::tpl(array(
            'obj' => $obj,
            'images' => $images,
            'comments' => $comments,
            'commetns_count' => $count,
            'pager' => $pager
        ),
            'News/Inner' );
    }

    public function comment_addAction(){
        if($_POST && $_SESSION['user']){
            $info = User::info();
            $post = $_POST;
            $post['user_id'] = $info->id;
            $post['date'] = time();
            $post['news_id'] = $_GET['id'];
            $post['status'] = 0;
            if(strlen($post['text']) > 2000) {
                Message::GetMessage(2, 'Слишком длинный комментарий!');
                echo json_encode( true );
                die;
            }
            else {
                if(Builder::factory('news_comments')->data($post)->add()) {
                    Message::GetMessage(1, 'Комментарий добавлен! Ждите одобрение администратора.');
                    echo json_encode( false );
                    die;
                }

            }
            //location('/news/'.$_GET['alias']);
        }

    }

}