<?php
    
    class SearchController {

    	// Search list
        public function indexAction() {
            $page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        	// Seo
        	conf::set( 'h1', 'Поиск' );
            conf::set( 'title', 'Поиск' );
            conf::set( 'keywords', 'Поиск' );
            conf::set( 'description', 'Поиск' );
            //conf::breadcrumbs( 'Поиск' );
            // Check query
            $query = urldecode(Arr::get($_GET, 'query'));
            if( !$query ) {
            	return $this->noResults($query);
            }

            if( mb_strlen($query, 'UTF-8') < 2 ) {
                return $this->noBig($query);
            }
            // Get count items per page
            $limit = (int) Arr::get($_GET, 'per_page') ? (int) Arr::get($_GET, 'per_page') : conf::get('limit');
            // Get sort type
            $sort = in_array(Arr::get($_GET, 'sort'), array('name', 'created_at', 'cost')) ? Arr::get($_GET, 'sort') : 'sort';
            $type = in_array(strtolower(Arr::get($_GET, 'type')), array('asc', 'desc')) ? strtoupper(Arr::get($_GET, 'type')) : 'ASC';
            // Get items list
            $_sql = 'SELECT news_images.image, news.*, (SELECT COUNT(news_comments.id) FROM news_comments WHERE news_id = news.id AND status = 1) as comments_qty
                        FROM news
                        LEFT JOIN news_images
                        ON news_images.news_id = news.id
                        AND news_images.main = 1
                    WHERE news.text LIKE "%'.mysql_real_escape_string($query).'%" 
                    AND news.created_at < '.time().'
                    AND news.status = 1
                    OR news.name LIKE "%'.mysql_real_escape_string($query).'%" 
                    AND news.created_at < '.time().' 
                    AND news.status = 1
                    ORDER BY news.created_at DESC
                    LIMIT '.$limit.' 
                    OFFSET '.(($page - 1) * $limit);

            $result = mysql::query($_sql);
            $views = $this->getNewsViews($result);
//var_dump($result);
            // Check for empty list
            if( !count($result)) {
            	return $this->noResults($query);
            }
            // Count of parent groups
            $_sql = 'SELECT COUNT(news.id) as count
                        FROM news
                        LEFT JOIN news_images
                        ON news_images.news_id = news.id
                        AND news_images.main = 1
                    WHERE news.text LIKE "%'.mysql_real_escape_string($query).'%" 
                    AND news.created_at < '.time().'
                    AND news.status = 1
                    OR news.name LIKE "%'.mysql_real_escape_string($query).'%" 
                    AND news.created_at < '.time().' 
                    AND news.status = 1';

            $count = mysql::query_one($_sql)->count;
            // Generate pagination
            $pager = Frontend::pager($count, $limit, $page);
            // Render page
            return support::tpl( array('result' => $result, 'views' => $views, 'pager' => $pager), 'News/ItemsList' );
        }


        // This we will show when no results
        public function noResults($query) {
        	return '<p>По запросу "'.$query.'" ничего не найдено!</p>';
        }

        public function noBig($query) {
            return '<p>Ваш запрос "'.$query.'" слишком короткий!</p>';
        }


        /**
         * @param $result
         * @return array|bool
         */
        public function getNewsViews($result)
        {
            if (!count($result)) {
                return false;
            }

            $ids = array();
            foreach ($result as $obj) {
                $ids[] = $obj->id;
            }
            if (!count($ids)) {
                return false;
            }
            $_views = Builder::factory('news_views')
                ->where('news_id', 'IN', $ids)
                ->find_all();

            $views = array();
            foreach ($_views as $image) {
                $views[$image->news_id] = $image->views;
            }
            return $views;
        }

    }