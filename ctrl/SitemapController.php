<?php
    
    class SitemapController {

        // Search list
        public function indexAction() {
            // Seo
            conf::set( 'title', 'Карта сайта' );
            conf::set( 'keywords', 'Карта сайта' );
            conf::set( 'description', 'Карта сайта' );
            // Get pages
            $result = Builder::factory('content')->where('status', '=', 1)->order_by('sort')->find_all();
            $pages = array();
            foreach ($result as $obj) {
                $pages[$obj->parent_id][] = $obj;
            }
            
            // Render page
            return support::tpl( array('pages' => $pages), 'Sitemap/Index' );
        }

    }