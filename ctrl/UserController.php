<?php
    
    class UserController {

        public function __construct() {
            conf::set( 'h1', 'Личный кабинет' );
            conf::set( 'title', 'Личный кабинет' );
            conf::set( 'keywords', 'Личный кабинет' );
            conf::set( 'description', 'Личный кабинет' );
        }


        public function indexAction() {
            $info = User::info();
            if( !$info ) { return conf::error(); }
            conf::breadcrumbs( 'Личный кабинет' );
            return support::tpl( array( 'user' => $info ), 'User/Index' );
        }

        public function registrationAction() {
            if($_POST['agree'] = 'on' && isset($_POST['email'],$_POST['password'])) {
                $post = $_POST;
                $post['status'] = 1;
                $name = preg_match('/^([a-z0-9_\.-]+)@*/',$post['email'], $name_reg);
                $name = Builder::factory('users')->where('name','=',$name_reg[1])->count_all() ? $name_reg[1].time() : $name_reg[1];
                $post['name'] = $name;
                unset($post['agree']);
                //if($post['role_id']) {unset($post['role_id']);}
                if (User::factory()->registration($post)) {
                    Email::sendBody(conf::get('admin_email'),
                                            $post['email'],
                                            conf::get('name_site'),
                                            'Вы успешно зарегистрировались на сайте "'.conf::get('name_site').'"!<br>
                                            Ваш пароль для входа: '.$_POST['password'].';');
                    Message::GetMessage(1, 'Вы успешно зарегистрировались! Мы отправили вам письмо на указанный e-mail.'); 
                }
                else {
                    Message::GetMessage(2, 'Email уже зарегистрирован!'); 
                }
            }
            else {
                Message::GetMessage(2, 'Вы не заполнили некоторые формы!'); 
            }
            location( '/' );
        }


        public function logoutAction() {
            if( !User::info() ) { return conf::error(); }
            User::factory()->logout();
            Message::GetMessage(1, 'Возвращайтесь еще!');
            location( '/' );
        }

        public function loginAction() {
            $email = Arr::get( $_POST, 'email' );
            $password = Arr::get( $_POST, 'password' );
            $remember = (Arr::get( $_POST, 'remember' ) == 'on') ? 1 : 0;

            $u = User::factory();
            $user = $u->get_user_by_email( $email, $password, $remember );
            $u->auth( $user, $remember );
            if($user) {
                Message::GetMessage(1, 'Вы вошли на сайт!'); 
            }
            else {
                Message::GetMessage(2, 'Неправильный пароль или e-mail!'); 
            }
            
            location( '/' );
        }

        public function editAction() {
           $post = $_POST;
           $info = User::info();
           if($post['password'] != '' && $post['password_old'] != '') {
                if($post['password'] == $post['password_old']) {
                    Message::GetMessage(2, 'Новый и старый пароль не могут быть одинаковы.'); 
                }
                else if(User::factory()->hash_password($post['password']) == $info->password) {
                    Message::GetMessage(2, 'Неправильный старый пароль.'); 
                }
                else {
                    unset($post['password_old']);
                    $post['email'] = $post['email'] == '' ? $info->email : $post['email'];
                    $post['password'] = User::factory()->hash_password($post['password']);
                    if(Builder::factory('users')->data($post)->where('id','=',$info->id)->edit()) {
                        Message::GetMessage(1, 'Данные обновились.'); 
                    }
                    else {
                        Message::GetMessage(2, 'Ошибка на сервере.'); 
                    }
                }
           }
           else {
                unset($post['password'],$post['password_old']);
                if(Builder::factory('users')->data($post)->where('id','=',$info->id)->edit()) {
                    Message::GetMessage(1, 'Данные обновились.'); 
                }
                else {
                    Message::GetMessage(2, 'Ошибка на сервере.'); 
                }
           }
           location('/user');
        }


        public function confirmAction() {
            //if( User::info() ) { return conf::error(); }
            if( !Route::param('hash') ) { return conf::error(); }

            $user = User::factory()->get_user_by_hash( Route::param('hash') );
            if( !$user ) { return conf::error(); }
            if( $user->status ) {
                Message::GetMessage(0, 'Вы уже подтвердили свой E-Mail!');
                location( '/' );
            }

            Builder::factory('users')->data(array('status' => 1))->where('id', '=', $user->id)->edit();

            User::factory()->auth( $user, 0 );
            Message::GetMessage(1, 'Вы успешно зарегистрировались на сайте! Пожалуйста укажите остальную информацию о себе в личном кабинете для того, что бы мы могли обращаться к Вам по имени');
            location( '/user' );
        }


        public function profileAction() {
            if( !User::info() ) { return conf::error(); }
            $this->addMeta('Редактирование личных данных');
            return support::tpl( array( 'user' => User::info() ), 'User/Profile' );
        }


        public function ordersAction() {
            if( !User::info() ) { return conf::error(); }
            if( (int) Route::param('id') ) {
                return $this->orderAction();
            }
            $this->addMeta('Мои заказы');
            $orders = mysql::query('SELECT  orders.*, SUM(orders_items.cost * orders_items.count) AS amount
                                    FROM orders
                                    LEFT JOIN orders_items ON orders_items.order_id = orders.id
                                    WHERE orders.user_id = "'.User::info()->id.'"
                                    GROUP BY orders.id ORDER BY orders.created_at DESC');
            return support::tpl( array( 'orders' => $orders, 'statuses' => conf::get('order.statuses') ), 'User/Orders' );
        }


        public function orderAction() {
            if( !User::info() ) { return conf::error(); }
            $this->addMeta('Заказ №' . Route::param('id'), true);
            $result = mysql::query_one('SELECT orders.*, SUM(orders_items.count) AS count, SUM(orders_items.cost * orders_items.count) AS amount
                                    FROM orders
                                    LEFT JOIN orders_items ON orders_items.order_id = orders.id
                                    WHERE orders.id = "'.Route::param('id').'"');
            $cart = mysql::query('SELECT catalog.*, catalog_images.image, orders_items.count, sizes.name AS size_name, orders_items.cost AS price, orders_items.size_id
                                    FROM orders_items
                                    LEFT JOIN catalog ON orders_items.catalog_id = catalog.id
                                    LEFT JOIN catalog_images ON catalog_images.main = "1" AND catalog_images.catalog_id = catalog.id
                                    LEFT JOIN sizes ON orders_items.size_id = sizes.id
                                    WHERE orders_items.order_id = "'.Route::param('id').'"');
            return support::tpl( array( 
                        'obj' => $result,
                        'cart' => $cart,
                        'payment' => conf::get('order.payment'),
                        'delivery' => conf::get('order.delivery'),
                        'statuses' => conf::get('order.statuses'),
            ), 'User/Order' );
        }


        public function printAction() {
            if( !User::info() ) { return conf::error(); }
            $order = mysql::query_one('SELECT orders.*, SUM(orders_items.count) AS count, SUM(orders_items.cost * orders_items.count) AS amount
                                       FROM orders
                                       LEFT JOIN orders_items ON orders_items.order_id = orders.id
                                       WHERE orders.id = "'.Route::param('id').'"');
            $list = mysql::query('SELECT catalog.*, catalog_images.image, orders_items.count, sizes.name AS size_name, orders_items.cost AS price, orders_items.size_id
                                    FROM orders_items
                                    LEFT JOIN catalog ON orders_items.catalog_id = catalog.id
                                    LEFT JOIN catalog_images ON catalog_images.main = "1" AND catalog_images.catalog_id = catalog.id
                                    LEFT JOIN sizes ON orders_items.size_id = sizes.id
                                    WHERE orders_items.order_id = "'.Route::param('id').'"');
            return support::tpl( array(
                'order' => $order,
                'list' => $list,
                'payment' => conf::get('order.payment'),
                'delivery' => conf::get('order.delivery'),
                'statuses' => conf::get('order.statuses'),
            ), 'User/Print' );
        }


        public function change_passwordAction() {
            if( !User::info() ) { return conf::error(); }
            $this->addMeta('Изменить пароль');
            return support::tpl( array(), 'User/ChangePassword' );
        }


        public function addMeta( $name, $order = false ) {
            conf::set( 'h1', $name );
            conf::set( 'title', $name );
            conf::set( 'keywords', $name );
            conf::set( 'description', $name );
            conf::breadcrumbs( 'Личный кабинет', '/user' );
            if( $order ) {
                conf::breadcrumbs( 'Заказы', '/user/orders' );
            }
            conf::breadcrumbs( $name );
        }
        
    }