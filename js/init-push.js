$(document).ready(function(){
     /* Push notifications*/
    (function (window) {
        var popup;
        var POPUP_WIDTH = window.innerWidth >= 400 ? 380 : 300;
        var POPUP_HEIGHT = 250;
        var cookieFlag = cookieHelper.getCookie('subscription');
        if(cookieFlag){
            console.log('no need to show subs');
        }
        else{
            console.log('show subs');
            $('.js-subscription').show();
        }
        $(document).on('click', '.js-subscription__close', function () {
            $(this).closest(".js-subscription").remove();
        });
        $(document).on('click', '.js-subscription__start', function (e) {
            var offsetLeft = (window.innerWidth - POPUP_WIDTH) / 2;
            var offsetTop = (window.innerHeight - POPUP_HEIGHT) / 2;
            popup = window.open('https://' + window.location.host + '/notifications', 'notifications',
                'width=' + POPUP_WIDTH +
                ',height=' + POPUP_HEIGHT +
                ',toolbar=no,scrollbars=no' +
                ',left=' + offsetLeft +
                ',top=' + offsetTop);
            console.log('subscription__start');
            $(this).closest(".js-subscription").remove();
        });
        $(document).on('click', '.js-subscription__decline', function (e) {
            cookieHelper.setCookie('subscription', false, {alias: '/'});
            $(this).closest(".js-subscription").remove();
        });
    })(window);
}); 