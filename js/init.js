function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

 // Загрузка всех не загруженых картинок
 function lazyLoadImg(objects,sourceAttr){
   sourceAttr = typeof sourceAttr !== "undefined" ? sourceAttr : 'data-src';
   $(objects).find('img['+ sourceAttr +']').each(function(index, el) {
       if(!$(el).hasClass('loaded') && $(el).attr(sourceAttr)){
           $(el)
           .attr('src',$(el).attr(sourceAttr))
           .removeAttr(sourceAttr)
           .addClass('loaded');
       }
   });
}
// Загрузка картинок у слайдера
function checkSlidersVisibleImgs(container){
   container = typeof container !== 'undefined' ? container : "body";
   $(container).find('.caroufredsel_wrapper>*').each(function(index, el) {
       $(el).trigger("currentVisible", function( items ) {
           lazyLoadImg(items);
       });
   });
}
// Загрузка картинок во время их появления на экране
function bindInviewLazy(container,sourceAttr1, sourceAttr2){
   container = typeof container !== 'undefined' ? container : "body";
   sourceAttr1 = typeof sourceAttr1 !== 'undefined' ? sourceAttr1 : "data-src";
   sourceAttr2 = typeof sourceAttr2 !== 'undefined' ? sourceAttr2 : "data-background";
   $(container).find('.lazyImg').bind('inview', function(event,isVisible) {
       if(isVisible){
           if(!$(this).hasClass('loaded') && $(this).attr(sourceAttr1)){      
               $(this)
               .attr('src',$(this).attr(sourceAttr1))
               .removeAttr(sourceAttr1)
               .addClass('loaded');
           }
       }
   });
   $(container).find('.lazyDiv').bind('inview', function(event,isVisible) {
	   if(isVisible){
           if(!$(this).hasClass('loaded') && $(this).attr(sourceAttr2)){                  
               $(this)
               .css('background-image',$(this).attr(sourceAttr2))
               .removeAttr(sourceAttr2)
               .addClass('loaded');
           }
       }
   });
}
function fbInit(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); 
	js.id = id;
	js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
	fjs.parentNode.insertBefore(js, fjs);
}
$(document).ready(function() { $('.wForm').each(function() {
        var formValid = $(this);
        formValid.validate({
            showErrors: function(errorMap, errorList) {
                if (errorList.length) {
                    var s = errorList.shift();
                    var n = [];
                    n.push(s);
                    this.errorList = n;
                }
                this.defaultShowErrors();
            },
            invalidHandler: function(form, validator) {
                $(validator.errorList[0].element).trigger('focus');
                formValid.addClass('no_valid');
            },
            submitHandler: function(form) {
                formValid.removeClass('no_valid');
                if (form.tagName === 'FORM') {
                    form.submit();
                } else {
                    /* Без тега FORM */
                    console.warn('Without tag FORM');
                }
            }
        });
    });

    /* Без тега FORM */
    $('.wSubmit').on('click', function(event) {
        var form = $(this).closest('.wForm');
        form.valid();
        if (form.valid()) {
            form.submit();
        }
    });
	
	//18.06.2015
    if ($('.st-control-b').length) {
        $('.st-control-b').on('click', function(event) {
            $(this).next('.car-wrap-t').find('.sl-thumb-cool').removeClass('dn-this');
        }).hover(function() {
            $(this).next('.car-wrap-t').find('.sl-thumb-cool').removeClass('dn-this');
        }, function() {});
        $('.sl-thumb-cool').on('mouseleave', function(event) {
            $(this).addClass('dn-this');
        });
    }

    $(window).load(function() {
		if($(".js-sticky").length && $('.js-sticky-parent').length) {
			$(".js-sticky").stick_in_parent({
				parent: '.js-sticky-parent',
				offset_top: 50
			});
		}
        if ($('.carfr-sl').length) {
            $('.carfr-sl').carouFredSel({
                auto: false,
				width:'100%',
				responsive: true,
                items: {
                    visible: 1
                },
                scroll: {
                    items: 1,
                    fx: "fade",
                    easing: "linear",
                    duration: 500
                },
                pagination: {
                    container: '.sl-thumb-cool',
                    anchorBuilder: function(nr) {
						var src = ($(this).find('img').attr('data-src')) ? $(this).find('img').attr('data-src') : $(this).find('img').attr('src');
                        return '<a href="#" class="thumb' + '"><img src="'  + src + '" data-num="'+$(this).find('img').data('num')+'"/></a>';
                    }
                },
				prev: {
					button: '#prev-sl'
				},
				next: {
					button: '#next-sl'
				}
            });
			
			$('#next-sl').on('click',function() {
				var cur=parseInt($('.f_alb').text());
				var total=parseInt($('.f_alb_total').text());
				if (cur!=total) {
					$('.f_alb').text(cur+1)
				} else {
					$('.f_alb').text(1)
				}
			})
			
			$('#prev-sl').on('click',function() {
				var cur=parseInt($('.f_alb').text());
				var total=parseInt($('.f_alb_total').text());
				if (cur!=1) {
					$('.f_alb').text(cur-1)
				} else {
					$('.f_alb').text(total)
				}
			})
			$('.sl-thumb-cool').on('click','img',function() {
				$('.f_alb').text($(this).data('num'));
			})
        }
    });

    /*----------------------------------------------*/

$(".w_tab:first").show();

$(".s_link:first").addClass('active');

$(".s_link").click(function() {

	var tab = $($(this).attr("href"));

	$(".w_tab").hide().filter(tab.fadeIn(150));

	$(".s_link").removeClass('active').filter($(this).addClass('active'));

	return false;
});

// detect transit support
var transitFlag = true;
if (!Modernizr.cssanimations) {
	transitFlag = false;
}

	$(".inps").validate({
});

$('.inps a').on('click', function(event) {
	var form = $(this).closest('.inps');
	form.valid();
	if (form.valid()) {
		form.addClass('success');
	} else {
		form.removeClass('success');
		return false;
	}
});

$('.enterReg2').on('click', function(event) {
	var form = $(this).closest('.statusu');
	form.valid();
	if (form.valid()) {
		form.addClass('success');
	} else {
		form.removeClass('success');
		return false;
	}
});

$('.tab .chob > a').each(function(i, el) {
	if($(el).hasClass('active')) {
		idvis = $(el).attr('data-id');
	}
	$("#"+idvis).show();
});

	$(document).on("click", ".tab .chob > a", function(e) {
	e.preventDefault();
	if (!$(this).hasClass('active')) {
		var data = $(this).attr('data-id');
		var taba = $(this).closest('.tab');
		var curr = taba.find(".active").attr('data-id');    
		$("#" + curr).stop().fadeOut("slow", function() {
			$("#" + data).stop().fadeIn("slow");
			taba.find("[data-id=" + data + "]").addClass('active');
			taba.find("[data-id=" + curr + "]").removeClass('active');
		});
	} else {
		return false;
	}
});
	
$(document).ready(function() {
	$('#slidet').carouFredSel({
		auto: 6000,
		prev:'#next',
		next:'#prev',
		width:'100%',
		responsive: true,
		scroll: {
			items: 1,
			easing: "swing",
			duration: 800,
			pauseOnHover: true
		},
		onCreate: function(){
			$('#slidet').css('opacity', '1');
			$('.slidert').css('background', 'none');
		}
	}, {
		transition: transitFlag
	});
	if ($('.mBanners').length) {
		$('.mBanners').each(function(){
			var mBannersTime = $(this).data('time');
			$(this).carouFredSel({
				auto: mBannersTime || 7000,
				width:'100%',
				responsive: true,
				scroll: {
					fx: 'crossfade',
					items: 1,
					easing: "swing",
					duration: 500
				}
			}, {
				transition: transitFlag
			});
		});
	}
});

$(window).load(function() {
	if ($('.carouselBlockIn').length) {
		$('.carouselBlockIn ul').carouFredSel({
			auto: false,
			width: '100%',
			responsive: true,
			items: {
				visible: {
					max: 2,
					min: 1
				},
				width: 321,
				height: 220
			},
			scroll: {
				items: 1
			},
			prev: '.prevC',
			next: '.nextC',
			swipe: {
				onTouch: true
			},
			onCreate: function(){
			   $('.preload2').hide();
			   $('.carouselBlockBot').css('display','table');
			}
		}, {
			transition: transitFlag
		});
	}

	
	//Подключение плагина ВК
	if ($('#vk_groups').length) {
		$.getScript('https://vk.com/js/api/openapi.js?116', function(){
		VK.Widgets.Group("vk_groups", {mode: 0, width: "223", height: "300", color1: 'FFFFFF', color2: '050505', color3: '646565'}, 88229604);
		});
	}

	//Подключение плагина Fb
	fbInit(document, 'script', 'facebook-jssdk');

	//Подключение плагина погоды
	if ($('#SinoptikInformer').length) {
		$.getScript('https://sinoptik.ua/informers_js.php?title=4&wind=3&cities=303027883&lang=ru');
	}

	//Подключение плагина валют
	if (typeof(iFinance) == "undefined") {
		if (typeof(iFinanceData) == "undefined") {
			$.getScript("https://i.i.ua/js/i/finance_informer.js?1");
			iFinanceData = [];
		}
		iFinanceData.push({b:15,c:[840,978,643],enc:0, lang:0,p:'96'});
	} else {
		window['oiFinance96'] = new iFinance();
		window['oiFinance96'].gogo({b:15,c:[840,978,643],enc:0, lang:0,p:'96'});
	}
});

$('.paginWrapp').on('click', '.curr', function(event) {
	return false;
});

$('.displit').on('click', 'a', function(event) {
	event.preventDefault();
	if (!$(this).hasClass('cur')) {
		$('.displit a').removeClass('cur');
		$(this).addClass('cur');
		if ($(this).text() == 'Фото') {
			$('.albbl > div').each(function(index, el) {
				if ($(el).hasClass('vi')) {
					$(el).addClass('display-none');
				} else {
					$(el).removeClass('display-none');
				}
			});
		}

		if ($(this).text() == 'Видео') {
			$('.albbl > div').each(function(index, el) {
				if ($(el).hasClass('ph')) {
					$(el).addClass('display-none');
				} else {
					$(el).removeClass('display-none');
				}
			});
		}

		if ($(this).text() == 'Все') {
			$('.albbl > div').each(function(index, el) {
				$(el).removeClass('display-inline');
			});
		}

	}
});

$('.enterReg').magnificPopup({
    type: 'inline',
    midClick: true,
    removalDelay: 300,
    mainClass: 'zoom-in'
});

$('#forget_pass').on('click', function(event) {
    $('#entrForm').removeClass('visForm');
    $('#forgetForm').addClass('visForm');
});

$('#remember_pass').on('click', function(event) {
    $('#forgetForm').removeClass('visForm');
    $('#entrForm').addClass('visForm');
});


/*-------------------------------  responsive EnterPopup  ------------------------------------*/

$('#enterReg').on('click', '.erTitle', function(event) {
    event.preventDefault();
    if($(window).width() < 720) {
        if(!$(this).parent().hasClass('wCur')) {
            $('#enterReg .popupBlock').removeClass('wCur').filter($(this).parent()).addClass('wCur');
        }
    }
});


$('li.flyout, .flyout-content, .onhoverdrop').on('mouseenter', function(event) {
    if ($(this).children('.flyout-content')) {
        $(this).children('.flyout-content').stop().fadeIn();
    }
});

$('li.flyout, .flyout-content, .onhoverdrop').on('mouseleave', function(event) {
    if ($(this).children('.flyout-content')) {
        $(this).children('.flyout-content').stop().fadeOut();
    }
});

/*----------------------------------------------*/

/* bMyScript */

// Ширина слайдера на главной
$('#slidet').children('div').css('width', $('#slidet').closest('.col').width() + 'px');

$(window).resize(function(){
	// Ширина слайдера на главной
	$('#slidet').children('div').css('width', $('#slidet').closest('.col').width() + 'px');
});

// Адаптивное меню
var socials = '<div class="soc">';
	socials += '<a href="https://www.youtube.com/channel/UCv-rw5I39d4NbR_WmST29Pw"><img src="https://khersondaily.com/pic/youtube.png" alt=""></a>';
	socials += '<a href="https://vk.com/khersondaily"><img src="https://khersondaily.com/pic/vkb.png" alt=""></a>';
	socials += '<a href="https://www.facebook.com/pages/%D0%A5%D0%B5%D1%80%D1%81%D0%BE%D0%BD-Daily/711871785597486?ref=hl"><img src="https://khersondaily.com/pic/ffb.png" alt=""></a>';
socials += '</div>';
var email = '<a class="mailto" href="mailto:khersondaily@gmail.com">khersondaily@gmail.com</a>';
$('.js-menuContent').mmenu({
	extensions: ['theme-dark', 'effect-menu-slide', 'pagedim-black'],
	navbar: {
		title: 'Категории'
	},
	navbars	: [
		{
			position : 'top'
		},{
			position : 'bottom',
			content : [socials]
		},{
			position : 'bottom',
			content : [email]
		}
	]
});

// Запуск плагина для загрузки картинок, когда они видны
bindInviewLazy();

$('#login').click(function(e){
	e.preventDefault();
	$.post('/form/login',
		{
			password: $('.log input[name="password"]').val(),
			email: $('.log input[name="email"]').val(),
			remember: $('.log input[name="remember"]').val()
		},
		function(data){
			if(data.success == true) {
				location.reload() 
			}
			else {
				var n = noty({
					text: data.response,
					type: 'alert'
				});
			}
		},'json');
});
$('#reg').click(function(e){
	e.preventDefault();
	$.post('/form/registration',
		{
			password: $('.reg input[name="password"]').val(),
			email: $('.reg input[name="email"]').val(),
			agree: $('.reg input[name="agree"]').val()
		},
		function(data){
			if(data.success == true) {
				var n = noty({
					text: data.response,
					type: 'success'
				}); 
			}
			else {
				var n = noty({
					text: data.response,
					type: 'alert'
				});
			}
		},'json');
});

    

	if (!getCookie('kdaily') == "1") {
		setTimeout(function () {
			$('.facebook-block').addClass('is-visible');
			setCookie('kdaily', '1', 7);
        }, 30000)
	}
    

	$('.facebook-block').on('click', '.js-close-fb', function(e){
		$(e.delegateTarget).removeClass('is-visible');
        setCookie('kdaily', '1', 7);
	});
});

$(document).ready(function() {
	$('.popap-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
});