var typeNotyMessage;
var generate = function( message, typeNotyMessage ){
    if( !typeNotyMessage ) {
        typeNotyMessage = 'alert';
    }
    noty({
        layout: 'top',
        text: message,
        timeout: 2500,
        type: typeNotyMessage
    });
}

$(function(){

    if( $('ul.color_c').length ) {
        $('ul.color_c input').each(function(){
            if ($(this).prop('disabled')) {
                $(this).closest('li').css('opacity', '0.2');
                $(this).closest('li').find('ins').css('border', '0');
                $(this).css('cursor', 'auto');
            }
        });
        $('ul.color_c a').on('click', function(){
            window.location.href = $(this).attr('href');
        });
    }

    var v1 = parseInt($("#amount").val(),10);
    var v2 = parseInt($("#amount2").val(),10);
    var min = parseInt($("#amount").data('cost'),10);
    var max = parseInt($("#amount2").data('cost'),10);

    if ($(".price_ui").length) {
        
        var slider = $("#slider-range").slider({
            range: true,
            min: min,
            max: max,
            step: 1,
            values: [v1, v2],
            slide: function(event, ui) {
                $("#amount").val(ui.values[0]);
                $("#amount2").val(ui.values[1]);
            },
        });
        $("#amount").val($("#slider-range").slider("values", 0));
        $("#amount2").val($("#slider-range").slider("values", 1));

        $("#amount").ForceNumericOnly();
        $("#amount2").ForceNumericOnly();

        $("#amount").keyup(function() {
            if ($("#amount2").val().length < 1) {
                slider.slider("values", [$(this).val(), min]);
            } else {
                if (parseInt($("#amount2").val()) < parseInt($(this).val())) {
                    return false;
                } else {
                    slider.slider("values", [$(this).val(), $("#amount2").val()]);
                }
            }
        });

        $("#amount2").keyup(function() {
            if ($("#amount").val().length < 1) {
                slider.slider("values", [0, $(this).val()]);
            } else {
                if (parseInt($("#amount").val()) > parseInt($(this).val())) {
                    return false;
                } else {
                    slider.slider("values", [$("#amount").val(), $(this).val()]);
                }
            }
        });

        $("#amount2").blur(function(){
            if($("#amount2").val() < $("#amount").val()) {
               $("#amount2").val($("#amount").val());
                slider.slider("values", [$("#amount").val(), $("#amount").val()]);
            }
        });

        $("#amount").blur(function(){
            if($("#amount").val() < min) {
               $("#amount").val(min);
                slider.slider("values", [min, $("#amount2").val()]);
            }
        });

        $("input[type='reset']").on('click', function() {
            slider.slider("values", [min, max]);
        });

        $("input[type='reset']").on('mouseenter', function() {
            $(".reset_but span").css('border-bottom','1px dotted #ccc');
        });

        $("input[type='reset']").on('mouseleave', function() {
            $(".reset_but span").css('border-bottom','transparent');
        });

    }

    $('.enterReg5').on('click', function(){
        var id = $(this).data('id');
        $('#idFastOrder').val(id);
    });

    if($(".prevue_block").length) {
        $(".prevue_block").on('click', '.img_prevue', function() {
            var src_img = $(this).attr('data-img-src');
            var src_img_original = $(this).attr('data-img-src-original');
            $(".big_prevue").find("img").attr('src',src_img);
            $(".big_prevue").attr('href',src_img_original);
        });
    }

    if( $('.lk_menu').length ) {
        var h1 = $('.lk_menu').height();
        var h2 = $('.lk_content').height();
        if( h1 > h2 ) {
            $('.lk_content').height( h1 );
        }
        if( h2 > h1 ) {
            $('.lk_menu').height( h2 );
        }
    }

    $('.wSubmit').on('click', function(event) {
        var button = $(this);
        var form = button.closest('.wForm');
        form.validate({
            errorElement: "span",
            errorClass: "wError",
            rules: {
                enter_email: {email: true},
                forget_email: {email: true},
                enter_pass: {minlength: 4},
                reg_pass: {minlength: 4},
                phone_num: {phoneUA: true},
                password: "required",
                confirm: {
                  equalTo: "#password"
                },
            },
            showErrors: function(errorMap, errorList) {
                if (errorList.length) {
                    var s = errorList.shift();
                    var n = [];
                    n.push(s);
                    this.errorList = n;
                }
                this.defaultShowErrors();
            },
            invalidHandler: function(form, validator) {
                $(validator.errorList[0].element).trigger('focus');
            }
        });
        form.valid();
        if (form.valid()) {
            form.addClass('success');
            if( form.data('ajax') ) {
                var data = form.find('input,textarea,select').serializeArray();
                $.ajax({
                    url: '/form/' + form.data('ajax'),
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        data: data
                    },
                    success: function(data){
                        if( data.success ) {
                            if (!data.noclear) {
                                form.find('input').each(function(){
                                    if( $(this).attr('type') != 'hidden' && $(this).attr('type') != 'checkbox' ) {
                                        $(this).val('');
                                    }
                                });
                                form.find('textarea').val('');
                            }
                            if ( data.response ) {
                                generate(data.response, 'success');
                            }
                        } else {
                            if ( data.response ) {
                                generate(data.response, 'warning');
                            }
                        }

                        if( data.redirect ) {
                            window.location.href = data.redirect;
                        }
                    }
                });
                return false;
            }
        } else {
            form.removeClass('success');
            return false;
        }
    });

    /// CART START
    var setTopCartCount = function(count){
        $('#topCartCount').text(count);
    }
    $('.addToCart').on('click', function(e){
        e.preventDefault();
        var it = $(this);
        var id = it.data('id');
        var size;
        if ($('#select5').length) {
            size = $('#select5').val();
        } else {
            size = 0;
        }
        $.ajax({
            url: '/ajax/addToCart',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id,
                size: size
            },
            success: function(data){
                if( data.success ) {
                    var html = '';
                    var item;
                    var count = 0;
                    var amt;
                    var amount = 0;
                    for( var i = 0; i < data.cart.length; i++ ) {
                        item = data.cart[i];
                        amt = parseInt( item.count ) * parseInt( item.cost );
                        html += '<li class="wb_item" data-size="'+item.size_id+'" data-id="'+item.id+'" data-count="'+item.count+'" data-price="'+item.cost+'">';
                        html += '<div class="wb_li">';
                        if ( item.image ) {
                            html += '<div class="wb_side"><div class="wb_img">';
                            html += '<a href="/catalog/'+item.alias+'" class="wbLeave"><img src="'+item.image+'" /></a>';
                            html += '</div></div>';
                        }
                        html += '<div class="wb_content">';
                        html += '<div class="wb_row">';
                        html += '<div class="wb_del"><span title="Удалить товар">Удалить товар</span></div>';
                        html += '<div class="wb_ttl"><a href="/catalog/'+item.alias+'" class="wbLeave">'+item.name+'</a></div>';
                        html += '</div>';
                        html += '<div class="wb_cntrl">';
                        html += '<div class="wb_price_one"><p><span>'+item.cost+'</span> грн.</p></div>';
                        html += '<div class="wb_amount_wrapp">';
                        html += '<div class="wb_amount">';
                        html += '<input type="text" value="'+item.count+'">';
                        html += '<span data-spin="plus"></span>';
                        html += '<span data-spin="minus"></span>';
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="wb_price_totl"><p><span>'+amt+'</span> грн.</p></div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</li>';
                        amount += amt;
                        count += parseInt( item.count );
                    }
                    $('#topCartList').html(html);
                    $('#topCartAmount').html(amount);
                    setTopCartCount(count);
                }
                $('.wb_edit_init').click();
            }
        });
    });
    /// CART END
});
