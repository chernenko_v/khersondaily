console.log('pushes');
// document.addEventListener('DOMContentLoaded',function(){

window.Pushes = (function () {
    if ('serviceWorker' in navigator) {
        'use strict';
        console.log('Service Worker is supported');

        var SERVICE_WORKER_FILE_PATH = '/sw.js?2';
        var SUBSCRIBE_URL = "/pushes/subscribe";
        //var SUBSCRIBE_URL = "push/sendId";
        var UNSUBSCRIBE_URL = "/pushes/unsubscribe";
        var API = {
            registration: '',
            subscription: '',
            isSubscribed: false,
            init: function () {
                var that = this;
                console.log('init');
                navigator.serviceWorker.register(SERVICE_WORKER_FILE_PATH).then(function () {
                    return navigator.serviceWorker.ready;
                }).then(function (serviceWorkerRegistration) {
                    that.registration = serviceWorkerRegistration;
                    serviceWorkerRegistration.pushManager.getSubscription().then(function (subscription) {
                        if (subscription) {
                            that.subscription = subscription;
                            that.isSubscribed = true;
                            cookieHelper.setCookie('subscription', true, {alias: '/'});

                            console.log(subscription);
                            console.log(subscription.endpoint.split('/').slice(-1)[0]);

                            alert('Вы уже подписаны на уведомления');
                            // window.close();

                            // var clientAuthSecret = new Uint8Array(subscription.getKey('auth'));
                            // var clientPublicKey = new Uint8Array(subscription.getKey('p256dh'));
                        }
                        else {
                            console.log('no subscription');
                            that.isSubscribed = false;
                            that.subscribe();
                        }
                    }, function () {
                        console.log('no push manager subscription');
                    });
                }).catch(function (error) {
                    console.log('Service Worker Error :^(', error);
                    alert('На вашей платформе уведомления не поддерживаются');
                    cookieHelper.setCookie('subscription', false, {alias: '/'});
                    window.close();
                });
            },
            subscribe: function () {
                var that = this;
                console.log('subs');
                that.registration.pushManager.subscribe({
                        userVisibleOnly: true
                    })
                    .then(function (pushSubscription) {
                        console.log('subscribed');
                        that.subcription = pushSubscription;
                        var message = "regid=" + pushSubscription.endpoint.split('/').slice(-1)[0];
                        console.log(message);
                        fetch(SUBSCRIBE_URL, {
                            method: "POST",
                            headers: {
                                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                            },
                            body: message
                        }).then(function (response) {
                            if (response.status === 200) {
                                that.isSubscribed = true;
                                cookieHelper.setCookie('subscription', true, {alias: '/'});
                                // window.close();
                            }
                        }).catch(function (error) {
                            console.log('Service Worker Error :^(', error);
                        });
                    },function(err){
                        console.log(err);
                        // user declined subscription
                        cookieHelper.setCookie('subscription', false, {alias: '/'});
                        window.close();
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            },
            unsubscribe: function () {
                var that = this;
                var message = {
                    reg_id: that.subscription.endpoint.split('/').slice(-1)[0]
                };
                that.subscription.unsubscribe().then(function (event) {
                    console.log('Unsubscribed!', event);
                    that.isSubscribed = false;
                    fetch(UNSUBSCRIBE_URL, {
                        method: "POST",
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(message)
                    }).then(function (response) {
                        if (response.status === 200) {
                            that.isSubscribed = true;
                        }
                    });
                }).catch(function (error) {
                    console.log('Error unsubscribing', error);
                });
            }
        };
        API.init();
        return API;
    }
    else {
        return null;
    }
})(window);
