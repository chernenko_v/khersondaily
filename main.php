<?php
    #error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    ini_set('display_errors','off');

    define ('APPLICATION','');
	ob_start();	
	header("Cache-Control: public");
	header("Expires: " . date("r", time() + 3600));
	header('Content-Type: text/html; charset=UTF-8');
    
    define('HOST', dirname(__FILE__));

    // require_once HOST.'/plagin/Profiler/Profiler.php';
    
    @session_start();
    
    function __autoload($class) {
        if (file_exists(HOST.'/classes/'.$class.'.class.php')) {
            require_once HOST.'/classes/'.$class.'.class.php';
        } else {
            $folders = scandir(HOST.'/plagin');
            unset($folders[0], $folders[1]);
            foreach ($folders as $folder) {
                $arr = explode('_', $class);
                $path = HOST.'/plagin/'.$folder.'/'.implode('/', $arr).'.php';
                if(file_exists($path)) {
                    require_once $path;
                }
            }
        }
    }
    include_once "classes/free.class.php";
    // include_once "putt.php";
    mysql::connect();

    define('MAIN_PATH',		"https://".$_SERVER [ 'HTTP_HOST']);
    define('CTRL_PATH',     HOST.'/ctrl');
    define('TPL_PATH',      HOST.'/tpl');
    define('MODEL_PATH',    HOST.'/classes');
    define('PLAGINS_PATH',  HOST.'/plagin');
    define('CACHE_PATH',    HOST.'/cache');
    define('ADMIN_PATH',    HOST.'/backend');
	//define('SITE_PATH',     '//khersondaily.com'); // Для того, что бы картинки на локалку попадали с рабочего сайта
	define('SITE_PATH',   "https://".$_SERVER [ 'HTTP_HOST']); // Поменять на эту ссылку при переносе на рабочий сайт

    User::factory()->is_remember();
    // ( new Cron() )->check();
    $cron = new Cron;
    $cron->check();