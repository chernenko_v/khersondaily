'use strict';

var debug = false;
var NO_IMG = "/pic/no-image.png";
var NO_AVATAR = '/pic/no-avatar.png';
var PUSH_DATA_URL = '/pushes/pushes';

self.addEventListener('push', function (event) {
    event.waitUntil(
        self.registration.pushManager.getSubscription().then(function (pushSubscription) {
            if (pushSubscription) {            
                var regId = pushSubscription.endpoint.split('/').slice(-1)[0];
                console.log(regId);
                fetch(PUSH_DATA_URL, {
                    method:"POST",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: "regid=" + regId
                }).then(function (response) {
                    if(response){
                        response.json().then(function (data) {                        
                            if(data && !Array.isArray(data)){
                                self.registration.showNotification(data.title, {
                                    body: data.body,
                                    icon: data.icon || NO_IMG,
                                    tag: data.tag,
                                    data: {
                                        url: data.url
                                    }
                                });
                            }
                            else{
                                console.log('push is empty');
                            }
                        }).catch(function(e){
                            console.error('response from server is wrong',e);                        
                        });
                    }
                });
            }
        })
    )

});
self.addEventListener('notificationclick', function (event) {    
    event.notification.close();
    var url = event.notification.data.url;
    event.waitUntil(
        clients.matchAll({
                type: 'window'
            })
            .then(function (windowClients) {
                for (var i = 0; i < windowClients.length; i++) {
                    var client = windowClients[i];
                    if (client.url === url && 'focus' in client) {
                        return client.focus();
                    }
                }
                if (clients.openWindow) {
                    return clients.openWindow(url);
                }
            })
    );
});