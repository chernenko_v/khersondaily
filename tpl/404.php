<!DOCTYPE html>
<html lang="ru-ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>  
    <?php echo Widgets::get('head'); ?>
</head>
<body class="errorPage">
	<div class="errorWrapper">
		<div class="errorHolder">
			<div class="errorContent">
				<?php echo conf::get('content'); ?>
			</div>
		</div>
	</div>
</body>
</html>