<!DOCTYPE html>
<html lang="ru-ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>  
    <?php echo Widgets::get('head'); ?>
    <?php foreach ( conf::get( 'metrika', 'head' ) as $metrika ): ?>
        <?php echo $metrika; ?>
    <?php endforeach ?>
</head>
<body>
    <?php foreach ( conf::get( 'metrika', 'body' ) as $metrika ): ?>
        <?php echo $metrika; ?>
    <?php endforeach ?>
   <div class="wWrapper">
        <?php echo Widgets::get('header'); ?>
        <!-- .wHeader -->
        <div class="wConteiner">
			<div class="grid grid--side-8 grid--indent-8 grid--size">
				<div class="grid__row">
                    <!--баннер №3-->
                    <div class="col col--12">

                    </div>
					<div class="col col--xs-12 col--sm-7 col--md-9">
						<div class="main_page grid grid--indent-8">
							<?php echo Widgets::get('Banners'); ?>
							<div class="grid__row">
								<div class="col col--xs-12">
									<?php echo $content; ?>
								</div>
							</div>
						</div>
					</div>
					<!-- .wMiddle -->
					<div class="col col--xs-12 col--sm-5 col--md-3">
						<?php require 'right.php'; ?>
					</div>
					<!-- .wRight -->
				</div>
			</div>
        </div>
        <!-- .wConteiner -->
    </div>
    <!-- .wWrapper -->
    <?php echo Widgets::get('footer'); ?>
    <!-- .wFooter -->
    </body>

</html>