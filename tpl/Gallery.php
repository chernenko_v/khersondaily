<!DOCTYPE html>
<html lang="ru-ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->

<head>
    <?php echo Widgets::get('head'); ?>
    <?php foreach ( conf::get( 'metrika', 'head' ) as $metrika ): ?>
        <?php echo $metrika; ?>
    <?php endforeach ?>
</head>

<body>
    <?php foreach ( conf::get( 'metrika', 'body' ) as $metrika ): ?>
        <?php echo $metrika; ?>
    <?php endforeach ?>
    <!-- Yandex.Metrika informer -->
    <div class="wWrapper">
		<?php echo Widgets::get('header'); ?>
        <!-- .wHeader -->
        <div class="wConteiner photo-alb-page">
			<div class="grid grid--side-8 grid--indent-8 grid--size">
				<?php echo Widgets::get('Banners'); ?>
				<div class="grid__row">
					<?php echo $content; ?>
				</div>
				<!-- .wMiddle -->
			</div>
        </div>
        <!-- .wConteiner -->
    </div>
    <!-- .wWrapper -->
	<?php echo Widgets::get('footer'); ?>
    </div>
    </div>
    <!-- .wFooter -->
</body>

</html>
