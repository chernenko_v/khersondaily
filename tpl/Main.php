<!DOCTYPE html>
<html lang="ru-ru" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Widgets::get('head'); ?>
    <?php foreach ( conf::get( 'metrika', 'head' ) as $metrika ): ?>
        <?php echo $metrika; ?>
    <?php endforeach ?>
</head>
<body>
    <?php foreach ( conf::get( 'metrika', 'body' ) as $metrika ): ?>
        <?php echo $metrika; ?>
    <?php endforeach ?>
	<div class="wWrapper">
        <?php echo Widgets::get('Header'); ?>
        <!-- .wHeader -->
        <div class="wConteiner">
            <div class="grid grid--side-8 grid--indent-8 grid--size">
					<div class="grid__row js-sticky-parent">
                            <!--баннер №3-->
                        <div class="col col--12">
                            <?php include_once ('Banner/HeadBanner.php')?>
                        </div>
						<div class="col col--xs-12 col--sm-7 col--md-9">
							<div class="main_page grid grid--indent-8">
								<?php echo Widgets::get('Banners'); ?>
								<div class="grid__row">
									<div class="col col--xs-12">
										<?php echo Widgets::get('Slider'); ?>
									</div>
								</div>
                                    <!--баннер №5-->
                               

								<div class="grid__row grid__row--right">
									<div class="col col--xs-12 col--sm-12 col--md-6">
										<?php echo Widgets::get('News'); ?>
									</div>
									<div class="col col--xs-12 col--sm-12 col--md-6">
										<?php echo Widgets::get('People'); ?>
									</div>
								</div>
								<div class="grid__row">
									<div class="col col--xs-12">
										<?php echo Widgets::get('video'); ?>
									</div>
								</div>
							</div>
						</div>
						<!-- .wMiddle -->
						<div class="col col--xs-12 col--sm-5 col--md-3">
							<?php require 'right.php'; ?>
						</div>
						<!-- .wRight -->
					</div>
<!--					<div class="photoreportage">-->
<!--						<div class="photoreportage__title">-->
<!--							Фоторепортаж-->
<!--						</div>-->
<!--						<div class="reportage">-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						  <div><img src="https://picsum.photos/150/100/?random" alt=""></div>-->
<!--						</div>-->
<!--					</div>-->

				<hr class="bl dopMT">
				<?php echo Widgets::get('news_tree'); ?>
				<!-- Доп -->
            </div>
        </div>
        <!-- .wConteiner -->
    </div>
    <!-- .wWrapper -->
    <?php echo Widgets::get('Footer'); ?>
    <!-- .wFooter -->
</body>
</html>
