<div class="main_page onekol">
    <div>
        <hr class="bl">
        <p class="tit">Новости</p>
        <hr class="ser">
        <div class="nnb">
            <?php if(!count($result)){ ?>
                <p>По заданым параметрам товаров нет</p>
            <?php } else {
                foreach($result as $obj){
                    echo support::tpl(array('obj' => $obj, 'views' => $views), 'News/ListItemTemplate');
                }
                echo $pager;
            } ?>
        </div>
    </div>
</div>
