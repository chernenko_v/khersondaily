                    <p class="tit"><?php echo $obj->rubric ?></p>
                    <hr class="ser">
                    <h1 class="nazvn"><?php echo $obj->name; ?></h1>
                    <p class="date"><?php echo date('j', $obj->date ); ?>
                                    <?php echo support::month( date( 'm', $obj->date ) ); ?>,
                                    <span> <?php echo date('G:i', $obj->date ); ?></span>
                    </p>
                    <hr class="ser">
                    <?php if(support::is_file( HOST . '/images/news/small/' . $obj->image )): ?>
	                    <div class="imgn">
	                        <img src="/images/news/big/<?php echo $obj->image; ?>" alt="" />
	                    </div>
                    <?php endif; ?>
                    <div class="wTxt nt">
                        <?php echo $obj->text; ?>
                    </div>
                    <?php if(count($images) > 1): ?>
                    <div class="carbb">
                        <div class="preload2"></div>
                        <div class="carouselBlock">
                            <div class="overflh">
                            <div class="carouselBlockIn">
                                <ul>
                                        <?php foreach ($images as $image): ?>
                                            <li>
                                                <a class="fresco" data-fresco-group="unique_name" href="/images/news/big/<?php echo $image->image; ?>">
                                                    <img src="/images/news/big/<?php echo $image->image; ?>" alt=""></a>


                                            </li>
                                        <?php endforeach; ?>
                                </ul>   
                            </div>
                            </div>
                            <div class="carouselBlockBot">
                                <a href="#" class="prevC"></a>
                                <a href="#" class="nextC"></a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <hr class="ser">
                    <div class="soci">
                        <div class="fll">
                            <script type="text/javascript">(function() {
                                    if (window.pluso)if (typeof window.pluso.start == "function") return;
                                    if (window.ifpluso==undefined) { window.ifpluso = 1;
                                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                                        var h=d[g]('body')[0];
                                        h.appendChild(s);
                                    }})();</script>
                            <div class="pluso" data-background="#ebebeb" data-options="medium,square,line,horizontal,counter,theme=02" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php echo Widgets::get('mustRead'); ?>
                    <hr class="bl">
                    <p class="tit fll">Комментарии</p><div class="kolcomm">Комментариев <span><?php echo $commetns_count; ?></span></div>
                    <div class="clear"></div>
                    <hr class="ser">
                    <?php
                        $info = User::info();
                    	if($_SESSION['user']) :
                    ?>
	                    <div class="comment">
	                        <div data-form="true" class="statusu">
	                            <p class="username"><?php echo $info->name; ?></p>
                                <p class="date"><?php echo date('d.m Y, G:i'); ?></p>
	                               <form action="/news/comment_add?id=<?php echo $obj->id; ?>&alias=<?php echo $obj->alias; ?>" method="POST">
                                        <div class="comment">
                                            <textarea data-rule-minlength="4" name="text" required placeholder="Ввести текст комментария" id=""></textarea>
                                        </div>
                                        <button class="enterReg2" id="commadd" href="#">Отправить</button>
                                    </form>
	                        </div>
	                        <div class="clear"></div>
                            <script type="text/javascript">
                                document.getElementById('commadd').onclick = function(e){
                                    e.preventDefault();
                                    $.post('/news/comment_add?id=<?php echo $obj->id; ?>&alias=<?php echo $obj->alias; ?>',
                                        {
                                            text: $('.comment textarea[name="text"]').val()
                                        },
                                        function(data){
                                            location.reload() 
                                        },'json');
                                };
                            </script>
	                    </div>
                    <?php
                    	else :
                    ?>
                		<div class="comment">
	                        <div data-form="true" class="statusu">
	                            <p class="username">Anonymous</p>
	                            <p class="date">23.11 2014, 12:40</p>
	                            <div>
                                    <textarea data-rule-minlength="4" disabled="disabled" name="comment" required placeholder="Зарегистрируйтесь или авторизируйтесь на сайте чтобы оставить комментарий..." id=""></textarea>
                                </div>
	                        </div>
	                            <div class="clear"></div>
	                    </div>
	                <?php
	                	endif;
	                ?>
                    <?php foreach ($comments as $obj):?>
                        <div class="comment">
                            <div class="statusu">
                                <p class="username"><?php echo $obj->name; ?></p>
                                <p class="date"><?php echo date('d.m Y, G:i',$obj->date); ?></p>
                                <div class="scom"><?php echo $obj->text; ?></div></div>
                                <div class="clear"></div>
                        </div>
                        <?php if($obj->answer): ?>
                        <div class="comment level2">
                            <div class="statusu">
                                <p class="username">Admin</p>
                                <p class="date"><?php echo date('d.m Y, G:i',$obj->answer_date) ?></p>
                                <div class="scom"><?php echo $obj->answer; ?></div></div>
                                <div class="clear"></div>
                        </div>
                        <?php endif;?>
                    <?php endforeach; ?>
                    <?php echo $pager; ?>