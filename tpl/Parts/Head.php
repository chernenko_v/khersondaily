    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo conf::get( 'title' ); ?> - <?php echo conf::get( 'url_site' ); ?><?php echo Route::param('page') > 1?' Страница '.Route::param('page') :''?></title>
    <?php if (Route::param('page') > 1 OR isset($_GET['sort'])):?>
    <link rel="canonical" href="<?php echo MAIN_PATH.'/'.(Route::controller() ? Route::controller() : '').(Route::param('alias') ? '/'.Route::param('alias') : ''); ?>" />
    <?php if (Route::param('page') > 1): ?>
        <?php if (trim(support::get_page_navi('next'))): ?>
            <link rel="next" href="<?php echo support::get_page_navi('next'); ?>" />
        <?php endif ?>
        <link rel="prev" href="<?php echo support::get_page_navi('prev'); ?>" />
    <?php endif ?>
<?php else: ?>
    <meta name="description" lang="ru-ru" content="<?php echo conf::get( 'description' ); ?>">
    <meta name="keywords" lang="ru-ru" content="<?php echo conf::get( 'keywords' ); ?>">
<?php endif; ?>

    <? if (conf::get('image_src')) {
        echo '<link rel="image_src" href="'.conf::get('image_src').'" />
        ';
    } ?>

    <? if(conf::get('og_title')){
        echo '<meta property="og:title" content="'.conf::get('og_title').'" />
        ';
    } ?>
    <? if(conf::get('og_description')){
        echo '<meta property="og:description" content="'.conf::get('og_description').'" />
        ';
    } ?>
    <? if(conf::get('og_image')){
        echo '<meta property="og:image" content="'.conf::get('og_image').'" />
        ';
    } ?>
    <meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="target-densitydpi=device-dpi">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="shortcut icon" href="/favicon.ico">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
	<meta name="msapplication-config" content="browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css"  href="/css/critical.min.css">
    

    <?php
        if($_SESSION['GLOBAL_MESSAGE']) {
            echo $_SESSION['GLOBAL_MESSAGE'];
            unset($_SESSION['GLOBAL_MESSAGE']);
        }
    ?>