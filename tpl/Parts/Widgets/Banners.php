<?php if (!empty($result)) { ?>
<div class="grid__row">
	<div class="col col--xs-12">
		<div class="banners-line wSize">
			<?php foreach ( $result as $obj ): ?>
					<?php if ( support::is_file( HOST . '/images/banners/' . $obj->image ) ): ?>
						<img src="/images/banners/<?php echo $obj->image; ?>" alt="">
					<?php endif; ?>
			<?php endforeach; ?>	
		</div>
	</div>
</div>
<?php } ?>