<ul>
    <li <?php echo (Route::controller() == 'catalog' AND in_array(Route::action(), array('index', 'list', 'groups', 'item'))) ? 'class="active_li_top"' : ''; ?>>
        <a href="/catalog"><div>каталог</div></a><div class="list top_list">
        <ul>
            <?php foreach( $result[0] as $main ): ?>
                <li>
                    <a href="/catalog/<?php echo $main->alias; ?>" class="title_li"><?php echo $main->name; ?></a>
                    <?php if( isset($result[$main->id]) ): ?>
                        <ul>
                            <?php foreach( $result[$main->id] as $obj ): ?>
                                <li><a href="/catalog/<?php echo $obj->alias; ?>"><?php echo $obj->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div></li>
    <li <?php echo (Route::controller() == 'catalog' AND Route::action() == 'new') ? 'class="active_li_top"' : ''; ?>><a href="/new"><div>новинки</div></a></li>
    <li <?php echo (Route::controller() == 'catalog' AND Route::action() == 'popular') ? 'class="active_li_top"' : ''; ?>><a href="/popular"><div>популярные</div></a></li>
    <li <?php echo (Route::controller() == 'catalog' AND Route::action() == 'sale') ? 'class="active_li_top"' : ''; ?>><a href="/sale"><div>акции</div></a></li>
    <li <?php echo Route::controller() == 'brands' ? 'class="active_li_top"' : ''; ?>><a href="/brands"><div>бренды</div></a></li>
</ul>