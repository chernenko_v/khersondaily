<a class="tit">Мудрость дня</a>
<hr class="ser">
<div class="cit">“<?php echo $obj->text; ?>”</div>
<div class="autor">
	<?php if(support::is_file( HOST . '/images/citations/' . $obj->image )): ?>
    	<div class="img"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/citations/' . $obj->image, 72, 72)?>" alt=""></div>
	<?php endif;?>
    <p class="name"><?php echo $obj->name; ?></p>
    <p class="pos"><?php echo $obj->job; ?></p>
    <div class="clear"></div>
</div>
<hr class="bl">
    <div class="display-none-xs">
        <a target="_blank" href="http://ukraineartnews.com/" /><img src="#" class="lazyImg" data-src="/pic/uaBanner.gif" alt="Ukraine Art News"></a>
    </div>
<hr class="bl">
<?php if($wisdow) :?>
    <a class="tit">Цитата недели</a>
    <hr class="ser">
    <div class="cit">“<?php echo $wisdow->text; ?>”</div>
    <div class="autor">
        <?php if(support::is_file( HOST . '/images/wisdow/' . $wisdow->image )): ?>
            <div class="img"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/wisdow/' . $wisdow->image, 72, 72)?>" alt=""></div>
        <?php endif;?>
        <p class="name"><?php echo $wisdow->name; ?></p>
        <p class="pos"><?php echo $wisdow->job; ?></p>
        <div class="clear"></div>
    </div>
    <hr class="bl">
<?php endif; ?>