<footer class="wFooter">
	<div class="grid grid--side-8 grid--indent-8 grid--size"> 
		<div class="grid__row">
			<div class="col col--xs-6">
				<div class="soc display-none-sm display-none-xs">
					<a href="<?php echo conf::get('yt'); ?>"><img src="/pic/youtube.png" alt=""></a>
					<a href="<?php echo conf::get('vk'); ?>"><img src="/pic/vkb.png" alt=""></a>
					<a href="<?php echo conf::get('fb'); ?>"><img src="/pic/ffb.png" alt=""></a>
					<a href="//khersondaily.com/rss"><img src="/pic/rss.png" alt=""></a>
				</div>
				<a href="/contacts" class="link-contacts">
					<span>Контакты</span>
				</a>
                <a href="/reklama" class="link-contacts">
                    <span>Реклама</span>
                </a>
				<!-- <a class="mailto display-none-sm display-none-xs" href="mailto:<?php //echo conf::get('admin_email'); ?>"><?php echo conf::get('admin_email'); ?></a> -->
				<div class="clear"></div>
				<p class="sert"><?php echo conf::get('subscribe_text'); ?></p>
				<p class="belt"><?php echo conf::get('footer_text'); ?></p>
				
				<?php if (true) { ?>


				<p class="counters">


					<!--LiveInternet counter--><script type="text/javascript">
						document.write("<a href=https://www.liveinternet.ru/click "+
						"target=_blank><img src=https://counter.yadro.ru/hit?t52.6;r"+
						escape(document.referrer)+((typeof(screen)=="undefined")?"":
						";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
						screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
						";"+Math.random()+
						"' alt='' title='LiveInternet: показано число просмотров и"+
						" посетителей за 24 часа' "+
						"border='0' width='88' height='31'><\/a>")
						</script>
					<!--/LiveInternet-->
					<!--bigmir)net TOP 100-->
					<script type="text/javascript" language="javascript"><!-- Yandex.Metrika informer -->
						bmN=navigator,bmD=document,bmD.cookie='b=b',i=0,bs=[],bm={v:16945305,s:16945305,t:12,c:bmD.cookie?1:0,n:Math.round((Math.random()* 1000000)),w:0};
						for(var f=self;f!=f.parent;f=f.parent)bm.w++;
						try{if(bmN.plugins&&bmN.mimeTypes.length&&(x=bmN.plugins['Shockwave Flash']))bm.m=parseInt(x.description.replace(/([a-zA-Z]|\s)+/,''));
						else for(var f=3;f<20;f++)if(eval('new ActiveXObject("ShockwaveFlash.ShockwaveFlash.'+f+'")'))bm.m=f}catch(e){;}
						try{bm.y=bmN.javaEnabled()?1:0}catch(e){;}
						try{bmS=screen;bm.v^=bm.d=bmS.colorDepth||bmS.pixelDepth;bm.v^=bm.r=bmS.width}catch(e){;}
						r=bmD.referrer.replace(/^w+:\/\//,'');if(r&&r.split('/')[0]!=window.location.host){bm.f=escape(r).slice(0,400);bm.v^=r.length}
						bm.v^=window.location.href.length;for(var x in bm) if(/^[vstcnwmydrf]$/.test(x)) bs[i++]=x+bm[x];
						bmD.write('<a href="https://www.bigmir.net/" target="_blank" onClick="img=new Image();img.src="https://www.bigmir.net/?cl=16945305";"><img src="https://c.bigmir.net/?'+bs.join('&')+'"  width="88" height="31" border="0" alt="bigmir)net TOP 100" title="bigmir)net TOP 100"></a>');
					</script>
					<noscript>
						<a href="https://www.bigmir.net/" target="_blank"><img src="https://c.bigmir.net/?v16945305&s16945305&t12" width="88" height="31" alt="bigmir)net TOP 100" title="bigmir)net TOP 100" border="0" /></a>
					</noscript>
					<!--bigmir)net TOP 100-->
					<!-- Yandex.Metrika counter 
					<script type="text/javascript">
						(function (d, w, c) {
							(w[c] = w[c] || []).push(function() {
								try {
									w.yaCounter29324365 = new Ya.Metrika({id:29324365,
											webvisor:true,
											clickmap:true,
											trackLinks:true,
											accurateTrackBounce:true});
								} catch(e) { }
							});

							var n = d.getElementsByTagName("script")[0],
								s = d.createElement("script"),
								f = function () { n.parentNode.insertBefore(s, n); };
							s.type = "text/javascript";
							s.async = true;
							s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

							if (w.opera == "[object Opera]") {
								d.addEventListener("DOMContentLoaded", f, false);
							} else { f(); }
						})(document, window, "yandex_metrika_callbacks");
					</script>
					<noscript><div><img src="//mc.yandex.ru/watch/29324365" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
					<!-- /Yandex.Metrika counter -->

                    <!--Oblivki-->
                    <script>window.RESOURCE_O1B2L3 = 'leokross.com';</script>
                    <script src="//leokross.com/tUY7.js" ></script>
                    <!--Oblivki-->

					<script>
					  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					  ga('create', 'UA-61927899-1', 'auto');
					  ga('send', 'pageview');

					</script>

                    <!-- I.UA counter --><a href="https://www.i.ua/" target="_blank" onclick="this.href='https://i.ua/r.php?192251';" title="Rated by I.UA">
<script type="text/javascript">
iS='http'+(window.location.protocol=='https:'?'s':'')+
'://r.i.ua/s?u192251&p62&n'+Math.random();
iD=document;if(!iD.cookie)iD.cookie="b=b; path=/";if(iD.cookie)iS+='&c1';
iS+='&d'+(screen.colorDepth?screen.colorDepth:screen.pixelDepth)
+"&w"+screen.width+'&h'+screen.height;
iT=iR=iD.referrer.replace(iP=/^[a-z]*:\/\//,'');iH=window.location.href.replace(iP,'');
((iI=iT.indexOf('/'))!=-1)?(iT=iT.substring(0,iI)):(iI=iT.length);
if(iT!=iH.substring(0,iI))iS+='&f'+escape(iR);
iS+='&r'+escape(iH);
iD.write('<img src="'+iS+'" border="0" width="88" height="31" />');
	
</script></a><!-- End of I.UA counter -->
				</p>


				<?php } ?>
			</div>
			<div class="col col--xs-6">
				<form class="search bottoms display-none-sm display-none-xs" action="/search" method="GET">
					<div><input type="text" placeholder="Поиск на сайте" name="query" id=""></div>
					<input type="submit" value="">
				</form>
				<div class="logb"><p href="#"><img src="/pic/logo.png" alt=""></p></div>
				<p class="copyright"><?php echo conf::get('copyright'); ?></p>
			</div>
		</div>
	</div>
</footer>
<div style="display: none;">
	<div id="enterReg" class="enterRegPopup zoomAnim">
		<div class="enterReg_top">
			<div class="popupBlock enterBlock wCur">
				<div class="erTitle">Вход на сайт</div>
					<div class="popupContent">
						<form action="/form/login" method="POST">
							<div id="entrForm" data-form="true" class="wForm enterBlock_form visForm">
								<div class="wFormRow log">
									<input type="email" name="email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
									<div class="inpInfo">E-mail</div>
								</div>
								<!-- .wFormRow -->
								<div class="wFormRow log">
									<input type="password" name="password" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Пароль" required="">
									<div class="inpInfo">Пароль</div>
								</div>
								<!-- .wFormRow -->
								<label class="checkBlock log">
									<input type="checkbox" name="remember" checked="checked">
									<ins></ins>
									<span>Запомнить данные</span>
								</label>
								<div class="tar">
									<button class="wSubmit enterReg_btn" id="login">войти</button>
								</div>
							</div>
						</form>
				</div>
			</div>
			<!-- .enterBlock -->
			<div data-form="true" class="popupBlock wForm regBlock">
				<div class="erTitle">Новый пользователь</div>
				<div class="popupContent">
					<form action="/login/registration" method="POST">
						<div class="wFormRow reg">
							<input type="email" name="email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
							<div class="inpInfo">E-mail</div>
						</div>
						<!-- .wFormRow -->
						<div class="wFormRow reg">
							<input type="password" name="password" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Пароль" required="">
							<div class="inpInfo">Пароль</div>
						</div>
						<!-- .wFormRow -->
						<label class="checkBlock reg">
							<input type="checkbox" name="agree" data-msg-required="Это поле нужно отметить" required="">
							<ins></ins>
							<span>Я согласен с условиями использования и обработку моих персональных данных</span>
						</label>
						<!-- .checkBlock -->
						<div class="tar">
							<button class="wSubmit enterReg_btn" id="reg">зарегистрироваться</button>
						</div>
					</form>
				</div>
				<!-- .popupContent -->
			</div>
			<!-- .regBlock -->
		</div>
		<!-- .enterReg_top -->

		<!-- .socEnter -->
	</div>
	<!-- #enterReg -->
</div>
<div class="display-none">
	<link rel="stylesheet" href="/css/all.min.css?06122018">
	<script src="/js/jquery-1.11.0.min.js"></script>
	<script src="/js/all.min.js?06122018" defer async></script>
	<script type="text/javascript" src="//www.gstatic.com/firebasejs/3.6.8/firebase.js"></script>
	<script type="text/javascript" src="/firebase_subscribe.js"></script>
 
	<!-- <script src="/js/init-push.js"></script>
	<script src="/js/push.js"></script>
	<script src="/js/cookieHelper.js"></script> -->
	<div id="seoTxt"></div>
	<noscript>
		<input id="wzmMsg_JsClose" type="checkbox" title="Закрыть">
		<div id="wzmMsg_JsInform" class="wzmMsg_Wrapp">
			<div class="wzmMsg_Text">
				<p>В Вашем браузере <strong>отключен JavaScript!</strong> Для корректной работы с сайтом необходима поддержка Javascript.</p>
				<p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>
			</div>
			<a href="//wezom.com.ua/" target="_blank" title="Студия Wezom" class="wzmMsg_Link">
				<img src="images/wezom-info-red.gif" width="50" height="18" alt="Студия Wezom">
			</a>
			<label for="wzmMsg_JsClose" class="wzmMsg_Close"><span>&times;</span></label>
		</div>
	</noscript>
</div>
<div class="facebook-block">
    <div class="facebook-block__title">Вподобайте нас у Facebook і отримуйте все найцікавіше у власну стрічку новин.</div>
    <div class="fb-like-box" style="margin-bottom: 10px;" data-href="https://www.facebook.com/pages/%D0%A5%D0%B5%D1%80%D1%81%D0%BE%D0%BD-Daily/711871785597486?ref=hl" data-width="280" data-height="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
    <div class="facebook-block__close js-close-fb">Я вже вподобав "Херсон Daily"</div>
</div>
<!-- <div class="subscribe js-subscription" style="display:none">
    <div class="subscribe__inner">
        <div class="subscribe__close js-subscription__close"></div>
        <div class="subscribe__arrowBorder"></div>
        <div class="subscribe__arrow"></div>
        <div class="subscribe__text">Подписаться на уведомления от kherson.net.ua</div>
        <div class="subscribe__buttons">
            <div class="subscribe__buttons__button js-subscription__start">Подписаться</div>
            <div class="subscribe__buttons__button subscribe__buttons__button--decline js-subscription__decline">Отказаться</div>
        </div>
    </div>
</div> -->