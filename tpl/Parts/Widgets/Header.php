<!--<div class="bg-link">
	<span class="bg-link__wrap">
		<a href="https://m.facebook.com/%D0%A4%D0%B8%D1%82%D0%BD%D0%B5%D1%81-%D0%BA%D0%BB%D1%83%D0%B1-%D0%9B%D0%95%D0%9E-799808726792231/" target="_blank" class="bg-link__item" style="background-image: url('https://khersondaily.com/pic/bgg/bg-leo.jpg')"></a>
		<span class="bg-link__center"></span>
		<a href="https://m.facebook.com/%D0%A4%D0%B8%D1%82%D0%BD%D0%B5%D1%81-%D0%BA%D0%BB%D1%83%D0%B1-%D0%9B%D0%95%D0%9E-799808726792231/" target="_blank" class="bg-link__item" style="background-image: url('https://khersondaily.com/pic/bgg/bg-leo.jpg')"></a>
	</span>
</div>-->
<header class="wHeader">
    <!--Баннер №2-->

    <div class="grid grid--side-8 grid--indent-8 grid--size">
		<div class="grid__row">
			<div class="col col--xs-12">
                <div class="menuBlock display-none-md">
                    <a href="#mmenu" class="menuButton js-menuButton"><ins>&nbsp;</ins></a>
                    <nav id="mmenu" class="menuContent js-menuContent">
                        <ul>
                            <?php foreach ( $contentMenu as $obj ): ?>
                                <li><a href="<?php echo href( $obj->url ); ?>"><span><?php echo $obj->name; ?></span></a>
                                    <?php if($menuNews[$obj->parent_id]): ?>
                                        <?php $news = $menuNews[$obj->parent_id]; ?>
                                        <ul>
                                            <?php for ( $i=0; $i < 2; $i++ ): ?>
                                                <?php if(isset($news[$i])): ?>
                                                    <?php $new = $news[$i] ?>
                                                    <li>
                                                        <a href="/news/<?php echo $new->alias; ?>">
                                                            <?php if(support::is_file( HOST . '/images/news/small/' . $new->image )): ?>
                                                                <div class="img lazyDiv" data-background="url('<?=bLibrary::getImageFromCache('/images/news/small/' . $new->image, 270, 160)?>')"></div>
                                                            <?php elseif(support::is_file( HOST . '/images/news/small/' . $new->images )): ?>
                                                                <div class="img lazyDiv" data-background="url('<?=bLibrary::getImageFromCache('/images/news/small/' . $new->images, 270, 160)?>')"></div>
                                                            <?php endif; ?>
                                                            <span><?php echo Text::limit_words($new->name, 11); ?></span>
                                                        </a>
                                                    </li>
                                                    <?php unset($news[$i]); ?>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                            <?php foreach ($news as $new): ?>
                                                <li>
                                                    <a href="/news/<?php echo $new->alias; ?>"><span><?php echo Text::limit_words($new->name, 5); ?></span></a>
                                                </li>
                                            <?php endforeach; ?>
                                            <li><a href="<?php echo href( $obj->url ); ?>"><span>Смотреть все новости »</span></a></li>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                            <?php if($_SESSION['user']) { ?>
                                <li class="display-none-sm display-none-md">
                                    <a href="/user" class="moinast">Мои настройки</a>
                                </li>
                                <li class="display-none-sm display-none-md">
                                    <a class="enterReg2" href="/user/logout">Выйти</a>
                                </li>
                            <?php } else { ?>
                                <li class="display-none-sm display-none-md">
                                    <a class="enterReg" href="#enterReg">Войти</a>
                                </li>
                            <?php } ?>
                            <li class="display-none-sm display-none-md">
								<span>
									<form class="search" action="/search" method="GET">
										<div><input type="text" placeholder="Поиск на сайте" name="query" value="<?php echo isset($_GET['query']) ? htmlspecialchars(urldecode($_GET['query'])) : ''; ?>"></div>
										<input type="submit" value="">
									</form>
								</span>
                            </li>
                        </ul>
                    </nav>
                </div>
				<p class="logo">
					<a href="/">
						<img src="/pic/logo.png" alt="">
					</a>
				</p>
				<!-- new menu -->
				<div class="onhoverdrop display-none-sm display-none-xs">
					<ul>
						<?php foreach ( $contentMenu as $obj ): ?>
								<li class="site-section-tab flyout"><a href="<?php echo href( $obj->url ); ?>"><?php echo $obj->name; ?></a>
								<?php if($menuNews[$obj->parent_id]): ?>
									<?php $news = $menuNews[$obj->parent_id]; ?>
									<ul class="flyout-content">
										<div class="photo-b">
											<?php for ( $i=0; $i < 2; $i++ ): ?>
												<?php if(isset($news[$i])): ?>
													<?php $new = $news[$i] ?>
														<li class="photo">
															<?php if(support::is_file( HOST . '/images/news/small/' . $new->image )): ?>
																<a href="/news/<?php echo $new->alias?>">
																	<div class="img lazyDiv" data-background="url('<?=bLibrary::getImageFromCache('/images/news/small/' . $new->image, 270, 160)?>')"></div>
																</a>
															<?php elseif(support::is_file( HOST . '/images/news/small/' . $new->images )): ?>
																<a href="/news/<?php echo $new->alias?>">
																	<div class="img lazyDiv" data-background="url('<?=bLibrary::getImageFromCache('/images/news/small/' . $new->images, 270, 160)?>')"></div>
																</a>
															<?php endif; ?>
															<a href="/news/<?php echo $new->alias; ?>" class="titka"><?php echo Text::limit_words($new->name, 11); ?></a>
														</li>
													<?php unset($news[$i]); ?>
												<?php endif; ?>
											<?php endfor; ?>
										</div>
										<div class="spb">
											<?php foreach ($news as $new): ?>
												<li class="spisok">
													<a href="/news/<?php echo $new->alias; ?>" class="titka-s">
														<?php echo Text::limit_words($new->name, 5); ?>
													</a>
												</li>
											<?php endforeach; ?>
											<li class="seemore"><a href="<?php echo href( $obj->url ); ?>">Смотреть все новости »</a></li>
										</div>
									</ul>
								<?php endif; ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<!-- /new menu -->
			</div>
		</div>
    </div>
</header>
<div class="display-block-xs" style="display: none;"><form class="search" style="top: 50px" action="/search" method="GET"><div><input type="text" placeholder="Поиск на сайте" name="query" value=""></div><input type="submit" value=""></form></div>
