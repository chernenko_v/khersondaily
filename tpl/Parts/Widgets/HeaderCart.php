<header class="wHeader">
	<div class="wSize">
		<div class="head_top">
			<div class="fll">
				<ul>
					<!-- active_a_top -> cur class -->
                    <li><a href="/"><div class="gl"></div></a></li>
                    <?php foreach ( $contentMenu as $obj ): ?>
                        <li><a href="<?php echo href( $obj->url ); ?>"><?php echo $obj->name; ?></a></li>
                    <?php endforeach; ?>
				</ul>
			</div>
			<div class="flr">
				<div class="block_p">
					<a href="tel:<?php echo conf::get( 'phone' ); ?>?call" class="head_phone"><?php echo conf::get( 'phone' ); ?></a>
					<a href="#enterReg2" class="call_back enterReg2"><span>ОБРАТНЫЙ ЗВОНОК</span></a>
				</div>
			</div>
			<div class="flc">
				<a href="/"><img src="/pic/logo.png" alt=""></a>
			</div>
		</div>
	</div>
</header>