<div class="lastNews">
	<hr class="bl">
	<a href="/news" class="tit">Последние новости</a>
	<hr class="ser">
	<div class="nnb" style="overflow: hidden;">
<?php
    //Счётчик. Будет считать кол-во выводимых новостей, обновляясь каждые conf::get('show_gallery') новости.
    $i = 1;
    //Нужно, чтобы доставать галереи из массива по порядку.
    $gal_obj_num = 0;
    foreach ($obj as $value){ 

		if( support::is_file(HOST.'/images/news/small/'.$value->image) ){	
			$Img =  '<div class="img"><a href="/news/'.$value->alias.'"><img src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'160\' height=\'128\'%3E%3C/svg%3E" class="lazyImg" data-src="'.bLibrary::getImageFromCache('/images/news/small/' . $value->image, 160, 128).'" alt=""></a></div>';
		}elseif(support::is_file( HOST . '/images/news/small/' . $value->images )){
			$Img =  '<div class="img"><a href="/news/'.$value->alias.'"><img src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'160\' height=\'128\'%3E%3C/svg%3E" class="lazyImg" data-src="'.bLibrary::getImageFromCache('/images/news/small/' . $value->images, 160, 128).'" alt=""></a></div>';	
		}else{
			$Img = '';	
		}
        print('<div class="s-news">
                <p><a href="/news/'.$value->alias.'" class="date">'.date('j', $value->date )
                                .' '.support::month( date( 'm', $value->date ) )
                                .'<span><b> '.date('G:i', $value->date )
                                .'</b></span></a></p>
                <a href="/news/'.$value->alias.'">'.$value->name.'</a>
                <div class="clear"></div>
                    '.$Img.'
                <p style="overflow: hidden; word-wrap:break-word;">
					<a href="/news/'.$value->alias.'">');
        if ($value->text_short) {
            echo Text::limit_words(strip_tags($value->text_short), 25);
        } else Text::limit_words(strip_tags($value->text), 25);
           print('</a></p>
                <div class="clear"></div>
            </div>');

        //Каждые три новости показываем одну из галерей. Галереи может и не быть.
        if($i == conf::get('show_gallery') && isset($gallery[$gal_obj_num])) {
            $img_count = Builder::factory('gallery_images')
                            ->where('parent_id','=',$gallery[$gal_obj_num]->id)
                            ->count_all();
            print('<div class="big-news">
                    <div class="rel">
                        <div class="img"><img src="" class="lazyImg" data-src="'.SITE_PATH.'/images/gallery/medium/'.$gallery[$gal_obj_num]->image.'" alt=""></div>
                        <div class="bottom">
                            <div><a href="/gallery/'. $gallery[$gal_obj_num]->alias.'">
                                    <img src="" class="lazyImg" data-src="'.SITE_PATH.'/pic/icofoto.png" alt="">
                                </a>
                            </div>
                            <div>
                                <p class="kolf">'.
                                    ($img_count ? $img_count : '0')
                                    .' <span>фотографии</span></p>
                                <a href="/gallery/'. $gallery[$gal_obj_num]->alias.'">'. Text::limit_words($gallery[$gal_obj_num]->name,5).'</a>
                            </div>
                        </div>
                    </div>
                </div>');
            $i = 1;
            $gal_obj_num++;
        }
        else $i++;
    } 
?>
	</div>
	<a href="/news" class="seeall clearFix">Еще новости</a>
</div>