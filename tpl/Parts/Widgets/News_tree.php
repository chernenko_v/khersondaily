<div class="row_news grid__row">
	<?php foreach ($result as $obj):?>
	<div class="col col--xs-12 col--sm-6 col--md-3">
		<p class="t_rt"><?php echo $obj->name; ?></p>
		<ul class="snm">
			<? $news = News::getNews($obj->id); ?>
			<?php foreach($news as $key => $news_res): ?>
			<li>
				<a href="/news/<?php echo $news_res->alias; ?>" class="preview_img">
					<?php if ($key == 0) : ?>
					<img src="" class="lazyImg" data-src="<?=SITE_PATH?>/images/news/small/<?php echo $news_res->image; ?>" alt="">
					<?php endif; ?>
                    <span><?php echo $news_res->name; ?></span>
				</a>
			</li>
			<?php endforeach; ?>
			
			<li class="seemore"><a href="/news/<?php echo $obj->alias; ?>">Смотреть все события »</a></li>
		</ul>
	</div>
	<?php endforeach; ?>
	<?php $res = News::getVideo(); ?>
	<div class="col col--xs-12 col--sm-6 col--md-3">
		<p class="t_rt">Видео</p>
		<ul class="snm">
			<!--<li>
				<a href="/gallery/<?php /*echo $photo['photo']->alias;*/ ?>" class="preview_img">
					<img src="" class="lazyImg" data-src="<?/*=SITE_PATH*/?>/images/gallery/medium/<?php /*echo $photo['photo']->image;*/ ?>" alt="">
					<span><?php /*echo $photo['photo']->name;*/ ?></span>
				</a>
			</li>-->

			<?php foreach($res['video'] as $key => $video): ?>
				<li>
					<?php if ($video->link): ?>
					<a href="<?php echo $video->link; ?>" class="preview_img">
						<?php if ($key == 0) : ?>
							<img src="" class="lazyImg" data-src="http://img.youtube.com/vi/<?php echo support::youtube_key( $video->link); ?>/1.jpg" alt="">
						<?php endif; ?>
						<span><?php echo $video->title; ?></span>
					</a>
					<?php else: ?>
					<a href="<?php echo $video->facebook_code; ?>" class="preview_img">
						<?php if ($key == 0 and support::is_file( HOST . '/images/videos/big/' . $video->image )) : ?>
							<img src="" class="lazyImg" data-src="/images/videos/big/<?php echo $video->image; ?>" alt="">
						<?php endif; ?>
						<span><?php echo $video->title; ?></span>
					</a>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
				
			<li class="seemore"><a href="/gallery">Смотреть все события »</a></li>
		</ul>
	</div>
</div>