<div class="people">
    <hr class="bl">
    <a href="/news/ludi" class="tit">Люди</a>
    <hr class="ser">
    <div class="llb">
        <!--<div class="ludi" style="text-align:center;" >
            <img src="/pic/gif-01.gif">
            <div class="clear"></div>
        </div>-->
        <?php $i = 1; ?>
        <?php
        foreach ($obj as $value) {
            ?>
            <?php if ($i == 3): ?>
                <!--Баннер №6-->
                <div class="ludi" style="text-align:center;">
                    <a href="http://shevchenko.if.ua" target="_blank"><img src="/pic/shevchenco.gif"></a>
                    <div class="clear"></div>
                </div>
            <?php endif;
            $i++; ?>
            <div class="ludi">
                <a href="/news/<?php echo $value->alias; ?>"><?php echo $value->name; ?></a>
                <div class="clear"></div>
                <?php if ($value->image): ?>
                    <?php if ($value->watermark_image != 0): ?>
                        <div class="img">
                            <a href="/news/<?php echo $value->alias; ?>">
                                <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='343' height='274'%3E%3C/svg%3E" class="lazyImg"
                                     data-src="<?= bLibrary::getImageFromCache('/images/news/bigwatermark/' . $value->image, 343, 274) ?>"
                                     width="343" height="274" alt="">
                            </a>
                        </div>
                    <?php else : ?>
                        <div class="img"><a href="/news/<?php echo $value->alias; ?>">
                                <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='343' height='274'%3E%3C/svg%3E" class="lazyImg"
                                     data-src="<?= bLibrary::getImageFromCache('/images/news/big/' . $value->image, 343, 274) ?>"
                                     width="343" height="274" alt="">
                            </a>
                        </div>
                    <?php endif; ?>

                <?php elseif (support::is_file(HOST . '/images/news/original/' . $value->images)): ?>
                    <?php if ($value->watermark_image != 0): ?>
                        <div class="img">
                            <a href="/news/<?php echo $value->alias; ?>">
                                <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='343' height='274'%3E%3C/svg%3E" class="lazyImg"
                                     data-src="<?= bLibrary::getImageFromCache('/images/news/bigwatermark/' . $value->image, 343, 274) ?>"
                                     width="343" height="274" alt="">
                            </a>
                        </div>
                    <?php else : ?>
                        <div class="img">
                            <a href="/news/<?php echo $value->alias; ?>">
                                <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='343' height='274'%3E%3C/svg%3E" class="lazyImg"
                                     data-src="<?= bLibrary::getImageFromCache('/images/news/big/' . $value->image, 343, 274) ?>"
                                     width="343" height="274" alt="">
                            </a>
                        </div>
                    <?php endif; ?>
                <?php endif ?>
                <p style="overflow: hidden; word-wrap:break-word;"><a href="/news/<?php echo $value->alias; ?>">
                        <?php if ($value->text_short) : ?>
                            <?php echo Text::limit_words(strip_tags($value->text_short), 20); ?>
                        <?php else: ?>
                            <?php echo Text::limit_words(strip_tags($value->text), 20); ?>
                        <?php endif; ?>
                    </a></p>
                <div class="clear"></div>
            </div>
            <?php
        }
        ?>
    </div>
    <a href="/news/ludi" class="seeall clearFix">Больше о людях</a>
</div>