<?php if ($obj): ?>
   <div class="photoreportage-wrapper">
		<div class="photoreportage">
			<div class="photoreportage__title">
				Фоторепортаж
			</div>
			<div class="reportage">
				<?php foreach ($obj as $value): ?>
					<?php if ($value): ?>
						<a href="/photoreport/<?php echo $value->alias; ?>" class="reportage__link">
							<span class="reportage__overlay">
								<p><?php echo Text::limit_words($value->name,5);?></p>
							</span>
								<?php if (support::is_file(HOST . '/images/photoreport/small/' . $value->image)) : ?>
									<div>
										<img src="<?php echo bLibrary::getImageFromCache('/images/photoreport/small/' . $value->image, 160, 128); ?>"
											 alt=""></div>
								<?php elseif (support::is_file(HOST . '/images/photoreport/small/' . $value->images)): ?>
									<div>
										<img src="<?php echo bLibrary::getImageFromCache('/images/photoreport/small/' . $value->images, 160, 128); ?>"
											 alt=""></div>
								<?php endif; ?>
						</a>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
    </div>
<?php endif; ?>
