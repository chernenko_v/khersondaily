<ul>
    <?php
        foreach ($result as $obj):
            ?>  
                <?php if(support::is_file( HOST .'/images/articles/big/'.$obj->image)): ?>
                <li>
                    <a href="/projects/<?php echo $obj->alias; ?>">
                        <img src="/images/articles/big/<?php echo $obj->image; ?>" alt="<?php echo $obj->name; ?>">
                        <p><?php echo $obj->name; ?></p>
                    </a>
                </li>
                <?php endif; ?>
            <?php
        endforeach;
    ?>
</ul>