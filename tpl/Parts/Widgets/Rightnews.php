<?php if (sizeof($result)): ?>
    <div>
        <hr class="bl">
        <a href="/news" class="tit">Лента новостей</a>
        <div class="newsline">
            <?php foreach ($result as $key => $item) : ?>
                <?php //if(date('d Y m', $item->date) == date('d Y m')):?>
                <div class="newsline__item">
                    <?php //echo date('j', $item->date).' '.support::month((int)date('n', $item->date)); ?>
                    <div class="newsline__item-date"><?php echo date('G:i', $item->date); ?></div>
                    <div class="newsline__item-link">
                        <?php if ($item->highlights && !$item->color_h1): ?>
                            <a href="/news/<?php echo $item->alias; ?>"><b><?php echo $item->name; ?></b></a>
<!--                        --><?php //elseif ($item->show_photoreport): ?>
<!--                            <a href="/photoreport/--><?php //echo $item->alias; ?><!--"><b>--><?php //echo $item->name; ?><!--</b></a>-->
                        <?php elseif ($item->highlights && $item->color_h1): ?>
                            <a href="/news/<?php echo $item->alias; ?>">
                                <b style="color:#e52837"><?php echo $item->name; ?></b></a>
                        <?php elseif (!$item->highlights && $item->color_h1): ?>
                            <a style="color:#e52837"
                               href="/news/<?php echo $item->alias; ?>"><?php echo $item->name; ?></a>
                        <?php else: ?>
                            <a href="/news/<?php echo $item->alias; ?>"><?php echo $item->name; ?></a>
                        <?php endif ?>
                        <?php if ($item->icon_video || $item->icon_image): ?>
                            <div class="newsline__item-image-wrapper">
                                <?php if ($item->icon_video): ?>
                                    <span class="newsline__item-image newsline__item-image--video" title="Видео"></span>
                                <?php endif; ?>
                                <?php if ($item->icon_image): ?>
                                    <span class="newsline__item-image newsline__item-image--photo" title="Фото"></span>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if($key == 9):?>
                <!-- start NEW_right_v1 for khersondaily.com -->
                <div id="adpartner-jsunit-3612">
                    <script type="text/javascript">
						var head = document.getElementsByTagName('head')[0];
						var script = document.createElement('script');
						script.type = 'text/javascript';
						script.src = "//a4p.adpartner.pro/jsunit?id=3612&ref=" + encodeURIComponent(document.referrer) + "&" + Math.random();
						head.appendChild(script);
                    </script>
                </div>
                <!-- end NEW_right_v1 for khersondaily.com -->
                <?php endif?>
            <?php endforeach; ?>
            <a href="/news" class="tit">Больше новостей</a>
        </div>
    </div>
<?php endif; ?>