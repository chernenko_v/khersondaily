<div class="grid__row">
	<div class="col col--xs-12">
		<hr class="ser">
		<p class="tit bolder">Смотрите также:</p>
	</div>
</div>
<div class="s4n grid__row">
	<?php foreach ($list as $item): ?>
	<div class="col col--xs-6 col--sm-6 col--md-3">
		<a href="/news/<?php echo $item->alias; ?>">
			<?php if (support::is_file( HOST .'/images/news/small/'.$item->image)):?>
			<img src="" class="lazyImg" data-src="<?=SITE_PATH?>/images/news/small/<?php echo $item->image; ?>" alt="">
			<?php endif; ?>
			<span><?php echo $item->name; ?></span>
		</a>
	</div>
	<?php endforeach; ?>
</div>