<div class="slidert">
    <div id="slidet">
        <?php foreach ( $result as $iindex => $obj ): ?>
            <div style="float: left;">
                <?php if ($obj->name): ?>
                    <a style="text-decoration: none; font-weight: 700; color: #000;" href="<?php echo href($obj->url); ?>" class="title"><?php echo $obj->name; ?></a>
                <?php endif ?>
                <div class="clear"></div>
                <?php if ($obj->description): ?>
                <p class="text"><?php echo $obj->description; ?></p>
                <?php endif ?>
				<?php if ($iindex == 0) { ?>
				<?php if ( support::is_file( HOST . '/images/slider/big/' . $obj->image ) ): ?>
                    <a target="_blank" href="<?php echo href($obj->url); ?>"><img src="<?=SITE_PATH?>/images/slider/big/<?php echo $obj->image; ?>" width="701" alt=""></a>
                <?php endif; ?>
				<?php } else { ?>
                <?php if ( support::is_file( HOST . '/images/slider/big/' . $obj->image ) ): ?>
                        <a target="_blank" href="<?php echo href($obj->url); ?>"><img src="<?=SITE_PATH?>/images/slider/big/<?php echo $obj->image; ?>" width="701" alt=""></a>
                <?php endif; ?>
				<?php } ?>
            </div>
        <?php endforeach ?>
    </div>
    <a href="#" id="prev"></a>
    <a href="#" id="next"></a>
</div>
