<div class="widg_tabs_wrap">
    <div class="title" id="wTabsLinks">
        <a href="#t_tw" class="s_link active">
            <img src="/pic/if_twitter.png" alt="">
        </a>
        <a href="#t_face" class="s_link">
            <img src="/pic/if_facebook.png" alt="">
        </a>
		<a href="https://www.instagram.com/khersondaily/" class="s_link" target="_blank">
            <img src="/pic/if_instagram.png" alt="">
        </a>
   <!-- <a href="t_inst" class="s_link">
            <img src="/pic/if_instagram.png" alt="">
        </a>-->
    </div>
    <div class="w_tabs" id="wTabs">
        <div class="w_tab" id="t_tw">
            <!--            <a href="https://twitter.com/intent/tweet?screen_name=Kherson_Daily" class="twitter-mention-button" data-show-count="false">Tweet to @Kherson_Daily</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>-->
            <!--            <script src="https://twitter.com/Kherson_Daily"></script>-->
            <a class="twitter-timeline" data-height="230" href="https://twitter.com/Kherson_Daily">Tweets by Kherson_Daily</a>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            <!--            <a class="twitter-timeline" href="https://twitter.com/Kherson_Daily">Tweets by Kherson_Daily</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>-->
        </div>
        <div class="w_tab" id="t_face">
            <div class="fb-like-box" data-href="https://www.facebook.com/pages/%D0%A5%D0%B5%D1%80%D1%81%D0%BE%D0%BD-Daily/711871785597486?ref=hl" data-width="223" data-height="300"
                 data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
        </div>
        <div class="w_tab" id="t_inst">
           <iframe src='/inwidget2/index.php?width=218&inline=3&view=9&toolbar=false' scrolling='no' frameborder='no'
                    style='border:none;width:218px;height:320px;overflow:hidden;'></iframe>
        </div>
    </div>
</div>
<div id="fb-root"></div>
<div style="margin-top: 20px;"></div>