<div class="tab">
    <div class="chob">
        <a data-id="ch" class="active" href="#">Читают </a>
		<span class="delimiter_left">/</span>
        <a data-id="ob" href="#">Обсуждают</a>
    </div>
    <hr class="ser">
    <div id="ch">
        <?php
            foreach ($top as $obj) {
        ?>
            <div class="bb">
				<p class="tit"><a href="/news/<?php echo $obj->alias; ?>"><?php echo $obj->name; ?></a></p>
				<div class="clear"></div>
            <?php if(support::is_file( HOST . '/images/news/small/' . $obj->image )) : ?>
                <div class="img"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/news/small/' . $obj->image, 62, 50)?>" alt=""></div>
            <?php elseif(support::is_file( HOST . '/images/news/small/' . $obj->images )): ?>	
				<div class="img"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/news/small/' . $obj->images, 62, 50)?>" alt=""></div>	
			<?php endif ?>
                <p class="txt"><?php echo Text::limit_words( strip_tags($obj->text), 7 ); ?></p>
				<div class="clear"></div>
            </div>
        <?php
            }
            unset($obj);
        ?>
    </div>
    <div id="ob">
        <?php
            foreach ($comm as $obj) {
        ?>
            <div class="bb">
				<p class="tit"><a href="/news/<?php echo $obj->alias; ?>"><?php echo $obj->name; ?></a></p>
				<div class="clear"></div>
            <?php if(support::is_file( HOST . '/images/news/small/' . $obj->image )): ?>
                <div class="img"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/news/small/' . $obj->image, 62, 50)?>" alt=""></div>
            <?php elseif(support::is_file( HOST . '/images/news/small/' . $obj->images )): ?>	
				<div class="img"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/news/small/' . $obj->images, 62, 50)?>" alt=""></div>	
			<?php endif ?>
                <p class="txt"><?php echo Text::limit_words( strip_tags($obj->text), 10 ); ?></p>
				<div class="clear"></div>
            </div>
        <?php
            }
            unset($obj);
        ?>
    </div>
</div>
<hr class="bl">