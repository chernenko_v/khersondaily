<div class="lk_menu">
    <div class="menuElement <?php echo Route::action() == 'index' ? 'current' : ''; ?>">
        <a href="/user">Личный кабинет</a>
    </div>
    <div class="menuElement <?php echo Route::action() == 'orders' ? 'current' : ''; ?>">
        <a href="/user/orders">Мои заказы</a>
    </div>
    <div class="menuElement <?php echo Route::action() == 'profile' ? 'current' : ''; ?>">
        <a href="/user/profile">Мои данные</a>
    </div>
    <div class="menuElement <?php echo Route::action() == 'change_password' ? 'current' : ''; ?>">
        <a href="/user/change_password">Изменить пароль</a>
    </div>
</div>