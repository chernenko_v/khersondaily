<a class="tit">Погода в Херсоне, <span><?php  echo date('j').' '.support::month(date('m')); ?></span></a>
<hr class="ser">
<div id="SinoptikInformer" style="width:100%;" class="SinoptikInformer type1">
	<div class="siHeader">
		<div class="siLh"><div class="siMh">
				<a onmousedown="siClickCount();" href="https://sinoptik.ua/" rel="nofollow" target="_blank">Погода</a>
				<a onmousedown="siClickCount();" class="siLogo" href="https://sinoptik.ua/" rel="nofollow" target="_blank"></a> 
				<span id="siHeader"></span></div>
		</div>
	</div>
	<div class="siBody">
		<div class="siCity">
			<div class="siCityName">
				<a onmousedown="siClickCount();" href="https://sinoptik.ua/погода-херсон" rel="nofollow" target="_blank">Погода в <span>Херсоне</span></a>
			</div>
			<div id="siCont0" class="siBodyContent">
				<div class="siLeft">
					<div class="siTerm"></div>
					<div class="siT" id="siT0"></div>
					<div id="weatherIco0"></div>
				</div>
				<div class="siInf">
					<p>влажность: <span id="vl0"></span></p>
					<p>давление: <span id="dav0"></span></p>
					<p>ветер: <span id="wind0"></span></p>
				</div>
			</div>
		</div>
		<div class="siLinks">
			<span><a onmousedown="siClickCount();" href="https://sinoptik.ua/погода-кременчуг" rel="nofollow" target="_blank">Погода в Кременчуге</a>&nbsp;</span>
			<span><a onmousedown="siClickCount();" href="https://sinoptik.ua/погода-мелитополь" rel="nofollow" target="_blank">Погода в Мелитополе</a>&nbsp;</span>
		</div>
	</div>
	<div class="siFooter"><div class="siLf"><div class="siMf"></div></div></div>
</div>