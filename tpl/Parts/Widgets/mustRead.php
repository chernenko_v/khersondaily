<hr class="ser">
<p class="tit bolder">Читайте также:</p>
<div class="s4n clearFix">
	<div class="grid__row">
		<?php foreach ($obj as $value): ?>
		<div class="col col--xs-6 col--sm-6 col--md-3">
			<? if (support::is_file( HOST .'/images/news/small/'.$value->image)):?>
			<img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/news/small/' . $value->image, 160, 128)?>" alt="">
			<a href="/news/<?php echo $value->alias; ?>"><?php echo $value->name; ?></a>
			<? else:?>
				<a href="/news/<?php echo $value->alias; ?>"><?php echo $value->name; ?></a>
				<?php echo $value->description; ?>
			<? endif;?>
		</div>
		<?php endforeach; ?>
	</div>
</div>