<?php /* <div style="display: none;">
<?php var_dump($obj); ?>
</div> */ ?>

<div class="videoBlock grid grid--indent-8">
	<div class="grid__row">
		<div class="col col--xs-12">
			<hr>
			<a class="tit" href="/gallery" style="text-decoration:none;">Видео</a>
			<hr class="ser">
		</div>
<?php
	for($i = 0; $i < count($obj); $i++) :
		if(!$i) :
?>
		<div class="col col--xs-12">
			<div class="vb">
				<p class="titv"><?php echo $obj[$i]->title; ?></p>
				<div class="video">
					<?php if ($obj[$i]->link): ?>
                    <iframe width="656" height="315" src="https://www.youtube.com/embed/<?php echo support::youtube_key($obj[$i]->link); ?>?rel=0" frameborder="0" allowfullscreen></iframe> 
					<?php else: ?>
					<iframe src="<?php echo $obj[$i]->facebook_code; ?>" frameborder="0" allowfullscreen></iframe> 
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="videos grid__row">
<?php
	else :
?>
		<div class="col col--xs-6 col--sm-6 col--md-3">
			<?php if ($obj[$i]->link): ?>
			<div class="small-vid">
				<a data-fresco-group="unique_name" href="<?php echo $obj[$i]->link; ?>" class="vidka fresco">
				<?php if(support::is_file( HOST . '/images/videos/big/' . $obj[$i]->image )): ?>
					<img src="" class="lazyImg" data-src="/images/videos/big/<?php echo $obj[$i]->image; ?>" alt="">
				<?php else: ?>
					<img src="" class="lazyImg" data-src="https://img.youtube.com/vi/<?php echo support::youtube_key($obj[$i]->link); ?>/1.jpg" alt="">
				<?php endif; ?>
				<span class="bgred"></span>
				</a>
				<p><?php echo $obj[$i]->title; ?></p>
			</div>
			<?php else: ?>
			<div class="small-vid">
				<a href="<?php echo $obj[$i]->facebook_code; ?>" class="vidka popap-video">
				<?php if(support::is_file( HOST . '/images/videos/big/' . $obj[$i]->image )): ?>
					<img src="" class="lazyImg" data-src="/images/videos/big/<?php echo $obj[$i]->image; ?>" alt="">
				<?php endif; ?>
				<span class="bgred"></span>
				</a>
				<p><?php echo $obj[$i]->title; ?></p>
			</div>
			<?php endif; ?>
		</div>
<?php
		endif;
	endfor;
?>
	</div>
</div>
