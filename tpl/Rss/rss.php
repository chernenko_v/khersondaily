<? print('<?xml version="1.0" encoding="utf-8"?>'); ?>

<rss version="2.0" xmlns="http://backend.userland.com/rss2"  xmlns:yandex="http://news.yandex.ru"  xmlns:media="http://search.yahoo.com/mrss/">
    <channel>
        <title>Новости: Khersondaily.com</title>
        <link>http://khersondaily.com/</link>
        <description>Новостной портал Khersondaily.com!</description>
        <image>
            <url>http://khersondaily.com/pic/logo.png</url>
            <title>Новости: Khersondaily.com</title>
            <link>http://khersondaily.com/</link>
        </image>
        <?foreach($news as $obj):?>
        <item>
            <?php if ((strip_tags($obj->name_rss) != null) || (strip_tags($obj->name_rss) != '')): ?>
                <title><?php echo strip_tags($obj->name_rss); ?></title>
            <?php else: ?>
                <title><?php echo strip_tags($obj->name); ?></title>
            <?php endif; ?>
            <link>http://khersondaily.com/news/<?=$obj->alias?></link>
            <description>
                <![CDATA[<? if(file_exists(HOST.IMG_NEWS_PATH.'/_'.$obj->image) and $obj->image!=''): ?><img src="http://kherson.in<?=IMG_NEWS_PATH.'/_'.$obj->image?>" alt="<?=$obj->zag?>"/><?endif?><?php
                $data_short = strip_tags(html_entity_decode(str_replace('&amp;nbsp;', '', $obj->text_short)));
                /*$data_short = str_replace("&amp;laquo;", '&laquo;', $data_short);
                $data_short = str_replace("&amp;raquo;", '&raquo;', $data_short);
                $data_short = str_replace("&amp;apos;", '&apos;', $data_short);
                $data_short = str_replace("&amp;quot;", '&quot;', $data_short);
                $data_short = str_replace('&amp;lt;', '&lt;', $data_short);
                $data_short = str_replace('&amp;gt;', '&gt;', $data_short);
                $data_short = str_replace('&amp;amp;', '&amp;', $data_short);*/

                echo $data_short; ?>]]>
            </description>
            <? if(file_exists(HOST.IMG_NEWS_PATH.'/_'.$obj->image) and $obj->image!=''): ?>
            <media:content url="http://khersondaily.com<?=IMG_NEWS_PATH.'/_'.$obj->image?>" type="image/jpeg" width="360" height="288" />
            <?endif?>
            <pubDate><?=date('r',$obj->created_at);?></pubDate>
            <?php $data = htmlentities(htmlspecialchars(strip_tags(html_entity_decode(str_replace('&amp;nbsp;', '', $obj->text)))));
            $data = str_replace("&amp;laquo;", '&laquo;', $data);
            $data = str_replace("&amp;raquo;", '&raquo;', $data);
            $data = str_replace("&amp;apos;", '&apos;', $data);
            $data = str_replace("&amp;apos;", '&apos;', $data);
            $data = str_replace("&amp;quot;", '&quot;', $data);
            $data = str_replace('&amp;lt;', '&lt;', $data);
            $data = str_replace('&amp;gt;', '&gt;', $data);
            $data = str_replace('&amp;amp;', '&amp;', $data);
            ?>
            <yandex:full-text><? echo $data;?></yandex:full-text>
        </item>
        <?endforeach?>
    </channel>
</rss>