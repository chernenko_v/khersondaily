<? print('<?xml version="1.0" encoding="utf-8"?>'); ?>

<rss version="2.0">
    <channel>
        <title>Новости.Общество. Khersondaily.com</title>
        <link>http://khersondaily.com/</link>
        <description>Новостной портал Khersondaily.com!</description>
        <image>
            <title>Новости.Общество. Khersondaily.com</title>
            <link>http://khersondaily.com/</link>
            <url>http://khersondaily.com/pic/logo.png</url>
        </image>
        <?foreach($news as $obj):?>
            <item>
                <?php if ((strip_tags($obj->name_rss) != null) || (strip_tags($obj->name_rss) != '')): ?>
                    <title><?php echo strip_tags($obj->name_rss); ?></title>
                <?php else: ?>
                    <title><?php echo strip_tags($obj->name); ?></title>
                <?php endif; ?>
                <link>http://khersondaily.com/news/<?=$obj->alias?></link>
                <description>
                    <![CDATA[<? if(file_exists(HOST.IMG_NEWS_PATH.'/_'.$obj->image) and $obj->image!=''): ?><img src="http://kherson.in<?=IMG_NEWS_PATH.'/_'.$obj->image?>" alt="<?=$obj->zag?>"/><?endif?><?php
                    $data_short = strip_tags(html_entity_decode(str_replace('&amp;nbsp;', '', Text::limit_words($obj->text,400))));
                    echo $data_short; ?>]]>
                </description>
                <?php if( support::is_file(HOST.'/images/news/original/'.$obj->image) ): ?>
                    <enclosure url="http://khersondaily.com/images/news/big/<?php echo $obj->image; ?>" type="image/jpeg" width="360" height="288" />
                <?php endif; ?>
                <pubDate><?=date('r',$obj->created_at);?></pubDate>
                <?php $data = htmlentities(htmlspecialchars(strip_tags(html_entity_decode(str_replace('&amp;nbsp;', '', $obj->text)))));
                $data = str_replace("&amp;apos;", '&apos;', $data);
                $data = str_replace("&amp;quot;", '&quot;', $data);
                $data = str_replace('&amp;lt;', '&lt;', $data);
                $data = str_replace('&amp;gt;', '&gt;', $data);
                $data = str_replace('&amp;amp;', '&amp;', $data);
                ?>
            </item>
        <?endforeach?>
    </channel>
</rss>