<?php 
$results = array();
$groups = Builder::factory('news_tree')
			->select(array(
				'id', 'name', 'alias'
			))
			->find_all();
foreach ($groups as $group) {
	$children = Builder::factory('news')
			->select(array(
				'id', 'name', 'alias'
			))
			->where('status', '=', 1)
			->where('date', '<=', time())
			->where('date', '>', time() - 7 * 24 * 60 * 60)
			->where('parent_id', '=', $group->id)
			->order_by('news.date','DESC')
			->find_all();
	$results[] = array(
		'id' => $group->id,
		'name' => $group->name,
		'alias' => $group->alias,
		'children' => $children
	);
}
?>
<nav class="wSitemap">
	<ul>
		<li><a href="/"><span>Главная</span></a></li>
		<li><a href="/news"><span>Все новости</span></a></li>
		<li><a href="/gallery"><span>Галерея</span></a></li>
		<?php foreach ($results as $result) { ?>
		<?php if (!empty($result['children'])) { ?>
		<li class="haveSubMenu">
			<a href="/news/<?php echo $result['alias']; ?>"><span><?php echo $result['name']; ?> (за последнюю неделю)</span></a>
			<ul>
				<?php foreach ($result['children'] as $child) { ?>
				<li><a href="/news/<?php echo $child->alias; ?>"><span><?php echo $child->name; ?></span></a></li>
				<?php } ?>
			</ul>
		</li>
		<?php } else { ?>
		<li><a href="/news/<?php echo $result['alias']; ?>"><span><?php echo $result['name']; ?></span></a></li>
		<?php } ?>
		<?php } ?>
	</ul>
</nav>