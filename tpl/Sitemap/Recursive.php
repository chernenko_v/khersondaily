<?php if(count($result[$cur])): ?>
    <ul>
        <?php foreach($result[$cur] AS $obj): ?>
            <li><a href="<?php echo $add.'/'.($obj->alias == 'index' ? '' : $obj->alias); ?>"><?php echo $obj->name; ?></a>
                <?php echo support::tpl(array('result' => $result, 'cur' => $obj->id, 'add' => $add), 'Sitemap/Recursive'); ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>