<p class="tit">Личный кабинет</p>
<hr class="ser">
<div data-form="true" class="inps">
	<form action="/form/change_password" method="POST">
       	<div class="edit"><label for="p1"> Старый пароль*</label><input type="password" data-rule-minlength="4" name="old_password"></div>
       	<div class="edit"><label for="p2"> Новый пароль*</label><input type="password" data-rule-equalTo="#p1" name="password"></div>
       	<div class="edit"><label for="p2"> Подтвердите пароль*</label><input type="password" data-rule-equalTo="#p1" name="confirm"></div>
	    
	    <a style="padding: 0px;"><button id="new_pass" style="border:0px;background: transparent;color:#fff;padding:10px 20px;">Сохранить</button></a>
    </form>
    <script type="text/javascript">
        document.getElementById('new_pass').onclick = function(e){
            e.preventDefault();
            $.post('/form/change_password',
                {
                    password: $('.edit input[name="password"]').val(),
                    old_password: $('.edit input[name="old_password"]').val(),
                    confirm: $('.edit input[name="confirm"]').val()
                },
                function(data){
                    if(data.success == true) {
                        var n = noty({
                            text: data.response,
                            type: 'success'
                        }); 
                    }
                    else {
                        var n = noty({
                            text: data.response,
                            type: 'alert'
                        });
                    }
                },'json');
        };
    </script>
</div>
<br><br>
<div data-form="true" class="inps">
	<form action="/form/edit_profile" method="POST">
	    <div class="data"><label for="n1">Никнейм*</label><input type="text" required name="name" id="n1" value="<?php echo $user->name; ?>"></div>
	    <div class="data"><label for="e1">Email*</label><input required data-rule-email="true" type="email"  name="email" id="e1" value="<?php echo $user->email; ?>"></div>
	    <a style="padding: 0px;"><button id="new_data" style="border:0px;background: transparent;color:#fff;padding:10px 20px;">Сохранить</button></a>
    </form>
    <script type="text/javascript">
        document.getElementById('new_data').onclick = function(e){
            e.preventDefault();
            $.post('/form/edit_profile',
                {
                    email: $('.data input[name="email"]').val(),
                    name: $('.data input[name="name"]').val()
                },
                function(data){
                    if(data.success == true) {
                        location.reload()
                    }
                    else {
                        var n = noty({
                            text: data.response,
                            type: 'alert'
                        });
                    }
                },'json');
        };
    </script>
</div>