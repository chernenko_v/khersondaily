<div class="wTxt" style="position: relative;">
    <a href="/user/print/id/<?php echo $obj->id; ?>" target="_blank" class="userOrderPrint">Распечатать</a>
    <div class="userOrderInfoBlock">
        <div class="userOrderTitle">Статус</div>
        <div class="userOrderValue"><?php echo $statuses[$obj->status]; ?></div>
    </div>
    <div class="userOrderInfoBlock">
        <div class="userOrderTitle">Итоговая сумма</div>
        <div class="userOrderValue"><?php echo (int) $obj->amount; ?> грн.</div>
    </div>
    <div class="userOrderInfoBlock">
        <div class="userOrderTitle">Способ оплаты</div>
        <div class="userOrderValue">
            <?php echo $payment[ $obj->payment ]; ?>
        </div>
    </div>
    <div class="userOrderInfoBlock">
        <div class="userOrderTitle">Доставка</div>
        <div class="userOrderValue">
            <?php echo $delivery[ $obj->delivery ] . ($obj->delivery == 2 ? ', '.$obj->number : ''); ?>
        </div>
    </div>
    <div class="userOrderInfoBlock">
        <div class="userOrderTitle">Адресат</div>
        <div class="userOrderValue">
            <?php echo $obj->name; ?>
        </div>
    </div>
    <div class="userOrderInfoBlock">
        <div class="userOrderTitle">Телефон</div>
        <div class="userOrderValue">
            <?php echo $obj->phone; ?>
        </div>
    </div>

    <div class="history wTxt onlyOneOrder">
        <table class="table-zebra myStyles">
            <tr>
                <th>Фото</th>
                <th>Товар</th>
                <th>Цена</th>
                <th>Количество</th>
                <th>Итог</th>
            </tr>
            <?php foreach ($cart as $item): ?>
                <tr>
                    <td>
                        <?php if(support::is_file(HOST.'/images/catalog/small/'.$item->image)): ?>
                            <a href="/catalog/<?php echo $item->alias; ?>" target="_blank">
                                <img src="/images/catalog/small/<?php echo $item->image; ?>" />
                            </a>
                        <?php endif; ?>
                    </td>
                    <td class="userOrderNotCenterTD">
                        <a href="/catalog/<?php echo $item->alias; ?>" target="_blank">
                            <?php echo $item->name . ( $item->size_name ? ', '.$item->size_name : '' ); ?>
                        </a>
                    </td>
                    <td>
                        <?php echo (int) $item->cost; ?> грн
                    </td>
                    <td>
                        <?php echo (int) $item->count; ?> шт
                    </td>
                    <td>
                        <?php echo (int) $item->count * (int) $item->cost; ?> грн
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>