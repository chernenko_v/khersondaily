<p class="tit"><?php echo $obj->rubric ?></p>
<hr class="ser">
<h1 class="nazvn"><?php echo $obj->name; ?></h1>
<p style="float: right;">Просмотров: <?php echo $obj->views ?></p>
<p class="date"><?php echo date('j', $obj->date); ?>
    <?php echo support::month(date('m', $obj->date)); ?>,
    <span> <?php echo date('G:i', $obj->date); ?></span>
</p>
<hr class="ser">
<?php if (support::is_file(HOST . '/images/photoreport/small/' . $image[0]->image)): ?>
    <?php if ($obj->watermark_image != 0): ?>
        <div class="imgn">
            <img src="" class="lazyImg" data-src="<?= SITE_PATH ?>/images/photoreport/originalwatermark/<?php echo  $image[0]->image; ?>"
                 alt=""/>
        </div>
    <?php else: ?>
        <div class="imgn">
            <img src="" class="lazyImg" data-src="<?= SITE_PATH ?>/images/photoreport/original/<?php echo  $image[0]->image; ?>" alt=""/>
        </div>
    <?php endif; ?>
<?php elseif (support::is_file(HOST . '/images/photoreport/small/' .  $image[0]->image)): ?>
    <?php if ($obj->watermark_image != 0): ?>
        <div class="imgn">
            <img src="" class="lazyImg" data-src="<?= SITE_PATH ?>/images/photoreport/originalwatermark/<?php echo $image[0]->image; ?>"
                 alt=""/>
        </div>
    <?php else: ?>
        <div class="imgn">
            <img src="" class="lazyImg" data-src="<?= SITE_PATH ?>/images/photoreport/original/<?php echo  $image[0]->image; ?>" alt=""/>
        </div>
    <?php endif; ?>
<?php endif; ?>
<div class="wTxt nt">
    <?php echo $obj->text; ?>
</div>

<?php if (count($images) > 1): ?>
<div class="demo-gallery">
    <h2 class="demo-gallery__title">Фотогалерея</h2>
    <ul id="lightgallery" class="list-unstyled row">
    <?php foreach ($images as $index => $image): ?>
        <li class="col-xs-6 col-sm-4 col-md-3" data-src="<?= SITE_PATH ?>/images/photoreport/original/<?php echo $image->image; ?>">
            <a href="">
                <img class="img-responsive" src="<?= SITE_PATH ?>/images/photoreport/original/<?php echo $image->image; ?>">
            </a>
        </li>
    <?php endforeach;?>
    </ul>
</div>
<?php endif; ?>
<hr class="ser">
<div class="soci">
    <div class="fll">
        <script type="text/javascript">(function () {
                if (window.pluso) if (typeof window.pluso.start == "function") return;
                if (window.ifpluso == undefined) {
                    window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })();</script>
        <div class="pluso" data-background="#ebebeb" data-options="small,square,line,horizontal,counter,theme=02"
             data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
    </div>
    <div class="clear"></div>
</div>
<?php echo Widgets::get('mustRead', ['id' => $obj->id, 'parent_id' => $obj->parent_id, 'keywords' => $obj->keywords]); ?>
<hr class="bl">
<p class="tit fll">Комментарии</p>
<div class="kolcomm">Комментариев <span><?php echo $commetns_count; ?></span></div>
<div class="clear"></div>
<hr class="ser">

<?php $info = User::info(); ?>
<div class="comment">
    <div class="statusu" data-form="true">
        <!--<form action="/photoreport/comment_add?id=<?php echo $obj->id; ?>&alias=<?php echo $obj->alias; ?>" method="POST">-->
        <?php if ($_SESSION['user']) : ?>
            <p class="username"><?php echo $info->name; ?></p><p class="date"><?php echo date('d.m Y, G:i'); ?></p>
        <?php else : ?>
            <input type="text" name="commentator_name" placeholder="Anonymous" id="">
            <span class="date"><?php echo date('d.m Y, G:i'); ?></span><br>
        <?php endif ?>
        <textarea name="text" data-rule-minlength="4" placeholder="Ввести текст комментария" id=""></textarea>
        <button class="enterComment" id="commadd" href="#">Отправить</button>
        <!--</form>-->
    </div>
    <div class="clear"></div>
    <script type="text/javascript">
        document.getElementById('commadd').onclick = function (e) {
            e.preventDefault();
            $.post('/photoreport/comment_add?id=<?php echo $obj->id; ?>&alias=<?php echo $obj->alias; ?>',
                {
                    <?php if(!$_SESSION['user']) : ?>commentator_name: $('.comment input[name="commentator_name"]').val(),<?php endif ?>

                    text: $('.comment textarea[name="text"]').val()
                },
                function (data) {
                    if (data.success == true) {
                        location.reload();
                    } else {
                        var n = noty({
                            text: data.response,
                            type: 'alert'
                        });
                    }
                }, 'json');
        };
    </script>
</div>

<?php foreach ($comments as $obj): ?>
    <div class="comment">
        <div class="statusu">
            <p class="username"><?php echo $obj->name ? $obj->name : $obj->user_name; ?></p>
            <p class="date"><?php echo date('d.m Y, G:i', $obj->date); ?></p>
            <div class="scom"><?php echo $obj->text; ?></div>
        </div>
        <div class="clear"></div>
    </div>
    <?php if ($obj->answer): ?>
        <div class="comment level2">
            <div class="statusu">
                <p class="username">Admin</p>
                <p class="date"><?php echo date('d.m Y, G:i', $obj->answer_date) ?></p>
                <div class="scom"><?php echo $obj->answer; ?></div>
            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
<?php echo $pager; ?>