<div class="main_page onekol">
    <div>
        <p class="tit"><?php echo conf::get('h1'); ?></p>
        <hr class="ser">
        <div class="nnb">
            <?php
            //Счётчик. Будет считать кол-во выводимых новостей, обновляясь каждые n новости.
            $i = 1;
            //Тоже что-то считает. Нужно, чтобы доставать галереи из массива.
            $gal_obj_num = 0;
            ?>
            <?php foreach ( $result as $obj ): ?>
                <div class="s-news">
                    <p>
                        <a href="/photoreport/<?php echo $obj->alias; ?>" class="date">
                            <?php echo date('j',$obj->date).' '.support::month(date('m',$obj->date)).'<span> <b>'.date('G:i', $obj->date ).'</b></span>'; ?>
                        </a>
                    </p>
                    <a href="/photoreport/<?php echo $obj->alias; ?>">
                        <?php echo $obj->name; ?>
                    </a>
                    <div class="clear"></div>
                    <?php if( support::is_file(HOST.'/images/photoreport/small/'.$obj->image) ): ?>
                        <div class="img">
                            <a href="/photoreport/<?php echo $obj->alias; ?>"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/photoreport/small/' . $obj->image, 160, 128)?>" alt="<?php echo $obj->name; ?>"></a>
                        </div>
                    <?php elseif(support::is_file( HOST . '/images/photoreport/small/' . $obj->images )): ?>
                        <div class="img">
                            <a href="/photoreport/<?php echo $obj->alias; ?>"><img src="" class="lazyImg" data-src="<?=bLibrary::getImageFromCache('/images/photoreport/small/' . $obj->images, 160, 128)?>" alt="<?php echo $obj->name; ?>"></a>
                        </div>
                    <?php endif ?>
                    <p>
                        <?php if ($obj->text_short) :?>
                            <a href="/photoreport/<?php echo $obj->alias; ?>"><?php echo Text::limit_words(strip_tags($obj->text_short),70); ?></a>
                        <?php else: ?>
                            <a href="/photoreport/<?php echo $obj->alias; ?>"><?php echo Text::limit_words(strip_tags($obj->text),70); ?></a>
                        <?php endif; ?>
                    </p>

                    <div class="clear"></div>
                    <div class="vc_b">
                        <p class="views">Просмотров: <?php echo $obj->views; ?></p>
                        <p class="comments">Комментариев: <a href="/photoreport/<?php echo $obj->alias; ?>"><?php echo (int)$obj->comments_qty; ?></a></p>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php endforeach; ?>
            <?php echo $pager; ?>
        </div>
    </div>
</div>
