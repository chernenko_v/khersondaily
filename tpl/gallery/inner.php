<div class="albbl">
<?php foreach ($result as $obj): ?>
	<?php if(support::is_file( HOST . '/images/gallery/big/' . $obj->image )) : ?>
	    <div class="vsmal-news">
	        <div class="rel">
	        <a class="fresco" data-fresco-group="unique_name" href="/images/gallery/big/<?php echo $obj->image;?>">
	        <div class="blackbg"></div>
	            <div class="img"><img src="" class="lazyImg" data-src="<?=SITE_PATH?>/images/gallery/medium/<?php echo $obj->image; ?>" alt=""></div>
	        </a>
	        </div>
	    </div>
	<?php endif;?>
<?php endforeach; ?>
</div>
<div class="clear"></div>
<?php echo $pager; ?>