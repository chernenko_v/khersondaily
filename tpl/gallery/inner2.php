<div class="col col--xs-12 col--sm-7 col--md-9">
	<h1 class="nazvn"><?php echo $gallery->h1; ?></h1>
	<hr class="ser">
	<!-- slider -->
		<div class="st-control-b"></div>
		<div class="car-wrap-t">
			<div class="imgn carfr-sl">
				<?php $i=1; ?>
				<?php foreach ($result as $index => $obj): ?>
					<?php if ($index == 0) { ?>
					<?php if(support::is_file( HOST . '/images/gallery/big/' . $obj->image )) : ?>
					<div class="sl_item_bg">
						<img src="<?=SITE_PATH?>/images/gallery/medium/<?php echo $obj->image; ?>" alt="" data-num="<?php echo $i; $i++; ?>" />
					</div>
					<?php endif; ?>
					<?php } else { ?>
					<?php if(support::is_file( HOST . '/images/gallery/big/' . $obj->image )) : ?>
					<div class="sl_item_bg">
						<img src="" class="lazyImg" data-src="<?=SITE_PATH?>/images/gallery/medium/<?php echo $obj->image; ?>" alt="" data-num="<?php echo $i; $i++; ?>" />
					</div>
					<?php endif; ?>
					<?php } ?>
				<?php endforeach ;?>
			</div>
			<div id="prev-sl"></div>
			<div id="next-sl"></div>
			<div class="sl-thumb-cool dn-this"></div>
		</div>
	<!-- .slider -->
	<div class="wTxt nt"></div>
	<div class="photo-alb-page grid grid--indent-8">
		<?php echo Widgets::get('seeAlso'); ?>
	</div>
</div>
<!-- .wMiddle -->
<div class="col col--xs-12 col--sm-5 col--md-3">
	<div class="photo-alb-page">
		<div class="soc-groups-new">
			<span class="soc-icon fb-soc-icon"></span>
			<span class="soc-icon tw-soc-icon"></span>
			<span class="soc-icon plus-soc-icon"></span>
			<div class="soc-gr-in">
				<div class="sgin-inner">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
					<span style="padding-top:10px; display:block;">
					<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
					</span>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					<!--<a href="#" class="soc-link"></a>
					<a href="#" class="soc-link"></a>
					<a href="#" class="soc-link"></a>-->
				</div>
			</div>
		</div>
		<div class="alb-count-new display-none-xs">
			<span class="f_alb">1</span><span class="f_sep">/</span><span class="f_alb_total"><?php echo count($result); ?><span>
		</div>
		<div class="cit-block">
			<div class="c-text">
				<?php echo $gallery->text; ?>
			</div>
		</div>
		<?php echo Widgets::get('albums'); ?>
	</div>
</div>
<!-- .wRight -->