<div class="displit">
    <p>Показывать:</p>
    <a href="#">Фото</a>
    <a href="#">Видео</a>
    <a class="cur" href="#">Все</a>
</div>
<div class="albbl">
    <?php foreach ($result_gallery as $obj): ?>
    <?php $img_num = Builder::factory('gallery_images')->where('parent_id','=',$obj->id)->count_all(); ?>
    <div class="smal-news ph">
        <div class="rel">
            <div class="img"><img src="" class="lazyImg" data-src="<?=SITE_PATH?>/images/gallery/medium/<?php echo $obj->image; ?>" alt=""></div>
            <div class="bottom">
                <div><a href="#"><img src="" class="lazyImg" data-src="<?=SITE_PATH?>/images/gallery/medium/<?php echo $obj->image; ?>" alt=""></a></div>
                <div>
                    <p class="kolf">
                        <?php 
                            echo $img_num ? $img_num : 0;
                        ?>
                    <span>фотографии</span></p>
                    <a href="/gallery/<?php echo $obj->alias; ?>"><?php  echo Text::limit_words($obj->name,5); ?></a>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; unset($obj);?>
    <?php foreach ($result_videos as $obj): ?>
    <div class="smal-news vi">
        <div class="rel">
			<?php if ($obj->link): ?>
            <div class="img">
                <?php if(support::is_file( HOST . '/images/videos/big/' . $obj->image )) { ?>
                    <img style="min-width: 310px;" src="" class="lazyImg" data-src="/images/videos/big/<?php echo $obj->image; ?>" alt="">
                <?php } else {?>
                    <img style="min-width: 310px;" src="" class="lazyImg" data-src="http://img.youtube.com/vi/<?php echo support::youtube_key($obj->link); ?>/1.jpg" alt="">
                <?php }?>
            </div>
            <div class="bottom">
                <div><a href="http://www.youtube.com/embed/<?php echo support::youtube_key($obj->link); ?>" class="fresco"><img src="/pic/play.png" alt=""></a></div>
                <div>
                    <p class="kolf"><span>Видео</span></p>
                    <a href="http://www.youtube.com/embed/<?php echo support::youtube_key($obj->link); ?>" class="fresco"><?php echo Text::limit_words($obj->title,5); ?></a>
                </div>
            </div>
			<?php else: ?>
			<div class="img">
				<?php if(support::is_file( HOST . '/images/videos/big/' . $obj->image )): ?>
                    <img style="min-width: 310px;" src="" class="lazyImg" data-src="/images/videos/big/<?php echo $obj->image; ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="bottom">
                 <div><a href="<?php echo $obj->facebook_code; ?>" class="popap-video"><img src="/pic/play.png" alt=""></a></div>
                <div>
                    <p class="kolf"><span>Видео</span></p>
                    <a href="<?php echo $obj->facebook_code; ?>" class="popap-video"><?php echo Text::limit_words($obj->title,5); ?></a>
                </div>
            </div>
			<?php endif; ?>
        </div>
    </div>
    <?php endforeach; unset($obj);?>
    <div class="clear"></div>
</div>
<?php echo $pager; ?>